package es.upv.inf.darodgo3.naoAPI.Artifacts;

import com.aldebaran.qi.CallError;
import com.aldebaran.qi.helper.proxies.ALMemory;
import com.aldebaran.qi.helper.proxies.ALSystem;

import cartago.Artifact;
import cartago.OPERATION;
import cartago.OpFeedbackParam;
import es.upv.inf.darodgo3.naoAPI.NaoRobotProxys;

/**
 * Este artefacto proporciona un conjunto de operaciones relacionadas con la
 * memoria, el sistema y algunos comportamientos genéricos del robot.
 * 
 * @author Dario Rodriguez Gonzalez
 *
 */
public class Core extends Artifact {

	private ALMemory memory;
	private ALSystem system;
	private String url;
	private boolean isVirtual;

	/**
	 * Igual que init(url, false)
	 * 
	 * @param urlRobot
	 *            La url del robot
	 */
	void init(String urlRobot) {
		init(urlRobot, false);
	}

	/**
	 * Método que se ejecuta al crear el artefacto. Este método recupera de
	 * NaoRobotProxys los proxys necesarios para que el artefacto pueda
	 * funcionar, los cuales son: ALSystem y ALMemory. Una vez recuperados los
	 * almacena en atributos de clase, así las operaciones podrán hacer uso de
	 * los mismos sin tener que volver a recuperarlos.
	 * 
	 * @param urlRobot
	 *            La url del robot
	 * @param isVirtual
	 *            Indica si el robot es virtual o no
	 */
	void init(String urlRobot, boolean isVirtual) {
		this.url = urlRobot;
		this.memory = NaoRobotProxys.getInstance(urlRobot, isVirtual).getMemory();
		this.system = NaoRobotProxys.getInstance(urlRobot, isVirtual).getSystem();
		this.isVirtual = isVirtual;
	}

	/**
	 * Obtiene el nombre del robot y lo devuelve en el parametro de salida res
	 * como un String. Esta es una operación sincrona. Esta operación no está
	 * disponible para robots virtuales.
	 * 
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),get_robot_name]
	 */
	@OPERATION
	public void getRobotName(OpFeedbackParam<String> res) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			res.set(this.system.robotName());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación getRobotName", "get_robot_name");
		}
	}

	/**
	 * Apaga el robot. Esta es una operación sincrona. Esta operación no está
	 * disponible para robots virtuales.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),shutdown_robot]
	 */
	@OPERATION
	public void shutdownRobot() {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			this.system.shutdown();
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación shutdownRobot", "shutdown_robot");
		}
	}

	/**
	 * Reinicia el robot. Esta es una operación sincrona. Esta operación no está
	 * disponible para robots virtuales.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),reboot_robot]
	 */
	@OPERATION
	public void rebootRobot() {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			this.system.reboot();
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación rebootRobot", "reboot_robot");
		}
	}

	/**
	 * Obtiene la cantidad de memoria disponible en el heap del robot y la
	 * devuelve en el parametro de salida res como un Integer. Esta es una
	 * operación sincrona. Esta operación no está disponible para robots
	 * virtuales.
	 * 
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),get_free_memory]
	 */
	@OPERATION
	public void getFreeMemory(OpFeedbackParam<Integer> res) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			res.set(this.system.freeMemory());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación getFreeMemory", "get_free_memory");
		}
	}

	/**
	 * Obtiene la cantidad de memoria total del robot y la devuelve en el
	 * parametro de salida res como un Integer. Esta es una operación sincrona.
	 * Esta operación no está disponible para robots virtuales.
	 * 
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),get_total_memory]
	 */
	@OPERATION
	public void getTotalMemory(OpFeedbackParam<Integer> res) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			res.set(this.system.totalMemory());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación getTotalMemory", "get_total_memory");
		}
	}

	/**
	 * Obtiene el valor almacenado en la memoria bajo la clave "key"
	 * proporcionada. Este valor se devuelve en el parametro de salida res. Esta
	 * es una operación sincrona.
	 * 
	 * @param key
	 *            Nombre del valor a recuperar
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),get_memory_data(key)]
	 */
	@OPERATION
	public void getMemoryData(String key, OpFeedbackParam<Object> res) {
		try {
			res.set(memory.getData(key));
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación getMemoryData", "get_memory_data", key);
		}
	}

	/**
	 * Esta operación termina todas las conexión y subscripciones a eventos
	 * establecidas anteriormente, con el fin de poder terminar el programa
	 * correctamente. Por último, deshace el artefacto.
	 */
	@OPERATION
	public void endCore() {
		dispose();
	}

	/**
	 * Cierra la conexión tcp con el robot y termina el programa.
	 */
	@OPERATION
	public void exit() {
		NaoRobotProxys.getInstance(url).getApplication().stop();
		System.exit(0);
	}
}
