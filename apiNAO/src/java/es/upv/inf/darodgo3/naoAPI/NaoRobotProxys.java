package es.upv.inf.darodgo3.naoAPI;

import java.util.HashMap;
import java.util.Map;

import com.aldebaran.qi.Application;
import com.aldebaran.qi.helper.proxies.ALAnimatedSpeech;
import com.aldebaran.qi.helper.proxies.ALAudioPlayer;
import com.aldebaran.qi.helper.proxies.ALBasicAwareness;
import com.aldebaran.qi.helper.proxies.ALLeds;
import com.aldebaran.qi.helper.proxies.ALMemory;
import com.aldebaran.qi.helper.proxies.ALMotion;
import com.aldebaran.qi.helper.proxies.ALNavigation;
import com.aldebaran.qi.helper.proxies.ALRobotPosture;
import com.aldebaran.qi.helper.proxies.ALSonar;
import com.aldebaran.qi.helper.proxies.ALSpeechRecognition;
import com.aldebaran.qi.helper.proxies.ALSystem;
import com.aldebaran.qi.helper.proxies.ALTextToSpeech;
import com.aldebaran.qi.helper.proxies.ALVideoDevice;
import com.aldebaran.qi.helper.proxies.ALVisualCompass;

public class NaoRobotProxys {
	public class NaoInstance {

		private Application application;
		private ALMotion move = null;
		private ALTextToSpeech speech = null;
		private ALRobotPosture posture = null;
		private ALMemory memory = null;
		private ALSonar sonar = null;
		private ALNavigation nav = null;
		private ALAnimatedSpeech animatedSpeech = null;
		private ALVideoDevice video = null;
		private ALAudioPlayer audio = null;
		private ALLeds leds = null;
		private ALBasicAwareness awareness = null;
		private ALSpeechRecognition speechRecognition = null;
		private ALSystem system = null;
		private ALVisualCompass compass = null;

		public NaoInstance(String[] args, String url, boolean isVirtual) {
			try {
				System.out.println("Creando el nao: " + url);
				this.application = new Application(args, url);
				this.application.start();

				this.posture = new ALRobotPosture(application.session());
				this.speech = new ALTextToSpeech(application.session());
				this.animatedSpeech = new ALAnimatedSpeech(application.session());
				this.move = new ALMotion(application.session());
				this.memory = new ALMemory(application.session());
				this.sonar = new ALSonar(application.session());
				this.nav = new ALNavigation(application.session());
				this.video = new ALVideoDevice(application.session());
				this.audio = new ALAudioPlayer(application.session());
				this.leds = new ALLeds(application.session());
				if (!isVirtual) {
					this.compass = new ALVisualCompass(application.session());
					this.system = new ALSystem(application.session());
					this.awareness = new ALBasicAwareness(application.session());
					this.speechRecognition = new ALSpeechRecognition(application.session());
				}
			} catch (Exception e) {
				e.printStackTrace();
//				System.exit(-1);
			}
		}

		public Application getApplication() {
			return application;
		}

		public ALMotion getMove() {
			return move;
		}

		public ALTextToSpeech getSpeech() {
			return speech;
		}

		public ALRobotPosture getPosture() {
			return posture;
		}

		public ALMemory getMemory() {
			return memory;
		}

		public ALSonar getSonar() {
			return this.sonar;
		}

		public ALNavigation getNavigation() {
			return this.nav;
		}

		public ALAnimatedSpeech getAnimatedSpeech() {
			return this.animatedSpeech;
		}

		public ALVideoDevice getVideo() {
			return this.video;
		}

		public ALAudioPlayer getAudio() {
			return this.audio;
		}

		public ALLeds getLeds() {
			return this.leds;
		}

		public ALBasicAwareness getAwareness() {
			return this.awareness;
		}

		public ALSpeechRecognition getSpeechRecognition() {
			return this.speechRecognition;
		}
		
		public ALVisualCompass getVisualCompass(){
			return this.compass;
		}
		
		public ALSystem getSystem() {
			return this.system;
		}
	}
	
	public final static String suscriberName = "JASONNAOAPI";

	/**
	 * Lista de instancias. Para cada url una instancia.
	 */
	private static Map<String, NaoInstance> instances = new HashMap<>();

	/**
	 * Esta clase no se puede instanciar
	 */
	private NaoRobotProxys() {

	}

	public static NaoInstance getInstance(String url) {
		String[] args = { "" };
		return getInstance(args, url, false);
	}
	
	public static NaoInstance getInstance(String url, boolean isVirtual) {
		String[] args = { "" };
		return getInstance(args, url, isVirtual);
	}

	public static NaoInstance getInstance(String[] args, String url, boolean isVirtual) {
		NaoInstance inst = instances.get(url);
		if (inst == null) {
			inst = new NaoRobotProxys().new NaoInstance(args, url, isVirtual);
			instances.put(url, inst);
		}

		return inst;
	}
}
