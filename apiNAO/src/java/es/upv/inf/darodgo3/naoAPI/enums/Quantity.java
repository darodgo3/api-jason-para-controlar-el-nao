/**
 * 
 */
package es.upv.inf.darodgo3.naoAPI.enums;

/**
 * @author dario
 *
 */
public enum Quantity {
	TODO(1.0f),MUCHO(0.75f),MEDIO(0.5f),POCO(0.25f),NADA(0.0f);
	
	private final float quantity;
	
	private Quantity(float quantity){
		this.quantity=quantity;		
	}
	
	public final float getQuantity(){
		return this.quantity;
	}
	
	public static Quantity fromString(String quantity){
		for(Quantity e : Quantity.values()){
            if(quantity.toLowerCase().equals(e.name().toLowerCase())) return e;
        }
        return null;
	}
}
