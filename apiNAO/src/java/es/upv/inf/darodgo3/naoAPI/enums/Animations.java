package es.upv.inf.darodgo3.naoAPI.enums;

public enum Animations {
	BodyTalk1("animations/Sit/BodyTalk/BodyTalk_1"),
	BodyTalk2("animations/Sit/BodyTalk/BodyTalk_2"),
	BodyTalk3("animations/Sit/BodyTalk/BodyTalk_3"),
	BodyTalk4("animations/Sit/BodyTalk/BodyTalk_4"),
	BodyTalk5("animations/Sit/BodyTalk/BodyTalk_5"),
	BodyTalk6("animations/Sit/BodyTalk/BodyTalk_6"),
	BodyTalk7("animations/Sit/BodyTalk/BodyTalk_7"),
	BodyTalk8("animations/Sit/BodyTalk/BodyTalk_8"),
	BodyTalk9("animations/Sit/BodyTalk/BodyTalk_9"),
	BodyTalk10("animations/Sit/BodyTalk/BodyTalk_10"),
	BodyTalk11("animations/Sit/BodyTalk/BodyTalk_11"),
	BodyTalk12("animations/Sit/BodyTalk/BodyTalk_12"),
	BodyTalk13("animations/Sit/BodyTalk/BodyTalk_13"),
	BodyTalk14("animations/Sit/BodyTalk/BodyTalk_14"),
	BodyTalk15("animations/Sit/BodyTalk/BodyTalk_15"),
	BodyTalk16("animations/Sit/BodyTalk/BodyTalk_16"),
	BodyTalk17("animations/Sit/BodyTalk/BodyTalk_17"),
	BodyTalk18("animations/Sit/BodyTalk/BodyTalk_18"),
	BodyTalk19("animations/Sit/BodyTalk/BodyTalk_19"),
	BodyTalk20("animations/Sit/BodyTalk/BodyTalk_20"),
	BodyTalk21("animations/Sit/BodyTalk/BodyTalk_21"),
	BodyTalk22("animations/Sit/BodyTalk/BodyTalk_22"),
	BowShort1("animations/Stand/Gestures/BowShort_1"),
	Enthusiastic4("animations/Stand/Gestures/Enthusiastic_4"),
	Enthusiastic5("animations/Stand/Gestures/Enthusiastic_5"),
	Explain1("animations/Stand/Gestures/Explain_1	animation"),
	Explain10("animations/Stand/Gestures/Explain_10"),
	Explain11("animations/Stand/Gestures/Explain_11"),
	Explain2("animations/Stand/Gestures/Explain_2"),
	Explain3("animations/Stand/Gestures/Explain_3"),
	Explain4("animations/Stand/Gestures/Explain_4"),
	Explain5("animations/Stand/Gestures/Explain_5"),
	Explain6("animations/Stand/Gestures/Explain_6"),
	Explain7("animations/Stand/Gestures/Explain_7"),
	Explain8("animations/Stand/Gestures/Explain_8"),
	Hey1("animations/Stand/Gestures/Hey_1"),
	Hey6("animations/Stand/Gestures/Hey_6"),
	IDontKnow1("animations/Stand/Gestures/IDontKnow_1"),
	IDontKnow2("animations/Stand/Gestures/IDontKnow_2"),
	Me1("animations/Stand/Gestures/Me_1"),
	Me2("animations/Stand/Gestures/Me_2"),
	No3("animations/Stand/Gestures/No_3"),
	No8("animations/Stand/Gestures/No_8"),
	No9("animations/Stand/Gestures/No_9"),
	Please1("animations/Stand/Gestures/Please_1"),
	Yes1("animations/Stand/Gestures/Yes_1"),
	Yes2("animations/Stand/Gestures/Yes_2"),
	Yes3("animations/Stand/Gestures/Yes_3"),
	YouKnowWhat1("animations/Stand/Gestures/YouKnowWhat_1"),
	YouKnowWhat5("animations/Stand/Gestures/YouKnowWhat_5"),
	You1("animations/Stand/Gestures/You_1"),
	You4("animations/Stand/Gestures/You_4");

	private final String name;
	
	private Animations(String text){
		this.name = text;
	}

	public String getName() {
		return name;
	}
	
	public static Animations fromString(String animation){
		for(Animations e : Animations.values()){
            if(animation.toLowerCase().equals(e.name().toLowerCase())) return e;
        }
        return null;
	}
}
