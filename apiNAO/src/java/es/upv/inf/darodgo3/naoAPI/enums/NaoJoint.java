package es.upv.inf.darodgo3.naoAPI.enums;

public enum NaoJoint {
	HeadYaw (-2.0857, 2.0857, "HeadYaw"),
	HeadPitch (-0.6720, 0.5149, "HeadPitch"),
	LShoulderPitch (-2.0857, 2.0857, "LShoulderPitch"),
	LShoulderRoll (-0.3142, 1.3265, "LShoulderRoll"),
	LElbowYaw (-2.0857, 2.0857, "LElbowYaw"),
	LElbowRoll (-1.5446, -0.0349, "LElbowRoll"),
	LWristYaw (-1.8238, 1.8238, "LWristYaw"),
	LHand (0, 1, "LHand"),
	RShoulderPitch (-2.0857, 2.0857, "RShoulderPitch"),
	RShoulderRoll (-0.3142, 1.3265, "RShoulderRoll"),
	RElbowYaw (-2.0857, 2.0857, "RElbowYaw"),
	RElbowRoll (-1.5446, -0.0349, "RElbowRoll"),
	RWristYaw (-1.8238, 1.8238, "RWristYaw"),
	RHand (0, 1, "RHand"),
	LHipYawPitch (-1.145303, 0.740810, "LHipYawPitch"),
	RHipYawPitch (-1.145303, 0.740810, "RHipYawPitch"),
	LHipRoll (-0.379472, 0.790477, "LHipRoll"),
	LHipPitch (-1.535889, 0.484090, "LHipPitch"),
	LKneePitch (-0.092346, 2.112528, "LKneePitch"),
	LAnklePitch (-1.189516, 0.922747, "LAnklePitch"),
	LAnkleRoll (-0.397880, 0.769001, "LAnkleRoll"),
	RHipRoll (-0.379472, 0.790477, "RHipRoll"),
	RHipPitch (-1.535889, 0.484090, "RHipPitch"),
	RKneePitch (-0.092346, 2.112528, "RKneePitch"),
	RAnklePitch (-1.189516, 0.922747, "RAnklePitch"),
	RAnkleRoll (-0.397880, 0.769001, "RAnkleRoll");
	
	private final double minVal;
	private final double maxVal;
	private final String name;
	
	private NaoJoint(double minVal, double maxVal, String name){
		this.minVal = minVal;
		this.maxVal = maxVal;
		this.name = name;
	}

	public double getMinRad() {
		return minVal;
	}

	public double getMaxRad() {
		return maxVal;
	}
	
	public double getMinDeg() {
		if (this.name != "RHand" && this.name != "LHand")
			return Math.toDegrees(minVal);
		
		return minVal;
	}

	public double getMaxDeg() {
		if (this.name != "RHand" && this.name != "LHand")
			return Math.toDegrees(this.maxVal);
		
		return this.maxVal;
	}

	public String getName() {
		return name;
	}

	public static NaoJoint fromString(String name){
		for(NaoJoint e : NaoJoint.values()){
            if(name.toLowerCase().equals(e.name().toLowerCase())) return e;
        }
        return null;
	}
}
