package es.upv.inf.darodgo3.naoAPI.Artifacts;

import java.util.List;

import com.aldebaran.qi.CallError;
import com.aldebaran.qi.helper.proxies.ALLeds;
import com.aldebaran.qi.helper.proxies.ALMemory;
import com.aldebaran.qi.helper.proxies.ALSonar;

import cartago.Artifact;
import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;
import cartago.ObsProperty;
import cartago.OpFeedbackParam;
import es.upv.inf.darodgo3.naoAPI.NaoRobotProxys;
import es.upv.inf.darodgo3.naoAPI.enums.Quantity;
import es.upv.inf.darodgo3.naoAPI.util.ListParser;
import es.upv.inf.darodgo3.naoAPI.util.NumberParser;

/**
 * Este artefacto porporciona un conjunto de operaciones relacionadas con los
 * sensores del robot. Además, se suscribe a algunos eventos de los sensores, y
 * genera o actualiza creencias en el robot, para así tener en el disponibles lo
 * datos de los sensores.
 * 
 * @author Dario Rodriguez Gonzalez
 *
 */
public class Sensors extends Artifact {
	private ALMemory memory;
	private ALSonar sonar;
	private ALLeds leds;

	private ObsProperty gyroscope;
	private ObsProperty accelerometer;
	private ObsProperty angles;

	/**
	 * Igual que init(url, false)
	 * 
	 * @param url
	 *            La url del robot
	 */
	void init(String url) {
		init(url, false);
	}

	/**
	 * Método que se ejecuta al crear el artefacto. Este método recupera de
	 * NaoRobotProxys los proxys necesarios para que el artefacto pueda
	 * funcionar, los cuales son: ALMemory, ALSonar y ALLeds. Una vez
	 * recuperados los almacena en atributos de clase, así las operaciones
	 * podrán hacer uso de los mismos sin tener que volver a recuperarlos.
	 * 
	 * Además, crea la siguientes propiedades observables, las cuales se
	 * transforman automaticamente en creencias en el agente Jason:
	 * 
	 * sonarLeft: indica el valor de SonarLeftDetected. Cada vez que el robot
	 * cambie este valor, este será también actualizado en la base de creencias
	 * del agente Jason.
	 * 
	 * sonarNothingLeft: indica el valor de SonarNothingLeftDetected. Cada vez
	 * que el robot cambie este valor, este será también actualizado en la base
	 * de creencias del agente Jason.
	 * 
	 * sonarRight: indica el valor de SonarRightDetected. Cada vez que el robot
	 * cambie este valor, este será también actualizado en la base de creencias
	 * del agente Jason.
	 * 
	 * sonarNothingRight: indica el valor de sonarNothingRightDetected. Cada vez
	 * que el robot cambie este valor, este será también actualizado en la base
	 * de creencias del agente Jason.
	 * 
	 * sonarLateralRight: indica el valor de SonarLateralRightDetected. Cada vez
	 * que el robot cambie este valor, este será también actualizado en la base
	 * de creencias del agente Jason.
	 * 
	 * sonarLateralRight: indica el valor de SonarLateralRightDetected. Cada vez
	 * que el robot cambie este valor, este será también actualizado en la base
	 * de creencias del agente Jason.
	 * 
	 * sonarMiddle: indica el valor de SonarMiddleDetected. Cada vez que el
	 * robot cambie este valor, este será también actualizado en la base de
	 * creencias del agente Jason.
	 * 
	 * gyroscope: contiene los valores del giroscopio 3D
	 * 
	 * accelerometer: contiene los valores del acelerometro 3D
	 * 
	 * angles: contiene los angulos 3D
	 * 
	 * También genera las siguientes creencias:
	 * 
	 * frontTactilTouched: genera esta creencia cada vez que detecta que el
	 * sensor tactil frontal de la cabeza es accionado.
	 * 
	 * middleTactilTouched: genera esta creencia cada vez que detecta que el
	 * sensor tactil del medio de la cabeza es accionado.
	 * 
	 * rearTactilTouched: genera esta creencia cada vez que detecta que el
	 * sensor tactil de detras de la cabeza es accionado.
	 * 
	 * leftBumperPressed: genera esta creencia cada vez que detecta que el
	 * bumper del pie izquierdo es accionado
	 * 
	 * rightBumperPressed: genera esta creencia cada vez que detecta que el
	 * bumper del pie derecho es accionado
	 * 
	 * handLeftTouched: genera esta creencia cada vez que los sensores de
	 * presión de la mano izquierda detectan presión.
	 * 
	 * handRightTouched: genera esta creencia cada vez que los sensores de
	 * presión de la mano derecha detectan presión.
	 * 
	 * footContactChanged: genera esta creencia cada vez que los sensores FSR
	 * detectan que el robot no está en contacto
	 * 
	 * @param url
	 *            La url del robot
	 * @param isVirtual
	 *            Indica si el robot es virtual o no
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),sensors_init]
	 */
	void init(String url, boolean isVirtual) {
		try {
			defineObsProperty("finish", false);
			defineObsProperty("sonarLeft", 0f);
			defineObsProperty("sonarNothingLeft", 0f);
			defineObsProperty("sonarRight", 0f);
			defineObsProperty("sonarNothingRight", 0f);
			defineObsProperty("sonarLateralLeft", 0f);
			defineObsProperty("sonarLateralRight", 0f);
			defineObsProperty("sonarMiddle", 0f);

			defineObsProperty("gyroscope", 0f, 0f, 0f);
			gyroscope = getObsProperty("gyroscope");
			defineObsProperty("accelerometer", 0f, 0f, 0f);
			accelerometer = getObsProperty("accelerometer");
			defineObsProperty("angles", 0f, 0f, 0f);
			angles = getObsProperty("angles");

			this.memory = NaoRobotProxys.getInstance(url, isVirtual).getMemory();
			this.sonar = NaoRobotProxys.getInstance(url, isVirtual).getSonar();
			this.leds = NaoRobotProxys.getInstance(url, isVirtual).getLeds();

			if (!isVirtual) {
				execInternalOp("getGyroscope");
			}

			// Cabeza
			memory.subscribeToEvent("FrontTactilTouched", touch -> {
				if ((float) touch > 0) {
					execInternalOp("sendSignal", "frontTactilTouched");
				}
			});
			memory.subscribeToEvent("MiddleTactilTouched", touch -> {
				if ((float) touch > 0) {
					execInternalOp("sendSignal", "middleTactilTouched");
				}
			});
			memory.subscribeToEvent("RearTactilTouched", touch -> {
				if ((float) touch > 0) {
					execInternalOp("sendSignal", "rearTactilTouched");
				}
			});

			// Pies
			memory.subscribeToEvent("LeftBumperPressed", touch -> {
				if ((float) touch > 0) {
					execInternalOp("sendSignal", "leftBumperPressed");
				}
			});
			memory.subscribeToEvent("RightBumperPressed", touch -> {
				if ((float) touch > 0) {
					execInternalOp("sendSignal", "rightBumperPressed");
				}
			});

			// Manos
			// Izquierda
			memory.subscribeToEvent("HandLeftBackTouched", touch -> {
				if ((float) touch > 0) {
					execInternalOp("sendSignal", "handLeftTouched");
				}
			});
			memory.subscribeToEvent("HandLeftLeftTouched", touch -> {
				if ((float) touch > 0) {
					execInternalOp("sendSignal", "handLeftTouched");
				}
			});
			memory.subscribeToEvent("HandLeftRightTouched", touch -> {
				if ((float) touch > 0) {
					execInternalOp("sendSignal", "handLeftTouched");
				}
			});

			// Derecha
			memory.subscribeToEvent("HandRightBackTouched", touch -> {
				if ((float) touch > 0) {
					execInternalOp("sendSignal", "handRightTouched");
				}
			});
			memory.subscribeToEvent("HandRightLeftTouched", touch -> {
				if ((float) touch > 0) {
					execInternalOp("sendSignal", "handRightTouched");
				}
			});
			memory.subscribeToEvent("HandRightRightTouched", touch -> {
				if ((float) touch > 0) {
					execInternalOp("sendSignal", "handRightTouched");
				}
			});

			sonar.subscribe(NaoRobotProxys.suscriberName);

			// Sonar
			memory.subscribeToEvent("SonarLeftDetected", dist -> {
				if ((float) dist > 0) {
					execInternalOp("updateProperty", "sonarLeft", dist);
				}
			});
			memory.subscribeToEvent("SonarMiddleDetected", dist -> {
				if ((float) dist > 0) {
					execInternalOp("updateProperty", "sonarMiddle", dist);
				}
			});
			memory.subscribeToEvent("SonarLeftNothingDetected", dist -> {
				if ((float) dist > 0) {
					execInternalOp("updateProperty", "sonarNothingLeft", Float.valueOf((float) dist));
				}
			});
			memory.subscribeToEvent("SonarRightDetected", dist -> {
				if ((float) dist > 0) {
					execInternalOp("updateProperty", "sonarRight", dist);
				}
			});
			memory.subscribeToEvent("SonarRightNothingDetected", dist -> {
				if ((float) dist > 0) {
					execInternalOp("updateProperty", "sonarNothingRight", dist);
				}
			});
			memory.subscribeToEvent("SonarLateralLeftDetected", dist -> {
				if ((float) dist > 0) {
					execInternalOp("updateProperty", "sonarLateralLeft", dist);
				}
			});
			memory.subscribeToEvent("SonarLateralRightDetected", dist -> {
				if ((float) dist > 0) {
					execInternalOp("updateProperty", "sonarLateralRight", dist);
				}
			});

			// FSR
			memory.subscribeToEvent("footContactChanged", value -> {
				if ((boolean) value == false) {
					execInternalOp("sendSignal", "footContactChanged");
				}
			});

		} catch (Exception ce) {
			failed("No se pudo completar la operación sensorsInit", "sensors_init", url);
		}

	}

	@INTERNAL_OPERATION
	private void sendSignal(String signal) {
		signal(signal);
	}

	@INTERNAL_OPERATION
	private void sendSignal(String signal, Object newValue) {
		signal(signal, newValue);
	}

	@INTERNAL_OPERATION
	private void updateProperty(String propName, Object newValue) {
		ObsProperty prop = getObsProperty(propName);
		prop.updateValue(newValue);
	}

	@INTERNAL_OPERATION
	private void getGyroscope() {
		try {
			while (true) {
				gyroscope.updateValues(
						this.memory.getData("Device/SubDeviceList/InertialSensor/GyroscopeX/Sensor/Value"),
						this.memory.getData("Device/SubDeviceList/InertialSensor/GyroscopeY/Sensor/Value"),
						this.memory.getData("Device/SubDeviceList/InertialSensor/GyroscopeZ/Sensor/Value"));

				accelerometer.updateValues(
						this.memory.getData("Device/SubDeviceList/InertialSensor/AccelerometerX/Sensor/Value"),
						this.memory.getData("Device/SubDeviceList/InertialSensor/AccelerometerY/Sensor/Value"),
						this.memory.getData("Device/SubDeviceList/InertialSensor/AccelerometerZ/Sensor/Value"));

				angles.updateValues(this.memory.getData("Device/SubDeviceList/InertialSensor/AngleX/Sensor/Value"),
						this.memory.getData("Device/SubDeviceList/InertialSensor/AngleY/Sensor/Value"),
						this.memory.getData("Device/SubDeviceList/InertialSensor/AngleZ/Sensor/Value"));

				await_time(500);

			}
		} catch (CallError | InterruptedException e) {
			// nada que hacer
			e.printStackTrace();
		}
	}

	/**
	 * Ajusta la intensidad de un LED o Grupo de LEDs durante un tiempo
	 * determinado. Esta es una operación sincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds
	 * @param intensity
	 *            La intensidad del led o del grupo de leds (un valor entre 0 y
	 *            1).
	 * @param duration
	 *            Duración en segundos
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),fade_leds(name,intensity,duration)]
	 */
	@OPERATION
	public void fadeLeds(String name, float intensity, float duration) {
		try {
			leds.fade(name, intensity, duration);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación fadeLeds", "fade_leds", name, intensity, duration);
		}
	}

	/**
	 * Identica a fadeLeds pero en vez de ser una operación sincrona es una
	 * operación asincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds
	 * @param intensity
	 *            La intensidad del led o del grupo de leds (un valor entre 0 y
	 *            1).
	 * @param duration
	 *            Duración en segundos
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),fade_leds_async(name,intensity,duration)]
	 */
	@OPERATION
	public void fadeLedsAsync(String name, float intensity, float duration) {
		try {
			leds.async().fade(name, intensity, duration);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación fadeLedsAsync", "fade_leds_async", name, intensity, duration);
		}
	}

	/**
	 * Establece el color de los leds durante un tiempo determinado. El color se
	 * indica como porcentaje de rojo, verde y azul. Esta es una operación
	 * sincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds
	 * @param red
	 *            Intensidad del canal rojo
	 * @param green
	 *            Intensidad del canal verde
	 * @param blue
	 *            Intensidad del canal azul
	 * @param duration
	 *            Duracion en segundos
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),fade_RGB_leds(name,red,green,blue,duration)]
	 */
	@OPERATION
	public void fadeRGBLeds(String name, float red, float green, float blue, float duration) {
		try {
			this.leds.fadeRGB(name, red, green, blue, duration);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación fadeRGBLeds", "fade_RGB_leds", name, red, green, blue, duration);
		}
	}

	/**
	 * Identica a fadeRGBLeds pero en vez de ser una operación sincrona es una
	 * operación asincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds
	 * @param red
	 *            Intensidad del canal rojo
	 * @param green
	 *            Intensidad del canal verde
	 * @param blue
	 *            Intensidad del canal azul
	 * @param duration
	 *            Duracion en segundos
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),fade_RGB_leds_async(name,red,green,blue,duration)]
	 */
	@OPERATION
	public void fadeRGBLedsAsync(String name, float red, float green, float blue, float duration) {
		try {
			this.leds.async().fadeRGB(name, red, green, blue, duration);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación fadeRGBLedsAsync", "fade_RGB_leds_async", name, red, green, blue,
					duration);
		}
	}

	/**
	 * Establece el color de los leds durante un tiempo determinado. Los colores
	 * soportados son: "white", "red", "green", "blue", "yellow", "magenta",
	 * "cyan". Esta es una operación sincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds
	 * @param colorName
	 *            El nombre del color.
	 * @param duration
	 *            Duracion en segundos
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),fade_color_leds(name,colorName,duration)]
	 */
	@OPERATION
	public void fadeColorLeds(String name, String colorName, float duration) {
		try {
			this.leds.fadeRGB(name, colorName, duration);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación fadeColorLeds", "fade_color_leds", name, colorName, duration);
		}
	}

	/**
	 * Identica a fadeColorLeds pero en vez de ser una operación sincrona es una
	 * operación asincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds
	 * @param colorName
	 *            El nombre del color.
	 * @param duration
	 *            Duracion en segundos
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),fade_color_leds_async(name,colorName,duration)]
	 */
	@OPERATION
	public void fadeColorLedsAsync(String name, String colorName, float duration) {
		try {
			this.leds.async().fadeRGB(name, colorName, duration);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación fadeColorLedsAsync", "fade_color_leds_async", name, colorName,
					duration);
		}
	}

	/**
	 * Obtiene la intensidad de un led o de un grupo de leds y la devuelve por
	 * el pametro de salida. Esta es una operación sincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),get_intensity_leds(name)]
	 */
	@OPERATION
	public void getIntensityLeds(String name, OpFeedbackParam<Object> res) {
		try {
			res.set(leds.getIntensity(name));
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación getIntensityLeds", "get_intensity_leds", name);
		}
	}

	/**
	 * Identica a fadeColorLeds pero en vez de ser una operación sincrona es una
	 * operación asincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),get_intensity_leds_async(name)]
	 */
	@OPERATION
	public void getIntensityLedsAsync(String name, OpFeedbackParam<Object> res) {
		try {
			res.set(leds.async().getIntensity(name));
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación getIntensityLedsAsync", "get_intensity_leds_async", name);
		}
	}

	/**
	 * Enciende un led o un grupo de leds. Esta es una operación sincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),leds_on(name)]
	 */
	@OPERATION
	public void ledsOn(String name) {
		try {
			leds.on(name);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación ledsOn", "leds_on", name);
		}
	}

	/**
	 * Identica a ledsOn pero en vez de ser una operación sincrona es una
	 * operación asincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),leds_on_async(name)]
	 */
	@OPERATION
	public void ledsOnAsync(String name) {
		try {
			leds.async().on(name);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación ledsOnAsync", "leds_on_async", name);
		}
	}

	/**
	 * Apaga un led o un grupo de leds. Esta es una operación sincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),leds_off(name)]
	 */
	@OPERATION
	public void ledsOff(String name) {
		try {
			leds.off(name);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación ledsOff", "leds_off", name);
		}
	}

	/**
	 * Identica a ledsOff pero en vez de ser una operación sincrona es una
	 * operación asincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),leds_off_async(name)]
	 */
	@OPERATION
	public void ledsOffAsync(String name) {
		try {
			leds.async().off(name);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación ledsOffAsync", "leds_off_async", name);
		}
	}

	/**
	 * Establece la intensidad de un led o un grupo de leds. El nuevo valor de
	 * intensidad puede ser indicado como un número entre 0 y 1, o como un valor
	 * del enumerado Quantity: todo, mucho, medio, poco, nada. Esta es una
	 * operación sincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds.
	 * @param intensity
	 *            Nueva intensidad
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),set_intensity_leds(name,intensity)]
	 */
	@OPERATION
	public void setIntensityLeds(String name, Object intensity) {
		try {
			if (intensity instanceof String) {
				leds.setIntensity(name, Quantity.fromString((String) intensity).getQuantity());
			} else {
				leds.setIntensity(name, NumberParser.cartagoNumberToFloat(intensity));
			}
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación setIntensityLeds", "set_intensity_leds", name, intensity);
		}
	}

	/**
	 * Identica a ledsOff pero en vez de ser una operación sincrona es una
	 * operación asincrona.
	 * 
	 * @param name
	 *            Nombre del led o del grupo de leds.
	 * @param intensity
	 *            Nueva intensidad
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),set_intensity_leds_async(name,intensity)]
	 */
	@OPERATION
	public void setIntensityLedsAsync(String name, Object intensity) {
		try {
			if (intensity instanceof String) {
				leds.async().setIntensity(name, Quantity.fromString((String) intensity).getQuantity());
			} else {
				leds.async().setIntensity(name, NumberParser.cartagoNumberToFloat(intensity));
			}
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación setIntensityLedsAsync", "set_intensity_leds_async", name,
					intensity);
		}
	}

	/**
	 * Una animación para mostrar una dirección con las orejas. Esta es una
	 * operación sincrona.
	 * 
	 * @param degrees
	 *            El ángulo que desea mostrar en grados (int). 0 está hacia
	 *            arriba, 90 está hacia adelante, 180 está hacia abajo y 270
	 *            está de vuelta.
	 * @param duration
	 *            La duración en segundos de la animación
	 * @param leaveOnAtEnd
	 *            Si es true, el último led se deja encendido al final de la
	 *            animación.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),ear_leds_set_angle(degrees,duration,leaveOnAtEnd)]
	 */
	@OPERATION
	public void earLedsSetAngle(int degrees, float duration, boolean leaveOnAtEnd) {
		try {
			leds.earLedsSetAngle(degrees, duration, leaveOnAtEnd);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación earLedsSetAngle", "ear_leds_set_angle", degrees, duration,
					leaveOnAtEnd);
		}
	}

	/**
	 * Identica a earLedsSetAngle pero en vez de ser una operación sincrona es
	 * una operación asincrona.
	 * 
	 * @param degrees
	 *            El ángulo que desea mostrar en grados (int). 0 está hacia
	 *            arriba, 90 está hacia adelante, 180 está hacia abajo y 270
	 *            está de vuelta.
	 * @param duration
	 *            La duración en segundos de la animación
	 * @param leaveOnAtEnd
	 *            Si es true, el último led se deja encendido al final de la
	 *            animación.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),ear_leds_set_angle_async(degrees,duration,leaveOnAtEnd)]
	 */
	@OPERATION
	public void earLedsSetAngleAsync(int degrees, float duration, boolean leaveOnAtEnd) {
		try {
			leds.async().earLedsSetAngle(degrees, duration, leaveOnAtEnd);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación earLedsSetAngleAsync", "ear_leds_set_angle_async", degrees,
					duration, leaveOnAtEnd);
		}
	}

	/**
	 * Iniciar una animación aleatoria en los les de los ojos. Esta es una
	 * operación sincrona.
	 * 
	 * @param duration
	 *            Duración aproximada de la animación en segundos.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),random_eye_leds(duration)]
	 */
	@OPERATION
	public void randomEyeLeds(float duration) {
		try {
			leds.randomEyes(duration);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación randomEyeLeds", "random_eye_leds", duration);
		}
	}

	/**
	 * Identica a randomEyeLeds pero en vez de ser una operación sincrona es una
	 * operación asincrona.
	 * 
	 * @param duration
	 *            Duración aproximada de la animación en segundos.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),random_eye_leds_async(duration)]
	 */
	@OPERATION
	public void randomEyeLedsAsync(float duration) {
		try {
			leds.async().randomEyes(duration);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación randomEyeLedsAsync", "random_eye_leds_async", duration);
		}
	}

	/**
	 * Lanzar una animación rasta verde / amarillo / rojo en los leds de todo el
	 * cuerpo. Esta es una operación sincrona.
	 * 
	 * @param duration
	 *            Duración aproximada de la animación en segundos.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),rasta_leds(duration)]
	 */
	@OPERATION
	public void rastaLeds(float duration) {
		try {
			leds.rasta(duration);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación rastaLeds", "rasta_leds", duration);
		}
	}

	/**
	 * Identica a rastaLeds pero en vez de ser una operación sincrona es una
	 * operación asincrona.
	 * 
	 * @param duration
	 *            Duración aproximada de la animación en segundos.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),rasta_leds_async(duration)]
	 */
	@OPERATION
	public void rastaLedsAsync(float duration) {
		try {
			leds.async().rasta(duration);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación rastaLedsAsync", "rasta_leds_async", duration);
		}
	}

	/**
	 * Lanza una animación la cual rota los leds de los ojos. Esta es una
	 * operación sincrona.
	 * 
	 * @param rgb
	 *            El valor del color en RGB. Se debe proporcionar como número
	 *            entero. Para ello primero se debe representar el color en
	 *            hexadecimal como 0xRRGGBB y luego pasarlo a entero.
	 * @param timeForRotation
	 *            Tiempo aproximado para hacer una vuelta.
	 * @param totalDuration
	 *            Duración aproximada de la animación en segundos.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),rotate_eye_leds(rgb,timeForRotation,totalDuration)]
	 */
	@OPERATION
	public void rotateEyeLeds(int rgb, float timeForRotation, float totalDuration) {
		try {
			leds.rotateEyes(rgb, timeForRotation, totalDuration);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación rotateEyeLeds", "rotate_eye_leds", rgb, timeForRotation,
					totalDuration);
		}
	}

	/**
	 * Identica a rotateEyeLeds pero en vez de ser una operación sincrona es una
	 * operación asincrona.
	 * 
	 * @param rgb
	 *            El valor del color en RGB. Se debe proporcionar como número
	 *            entero. Para ello primero se debe representar el color en
	 *            hexadecimal como 0xRRGGBB y luego pasarlo a entero.
	 * @param timeForRotation
	 *            Tiempo aproximado para hacer una vuelta.
	 * @param totalDuration
	 *            Duración aproximada de la animación en segundos.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),rotate_eye_leds_async(rgb,timeForRotation,totalDuration)]
	 */
	@OPERATION
	public void rotateEyeLedsAsync(int rgb, float timeForRotation, float totalDuration) {
		try {
			leds.async().rotateEyes(rgb, timeForRotation, totalDuration);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación rotateEyeLeds", "rotate_eye_leds_async", rgb, timeForRotation,
					totalDuration);
		}
	}

	/**
	 * Crea un nuevo grupo de leds a partir de una lista de nombres. Esta es una
	 * operación sincrona.
	 * 
	 * @param groupName
	 *            Nombre del nuevo grupo de leds
	 * @param ledNames
	 *            Lista con los nombres de los leds que formarán este grupo.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),create_leds_group(groupName,ledNames)]
	 */
	@OPERATION
	public void createLedsGroup(String groupName, Object[] ledNames) {
		try {
			List<String> lledNames = ListParser.cartagoStringListToList(ledNames);
			leds.createGroup(groupName, lledNames);
		} catch (Exception e) {
			failed("No se pudo completar la operación createLedsGroup", "create_leds_group", groupName, ledNames);
		}
	}

	/**
	 * Devuelve por el parametro de salida res el nombre de los leds que forman
	 * un grupo. Esta es una peración sincrona.
	 * 
	 * @param groupName
	 *            Nombre del nuevo grupo de leds
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),list_leds_group(groupName)]
	 */
	@OPERATION
	public void listLedsGroup(String groupName, OpFeedbackParam<Object[]> res) {
		String[] group;

		try {
			List<String> l = leds.listGroup(groupName);

			group = l.toArray(new String[l.size()]);
			res.set(group);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación listLedsGroup", "list_leds_group", groupName);
		}
	}

	/**
	 * Esta operación termina todas las conexión y subscripciones a eventos
	 * establecidas anteriormente, con el fin de poder terminar el programa
	 * correctamente. Por último, deshace el artefacto.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg)]
	 */
	@OPERATION
	public void endSensors() {
		try {
			sonar.unsubscribe(NaoRobotProxys.suscriberName);
			memory.unsubscribeAllEvents();
			dispose();
		} catch (CallError | InterruptedException e) {
			failed("Error al terminar el artefacto Sensors");
		}
	}
}
