package es.upv.inf.darodgo3.naoAPI.Artifacts;

import java.util.List;

import com.aldebaran.qi.CallError;
import com.aldebaran.qi.helper.proxies.ALAnimatedSpeech;
import com.aldebaran.qi.helper.proxies.ALMemory;
import com.aldebaran.qi.helper.proxies.ALSpeechRecognition;
import com.aldebaran.qi.helper.proxies.ALTextToSpeech;

import cartago.Artifact;
import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;
import cartago.OpFeedbackParam;
import es.upv.inf.darodgo3.naoAPI.NaoRobotProxys;
import es.upv.inf.darodgo3.naoAPI.enums.Animations;
import es.upv.inf.darodgo3.naoAPI.util.ListParser;

/**
 * Este artefacto CArtAgO permite controlar los componentes de audio del robot.
 * Puede hacer hablar al robot, cambiar el idioma del robot, cambiar la voz del
 * robot, permite habilitar el reconocimiento de voz, etc.
 * 
 * @author Dario Rodriguez Gonzalez
 *
 */
public class Audio extends Artifact {
	private boolean suscribed = false;
	private boolean isVirtual = false;
	// private ALAudioPlayer audio;
	private ALSpeechRecognition speechRecognition;
	private ALMemory memory;
	private ALTextToSpeech speech;
	private ALAnimatedSpeech animatedSpeech;

	/**
	 * Igual que init(url, false)
	 * 
	 * @param url
	 *            La url del robot
	 */
	void init(String url) {
		init(url, false);
	}

	/**
	 * Método que se ejecuta al crear el artefacto. Este método recupera de
	 * NaoRobotProxys los proxys necesarios para que el artefacto pueda
	 * funcionar, los cuales son: ALSpeechRecognition, ALMemory, ALTextToSpeech,
	 * ALAnimatedSpeech. Una vez recuperados los almacena en atributos de clase,
	 * así las operaciones podrán hacer uso de los mismos sin tener que volver a
	 * recuperarlos.
	 * 
	 * @param url
	 *            La url del robot
	 * @param isVirtual
	 *            Indica si el robot es virtual o no
	 */
	void init(String url, boolean isVirtual) {
		// this.audio = NaoRobotProxys.getInstance(url).getAudio();
		this.speechRecognition = NaoRobotProxys.getInstance(url, isVirtual).getSpeechRecognition();
		this.memory = NaoRobotProxys.getInstance(url, isVirtual).getMemory();
		this.speech = NaoRobotProxys.getInstance(url, isVirtual).getSpeech();
		this.animatedSpeech = NaoRobotProxys.getInstance(url, isVirtual).getAnimatedSpeech();

		this.isVirtual = isVirtual;

		// try {
		// audio.playSoundSetFile("Aldebaran",
		// "enu_ono_laugh_expressive_01");
		// } catch (CallError | InterruptedException e) {
		// e.printStackTrace();
		// }
	}

	/**
	 * Convierte un texto a sonido: toma un String como entrada, lo convierte en
	 * sonido y lo emite en ambos altavoces. Esta es una operación sincrona.
	 * 
	 * @param text2say
	 *            El texto que será convertido en sonido.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),say(text2say)]
	 */
	@OPERATION
	public void say(String text2say) {
		try {
			speech.say(text2say);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación say", "say", text2say);
		}
	}

	/**
	 * Igual que say, pero en vez de ser una operación sincrona es una operación
	 * asincrona.
	 * 
	 * @param text2say
	 *            El texto que será convertido en sonido
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),say_async(text2say)]
	 */
	@OPERATION
	public void sayAsync(String text2say) {
		try {
			speech.async().say(text2say);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación sayAsync", "say_async", text2say);
		}
	}

	/**
	 * Convierte un texto en sonido y lo reproduce al cabo de un tiempo, el cual
	 * es indicado como parametro. Esta es una operación asincrona.
	 * 
	 * @param text2say
	 *            El texto que será convertido en sonido
	 * @param delay
	 *            El tiempo de retraso, en milisegundos.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),say_async_with_delay(text2say, delay)]
	 */
	@OPERATION
	public void sayAsyncWithDelay(String text2say, long delay) {
		try {
			await_time(delay);
			speech.async().say(text2say);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación sayAsyncWithDelay", "say_async_with_delay", text2say, delay);
		}
	}

	/**
	 * Convierte un texto en sonido y lo guarda en un fichero. Esta es una
	 * operación sincrona.
	 * 
	 * @param text2say
	 *            El texto que será convertido en sonido
	 * @param fileName
	 *            Nombre del fichero donde se guardará el sonido.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),say_to_file(text2say,
	 *             fileName)]
	 */
	@OPERATION
	public void sayToFile(String text2say, String fileName) {
		try {
			this.speech.sayToFile(text2say, fileName);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación sayToFile", "say_to_file", text2say, fileName);
		}
	}

	/**
	 * Igual que sayToFile, pero en vez de ser una operación sincrona es una
	 * operación asincrona.
	 * 
	 * @param text2say
	 *            El texto que será convertido en sonido
	 * @param fileName
	 *            Nombre del fichero donde se guardará el sonido.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),say_to_file_async(text2say, fileName)]
	 */
	@OPERATION
	public void sayToFileAsync(String text2say, String fileName) {
		try {
			this.speech.async().sayToFile(text2say, fileName);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación sayToFileAsync", "say_to_file_async", text2say, fileName);
		}
	}

	/**
	 * Hace que el robot diga "Hola" mientras mueve el brazo como si estuviera
	 * saludando. Esta es una operación sincrona.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),say_hello_animated]
	 */
	@OPERATION
	public void sayHelloAnimated() {
		try {
			this.animatedSpeech
					.say("^start(" + Animations.Hey1.getName() + ") Hola ^wait(" + Animations.Hey1.getName() + ")");
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación sayHelloAnimated", "say_hello_animated");
		}
	}

	/**
	 * Hace que el robot hable a la vez que mueve su cuerpo siguiendo una
	 * animación, la cual es proporcionada como parametro. Esta es una operación
	 * sincrona.
	 * 
	 * @param animation
	 *            La animación que efectuará mientras habla. Todas las
	 *            animaciones están disponbiles en:
	 *            http://doc.aldebaran.com/2-1/naoqi/audio/alanimatedspeech_advanced.html#animated-speech-list-behaviors-nao
	 * @param text2say
	 *            El texto que se convertirá a voz
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),say_animated(animation,text2say)]
	 */
	@OPERATION
	public void sayAnimated(String animation, String text2say) {
		String finalAnimation = animation;
		if (Animations.fromString(animation).getName() != null) {
			finalAnimation = Animations.fromString(animation).getName();
		}
		try {
			this.animatedSpeech.say("^start(" + finalAnimation + ")" + text2say + "^wait(" + finalAnimation + ")");
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación sayAnimated", "say_animated", animation, text2say);
		}
	}

	/**
	 * Igual que sayAnimated, pero en vez de ser una operación sincrona es una
	 * operación asincrona.
	 * 
	 * @param animation
	 *            La animación que efectuará mientras habla. Todas las
	 *            animaciones están disponbiles en:
	 *            http://doc.aldebaran.com/2-1/naoqi/audio/alanimatedspeech_advanced.html#animated-speech-list-behaviors-nao
	 * @param text2say
	 *            El texto que se convertirá a voz
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),say_animated(animation,text2say)]
	 */
	@OPERATION
	public void sayAnimatedAsync(String animation, String text2say) {
		String finalAnimation = animation;
		if (Animations.fromString(animation).getName() != null) {
			finalAnimation = Animations.fromString(animation).getName();
		}
		try {
			this.animatedSpeech.async()
					.say("^start(" + finalAnimation + ")" + text2say + "^wait(" + finalAnimation + ")");
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación sayAnimatedAsync", "say_animated_async", animation, text2say);
		}
	}

	/**
	 * Establece la lista de palabras (vocabulario) que debe ser reconocidas por
	 * el motor de reconocimiento de voz. Esta es una operación sincrona. Esta
	 * operación no está disponible para robots virtuales.
	 * 
	 * @param vocabulary
	 *            Lista de palabras que deben ser reconocidas
	 * @param enabledWordSpotting
	 *            Si está deshabilitado, el motor espera escuchar una de las
	 *            palabras especificadas, voz entero, el motor intentará
	 *            detectarlas.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),set_vocabulary(vocabulary,enabledWordSpotting)]
	 */
	@OPERATION
	public void setVocabulary(Object[] vocabulary, boolean enabledWordSpotting) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			List<String> lvocabulary = ListParser.cartagoStringListToList(vocabulary);
			this.speechRecognition.pause(true);
			this.speechRecognition.setVocabulary(lvocabulary, enabledWordSpotting);
			this.speechRecognition.pause(true);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación setVocabulary", "set_vocabulary", vocabulary,
					enabledWordSpotting);
		}
	}

	/**
	 * Empieza el proceso de reconocimiento de palabras. Cuando una palabra es
	 * detectada, esta genera una creencia en Jason la cual sigue la siguiente
	 * forma: wordRecognized(word), donde word es la palabra reconocida. Esta es
	 * una operación asincrona. Esta operación no está disponible para robots
	 * virtuales.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),start_word_recognition]
	 */
	@OPERATION
	public void startWordRecognition() {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			speechRecognition.subscribe(NaoRobotProxys.suscriberName);
			this.suscribed = true;
			this.memory.subscribeToEvent("WordRecognized", value -> {
				@SuppressWarnings("unchecked")
				String word = (String) ((List<Object>) value).get(0);
				execInternalOp("sendSignal", "wordRecognized", word);
			});
		} catch (Exception e) {
			failed("No se pudo completar la operación startWordRecognition", "start_word_recognition");
		}
	}

	/**
	 * Para el reconocimiento de palabras. Despues de ejecutar esta operación no
	 * se reconocerá ninguna palabra más hasta que se vuelva a ejecutar
	 * startWordRecognition. Es una operación sincrona. Esta operación no está
	 * disponible para robots virtuales.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),stop_word_recognition]
	 */
	@OPERATION
	public void stopWordRecognition() {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			speechRecognition.unsubscribe(NaoRobotProxys.suscriberName);
			this.suscribed = false;
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación stopWordRecognition", "stop_word_recognition");
		}
	}

	/**
	 * Obtiene la lista de idiomas instalados en el sistema del robot, y los
	 * devuelve en el parametro de salida res como un String. Esta es una
	 * operación sincrona.
	 * 
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),speech_get_available_languajes]
	 */
	@OPERATION
	public void speechGetAvailableLanguages(OpFeedbackParam<String> res) {
		try {
			res.set(ListParser.javaStringListToCartagoString(this.speech.getAvailableLanguages()));
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación speechGetAvailableLanguages", "speech_get_available_languajes");
		}
	}

	/**
	 * Cambia el idioma del motor que traduce el texto a sonido. Este idioma
	 * debe estar instalado en el robot. Para ello puede hacer uso de la
	 * operación speechGetAvailableLanguages. Esta es una operación sincrona.
	 * 
	 * @param lang
	 *            Nuevo idoma del robot
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),speech_set_languaje(lang)]
	 */
	@OPERATION
	public void speechSetLanguage(String lang) {
		try {
			this.speech.setLanguage(lang);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación speechSetLanguage", "speech_set_languaje", lang);
		}
	}

	/**
	 * Obtiene la lista de voces instaladas en el sistema del robot, y las
	 * devuelve en el parametro de salida res como un String. Esta es una
	 * operación sincrona.
	 * 
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),speech_get_available_voices]
	 */
	@OPERATION
	public void speechGetAvailableVoices(OpFeedbackParam<String> res) {
		try {
			res.set(ListParser.javaStringListToCartagoString(this.speech.getAvailableVoices()));
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación speechGetAvailableVoices", "speech_get_available_voices");
		}
	}

	/**
	 * Devuelve el valor de uno de los parámetros de voz en el parametro de
	 * salida res. Los parámetros disponibles son: "pitchShift", "doubleVoice",
	 * "doubleVoiceLevel" y "doubleVoiceTimeShift". Esta es una operación
	 * sincrona.
	 * 
	 * @param parameter
	 *            Nombre del parametro a obtener
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),speech_get_parameter(parameter)]
	 */
	@OPERATION
	public void speechGetParameter(String parameter, OpFeedbackParam<Float> res) {
		try {
			res.set(this.speech.getParameter(parameter));
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación speechGetParameter", "speech_get_parameter", parameter);
		}
	}

	/**
	 * Cambia los parámetros de la voz. Esta es una operación sincrona. Los
	 * parámetros disponibles son:
	 * 
	 * pitchShift: aplica un cambio de tono a la voz.El valor indica la relación
	 * entre las nuevas frecuencias fundamentales y las anteriores (ejemplos:
	 * 2.0: una octava arriba, 1.5: una quinta arriba). El rango correcto es
	 * (1.0 - 4), o 0 para desactivar el efecto.
	 * 
	 * DoubleVoice: añade una segunda voz a la primera. El valor indica la
	 * relación entre la frecuencia fundamental de la segunda voz y la primera.
	 * El rango correcto es (1.0-4) o 0 para desactivar el efecto.
	 * 
	 * doubleVoiceLevel: el valor correspondiente es el nivel de la voz doble
	 * (1.0: igual a la voz principal). El rango correcto es (0 - 4).
	 * 
	 * DoubleVoiceTimeShift: el valor correspondiente es el retardo entre la voz
	 * doble y la principal. El rango correcto es (0 - 0.5) Si el valor del
	 * efecto no está disponible, el parámetro del efecto permanece sin cambios.
	 * 
	 * @param parameter
	 *            Nombre del parametro
	 * @param value
	 *            Nuevo valor
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),speech_set_parameter(parameter,value)]
	 */
	@OPERATION
	public void speechSetParameter(String parameter, float value) {
		try {
			this.speech.setParameter(parameter, value);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación speechSetParameter", "speech_set_parameter", parameter, value);
		}
	}

	/**
	 * Obtiene la actual del robot, y la devuelve en el parametro de salida res
	 * como un String. Esta es una operación sincrona.
	 * 
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),speech_get_voice]
	 */
	@OPERATION
	public void speechGetVoice(OpFeedbackParam<String> res) {
		try {
			res.set(this.speech.getVoice());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación speechGetVoice", "speech_get_voice");
		}
	}

	/**
	 * Cambia la voz del robot. Esta voz debe estar instalada en el robot. Para
	 * ello puede hacer uso de la operación speechGetAvailableVoices. Esta es
	 * una operación sincrona.
	 * 
	 * @param voice
	 *            Nuevo idoma del robot
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),speech_set_voice(voice)]
	 */
	@OPERATION
	public void speechSetVoice(String voice) {
		try {
			this.speech.setVoice(voice);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación speechSetVoice", "speech_set_voice", voice);
		}
	}

	/**
	 * Obtiene el volumen actual del texto a voz y lo devuelve en el parametro
	 * de salida res. El volumen es un número entero el cual va de 0 a 100. Esta
	 * es una operación sincrona
	 * 
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),speech_get_volume]
	 */
	@OPERATION
	public void speechGetVolume(OpFeedbackParam<Float> res) {
		try {
			res.set(this.speech.getVolume());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación speechGetVolume", "speech_get_volume");
		}
	}

	/**
	 * Establece el volumen de la salida de texto a voz. El volumen es un número
	 * en coma flotante el cual va de 0.0 a 1.0. Esta es una operación sincrona.
	 * 
	 * @param volume
	 *            Nuevo valor de volumen.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),speech_set_volume(volume)]
	 */
	@OPERATION
	public void speechSetVolume(float volume) {
		try {
			this.speech.setVolume(volume);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación speechSetVolume", "speech_set_volume", volume);
		}
	}

	/**
	 * Esta operación detiene la actual y todas las tareas de conversión te
	 * texto a sonido pendientes inmediatamente.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),speech_stop_all]
	 */
	@OPERATION
	public void speechStopAll() {
		try {
			this.speech.stopAll();
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación speechStopAll", "speech_stop_all");
		}
	}

	@INTERNAL_OPERATION
	private void sendSignal(String signal, String value) {
		signal(signal, value);
	}

	/**
	 * Esta operación termina todas las conexión y subscripciones a eventos
	 * establecidas anteriormente, con el fin de poder terminar el programa
	 * correctamente. Por último, deshace el artefacto.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg)]
	 */
	@OPERATION
	public void endAudio() {
		if (this.suscribed)
			try {
				speechRecognition.unsubscribe(NaoRobotProxys.suscriberName);
				dispose();
			} catch (CallError | InterruptedException e) {
				failed("Error al terminar el artefacto Audio");
			}
	}

}
