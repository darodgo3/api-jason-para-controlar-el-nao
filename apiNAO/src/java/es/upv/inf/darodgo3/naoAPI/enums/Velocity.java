/**
 * 
 */
package es.upv.inf.darodgo3.naoAPI.enums;

/**
 * @author dario
 *
 */
public enum Velocity {
	MUYRAPIDO(1.0f),RAPIDO(0.8f),NORMAL(0.5f),LENTO(0.3f),MUYLENTO(0.1f);
	
	private final float velocidad;
	
	private Velocity(float velocidad){
		this.velocidad=velocidad;		
	}
	
	public final float getVelocity(){
		return this.velocidad;
	}
	
	public static Velocity fromString(String vel){
		for(Velocity e : Velocity.values()){
            if(vel.toLowerCase().equals(e.name().toLowerCase())) return e;
        }
        return null;
	}
}
