// CArtAgO artifact code for project tfg

package es.upv.inf.darodgo3.naoAPI.Artifacts;

import cartago.Artifact;
import cartago.ArtifactConfig;
import cartago.ArtifactId;
import cartago.OPERATION;
import cartago.OpFeedbackParam;
import cartago.OperationException;

/**
 * Artefacto auxiliar el cual nos permite crear todos los artefactos
 * relacionados con el robot (Audio, Core, Motion, PeoplePerception, Sensors y
 * Vision) empleando solo una instrucción.
 * 
 * @author Dario Rodriguez Gonzalez
 *
 */
public class NaoRobot extends Artifact {
	void init() {

	}

	/**
	 * Operación para crear los artefactos Audio, Core, Motion,
	 * PeoplePerception, Sensors y Vision
	 * 
	 * @param url
	 *            La url del robot
	 * @param isVirtual
	 *            Indica si es un robot virtual o no
	 * @param idSensors
	 *            Parametro de salida el cual nos proporciona el identificador
	 *            del artefacto Sensors
	 * @param idAudio
	 *            Parametro de salida el cual nos proporciona el identificador
	 *            del artefacto Audio
	 * @param idCore
	 *            Parametro de salida el cual nos proporciona el identificador
	 *            del artefacto Core
	 * @param idMotion
	 *            Parametro de salida el cual nos proporciona el identificador
	 *            del artefacto Motion
	 * @param idVision
	 *            Parametro de salida el cual nos proporciona el identificador
	 *            del artefacto Vision
	 * @param idPeoplePerception
	 *            Parametro de salida el cual nos proporciona el identificador
	 *            del artefacto PeoplePerception
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),create_robot_artifacts(url,isVirtual)]
	 */
	@OPERATION
	public void createRobotArtifacts(String url, boolean isVirtual, OpFeedbackParam<ArtifactId> idSensors,
			OpFeedbackParam<ArtifactId> idAudio, OpFeedbackParam<ArtifactId> idCore,
			OpFeedbackParam<ArtifactId> idMotion, OpFeedbackParam<ArtifactId> idVision,
			OpFeedbackParam<ArtifactId> idPeoplePerception) {
		try {
			idCore.set(makeArtifact("core", "es.upv.inf.darodgo3.naoAPI.Artifacts.Core",
					new ArtifactConfig(url, isVirtual)));
			idMotion.set(makeArtifact("motion", "es.upv.inf.darodgo3.naoAPI.Artifacts.Motion",
					new ArtifactConfig(url, isVirtual)));
			idSensors.set(makeArtifact("sensors", "es.upv.inf.darodgo3.naoAPI.Artifacts.Sensors",
					new ArtifactConfig(url, isVirtual)));
			idVision.set(makeArtifact("vision", "es.upv.inf.darodgo3.naoAPI.Artifacts.Vision",
					new ArtifactConfig(url, isVirtual)));
			idAudio.set(makeArtifact("audio", "es.upv.inf.darodgo3.naoAPI.Artifacts.Audio",
					new ArtifactConfig(url, isVirtual)));
			idPeoplePerception.set(makeArtifact("peoplePerception",
					"es.upv.inf.darodgo3.naoAPI.Artifacts.PeoplePerception", new ArtifactConfig(url, isVirtual)));

		} catch (OperationException e) {
			failed("No se pudo completar la operación createRobotArtifacts", "create_robot_artifacts", url, isVirtual);
		}
	}
}
