package es.upv.inf.darodgo3.naoAPI.Artifacts;

import cartago.Artifact;
import cartago.INTERNAL_OPERATION;
import net.java.games.input.Component;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Event;
import net.java.games.input.EventQueue;

/**
 * Este artefacto utiliza la librería Jinput para obtener las pulsaciones de un
 * mando de consola. Cuando obtiene una pulsación, genera una creencia en el
 * agente Jason la cual siguel a siguiente estructura: nombre_del_boton(valor).
 * Para poder saber los nombres de los botones y los valores que obtiene, al
 * crear el artefacto se le puede proporcionar un parametro, el cual si está a
 * "yes" mostrara los botones en la consola.
 * 
 * @author Dario Rodriguez Gonzalez
 *
 */
public class GamePad extends Artifact {

	public void init() {
		init("no");
	}

	public void init(String showKeys) {
		execInternalOp("getKeys", showKeys);
	}

	@INTERNAL_OPERATION
	private void sendSignal(String signal, Object... rest) {
		signal(signal.toLowerCase());
	}

	@INTERNAL_OPERATION
	private void getKeys(String showKeys) {
		while (true) {
			Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
			if (controllers.length == 0) {
				System.out.println("Found no controllers.");
				System.exit(0);
			}

			for (int i = 0; i < controllers.length; i++) {
				controllers[i].poll();

				EventQueue queue = controllers[i].getEventQueue();

				Event event = new Event();

				while (queue.getNextEvent(event)) {
					Component comp = event.getComponent();

					if (comp.getName().toLowerCase().contains(" ")) {
						String[] a = comp.getName().toLowerCase().split(" ");
						signal(a[0] + a[1], event.getValue());

						if (showKeys.equals("yes")) {
							System.out.println(a[0] + a[1] + " " + event.getValue());
						}
					} else {
						signal(comp.getName().toLowerCase(), event.getValue());

						if (showKeys.equals("yes")) {
							System.out.println(comp.getName().toLowerCase() + " " + event.getValue());
						}
					}

				}
			}

			await_time(100);
		}
	}
}
