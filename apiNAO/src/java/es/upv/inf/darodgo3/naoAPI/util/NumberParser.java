package es.upv.inf.darodgo3.naoAPI.util;

public class NumberParser {
	public static Double cartagoNumberToDouble(Object number){
		if (number instanceof Byte){
			return ((Byte) number).doubleValue();
		} else if (number instanceof Short) {
			return ((Short) number).doubleValue();
		} else if (number instanceof Integer) {
			return ((Integer) number).doubleValue();
		} else if (number instanceof Long) {
			return ((Long) number).doubleValue();
		} else if (number instanceof Float) {
			return ((Float) number).doubleValue();
		} else if (number instanceof Double) {
			return (Double) number;
		}
		
		return null;
	}
	
	public static Float cartagoNumberToFloat(Object number){
		if (number instanceof Byte){
			return ((Byte) number).floatValue();
		} else if (number instanceof Short) {
			return ((Short) number).floatValue();
		} else if (number instanceof Integer) {
			return ((Integer) number).floatValue();
		} else if (number instanceof Long) {
			return ((Long) number).floatValue();
		} else if (number instanceof Float) {
			return (Float) number;
		} else if (number instanceof Double) {
			return ((Double) number).floatValue();
		}
		
		return null;
	}
}
