package es.upv.inf.darodgo3.naoAPI.enums;

public enum LedNames {
	Brain0("Brain0"),
	Brain1("Brain1"),
	Brain2("Brain2"),
	Brain3("Brain3"),
	Brain4("Brain4"),
	Brain5("Brain5"),
	Brain6("Brain6"),
	Brain7("Brain7"),
	Brain8("Brain8"),
	Brain9("Brain9"),
	Brain10("Brain10"),
	Brain11("Brain11"),
	RightFaceLed1("RightFaceLed1"),
	RightFaceLed2("RightFaceLed2"),
	RightFaceLed3("RightFaceLed3"),
	RightFaceLed4("RightFaceLed4"),
	RightFaceLed5("RightFaceLed5"),
	RightFaceLed6("RightFaceLed6"),
	RightFaceLed7("RightFaceLed7"),
	LeftFaceLed1("LeftFaceLed1"),
	LeftFaceLed2("LeftFaceLed2"),
	LeftFaceLed3("LeftFaceLed3"),
	LeftFaceLed4("LeftFaceLed4"),
	LeftFaceLed5("LeftFaceLed5"),
	LeftFaceLed6("LeftFaceLed6"),
	LeftFaceLed7("LeftFaceLed7"),
	RightEarLed1("RightEarLed1"),
	RightEarLed2("RightEarLed2"),
	RightEarLed3("RightEarLed3"),
	RightEarLed4("RightEarLed4"),
	RightEarLed5("RightEarLed5"),
	RightEarLed6("RightEarLed6"),
	RightEarLed7("RightEarLed7"),
	RightEarLed8("RightEarLed8"),
	RightEarLed9("RightEarLed9"),
	RightEarLed10("RightEarLed10"),
	LeftEarLed1("LeftEarLed1"),
	LeftEarLed2("LeftEarLed2"),
	LeftEarLed3("LeftEarLed3"),
	LeftEarLed4("LeftEarLed4"),
	LeftEarLed5("LeftEarLed5"),
	LeftEarLed6("LeftEarLed6"),
	LeftEarLed7("LeftEarLed7"),
	LeftEarLed8("LeftEarLed8"),
	LeftEarLed9("LeftEarLed9"),
	LeftEarLed10("LeftEarLed10"),
	LeftFootLedsBlue("LeftFootLedsBlue"),
	LeftFootLedsGreen("LeftFootLedsGreen"),
	LeftFootLedsRed("LeftFootLedsRed");

	private String name;

	private LedNames(String name){
		this.name = name;
	}

	public String getName(){
		return this.name;
	}
	
	public static LedNames fromString(String led){
		for(LedNames e : LedNames.values()){
            if(led.toLowerCase().equals(e.name().toLowerCase())) return e;
        }
        return null;
	}
}
