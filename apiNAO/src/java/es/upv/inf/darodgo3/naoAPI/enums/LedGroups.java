package es.upv.inf.darodgo3.naoAPI.enums;

public enum LedGroups {
	AllLeds("AllLeds"),
	AllLedsBlue("AllLedsBlue"),
	AllLedsGreen("AllLedsGreen"),
	AllLedsRed("AllLedsRed"),
	BrainLeds("BrainLeds"),
	BrainLedsBack("BrainLedsBack"),
	BrainLedsMiddle("BrainLedsMiddle"),
	BrainLedsFront("BrainLedsFront"),
	BrainLedsLeft("BrainLedsLeft"),
	BrainLedsRight("BrainLedsRight"),
	EarLeds("EarLeds"),
	RightEarLeds("RightEarLeds"),
	LeftEarLeds("LeftEarLeds"),
	RightEarLedsBack("RightEarLedsBack"),
	RightEarLedsFront("RightEarLedsFront"),
	LeftEarLedsBack("LeftEarLedsBack"),
	LeftEarLedsFront("LeftEarLedsFront"),
	RightEarLedsEven("RightEarLedsEven"),
	RightEarLedsOdd("RightEarLedsOdd"),
	LeftEarLedsEven("LeftEarLedsEven"),
	LeftEarLedsOdd("LeftEarLedsOdd"),
	FaceLeds("FaceLeds"),
	RightFaceLeds("RightFaceLeds"),
	LeftFaceLeds("LeftFaceLeds"),
	FaceLedsBottom("FaceLedsBottom"),
	FaceLedsExternal("FaceLedsExternal"),
	FaceLedsInternal("FaceLedsInternal"),
	FaceLedsTop("FaceLedsTop"),
	FaceLedsRightBottom("FaceLedsRightBottom"),
	FaceLedsRightExternal("FaceLedsRightExternal"),
	FaceLedsRightInternal("FaceLedsRightInternal"),
	FaceLedsRightTop("FaceLedsRightTop"),
	FaceLedsLeftBottom("FaceLedsLeftBottom"),
	FaceLedsLeftExternal("FaceLedsLeftExternal"),
	FaceLedsLeftInternal("FaceLedsLeftInternal"),
	FaceLedsLeftTop("FaceLedsLeftTop"),
	RightFaceLedsBlue("RightFaceLedsBlue"),
	RightFaceLedsGreen("RightFaceLedsGreen"),
	RightFaceLedsRed("RightFaceLedsRed"),
	LeftFaceLedsBlue("LeftFaceLedsBlue"),
	LeftFaceLedsGreen("LeftFaceLedsGreen"),
	LeftFaceLedsRed("LeftFaceLedsRed"),
	ChestLeds("ChestLeds"),
	ChestLedsBlue("ChestLedsBlue"),
	ChestLedsGreen("ChestLedsGreen"),
	ChestLedsRed("ChestLedsRed"),
	FeetLeds("FeetLeds"),
	LeftFootLeds("LeftFootLeds"),
	RightFootLeds("RightFootLeds");

	private String group;
	
	private LedGroups(String group){
		this.group = group;
	}
	
	public String getGroup(){
		return this.group;
	}
	
	public static LedGroups fromString(String led){
		for(LedGroups e : LedGroups.values()){
            if(led.toLowerCase().equals(e.name().toLowerCase())) return e;
        }
        return null;
	}
	
}
