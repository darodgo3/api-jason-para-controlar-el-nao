// CArtAgO artifact code for project tfg

package es.upv.inf.darodgo3.naoAPI.Artifacts;

import com.aldebaran.qi.CallError;
import com.aldebaran.qi.helper.proxies.ALBasicAwareness;

import cartago.Artifact;
import cartago.OPERATION;
import es.upv.inf.darodgo3.naoAPI.NaoRobotProxys;

/**
 * Este artefacto proporciona un conjunto de métodos relacionados con la
 * percepción de personas y sus características.
 * 
 * @author Dario Rodriguez Gonzalez
 *
 */
public class PeoplePerception extends Artifact {
	private ALBasicAwareness awareness;
	private boolean isVirtual;

	/**
	 * Igual que init(url, false)
	 * 
	 * @param url
	 *            La url del robot
	 */
	void init(String url) {
		init(url, false);
	}

	/**
	 * Método que se ejecuta al crear el artefacto. Este método recupera de
	 * NaoRobotProxys los proxys necesarios para que el artefacto pueda
	 * funcionar, los cuales son: ALBasicAwareness. Una vez recuperados los
	 * almacena en atributos de clase, así las operaciones podrán hacer uso de
	 * los mismos sin tener que volver a recuperarlos.
	 * 
	 * Además de esto, se habilitarán los siguientes estimulos los cuales harán
	 * reaccionar al robot en la busqueda del personas: sonido, movimiento,
	 * gente y tacto.
	 * 
	 * Por último establece el tipo de seguimiento a las personas, el cual será:
	 * SemiEngaged. Este actua de la siguiente manera: Cuando el robot está en
	 * contacto con una persona, sigue escuchando los estímulos, y si obtiene un
	 * estímulo, mirará en su dirección, pero siempre volverá a la persona con
	 * la que está comprometido. Si pierde a la persona, escuchará los estímulos
	 * nuevamente y podrá involucrarse con otra persona.
	 * 
	 * @param url
	 *            La url del robot
	 * @param isVirtual
	 *            Indica si el robot es virtual o no
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),awareness_init(url,isVirtual)]
	 */
	void init(String url, boolean isVirtual) {
		this.awareness = NaoRobotProxys.getInstance(url, isVirtual).getAwareness();

		this.isVirtual = isVirtual;

		if (!isVirtual) {
			try {
				awareness.setEngagementMode("SemiEngaged");
				awareness.setStimulusDetectionEnabled("Sound", true);
				awareness.setStimulusDetectionEnabled("Movement", true);
				awareness.setStimulusDetectionEnabled("People", true);
				awareness.setStimulusDetectionEnabled("Touch", true);
			} catch (CallError | InterruptedException e) {
				failed("No se pudo crear el artefacto Awareness", "awareness_init", url, isVirtual);
			}
		}
	}

	/**
	 * Inicia el proceso se seguimiento de personas. El tipo de seguimiento se
	 * indica por parametro, el cual puede ser:
	 * 
	 * Head: El seguimiento sólo utiliza la cabeza
	 * 
	 * BodyRotation: El seguimiento utiliza la cabeza y la rotación del cuerpo
	 * 
	 * WholeBody: El seguimiento utiliza todo el cuerpo, pero no lo hace girar
	 * 
	 * MoveContextually: El seguimiento utiliza la cabeza y realiza de forma
	 * autónoma pequeños movimientos, como acercarse a la persona rastreada,
	 * retroceder, girar ...
	 * 
	 * Esta es una operación sincrona. Esta operación no está disponible para
	 * robots virtuales.
	 * 
	 * @param trackingMode
	 *            Tipo de seguimiento
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),start_awareness]
	 */
	@OPERATION
	public void startAwareness(String trackingMode) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			awareness.setTrackingMode(trackingMode);
			awareness.startAwareness();
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación startAwareness", "start_awareness", trackingMode);
		}
	}

	/**
	 * Termina el seguimiento de personas. Esta es una operación sincrona. Esta
	 * operación no está disponible para robots virtuales.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),stop_awareness]
	 */
	@OPERATION
	public void stopAwareness() {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			if (awareness.isAwarenessRunning()) {
				awareness.stopAwareness();
			}
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación stopAwareness", "stop_awareness");
		}
	}

	/**
	 * Esta operación termina todas las conexión y subscripciones a eventos
	 * establecidas anteriormente, con el fin de poder terminar el programa
	 * correctamente. Por último, deshace el artefacto.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg)]
	 */
	@OPERATION
	public void endPeoplePerception() {
		if (!this.isVirtual) {

			try {
				if (awareness.isAwarenessRunning())
					awareness.stopAwareness();
			} catch (CallError | InterruptedException e) {
				failed("Error al terminar el artefacto Awareness");
			}
		}

		dispose();
	}

}
