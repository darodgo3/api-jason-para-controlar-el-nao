package es.upv.inf.darodgo3.naoAPI.enums;

public enum MemoryEvents {
	ALAnimatedSpeech_EndOfAnimatedSpeech("ALAnimatedSpeech/EndOfAnimatedSpeech"),
	ALBehaviorManager_BehaviorAdded("ALBehaviorManager/BehaviorAdded"),
	ALBehaviorManager_BehaviorFailed("ALBehaviorManager/BehaviorFailed"),
	ALBehaviorManager_BehaviorRemoved("ALBehaviorManager/BehaviorRemoved"),
	ALBehaviorManager_BehaviorUpdated("ALBehaviorManager/BehaviorUpdated"),
	ALBehaviorManager_BehaviorsAdded("ALBehaviorManager/BehaviorsAdded"),
	ALBehaviorManager_BehaviorsLoaded("ALBehaviorManager/BehaviorsLoaded"),
	ALBehaviorManager_BehaviorsRemoved("ALBehaviorManager/BehaviorsRemoved"),
	ALFastPersonTracking_TrackedPersonNotFound("ALFastPersonTracking/TrackedPersonNotFound"),
	ALFindPersonHead_HeadNotFound("ALFindPersonHead/HeadNotFound"),
	ALFindPersonHead_HeadReached("ALFindPersonHead/HeadReached"),
	ALLocalization_FullScanBegin("ALLocalization/FullScanBegin"),
	ALLocalization_FullScanInsufficient("ALLocalization/FullScanInsufficient"),
	ALLocalization_FullScanSuccess("ALLocalization/FullScanSuccess"),
	ALLocalization_GoToBegin("ALLocalization/GoToBegin"),
	ALLocalization_GoToContinue("ALLocalization/GoToContinue"),
	ALLocalization_GoToFailed("ALLocalization/GoToFailed"),
	ALLocalization_GoToLost("ALLocalization/GoToLost"),
	ALLocalization_GoToNextMove("ALLocalization/GoToNextMove"),
	ALLocalization_GoToSuccess("ALLocalization/GoToSuccess"),
	ALLocalization_HalfScanBegin("ALLocalization/HalfScanBegin"),
	ALLocalization_HalfScanInsufficient("ALLocalization/HalfScanInsufficient"),
	ALLocalization_HalfScanSuccess("ALLocalization/HalfScanSuccess"),
	ALLocalization_LocalizeBegin("ALLocalization/LocalizeBegin"),
	ALLocalization_LocalizeDirectionBegin("ALLocalization/LocalizeDirectionBegin"),
	ALLocalization_LocalizeDirectionLost("ALLocalization/LocalizeDirectionLost"),
	ALLocalization_LocalizeDirectionSuccess("ALLocalization/LocalizeDirectionSuccess"),
	ALLocalization_LocalizeLost("ALLocalization/LocalizeLost"),
	ALLocalization_LocalizeSuccess("ALLocalization/LocalizeSuccess"),
	ALLocalization_OdometryBegin("ALLocalization/OdometryBegin"),
	ALLocalization_OdometryInsufficient("ALLocalization/OdometryInsufficient"),
	ALLocalization_StartingComputation("ALLocalization/StartingComputation"),
	ALLocalization_StoppingComputation("ALLocalization/StoppingComputation"),
	ALMemory_KeyAdded("ALMemory/KeyAdded"),
	ALMemory_KeyRemoved("ALMemory/KeyRemoved"),
	ALMemory_KeyTypeChanged("ALMemory/KeyTypeChanged"),
	ALMotion_Hatch_MoveFailed("ALMotion/Hatch/MoveFailed"),
	ALMotion_Protection_DisabledDevicesChanged("ALMotion/Protection/DisabledDevicesChanged"),
	ALMotion_RobotIsFalling("ALMotion/RobotIsFalling"),
	ALMotion_RobotIsStand("ALMotion/RobotIsStand"),
	ALMotion_Safety_ChainVelocityClipped("ALMotion/Safety/ChainVelocityClipped"),
	ALMotion_Safety_MoveFailed("ALMotion/Safety/MoveFailed"),
	ALMotion_Safety_PushRecovery("ALMotion/Safety/PushRecovery"),
	ALMotion_Safety_RobotPushed("ALMotion/Safety/RobotPushed"),
	ALMotion_Stiffness_restFinished("ALMotion/Stiffness/restFinished"),
	ALMotion_Stiffness_restStarted("ALMotion/Stiffness/restStarted"),
	ALMotion_Stiffness_wakeUpFinished("ALMotion/Stiffness/wakeUpFinished"),
	ALMotion_Stiffness_wakeUpStarted("ALMotion/Stiffness/wakeUpStarted"),
	ALMotionRecorder_CurrentFrame("ALMotionRecorder/CurrentFrame"),
	ALMotionRecorder_Recording("ALMotionRecorder/Recording"),
	ALMotionRecorder_StorePositionRequested("ALMotionRecorder/StorePositionRequested"),
	ALNavigation_SafetyBlindZoneDetected("ALNavigation/SafetyBlindZoneDetected"),
	ALPanoramaCompass_FullScanBegin("ALPanoramaCompass/FullScanBegin"),
	ALPanoramaCompass_HalfScanBegin("ALPanoramaCompass/HalfScanBegin"),
	ALPanoramaCompass_HalfScanInsufficient("ALPanoramaCompass/HalfScanInsufficient"),
	ALPanoramaCompass_StartShootOneFrame("ALPanoramaCompass/StartShootOneFrame"),
	ALPanoramaCompass_StopShootOneFrame("ALPanoramaCompass/StopShootOneFrame"),
	ALSentinel_BatteryLevel("ALSentinel/BatteryLevel"),
	ALTextToSpeech_CurrentBookMark("ALTextToSpeech/CurrentBookMark"),
	ALTextToSpeech_CurrentSentence("ALTextToSpeech/CurrentSentence"),
	ALTextToSpeech_CurrentWord("ALTextToSpeech/CurrentWord"),
	ALTextToSpeech_PositionOfCurrentWord("ALTextToSpeech/PositionOfCurrentWord"),
	ALTextToSpeech_Status("ALTextToSpeech/Status"),
	ALTextToSpeech_TextDone("ALTextToSpeech/TextDone"),
	ALTextToSpeech_TextInterrupted("ALTextToSpeech/TextInterrupted"),
	ALTextToSpeech_TextStarted("ALTextToSpeech/TextStarted"),
	ALTracker_ActiveTargetChanged("ALTracker/ActiveTargetChanged"),
	ALTracker_ArmTracking_Restarted("ALTracker/ArmTracking/Restarted"),
	ALTracker_ArmTracking_Stopped("ALTracker/ArmTracking/Stopped"),
	ALTracker_BlobDetected("ALTracker/BlobDetected"),
	ALTracker_CloseObjectDetected("ALTracker/CloseObjectDetected"),
	ALTracker_ColorBlobDetected("ALTracker/ColorBlobDetected"),
	ALTracker_FastPersonTracking("ALTracker/FastPersonTracking"),
	ALTracker_FindPersonHead("ALTracker/FindPersonHead"),
	ALTracker_HeadTracking_Restarted("ALTracker/HeadTracking/Restarted"),
	ALTracker_HeadTracking_Stopped("ALTracker/HeadTracking/Stopped"),
	ALTracker_ObjectLookAt("ALTracker/ObjectLookAt"),
	ALTracker_ObjectMoveTo("ALTracker/ObjectMoveTo"),
	ALTracker_SearchLoopOver("ALTracker/SearchLoopOver"),
	ALTracker_SecondTargetDetected("ALTracker/SecondTargetDetected"),
	ALTracker_StopSearch("ALTracker/StopSearch"),
	ALTracker_TargetDetected("ALTracker/TargetDetected"),
	ALTracker_TargetLost("ALTracker/TargetLost"),
	ALTracker_TargetReached("ALTracker/TargetReached"),
	ALWorldRepresentation_DisplayRequired("ALWorldRepresentation/DisplayRequired"),
	ActiveDiagnosisErrorChanged("ActiveDiagnosisErrorChanged"),
	AsrEmotion("AsrEmotion"),
	AutonomousLife_CompletedActivity("AutonomousLife/CompletedActivity"),
	AutonomousLife_FocusedActivity("AutonomousLife/FocusedActivity"),
	AutonomousLife_LaunchSuggestions("AutonomousLife/LaunchSuggestions"),
	AutonomousLife_NextActivity("AutonomousLife/NextActivity"),
	AutonomousLife_Report("AutonomousLife/Report"),
	AutonomousLife_State("AutonomousLife/State"),
	BatteryChargeCellVoltageMinChanged("BatteryChargeCellVoltageMinChanged"),
	BatteryChargeChanged("BatteryChargeChanged"),
	BatteryChargingFlagChanged("BatteryChargingFlagChanged"),
	BatteryDisChargingFlagChanged("BatteryDisChargingFlagChanged"),
	BatteryEmpty("BatteryEmpty"),
	BatteryFullChargedFlagChanged("BatteryFullChargedFlagChanged"),
	BatteryLowDetected("BatteryLowDetected"),
	BatteryNearlyEmpty("BatteryNearlyEmpty"),
	BatteryNotDetected("BatteryNotDetected"),
	BatteryPowerPluggedChanged("BatteryPowerPluggedChanged"),
	BehaviorsRun("BehaviorsRun"),
	CloseObjectDetection_ObjectDetected("CloseObjectDetection/ObjectDetected"),
	CloseObjectDetection_ObjectNotDetected("CloseObjectDetection/ObjectNotDetected"),
	DeviceNoLongerHotDetected("DeviceNoLongerHotDetected"),
	Dialog_Answered("Dialog/Answered"),
	Dialog_IsStarted("Dialog/IsStarted"),
	Dialog_LastInput("Dialog/LastInput"),
	Dialog_NotSpeaking10("Dialog/NotSpeaking10"),
	Dialog_NotSpeaking1access5("Dialog/NotSpeaking1access5"),
	Dialog_NotSpeaking20("Dialog/NotSpeaking20"),
	Dialog_NotSpeaking5("Dialog/NotSpeaking5"),
	Dialog_NotUnderstood("Dialog/NotUnderstood"),
	Dialog_SpeakLouder("Dialog/SpeakLouder"),
	EnablePowerMonitoring("EnablePowerMonitoring"),
	EngagementZones_FirstLimitDistanceUpdated("EngagementZones/FirstLimitDistanceUpdated"),
	EngagementZones_LimitAngleUpdated("EngagementZones/LimitAngleUpdated"),
	EngagementZones_MovementsInZonesUpdated("EngagementZones/MovementsInZonesUpdated"),
	EngagementZones_PeopleInZonesUpdated("EngagementZones/PeopleInZonesUpdated"),
	EngagementZones_PersonApproached("EngagementZones/PersonApproached"),
	EngagementZones_PersonEnteredZone1("EngagementZones/PersonEnteredZone1"),
	EngagementZones_PersonEnteredZone2("EngagementZones/PersonEnteredZone2"),
	EngagementZones_PersonEnteredZone3("EngagementZones/PersonEnteredZone3"),
	EngagementZones_PersonMovedAway("EngagementZones/PersonMovedAway"),
	EngagementZones_SecondLimitDistanceUpdated("EngagementZones/SecondLimitDistanceUpdated"),
	FrontTactilTouched("FrontTactilTouched"),
	GazeAnalysis_PeopleLookingAtRobot("GazeAnalysis/PeopleLookingAtRobot"),
	GazeAnalysis_PersonStartsLookingAtRobot("GazeAnalysis/PersonStartsLookingAtRobot"),
	GazeAnalysis_PersonStopsLookingAtRobot("GazeAnalysis/PersonStopsLookingAtRobot"),
	HandLeftBackTouched("HandLeftBackTouched"),
	HandLeftLeftTouched("HandLeftLeftTouched"),
	HandLeftRightTouched("HandLeftRightTouched"),
	HandRightBackTouched("HandRightBackTouched"),
	HandRightLeftTouched("HandRightLeftTouched"),
	HandRightRightTouched("HandRightRightTouched"),
	HeadProcessorIsCriticallyHot("HeadProcessorIsCriticallyHot"),
	HeadProcessorIsHot("HeadProcessorIsHot"),
	HotDeviceDetected("HotDeviceDetected"),
	HotJointDetected("HotJointDetected"),
	LeftBumperPressed("LeftBumperPressed"),
	MiddleTactilTouched("MiddleTactilTouched"),
	MovementDetection_MovementDetected("MovementDetection/MovementDetected"),
	MovementDetection_NoMovement("MovementDetection/NoMovement"),
	NAOqiReady("NAOqiReady"),
	Navigation_AvoidanceNavigator_AbsTargetModified("Navigation/AvoidanceNavigator/AbsTargetModified"),
	Navigation_AvoidanceNavigator_MovingToFreeZone("Navigation/AvoidanceNavigator/MovingToFreeZone"),
	Navigation_AvoidanceNavigator_ObstacleDetected("Navigation/AvoidanceNavigator/ObstacleDetected"),
	Navigation_AvoidanceNavigator_Status("Navigation/AvoidanceNavigator/Status"),
	Navigation_AvoidanceNavigator_TargetOutOfMap("Navigation/AvoidanceNavigator/TargetOutOfMap"),
	Navigation_AvoidanceNavigator_TooManyPathRecomputation("Navigation/AvoidanceNavigator/TooManyPathRecomputation"),
	Navigation_AvoidanceNavigator_TrajectoryProgress("Navigation/AvoidanceNavigator/TrajectoryProgress"),
	Navigation_FreeZoneFinder_ConstraintZone("Navigation/FreeZoneFinder/ConstraintZone"),
	Navigation_SafeNavigator_BlockingObstacle("Navigation/SafeNavigator/BlockingObstacle"),
	Navigation_SafeNavigator_DangerousObstacleDetected("Navigation/SafeNavigator/DangerousObstacleDetected"),
	Navigation_SafeNavigator_Status("Navigation/SafeNavigator/Status"),
	PassiveDiagnosisErrorChanged("PassiveDiagnosisErrorChanged"),
	PeoplePerception_JustArrived("PeoplePerception/JustArrived"),
	PeoplePerception_JustLeft("PeoplePerception/JustLeft"),
	PeoplePerception_MaximumDetectionRangeUpdated("PeoplePerception/MaximumDetectionRangeUpdated"),
	PeoplePerception_NonVisiblePeopleList("PeoplePerception/NonVisiblePeopleList"),
	PeoplePerception_PeopleDetected("PeoplePerception/PeopleDetected"),
	PeoplePerception_PeopleList("PeoplePerception/PeopleList"),
	PeoplePerception_PopulationReset("PeoplePerception/PopulationReset"),
	PeoplePerception_PopulationUpdated("PeoplePerception/PopulationUpdated"),
	PeoplePerception_RemovedPersonFromMemory("PeoplePerception/RemovedPersonFromMemory"),
	PeoplePerception_VisiblePeopleList("PeoplePerception/VisiblePeopleList"),
	PostureChanged("PostureChanged"),
	PostureFamilyChanged("PostureFamilyChanged"),
	PosturePerturbated("PosturePerturbated"),
	RearTactilTouched("RearTactilTouched"),
	RightBumperPressed("RightBumperPressed"),
	Segmentation3D_BlobTrackerUpdated("Segmentation3D/BlobTrackerUpdated"),
	Segmentation3D_SegmentationUpdated("Segmentation3D/SegmentationUpdated"),
	Segmentation3D_TopOfTrackedBlob("Segmentation3D/TopOfTrackedBlob"),
	Segmentation3D_TrackedBlobNotFound("Segmentation3D/TrackedBlobNotFound"),
	SittingPeopleDetection_PersonSittingDown("SittingPeopleDetection/PersonSittingDown"),
	SittingPeopleDetection_PersonStandingUp("SittingPeopleDetection/PersonStandingUp"),
	SonarLateralLeftDetected("SonarLateralLeftDetected"),
	SonarLateralRightDetected("SonarLateralRightDetected"),
	SonarLeftDetected("SonarLeftDetected"),
	SonarLeftNothingDetected("SonarLeftNothingDetected"),
	SonarMiddleDetected("SonarMiddleDetected"),
	SonarNothingDetected("SonarNothingDetected"),
	SonarRightDetected("SonarRightDetected"),
	SonarRightNothingDetected("SonarRightNothingDetected"),
	TemperatureDiagnosisErrorChanged("TemperatureDiagnosisErrorChanged"),
	TemperatureStatusChanged("TemperatureStatusChanged"),
	TouchChanged("TouchChanged"),
	UserSession_FocusedUser("UserSession/FocusedUser"),
	UserSession_ShouldExitInteractiveActivity("UserSession/ShouldExitInteractiveActivity"),
	VisualCompass_Deviation("VisualCompass/Deviation"),
	VisualCompass_FinalDeviation("VisualCompass/FinalDeviation"),
	VisualCompass_InvalidReference("VisualCompass/InvalidReference"),
	VisualCompass_Match("VisualCompass/Match"),
	VisualCompass_MoveAbort("VisualCompass/MoveAbort"),
	VisualCompass_NewReferenceImageSet("VisualCompass/NewReferenceImageSet"),
	WavingDetection_PersonWaving("WavingDetection/PersonWaving"),
	WavingDetection_PersonWavingCenter("WavingDetection/PersonWavingCenter"),
	WavingDetection_PersonWavingLeft("WavingDetection/PersonWavingLeft"),
	WavingDetection_PersonWavingRight("WavingDetection/PersonWavingRight"),
	ActivityFocusManager_CompletedActivity("_ActivityFocusManager/CompletedActivity"),
	ActivityFocusManager_FocusedActivity("_ActivityFocusManager/FocusedActivity"),
	ActivityFocusManager_NextActivity("_ActivityFocusManager/NextActivity"),
	notificationAdded("notificationAdded"),
	notificationRead("notificationRead"),
	notificationRemoved("notificationRemoved"),
	packageInstalled("packageInstalled"),
	packageRemoved("packageRemoved"),
	preferenceAdded("preferenceAdded"),
	preferenceChanged("preferenceChanged"),
	preferenceDomainRemoved("preferenceDomainRemoved"),
	preferenceRemoved("preferenceRemoved"),
	preferenceSynchronized("preferenceSynchronized"),
	redBallDetected("redBallDetected"),
	robotHasFallen("robotHasFallen"),
	robotIsWakeUp("robotIsWakeUp");
	
	private String event;
	
	private MemoryEvents(String s){
		this.event = s;
	}
	
	public String getEvent(){
		return this.event;
	}
	
	public static MemoryEvents fromString(String mem){
		for(MemoryEvents e : MemoryEvents.values()){
            if(mem.toLowerCase().equals(e.name().toLowerCase())) return e;
        }
        return null;
	}


}
