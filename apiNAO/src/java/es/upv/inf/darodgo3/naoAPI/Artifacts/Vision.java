package es.upv.inf.darodgo3.naoAPI.Artifacts;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JFrame;

import com.aldebaran.qi.CallError;
import com.aldebaran.qi.helper.proxies.ALVideoDevice;
import com.aldebaran.qi.helper.proxies.ALVisualCompass;

import cartago.Artifact;
import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;
import cartago.OpFeedbackParam;
import es.upv.inf.darodgo3.naoAPI.NaoRobotProxys;
import es.upv.inf.darodgo3.naoAPI.util.NumberParser;
import es.upv.inf.darodgo3.naoAPI.util.image.Picture;

/**
 * Este artefacto porporciona un conjunto de operaciones relacionadas con la
 * vision del robot. Con dichas operaciones podremos obtener la imagen que están
 * capturando las camarás del robot, movernos teniendo como referencia una
 * imagen (con el fin de no desviarnos), etc.
 * 
 * @author Dario Rodriguez Gonzalez
 *
 */
public class Vision extends Artifact {
	private ALVideoDevice video;
	private ALVisualCompass compass;
	private String moduleName;
	private boolean isVirtual;

	private ImageFrame f = null;
	private boolean endVideoLoop = false;

	private int topCamera = 0;
	private final int resolution = 2; // 640 x 480
	private final int colorspace = 11; // RGB
	private final int frameRate = 10; // FPS

	// TODO: poder cambiar los parametros de la vision. -> para trabajos futuros

	/**
	 * Igual que init(url, false)
	 * 
	 * @param url
	 *            La url del robot
	 */
	void init(String url) {
		init(url, false);
	}

	/**
	 * Método que se ejecuta al crear el artefacto. Este método recupera de
	 * NaoRobotProxys los proxys necesarios para que el artefacto pueda
	 * funcionar, los cuales son: ALVideoDevice, ALVisualCompass. Una vez
	 * recuperados los almacena en atributos de clase, así las operaciones
	 * podrán hacer uso de los mismos sin tener que volver a recuperarlos.
	 * 
	 * @param url
	 *            La url del robot
	 * @param isVirtual
	 *            Indica si el robot es virtual o no
	 */
	void init(String url, boolean isVirtual) {

		video = NaoRobotProxys.getInstance(url, isVirtual).getVideo();
		this.compass = NaoRobotProxys.getInstance(url, isVirtual).getVisualCompass();
		this.isVirtual = isVirtual;

	}

	@SuppressWarnings("unchecked")
	@INTERNAL_OPERATION
	private void getVideo(boolean mode) {
		Object img;
		List<Object> image;
		byte[] rawData;

		do {
			try {
				img = video.getImageRemote(moduleName);
				if (img != null) {
					image = (List<Object>) img;
					ByteBuffer buffer = (ByteBuffer) image.get(6);
					rawData = buffer.array();
					if (f == null) {
						f = new ImageFrame(Picture.toPicture(rawData));
						f.setVisible(true);
					} else {
						f.reDraw(Picture.toPicture(rawData));
					}

					video.releaseImage(moduleName);

				}

				await_time(50);
			} catch (CallError | InterruptedException e) {
				e.printStackTrace();
			}

		} while (mode & !endVideoLoop);
	}

	class ImageFrame extends JFrame {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private static final int HEIGHT = 640;
		private static final int WIDTH = 480;

		private BufferedImage image;
		private PicturePanel picturePanel;

		@SuppressWarnings("serial")
		class PicturePanel extends Panel {
			private BufferedImage image;

			public PicturePanel(BufferedImage image) {
				this.image = image;
			}

			@Override
			public void paint(Graphics g) {
				super.paint(g);

				int w = getWidth();
				int h = getHeight();
				int imageWidth = image.getWidth(this);
				int imageHeight = image.getHeight(this);
				int x = (w - imageWidth) / 2;
				int y = (h - imageHeight) / 2;
				g.drawImage(image, x, y, this);
			}

			public void changeImg(BufferedImage img) {
				this.image = img;
			}
		}

		public ImageFrame(BufferedImage img) {
			super("Nao Camara");
			setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
			image = img;
			this.picturePanel = new PicturePanel(image);
			add(picturePanel);
			Dimension preferredSize = new Dimension(WIDTH, HEIGHT);
			picturePanel.setPreferredSize(preferredSize);

			setSize(HEIGHT, WIDTH + 30);
			setVisible(true);
		}

		public void reDraw(BufferedImage img) {
			picturePanel.changeImg(img);
			picturePanel.repaint();
		}
	}

	/**
	 * Muestra una ventana en la pantalla del ordenador el cual está ejecutando
	 * el programa Jason. En esta pantalla se mostrará el video o una foto
	 * tomadas mediante la cámara frontal del robot. Para tomar esta imagen, se
	 * establece una conexión con el robot. Esta es una operación asincrona.
	 * 
	 * @param mode
	 *            Si está a true muestra en la ventana el video, si está a false
	 *            muestra una foto tomada en el momento de llamar a la operación
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),show_camera(mode)]
	 */
	@OPERATION
	public void showCamera(boolean mode) {
		try {
			endVideoLoop = false;

			moduleName = video.subscribeCamera(NaoRobotProxys.suscriberName, topCamera, resolution, colorspace,
					frameRate);

			execInternalOp("getVideo", mode);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación showCamera", "show_camera", mode);
		}
	}

	/**
	 * Cierra la ventana creada por la operación showCamera y cierra la conexión
	 * con el robot la cual le proporciona la imagen. Esta es una operación
	 * sincrona.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),close_camera]
	 */
	@OPERATION
	public void closeCamera() {
		if (f != null) {
			endVideoLoop = true;

			f.setVisible(false);
			f.dispose();

			try {
				this.video.unsubscribe(NaoRobotProxys.suscriberName);
			} catch (CallError | InterruptedException e) {
				failed("No se pudo completar la operación closeCamera", "close_camera");
			}
		}
	}

	/**
	 * Se subscribe al servicio del compas visual, para así poder utilizar las
	 * operaciones que lo usan. Esta es una operación sincrona. Esta operación
	 * no está disponible para robots virtuales.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),compass_suscribe]
	 */
	@OPERATION
	public void compassSuscribe() {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			this.compass.subscribe(NaoRobotProxys.suscriberName);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassSuscribe", "compass_suscribe");
		}
	}

	/**
	 * Se desubscribe del servicio del compas visual. No se podrán usar las
	 * operaciones que lo usan hasta que se vuelva a subscribir. Esta es una
	 * operación sincrona. Esta operación no está disponible para robots
	 * virtuales.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),compass_unsuscribe]
	 */
	@OPERATION
	public void compassUnsuscribe() {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			this.compass.unsubscribe(NaoRobotProxys.suscriberName);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassUnsuscribe", "compass_unsuscribe");
		}
	}

	/**
	 * Esta operación establece si la imagen de referencia es refrescada al
	 * suscribirse al servicio del compas visiual, o si por el contrario se debe
	 * actualizar manualmente. Esta es una operación sincrona. Esta operación no
	 * está disponible para robots virtuales.
	 * 
	 * @param refresh
	 *            Verdadero para refrescar automaticamente, falso para refrescar
	 *            manualmente
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),compass_enable_reference_refresh(refresh)]
	 */
	@OPERATION
	public void compassEnableReferenceRefresh(boolean refresh) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			this.compass.enableReferenceRefresh(refresh);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassEnableReferenceRefresh",
					"compass_enable_reference_refresh", refresh);
		}
	}

	/**
	 * Mueve al robot una distancia a lo largo del eje x, utilizando para ello
	 * el servicio del compas visual. Esta es una operación asincrona. Esta
	 * operación no está disponible para robots virtuales.
	 * 
	 * @param x
	 *            Distancia a la que se va a mover el robot en metros.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),compass_enable_reference_refresh(refresh)]
	 */
	@OPERATION
	public void compassMoveStraightTo(Object x) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			this.compass.moveStraightTo(NumberParser.cartagoNumberToFloat(x));
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassMoveStraightTo", "compass_move_straight_to", x);
		}
	}

	/**
	 * Mueve al robot a la posición que se le proporciona. Esta es una operación
	 * asincrona. Esta operación no está disponible para robots virtuales.
	 * 
	 * @param x
	 *            Posición del eje X en metros
	 * @param y
	 *            Posición del eje Y en metros
	 * @param theta
	 *            Rotación sobre el eje Z en grados. Este valor debe estar entre
	 *            el rango: [-180, 180]
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),compass_move_to(x,y,theta)]
	 */
	@OPERATION
	public void compassMoveTo(Object x, Object y, Object theta) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			this.compass.moveTo(NumberParser.cartagoNumberToFloat(x), NumberParser.cartagoNumberToFloat(y),
					Double.valueOf((Math.toRadians(NumberParser.cartagoNumberToFloat(theta)))).floatValue());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassMoveTo", "compass_move_to", x, y, theta);
		}
	}

	/**
	 * Estalece la imagen actual como referencia para el compas visual. Esta es
	 * una operación sincrona. Esta operación no está disponible para robots
	 * virtuales.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),compass_set_current_image_as_reference]
	 */
	@OPERATION
	public void compassSetCurrentImageAsReference() {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			this.compass.setCurrentImageAsReference();
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassSetCurrentImageAsReference",
					"compass_set_current_image_as_reference");
		}
	}

	/**
	 * Obtiene el identificador de la cámara activa, la cual está siendo
	 * utilizada por el servicio del compas visual. Esta operación no está
	 * disponible para robots virtuales.
	 * 
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),compass_get_active_camera]
	 */
	@OPERATION
	public void compassGetActiveCamera(OpFeedbackParam<Integer> res) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			res.set(this.compass.getActiveCamera());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassGetActiveCamera", "compass_get_active_camera");
		}
	}

	/**
	 * Establece la cámara que será usada por el servicio del compas visual.
	 * Esta es una operación sincrona. Esta operación no está disponible para
	 * robots virtuales.
	 * 
	 * @param cameraId
	 *            Identificador de la camara
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),compass_set_active_camera(cameraId)]
	 */
	@OPERATION
	public void compassSetActiveCamera(Object cameraId) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			this.compass.setActiveCamera(NumberParser.cartagoNumberToDouble(cameraId).intValue());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassGetActiveCamera", "compass_set_active_camera", cameraId);
		}
	}

	/**
	 * Obtiene la resolución actual de la cámara que se está utilizando para el
	 * servicio de compas visual, y la devuelve por el parametro de salida. Esta
	 * es una operación sincrona. Esta operación no está disponible para robots
	 * virtuales.
	 * 
	 * @param res
	 *            Parametro de salida
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),compass_get_resolution]
	 */
	@OPERATION
	public void compassGetResolution(OpFeedbackParam<Integer> res) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			res.set(this.compass.getResolution());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassGetResolution", "compass_get_resolution");
		}
	}

	/**
	 * Establece la resolución actual de la cámara que se está utilizando para
	 * el servicio de compas visual. Esta es una operación sincrona. Esta
	 * operación no está disponible para robots virtuales.
	 * 
	 * @param resolution
	 *            Nueva resolución
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),compass_set_resolution(resolution)]
	 */
	@OPERATION
	public void compassSetResolution(Object resolution) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			this.compass.setResolution(NumberParser.cartagoNumberToDouble(resolution).intValue());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassSetResolution", "compass_set_resolution", resolution);
		}
	}

	/**
	 * Obtiene el periodo actual del servicio compas visual, y la duvuelve por
	 * el parametro de salida. Esta es una operación sincrona. Esta operación no
	 * está disponible para robots virtuales.
	 * 
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),compass_get_current_period]
	 */
	@OPERATION
	public void compassGetCurrentPeriod(OpFeedbackParam<Integer> res) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			res.set(this.compass.getCurrentPeriod());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassGetCurrentPeriod", "compass_get_current_period");
		}
	}

	/**
	 * Obtiene la precisión actual del servicio compas visual, y la duvuelve por
	 * el parametro de salida. Esta es una operación sincrona. Esta operación no
	 * está disponible para robots virtuales.
	 * 
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),compass_get_current_precision]
	 */
	@OPERATION
	public void compassGetCurrentPrecision(OpFeedbackParam<Float> res) {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			res.set(this.compass.getCurrentPrecision());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassGetCurrentPrecision", "compass_get_current_precision");
		}
	}

	/**
	 * Detiene la ejecución del programa hasta que el robot alcance la posición
	 * a la cual está caminando. Esta operación permite utilizar las operaciones
	 * compassMoveTo y compassMoveStraightTo de forma sincrona. Esta operación
	 * no está disponible para robots virtuales.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),compass_wait_until_target_reached]
	 */
	@OPERATION
	public void compassWaitUntilTargetReached() {
		if (this.isVirtual) {
			failed("Esta operación no está disponible para robots virtuales");
			return;
		}

		try {
			this.compass.waitUntilTargetReached();
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación compassWaitUntilTargetReached",
					"compass_wait_until_target_reached");
		}
	}

	/**
	 * Esta operación termina todas las conexión y subscripciones a eventos
	 * establecidas anteriormente, con el fin de poder terminar el programa
	 * correctamente. Por último, deshace el artefacto.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg)]
	 */
	@OPERATION
	public void endVision() {
		try {
			endVideoLoop = true;
			if (compass.isProcessing()) {
				compass.unsubscribe(NaoRobotProxys.suscriberName);
			}
			if (f != null) {
				video.unsubscribe(NaoRobotProxys.suscriberName);
				f.setVisible(false);
				f.dispose();
			}
			dispose();
		} catch (CallError | InterruptedException e) {
			failed("Error al terminar el artefacto Vision");
		}
	}
}