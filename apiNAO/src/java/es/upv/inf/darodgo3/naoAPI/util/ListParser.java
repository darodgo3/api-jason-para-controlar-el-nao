package es.upv.inf.darodgo3.naoAPI.util;

import java.util.ArrayList;
import java.util.List;

public class ListParser {
	public static List<String> cartagoStringListToList(Object[] list){
		List<String> result = new ArrayList<>();
		
		for(int i=0; i < list.length; i++){
			result.add((String) list[i]);
		}
		
		return result;
	}
	
	public static List<Double> cartagoNumberListToDoubleList(Object[] list){
		List<Double> result = new ArrayList<>();
		
		for(int i=0; i < list.length; i++){
			result.add(NumberParser.cartagoNumberToDouble(list[i]));
		}
		
		return result;
	}
	
	public static List<Float> cartagoNumberListToFloatList(Object[] list){
		List<Float> result = new ArrayList<>();
		
		for(int i=0; i < list.length; i++){
			result.add(NumberParser.cartagoNumberToFloat(list[i]));
		}
		
		return result;
	}
	
	public static String javaStringListToCartagoString(List<String> list) {
		String result = "[";
		
		for(String s: list) {
			result += s + ", ";
		}
		
		return result.substring(0, result.length() - 2) + "]";
	}
}
