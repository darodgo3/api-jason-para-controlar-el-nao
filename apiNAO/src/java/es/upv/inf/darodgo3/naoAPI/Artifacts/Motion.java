// CArtAgO artifact code for project tfg

package es.upv.inf.darodgo3.naoAPI.Artifacts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.aldebaran.qi.CallError;
import com.aldebaran.qi.helper.proxies.ALMotion;
import com.aldebaran.qi.helper.proxies.ALNavigation;
import com.aldebaran.qi.helper.proxies.ALRobotPosture;

import cartago.Artifact;
import cartago.OPERATION;
import cartago.ObsProperty;
import cartago.OpFeedbackParam;
import es.upv.inf.darodgo3.naoAPI.NaoRobotProxys;
import es.upv.inf.darodgo3.naoAPI.enums.Quantity;
import es.upv.inf.darodgo3.naoAPI.enums.Velocity;
import es.upv.inf.darodgo3.naoAPI.util.ListParser;
import es.upv.inf.darodgo3.naoAPI.util.NumberParser;

/**
 * Este artefacto porporciona un conjunto de operaciones relacionadas con el
 * movimiento del robot.
 * 
 * @author Dario Rodriguez Gonzalez
 *
 */
public class Motion extends Artifact {
	private ALMotion motion;
	private ALNavigation nav;
	private ALRobotPosture posture;
	private String url;

	private ObsProperty awaked;

	/**
	 * Igual que init(url, false)
	 * 
	 * @param url
	 *            La url del robot
	 */
	void init(String url) {
		init(url, false);
	}

	/**
	 * Método que se ejecuta al crear el artefacto. Este método recupera de
	 * NaoRobotProxys los proxys necesarios para que el artefacto pueda
	 * funcionar, los cuales son: ALMotion, ALNavigation y ALRobotPosture. Una
	 * vez recuperados los almacena en atributos de clase, así las operaciones
	 * podrán hacer uso de los mismos sin tener que volver a recuperarlos.
	 * Además, esta operación hará todo lo necesario para que el robot se
	 * levante automaticamente después de cada caida.
	 * 
	 * Por último, crea la siguiente propiedad observable, la cual se transforma
	 * automaticamente en una creencia en el agente Jason.
	 * 
	 * awaked: Indica si el robot esta despierto o si está dormido.
	 * 
	 * @param url
	 *            La url del robot
	 * @param isVirtual
	 *            Indica si el robot es virtual o no
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),motion_init(url,isVirtual)]
	 */
	void init(String url, boolean isVirtual) {
		motion = NaoRobotProxys.getInstance(url, isVirtual).getMove();
		nav = NaoRobotProxys.getInstance(url, isVirtual).getNavigation();
		posture = NaoRobotProxys.getInstance(url, isVirtual).getPosture();

		this.url = url;

		try {
			defineObsProperty("awaked",
					NaoRobotProxys.getInstance(url, isVirtual).getMemory().getData("robotIsWakeUp"));
			this.awaked = getObsProperty("awaked");

			NaoRobotProxys.getInstance(url, isVirtual).getMemory().subscribeToEvent("robotHasFallen", fallen -> {
				if ((boolean) fallen) {
					this.posture.goToPosture("StandInit", (float) 0.5);
				}
			});
		} catch (Exception e) {
			failed("No se pudo crear el artefacto Motion", "motion_init", url, isVirtual);
		}
	}

	/**
	 * El robot descansará: establece a una posición de relajación y seguridad y
	 * además apagará todos los motores. Esta es una operación sincrona.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),go_rest]
	 */
	@OPERATION
	public void goRest() {
		try {
			motion.rest();
			this.awaked.updateValue(false);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación goRest", "go_rest");
		}

	}

	/**
	 * El robot se despertará: enciente todos los motores y establece una
	 * posición inicial si es necesario. Esta es una operación sincrona.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),wake_up]
	 */
	@OPERATION
	public void wakeUp() {
		try {
			motion.wakeUp();
			this.awaked.updateValue(true);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación wakeUp", "wake_up");
		}
	}

	/**
	 * Hace que el robot establezca una postura predeterminada, en una fracción
	 * de tiempo dada. Esta es una operación sincrona.
	 * 
	 * @param posture
	 *            Nombre de la postura. Puede ser: Crouch, LyingBack,
	 *            LyingBelly, Sit, SitRelax, StandInit, Stand, StandZero
	 * @param maxSpeedFraction
	 *            Fracción de tiempo, la cual puede ser representada como un
	 *            valor numero el cual va de 0 a 1, o como un elemento del
	 *            enumerado Velocity.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),posture(posture,maxSpeedFraction)]
	 */
	@OPERATION
	public void posture(String posture, Object maxSpeedFraction) {
		try {
			if (maxSpeedFraction instanceof String) {
				this.posture.goToPosture(posture, Velocity.fromString((String) maxSpeedFraction).getVelocity());
			} else {
				this.posture.goToPosture(posture, NumberParser.cartagoNumberToFloat(maxSpeedFraction));
			}
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación posture", "posture", posture, maxSpeedFraction);
		}
	}

	/**
	 * Abre una mano del NAO. Esta es una operación sincrona.
	 * 
	 * @param handName
	 *            Nombre de la mano, puede ser RHand o LHand
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),open_hand_NAO(handName)]
	 */
	@OPERATION
	public void openHandNAO(String handName) {
		try {
			motion.openHand(handName);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación openHandNAO", "open_hand_NAO", handName);
		}
	}

	/**
	 * Cierra una mano del NAO. Esta es una operación sincrona.
	 * 
	 * @param handName
	 *            Nombre de la mano, puede ser RHand o LHand
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),close_hand_NAO(handName)]
	 */
	@OPERATION
	public void closeHandNAO(String handName) {
		try {
			motion.closeHand(handName);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación closeHandNAO", "close_hand_NAO", handName);
		}
	}

	/**
	 * Mueve las dos manos del NAO a una posición a la vez a una velocidad
	 * regulable. Esta es una operación sincrona.
	 * 
	 * @param amplitude
	 *            Amplitud a la que se quieren colocar las manos. Puede ser un
	 *            float o un String. Si es un string se debe proporcionar un
	 *            valor del enum Quantity (todo, mucho, medio...)
	 * @param vel
	 *            Velocidad con la que se colocará la nueva aplitud. Puede ser
	 *            un float o un String. Si es un string debe proporcionar un
	 *            valor del enum Velocity (muyrapido, rapido...).
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),move_both_hand_NAO(amplitude,vel)]
	 */
	@OPERATION
	public void moveBothHandNAO(Object amplitude, Object vel) {
		List<String> hands = new ArrayList<>();
		hands.add("LHand");
		hands.add("RHand");

		List<Float> angles = new ArrayList<>();
		if (amplitude instanceof String) {
			angles.add(Float.valueOf(Quantity.fromString((String) amplitude).getQuantity()));
			angles.add(Float.valueOf(Quantity.fromString((String) amplitude).getQuantity()));
		} else {
			Float a = NumberParser.cartagoNumberToFloat(amplitude);
			if (a != null) {
				angles.add(a);
				angles.add(a);
			}
		}

		try {
			if (vel instanceof String) {
				motion.setAngles(hands, angles, Velocity.fromString((String) vel).getVelocity());
			} else {
				Float v = NumberParser.cartagoNumberToFloat(vel);
				if (v != null)
					motion.setAngles(hands, angles, v);
			}

		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación moveBothHandNAO", "move_both_hand_NAO", amplitude, vel);
		}

		// bloquear hasta llegar a la posicion
		while (Motion.measureDistance(motion, hands, (List<Float>) angles) > 0.05) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// continuar normalmente
			}
		}
	}

	/**
	 * Interpola una o varias articulaciones a un ángulo de objetivo a lo largo
	 * de trayectorias temporizadas. Esta es una operación sincrona.
	 * 
	 * @param names
	 *            Nombres de las articulaciones
	 * @param keys
	 *            Angulos de las articulaciones (radianes)
	 * @param times
	 *            Tiempos en los cuales las articulaciones tienen que alcanzar
	 *            la nueva posicion
	 * @param isAbsolute
	 *            true si los angulos son absolutos, false si son relativos.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),angle_interpolation_NAO(names,keys,time,isAbsolute)]
	 */
	@OPERATION
	public void angleInterpolationNAO(Object[] names, Object[] keys, Object[] times, boolean isAbsolute) {
		List<Object> namesList = new ArrayList<>(Arrays.asList(names));

		List<List<Double>> angleLists = new ArrayList<>();
		List<List<Double>> timeLists = new ArrayList<>();

		try {
			for (int i = 0; i < names.length; i++) {
				angleLists.add(ListParser.cartagoNumberListToDoubleList((Object[]) keys[i]));
				timeLists.add(ListParser.cartagoNumberListToDoubleList((Object[]) times[i]));
			}

			motion.angleInterpolation(namesList, angleLists, timeLists, isAbsolute);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación angleInterpolationNAO", "angle_interpolation_NAO", names, keys,
					times, isAbsolute);
		}
	}

	/**
	 * Obtiene los ángulos (en radianes) de las articulaciones y se devuelve por
	 * el parametro de salida. Esta es una operación sincrona.
	 * 
	 * @param names
	 *            Nombres de las articulaciones
	 * @param useSensors
	 *            Si es true, se devolverán los angulos usando los sensores
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),get_angles_NAO(names,useSensors)]
	 */
	@OPERATION
	public void getAnglesNAO(Object[] names, Boolean useSensors, OpFeedbackParam<Object[]> res) {
		List<String> namesList = new ArrayList<>();
		List<Float> angles;
		Float[] anglesRes;

		for (int i = 0; i < names.length; i++) {
			namesList.add((String) names[i]);
		}

		try {
			angles = motion.getAngles(namesList, useSensors);

			anglesRes = angles.toArray(new Float[angles.size()]);
			res.set(anglesRes);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación getAnglesNAO", "get_angles_NAO", names, useSensors);
		}
	}

	/**
	 * Establece los angulos (en radianes) de las articulaciones proporcionadas
	 * a la velocidad indicada. La velocidad se puede indicar como un número
	 * entre 0 y 1, o como un valor del enumerado Velocity. Esta es una
	 * operación asincrona.
	 * 
	 * @param joints
	 *            Nombres de las articulaciones.
	 * @param angles
	 *            Angulo de destino de las articulaciones
	 * @param vel
	 *            Velocidad a la cual se moverán las articulaciones.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),set_angles_NAO(names,angles,vel)]
	 */
	@OPERATION
	public void setAnglesNAO(Object[] joints, Object[] angles, Object vel) {
		List<String> names = ListParser.cartagoStringListToList(joints);
		List<Float> langles;
		Float fractionMaxSpeed;

		try {
			if (angles[0] instanceof String) {
				langles = new ArrayList<>();
				for (int i = 0; i < angles.length; i++) {
					langles.add(Quantity.fromString((String) angles[i]).getQuantity());
				}
			} else {
				langles = ListParser.cartagoNumberListToFloatList(angles);
			}

			if (vel instanceof String) {
				fractionMaxSpeed = Velocity.fromString((String) vel).getVelocity();
			} else {
				fractionMaxSpeed = NumberParser.cartagoNumberToFloat(vel);
			}

			motion.setAngles(names, langles, fractionMaxSpeed);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación setAnglesNAO", "set_angles_NAO", joints, angles, vel);
		}
	}

	/**
	 * Igual que setAnglesNAO, pero en vez de ser una operación asincrona es una
	 * operación sincrona.
	 * 
	 * @param joints
	 *            Nombres de las articulaciones.
	 * @param angles
	 *            Angulo de destino de las articulaciones
	 * @param vel
	 *            Velocidad a la cual se moverán las articulaciones.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),set_angles_NAO_blocking(names,angles,vel)]
	 */
	@OPERATION
	public void setAnglesNAOBlocking(Object[] joints, Object[] angles, Object vel) {
		List<String> names = ListParser.cartagoStringListToList(joints);
		List<Float> langles = new ArrayList<>();
		Float fractionMaxSpeed;

		try {
			if (angles[0] instanceof String) {
				langles = new ArrayList<>();
				for (int i = 0; i < angles.length; i++) {
					langles.add(Quantity.fromString((String) angles[i]).getQuantity());
				}
			} else {
				langles = ListParser.cartagoNumberListToFloatList(angles);
			}

			if (vel instanceof String) {
				fractionMaxSpeed = Velocity.fromString((String) vel).getVelocity();
			} else {
				fractionMaxSpeed = NumberParser.cartagoNumberToFloat(vel);
			}

			motion.setAngles(names, langles, fractionMaxSpeed);

		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación setAnglesNAOBlocking", "set_angles_NAO_blocking", joints, angles,
					vel);
		}

		// bloquear hasta llegar a la posicion
		while (Motion.measureDistance(motion, names, langles) > 0.05) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// continuar normalmente
			}
		}
	}

	/**
	 * Devuelve el estado del gestor de caídas por el parametro de salida. True
	 * si el gestor de caídas está activado. Esta es una operación sincrona.
	 * 
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),get_fall_manager_enabled]
	 */
	@OPERATION
	public void getFallManagerEnabled(OpFeedbackParam<Object> res) {
		try {
			res.set(motion.getFallManagerEnabled());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación getFallManagerEnabled", "get_fall_manager_enabled");
		}
	}

	/**
	 * Activar/desactivar la protección del gestor de caídas para el robot.
	 * Cuando se detecta una caída, el robot adopta una configuración conjunta
	 * para protegerse y reducir la rigidez. Un evento de memoria llamado
	 * "robotHasFallen" se genera cuando el fallManager se ha activado. Para
	 * obtener este evento desde Jason ver el artefacto Sensors. Esta es una
	 * operación sincrona.
	 * 
	 * @param newValue
	 *            true para activar el gestor de caídas.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),set_fall_manager_enabled(newValue)]
	 */
	@OPERATION
	public void setFallManagerEnabled(boolean newValue) {
		try {
			motion.setFallManagerEnabled(newValue);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación setFallManagerEnabled", "set_fall_manager_enabled", newValue);
		}
	}

	/**
	 * Inicializa el proceso de movimiento. Comprueba la pose del robot y toma
	 * la postura correcta. No es necesario llamarla antes de caminar, ya que
	 * esta operación se utiliza internamente. Esta es una operación sincrona.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),move_init]
	 */
	public void moveInit() {
		try {
			motion.moveInit();
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación moveInit", "move_init");
		}
	}

	/**
	 * Comprueba si el robot esta levantado o no y duvuelve esa comprobación por
	 * el parametro de salida. Esta es una operación bloqueante.
	 * 
	 * @param res
	 *            Parametro de salida.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),robot_is_wake_up]
	 */
	@OPERATION
	public void robotIsWakeUp(OpFeedbackParam<Object> res) {
		try {
			res.set(motion.robotIsWakeUp());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación robotIsWakeUp", "robot_is_wake_up");
		}
	}

	/**
	 * Hace que el robot se mueva a la velocidad normalizada dada. Esta es una
	 * operación asincrona.
	 * 
	 * @param x
	 *            La velocidad normalizada a lo largo del eje x (entre -1 y 1).
	 * @param y
	 *            La velocidad normalizada a lo largo del eje y (entre -1 y 1).
	 * @param theta
	 *            La velocidad normalizada a lo largo del eje z (entre -1 y 1).
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),move_NAO(x,y,theta)]
	 */
	@OPERATION
	public void moveNAO(Object x, Object y, Object theta) {
		try {
			motion.moveToward(NumberParser.cartagoNumberToFloat(x), NumberParser.cartagoNumberToFloat(y),
					NumberParser.cartagoNumberToFloat(theta));
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación moveNAO", "move_NAO", x, y, theta);
		}
	}

	/**
	 * Hace que el robot se mueva en la posición dada. Esta es una operación
	 * asincrona.
	 * 
	 * @param x
	 *            La posición a lo largo del eje x [m].
	 * @param y
	 *            La posición a lo largo del eje y [m].
	 * @param theta
	 *            La posición a lo largo del eje z [grados].
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),move_to_NAO(x,y,theta)]
	 */
	@OPERATION
	public void moveToNAO(Object x, Object y, Object theta) {
		try {
			motion.moveTo(NumberParser.cartagoNumberToFloat(x), NumberParser.cartagoNumberToFloat(y),
					Double.valueOf((Math.toRadians(NumberParser.cartagoNumberToFloat(theta)))).floatValue());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación moveToNAO", "move_to_NAO", x, y, theta);
		}
	}

	/**
	 * Establece si los movimientos de brazos están habilitados durante el
	 * proceso de movimiento. Es una operación sincrona.
	 * 
	 * @param leftArmEnabled
	 *            Si true - Los movimientos del brazo izquierdo son controlados
	 *            por la tarea de movimiento
	 * @param rightArmEnabled
	 *            Si true - Los movimientos del brazo derecho son controlados
	 *            por la tarea de movimiento
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),set_move_arms_enabled(leftArmEnabled,rigthArmEnabled)]
	 */
	@OPERATION
	public void setMoveArmsEnabled(boolean leftArmEnabled, boolean rightArmEnabled) {
		try {
			this.motion.setMoveArmsEnabled(leftArmEnabled, rightArmEnabled);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación setMoveArmsEnabled", "set_move_arms_enabled", leftArmEnabled,
					rightArmEnabled);
		}
	}

	/**
	 * Espera hasta que finalice el proceso de movimiento: Esta operación se
	 * puede utilizar para bloquear la ejecución del programa hasta que la tarea
	 * de movimiento esté totalmente terminada, o lo que es lo mismo, si se usa
	 * despues de una operación de movimiento hace que esta sea sincrona.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),wait_until_move_is_finnished]
	 */
	@OPERATION
	public void waitUntilMoveIsFinished() {
		try {
			motion.waitUntilMoveIsFinished();
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación waitUntilMoveIsFinished", "wait_until_move_is_finnished");
		}
	}

	/**
	 * Comprueba si el proceso de movimiento es activo y duvuelve el resultado
	 * por el parametro de salida. Esta es una operación sincrona.
	 * 
	 * @param res
	 *            Parametro de salida
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),move_is_active]
	 */
	@OPERATION
	public void moveIsActive(OpFeedbackParam<Boolean> res) {
		try {
			res.set(this.motion.moveIsActive());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación moveIsActive", "move_is_active");
		}
	}

	/**
	 * Detiene el proceso de movimiento del robot que se está ejecutando. Esta
	 * es una operación sincrona.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),stop_move_NAO]
	 */
	@OPERATION
	public void stopMoveNAO() {
		try {
			motion.stopMove();
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación stopMoveNAO", "stop_move_NAO");
		}
	}

	/**
	 * Obtiene la distancia en metros desde la cual el robot debe detenerse si
	 * hay un obstáculo, y la devuelve por el parametro de salida. Esta es una
	 * operación sincrona.
	 * 
	 * @param res
	 *            Parametro de salida.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),navigation_get_security_distance]
	 */
	@OPERATION
	public void navigationGetSecurityDistance(OpFeedbackParam<Float> res) {
		try {
			res.set(this.nav.getSecurityDistance());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación navigationGetSecurityDistance",
					"navigation_get_security_distance");
		}
	}

	/**
	 * Establece la distancia en metros desde la cual el robot debe detenerse si
	 * hay un obstáculo. Esta es una operación sincrona.
	 * 
	 * @param dist
	 *            Distancia en metros.
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),navigation_set_security_distance(dist)]
	 */
	@OPERATION
	public void navigationSetSecurityDistance(Object dist) {
		try {
			this.nav.setSecurityDistance(NumberParser.cartagoNumberToFloat(dist));
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación navigationSetSecurityDistance",
					"navigation_set_security_distance", dist);
		}
	}

	/**
	 * Hace que el robot se mueva a la velocidad dada en unidades S.I.
	 * esquivando los obstaculos que se va encontrando. Esta es una operación
	 * sincrona.
	 * 
	 * @param x
	 *            La velocidad a lo largo del eje x [m/s]
	 * @param y
	 *            La velocidad a lo largo del eje y [m/s]
	 * @param theta
	 *            La velocidad a lo largo del eje x [rad/s]
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),navigation_move(x,y,theta)]
	 */
	@OPERATION
	public void navigationMove(Object x, Object y, Object theta) {
		try {
			this.nav.move(NumberParser.cartagoNumberToFloat(x), NumberParser.cartagoNumberToFloat(y),
					NumberParser.cartagoNumberToFloat(theta));
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación navigationMove", "navigation_move", x, y, theta);
		}
	}

	/**
	 * Hace que el robot se mueva a la posición proporcionada, esquivando los
	 * obstaculos que se va encontrando. Esta es una operación sincrona.
	 * 
	 * @param x
	 *            La posición a lo largo del eje x [m]
	 * @param y
	 *            La posición a lo largo del eje y [m]
	 * @param theta
	 *            El ángulo alrededor del eje z [grados]
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),navigation_move_to(x,y,theta)]
	 */
	@OPERATION
	public void navigationMoveTo(Object x, Object y, Object theta) {
		try {
			this.nav.moveTo(NumberParser.cartagoNumberToFloat(x), NumberParser.cartagoNumberToFloat(y),
					Double.valueOf((Math.toRadians(NumberParser.cartagoNumberToFloat(theta)))).floatValue());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación navigationMoveTo", "navigation_move_to", x, y, theta);
		}
	}

	/**
	 * Hace que el robot se mueva a la velocidad dada en la fracción de
	 * velocidad normalizada, esquivando los obstaculos que se va encontrando.
	 * Esta es una operación sincrona.
	 * 
	 * @param x
	 *            La velocidad a lo largo del eje x [0.0-1.0]
	 * @param y
	 *            La velocidad a lo largo del eje y [0.0-1.0]
	 * @param theta
	 *            La velocidad a lo largo del eje x [0.0-1.0]
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),navigation_move_toward(x,y,theta)]
	 */
	@OPERATION
	public void navigationMoveToward(Object x, Object y, Object theta) {
		try {
			this.nav.moveToward(NumberParser.cartagoNumberToFloat(x), NumberParser.cartagoNumberToFloat(y),
					NumberParser.cartagoNumberToFloat(theta));
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación navigationMoveToward", "navigation_move_toward", x, y, theta);
		}
	}

	/**
	 * Hace que el robot navegue a un objetivo métrico relativo expresado en 2D.
	 * El robot calcula un camino para evitar obstáculos. Es una operación
	 * asincrona.
	 * 
	 * @param x
	 *            La posición a lo largo del eje x [m].
	 * @param y
	 *            La posición a lo largo del eje y [m].
	 * @param theta
	 *            Orientación del robot (grados)
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),navigate_to(x,y,theta)]
	 */
	@OPERATION
	public void navigateTo(Object x, Object y, Object theta) {
		try {
			this.nav.navigateTo(NumberParser.cartagoNumberToFloat(x), NumberParser.cartagoNumberToFloat(y),
					Double.valueOf((Math.toRadians(NumberParser.cartagoNumberToFloat(theta)))).floatValue());
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación navigateTo", "navigate_to", x, y, theta);
		}
	}

	/**
	 * Detiene la operación navigateTo de forma segura.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),stop_navigate_to]
	 */
	@OPERATION
	public void stopNavigateTo() {
		try {
			this.nav.stopNavigateTo();
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación stopNavigateTo", "stop_navigate_to");
		}
	}

	/**
	 * Esta operación termina todas las conexión y suscripciones a eventos
	 * establecidas anteriormente, con el fin de poder terminar el programa
	 * correctamente. Por último, deshace el artefacto.
	 */
	@OPERATION
	public void endMotion() {
		dispose();
	}

	/////////////////////////// ATTACKS
	/////////////////////////// ///////////////////////////////////////////////////////

	/**
	 * Nao simula realizar un ataque. Esta es una operación sincrona.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),attack_NAO]
	 */
	@OPERATION
	public void attackNAO() {
		Float[] RElbowRollTimes = { 0.52f, 1.04f, 1.52f, 2.04f, 2.52f, 3.04f };
		Float[] RElbowRollKeys = { 0.219404f, 0.220938f, 0.219404f, 0.219404f, 0.185656f, 0.0828779f };

		Float[] RElbowYawTimes = { 0.52f, 1.04f, 1.52f, 2.04f, 2.52f, 3.04f };
		Float[] RElbowYawKeys = { 1.05688f, 1.05688f, 1.05688f, 0.992456f, 0.926494f, 1.01393f };

		Float[] RHandTimes = { 0.52f, 1.04f, 1.52f, 2.04f, 2.52f, 3.04f };
		Float[] RHandKeys = { 0.3116f, 0.3116f, 0.3116f, 0.312f, 0.3116f, 0.3116f };

		Float[] RShoulderPitchTimes = { 0.52f, 1.04f, 1.52f, 2.04f, 2.52f, 3.04f };
		Float[] RShoulderPitchKeys = { 1.42359f, 1.38218f, 0.54768f, -0.900416f, -0.977116f, 1.00941f };

		Float[] RShoulderRollTimes = { 0.52f, 1.04f, 1.52f, 2.04f, 2.52f, 3.04f };
		Float[] RShoulderRollKeys = { -0.122762f, -0.136568f, -0.587564f, -0.763974f, -0.713352f, 0.312894f };

		Float[] RWristYawTimes = { 0.52f, 1.04f, 1.52f, 2.04f, 2.52f, 3.04f };
		Float[] RWristYawKeys = { 0.248466f, 0.248466f, 0.248466f, 0.240796f, 0.239262f, 0.45709f };

		String[] namesArray = { "RElbowRoll", "RElbowYaw", "RHand", "RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación attackNAO", "attack_NAO");
		}
	}

	///////////////////// Movimientos proporcionados por la universidad de Notre
	///////////////////// Dame

	/**
	 * Nao dice "buen trabajo" mientras aplaude. Esta es una operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),movement_clapping_two]
	 */
	@OPERATION
	public void movementClappingTwo() {
		Float[] HeadPitchTimes = { 0.8f, 4.86667f };
		Float[] HeadPitchKeys = { -0.00771196f, -0.00771196f };

		Float[] HeadYawTimes = { 0.8f, 4.86667f };
		Float[] HeadYawKeys = { -0.00157596f, -0.00157596f };

		Float[] LAnklePitchTimes = { 0.8f, 4.86667f };
		Float[] LAnklePitchKeys = { -0.0922134f, -0.0922134f };

		Float[] LAnkleRollTimes = { 0.8f, 4.86667f };
		Float[] LAnkleRollKeys = { 0.0046224f, 0.0046224f };

		Float[] LElbowRollTimes = { 0.8f, 1.13333f, 1.4f, 1.66667f, 1.93333f, 2.2f, 2.46667f, 2.73333f, 3f, 3.26667f,
				3.53333f, 3.8f, 4.06667f, 4.86667f };
		Float[] LElbowRollKeys = { -0.312894f, -1.20253f, -1.20253f, -1.20253f, -1.20253f, -1.20253f, -1.20253f,
				-1.20253f, -1.20253f, -1.20253f, -1.20253f, -1.20253f, -1.20253f, -0.312894f };

		Float[] LElbowYawTimes = { 0.8f, 1.13333f, 1.26667f, 1.4f, 1.53333f, 1.66667f, 1.8f, 1.93333f, 2.06667f, 2.2f,
				2.33333f, 2.46667f, 2.6f, 2.73333f, 2.86667f, 3f, 3.13333f, 3.26667f, 3.4f, 3.53333f, 3.66667f, 3.8f,
				3.93333f, 4.06667f, 4.2f, 4.86667f };
		Float[] LElbowYawKeys = { -0.770111f, -0.541052f, -0.907571f, -0.541052f, -0.907571f, -0.541052f, -0.907571f,
				-0.541052f, -0.907571f, -0.541052f, -0.907571f, -0.541052f, -0.907571f, -0.541052f, -0.907571f,
				-0.541052f, -0.907571f, -0.541052f, -0.907571f, -0.541052f, -0.907571f, -0.541052f, -0.907571f,
				-0.541052f, -0.907571f, -0.770111f };

		Float[] LHandTimes = { 0.8f, 1.13333f, 1.4f, 1.66667f, 1.93333f, 2.2f, 2.46667f, 2.73333f, 3f, 3.26667f,
				3.53333f, 3.8f, 4.06667f, 4.86667f };
		Float[] LHandKeys = { 0.916751f, 0.945455f, 0.945455f, 0.945455f, 0.945455f, 0.945455f, 0.945455f, 0.945455f,
				0.945455f, 0.945455f, 0.945455f, 0.945455f, 0.945455f, 0.916751f };

		Float[] LHipPitchTimes = { 0.8f, 4.86667f };
		Float[] LHipPitchKeys = { 0.0442645f, 0.0442645f };

		Float[] LHipRollTimes = { 0.8f, 4.86667f };
		Float[] LHipRollKeys = { -0.0155321f, -0.0155321f };

		Float[] LHipYawPitchTimes = { 0.8f, 4.86667f };
		Float[] LHipYawPitchKeys = { 0.00916048f, 0.00916048f };

		Float[] LKneePitchTimes = { 0.8f, 4.86667f };
		Float[] LKneePitchKeys = { 0.0564382f, 0.0564382f };

		Float[] LShoulderPitchTimes = { 0.8f, 1.13333f, 1.4f, 1.66667f, 1.93333f, 2.2f, 2.46667f, 2.73333f, 3f,
				3.26667f, 3.53333f, 3.8f, 4.06667f, 4.86667f };
		Float[] LShoulderPitchKeys = { 1.57231f, 0.994838f, 0.994838f, 0.994838f, 0.994838f, 0.994838f, 0.994838f,
				0.994838f, 0.994838f, 0.994838f, 0.994838f, 0.994838f, 0.994838f, 1.57231f };

		Float[] LShoulderRollTimes = { 0.8f, 1.13333f, 1.4f, 1.66667f, 1.93333f, 2.2f, 2.46667f, 2.73333f, 3f, 3.26667f,
				3.53333f, 3.8f, 4.06667f, 4.86667f };
		Float[] LShoulderRollKeys = { 0.31903f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0.31903f };

		Float[] LWristYawTimes = { 0.8f, 1.13333f, 1.4f, 1.66667f, 1.93333f, 2.2f, 2.46667f, 2.73333f, 3f, 3.26667f,
				3.53333f, 3.8f, 4.06667f, 4.86667f };
		Float[] LWristYawKeys = { -1.00941f, 0.254818f, 0.254818f, 0.254818f, 0.254818f, 0.254818f, 0.254818f,
				0.254818f, 0.254818f, 0.254818f, 0.254818f, 0.254818f, 0.254818f, -1.00941f };

		Float[] RAnklePitchTimes = { 0.8f, 4.86667f };
		Float[] RAnklePitchKeys = { -0.0889992f, -0.0889992f };

		Float[] RAnkleRollTimes = { 0.8f, 4.86667f };
		Float[] RAnkleRollKeys = { -0.00149427f, -0.00149427f };

		Float[] RElbowRollTimes = { 0.8f, 1.13333f, 1.26667f, 1.4f, 1.53333f, 1.66667f, 1.8f, 1.93333f, 2.06667f, 2.2f,
				2.33333f, 2.46667f, 2.6f, 2.73333f, 2.86667f, 3f, 3.13333f, 3.26667f, 3.4f, 3.53333f, 3.66667f, 3.8f,
				3.93333f, 4.06667f, 4.2f, 4.86667f };
		Float[] RElbowRollKeys = { 0.308375f, 1.09956f, 0.787143f, 1.09956f, 0.787143f, 1.09956f, 0.787143f, 1.09956f,
				0.787143f, 1.09956f, 0.787143f, 1.09956f, 0.787143f, 1.09956f, 0.787143f, 1.09956f, 0.787143f, 1.09956f,
				0.787143f, 1.09956f, 0.787143f, 1.09956f, 0.787143f, 1.09956f, 0.787143f, 0.308375f };

		Float[] RElbowYawTimes = { 0.8f, 1.13333f, 1.26667f, 1.4f, 1.53333f, 1.66667f, 1.8f, 1.93333f, 2.06667f, 2.2f,
				2.33333f, 2.46667f, 2.6f, 2.73333f, 2.86667f, 3f, 3.13333f, 3.26667f, 3.4f, 3.53333f, 3.66667f, 3.8f,
				3.93333f, 4.06667f, 4.2f, 4.86667f };
		Float[] RElbowYawKeys = { 0.770025f, 0.541052f, 0.518363f, 0.541052f, 0.518363f, 0.541052f, 0.518363f,
				0.541052f, 0.518363f, 0.541052f, 0.518363f, 0.541052f, 0.518363f, 0.541052f, 0.518363f, 0.541052f,
				0.518363f, 0.541052f, 0.518363f, 0.541052f, 0.518363f, 0.541052f, 0.518363f, 0.541052f, 0.518363f,
				0.770025f };

		Float[] RHandTimes = { 0.8f, 1.13333f, 1.4f, 1.66667f, 1.93333f, 2.2f, 2.46667f, 2.73333f, 3f, 3.26667f,
				3.53333f, 3.8f, 4.06667f, 4.86667f };
		Float[] RHandKeys = { 0.917114f, 0.945455f, 0.945455f, 0.945455f, 0.945455f, 0.945455f, 0.945455f, 0.945455f,
				0.945455f, 0.945455f, 0.945455f, 0.945455f, 0.945455f, 0.917114f };

		Float[] RHipPitchTimes = { 0.8f, 4.86667f };
		Float[] RHipPitchKeys = { 0.032157f, 0.032157f };

		Float[] RHipRollTimes = { 0.8f, 4.86667f };
		Float[] RHipRollKeys = { 0.0046224f, 0.0046224f };

		Float[] RKneePitchTimes = { 0.8f, 4.86667f };
		Float[] RKneePitchKeys = { 0.0624798f, 0.0624798f };

		Float[] RShoulderPitchTimes = { 0.8f, 1.13333f, 1.4f, 1.66667f, 1.93333f, 2.2f, 2.46667f, 2.73333f, 3f,
				3.26667f, 3.53333f, 3.8f, 4.06667f, 4.86667f };
		Float[] RShoulderPitchKeys = { 1.57239f, 1.09956f, 1.09956f, 1.09956f, 1.09956f, 1.09956f, 1.09956f, 1.09956f,
				1.09956f, 1.09956f, 1.09956f, 1.09956f, 1.09956f, 1.57239f };

		Float[] RShoulderRollTimes = { 0.8f, 1.13333f, 1.4f, 1.66667f, 1.93333f, 2.2f, 2.46667f, 2.73333f, 3f, 3.26667f,
				3.53333f, 3.8f, 4.06667f, 4.86667f };
		Float[] RShoulderRollKeys = { -0.309909f, -0.0942478f, -0.0942478f, -0.0942478f, -0.0942478f, -0.0942478f,
				-0.0942478f, -0.0942478f, -0.0942478f, -0.0942478f, -0.0942478f, -0.0942478f, -0.0942478f, -0.309909f };

		Float[] RWristYawTimes = { 0.8f, 1.13333f, 1.4f, 1.66667f, 1.93333f, 2.2f, 2.46667f, 2.73333f, 3f, 3.26667f,
				3.53333f, 3.8f, 4.06667f, 4.86667f };
		Float[] RWristYawKeys = { 0.989389f, 1.22173f, 1.22173f, 1.22173f, 1.22173f, 1.22173f, 1.22173f, 1.22173f,
				1.22173f, 1.22173f, 1.22173f, 1.22173f, 1.22173f, 0.989389f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			NaoRobotProxys.getInstance(url).getSpeech().async().say("buen trabajo");
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación movementClappingTwo", "movement_clapping_two");
		}
	}

	/**
	 * NAO levanta el puño con su mano derecha y empuja su brazo derecho hacia
	 * arriba en el aire mientras dice "excelente". Esta es una operación
	 * sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),movement_excelent]
	 */
	@OPERATION
	public void movementExcelent() {
		Float[] HeadPitchTimes = { 0.8f, 1.6f };
		Float[] HeadPitchKeys = { 0.022968f, -0.43263f };

		Float[] HeadYawTimes = { 0.8f, 1.6f };
		Float[] HeadYawKeys = { 0.36505f, -0.544613f };

		Float[] LAnklePitchTimes = { 0.92f };
		Float[] LAnklePitchKeys = { -0.396619f };

		Float[] LAnkleRollTimes = { 0.92f };
		Float[] LAnkleRollKeys = { 0.0214759f };

		Float[] LElbowRollTimes = { 0.64f, 0.88f, 1.16f, 1.92f };
		Float[] LElbowRollKeys = { -1.52169f, -1.50328f, -1.18421f, -0.80991f };

		Float[] LElbowYawTimes = { 0.64f, 0.88f, 1.16f, 1.92f };
		Float[] LElbowYawKeys = { -1.4282f, -1.43433f, -1.69665f, -1.68284f };

		Float[] LHandTimes = { 0.56f, 1.08f };
		Float[] LHandKeys = { 0.216026f, 0.404752f };

		Float[] LHipPitchTimes = { 0.92f };
		Float[] LHipPitchKeys = { -0.287705f };

		Float[] LHipRollTimes = { 0.92f };
		Float[] LHipRollKeys = { -0.00767001f };

		Float[] LHipYawPitchTimes = { 0.92f };
		Float[] LHipYawPitchKeys = { -0.408044f };

		Float[] LKneePitchTimes = { 0.92f };
		Float[] LKneePitchKeys = { 0.790172f };

		Float[] LShoulderPitchTimes = { 0.64f, 0.88f, 1.16f, 1.92f };
		Float[] LShoulderPitchKeys = { 0.447886f, 0.501576f, 1.23943f, 1.78093f };

		Float[] LShoulderRollTimes = { 0.64f, 0.88f, 1.16f, 1.92f };
		Float[] LShoulderRollKeys = { 0.0475121f, 0.059784f, 0.087396f, 0.283749f };

		Float[] LWristYawTimes = { 0.56f, 1.08f };
		Float[] LWristYawKeys = { -0.842209f, -1.02629f };

		Float[] RAnklePitchTimes = { 0.92f };
		Float[] RAnklePitchKeys = { -0.353668f };

		Float[] RAnkleRollTimes = { 0.92f };
		Float[] RAnkleRollKeys = { 0.062894f };

		Float[] RElbowRollTimes = { 0.64f, 0.88f, 1.16f, 1.92f };
		Float[] RElbowRollKeys = { 1.31315f, 1.17969f, 1.01402f, 0.60904f };

		Float[] RElbowYawTimes = { 0.64f, 0.88f, 1.16f, 1.92f };
		Float[] RElbowYawKeys = { 1.90826f, 2.01409f, 1.14586f, 0.673385f };

		Float[] RHandTimes = { 0.56f, 1.08f };
		Float[] RHandKeys = { 0.725115f, 0.229844f };

		Float[] RHipPitchTimes = { 0.92f };
		Float[] RHipPitchKeys = { -0.192597f };

		Float[] RHipRollTimes = { 0.92f };
		Float[] RHipRollKeys = { -0.006136f };

		Float[] RKneePitchTimes = { 0.92f };
		Float[] RKneePitchKeys = { 0.668986f };

		Float[] RShoulderPitchTimes = { 0.64f, 0.88f, 1.16f, 1.92f };
		Float[] RShoulderPitchKeys = { 1.97276f, 1.96203f, 1.10759f, -1.09063f };

		Float[] RShoulderRollTimes = { 0.64f, 0.88f, 1.16f, 1.92f };
		Float[] RShoulderRollKeys = { -0.105888f, -0.122762f, -0.434165f, -0.713353f };

		Float[] RWristYawTimes = { 0.56f, 1.08f };
		Float[] RWristYawKeys = { -0.546147f, 1.06762f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			NaoRobotProxys.getInstance(url).getSpeech().async().say("excelente");
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación movementExcelent", "movement_excelent");
		}
	}

	/**
	 * NAO alza ambas manos por encima de la cabeza, baja los brazos y mueve sus
	 * manos de un lado a otro aplaudiendo. NAO dice: "Tienes razón", para
	 * aplaudir y animar a los niños. Esta es una operación sincrona.
	 * 
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),movement_you_are_right]
	 */
	@OPERATION
	public void movementYouAreRight() {
		Float[] LElbowRollTimes = { 0.933333f, 1.06667f, 1.26667f, 2f, 2.4f, 2.8f, 2.93333f, 3.06667f, 4.26667f };
		Float[] LElbowRollKeys = { -1.0821f, -0.785398f, -0.404916f, -0.00872665f, -0.74351f, -1.13795f, -1.19555f,
				-1.43466f, -1.56207f };

		Float[] LElbowYawTimes = { 2.4f, 2.93333f, 3.06667f, 3.2f, 3.33333f, 3.46667f, 3.6f, 3.86667f, 4f, 4.13333f,
				4.26667f, 4.4f, 4.66667f, 4.8f, 4.93333f, 5.06667f };
		Float[] LElbowYawKeys = { -0.303687f, -1.17635f, -0.759218f, -0.979378f, -1.213f, -0.704982f, -1.18396f,
				-0.720821f, -1.20951f, -0.644027f, -1.17635f, -0.644027f, -1.21201f, -0.79587f, -1.20428f, -0.720821f };

		Float[] LHandTimes = { 1.06667f, 1.26667f, 3.06667f };
		Float[] LHandKeys = { 1f, 1f, 1f };

		Float[] LShoulderPitchTimes = { 0.933333f, 1.06667f, 1.26667f, 2.4f, 2.8f, 2.93333f, 3.06667f, 4.26667f };
		Float[] LShoulderPitchKeys = { 0.274017f, -0.762709f, -1.22348f, -0.158825f, 0.705113f, 1.22348f, 1.36659f,
				1.39452f };

		Float[] LShoulderRollTimes = { 1.06667f, 1.26667f, 2.4f, 2.8f, 2.93333f, 3.06667f, 4.4f };
		Float[] LShoulderRollKeys = { 0.649262f, 0.694641f, 0.6353f, 0.382227f, 0.00872665f, 0.00872665f, 0.00872665f };

		Float[] RElbowRollTimes = { 0.733333f, 0.933333f, 1.26667f, 2f, 2.26667f, 2.4f, 2.8f, 2.93333f, 3.06667f, 3.2f,
				3.46667f, 3.6f, 3.86667f, 4f, 4.13333f, 4.4f, 4.66667f };
		Float[] RElbowRollKeys = { 0.401426f, 1.10704f, 0.432842f, 0.00872665f, 0.445059f, 0.729548f, 1.14319f, 1.3095f,
				1.41372f, 1.43117f, 1.48266f, 1.54287f, 1.56207f, 1.55334f, 1.56207f, 1.56207f, 1.5516f };

		Float[] RElbowYawTimes = { 2.26667f, 2.4f, 2.66667f, 2.8f, 2.93333f, 3.06667f, 3.2f, 3.33333f, 3.46667f, 3.6f,
				3.86667f, 4f, 4.13333f, 4.26667f, 4.4f, 4.53333f, 4.8f, 4.93333f, 5.06667f };
		Float[] RElbowYawKeys = { 0.296706f, 0.296706f, 0.804597f, 1.13097f, 1.16937f, 0.780162f, 1.0472f, 1.20428f,
				1.02451f, 1.12399f, 0.644027f, 1.15017f, 0.65861f, 1.13795f, 0.776672f, 1.09956f, 0.662724f, 1.0472f,
				0.720821f };

		Float[] RHandTimes = { 1.06667f, 1.26667f, 2.93333f, 3.06667f };
		Float[] RHandKeys = { 1f, 1f, 0.414271f, 1f };

		Float[] RShoulderPitchTimes = { 0.933333f, 1.06667f, 1.26667f, 2.13333f, 2.26667f, 2.4f, 2.66667f, 2.8f,
				2.93333f, 3.06667f, 3.2f, 3.46667f, 3.73333f, 4f, 4.13333f, 4.26667f, 4.4f, 4.53333f, 4.8f, 4.93333f,
				5.06667f };
		Float[] RShoulderPitchKeys = { 0.274017f, -0.762709f, -1.22348f, -0.920431f, -0.457276f, -0.129154f, 0.366519f,
				0.698132f, 1.22173f, 1.36659f, 1.36485f, 1.38405f, 1.37873f, 1.39215f, 1.38704f, 1.38467f, 1.34216f,
				1.39414f, 1.38392f, 1.37881f, 1.37881f };

		Float[] RShoulderRollTimes = { 1.06667f, 1.26667f, 2.26667f, 2.4f, 2.8f, 2.93333f, 3.06667f, 3.2f, 3.46667f,
				3.6f, 3.86667f, 4f, 4.13333f, 4.26667f, 4.4f, 4.53333f, 4.8f, 5.06667f };
		Float[] RShoulderRollKeys = { -0.694641f, -0.710349f, -0.645772f, -0.6353f, -0.382227f, -0.00872665f,
				-0.0593412f, -0.00872665f, -0.00872665f, -0.0785398f, -0.144862f, -0.0925025f, -0.178024f, -0.10821f,
				-0.1309f, -0.111701f, -0.10472f, -0.148353f };

		Float[] RWristYawTimes = { 2.8f };
		Float[] RWristYawKeys = { 0f };

		String[] namesArray = { "LElbowRoll", "LElbowYaw", "LHand", "LShoulderPitch", "LShoulderRoll", "RElbowRoll",
				"RElbowYaw", "RHand", "RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			NaoRobotProxys.getInstance(url).getSpeech().async().say("¡Tienes razón!");
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación movementYouAreRight", "movement_you_are_right");
		}
	}

	/**
	 * NAO apunta con la mano derecha sobre la cadera, alza la mano izquierda, y
	 * dice: "Eres muy inteligente". Esta es una operación sincrona.
	 * 
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),movement_you_are_so_smart]
	 */
	@OPERATION
	public void movementYouAreSoSmart() {
		Float[] HeadPitchTimes = { 0.6f, 1.46667f };
		Float[] HeadPitchKeys = { 0.31136f, -0.146608f };

		Float[] HeadYawTimes = { 0.6f, 1.46667f };
		Float[] HeadYawKeys = { -0.00310997f, -0.165714f };

		Float[] LAnklePitchTimes = { 2f };
		Float[] LAnklePitchKeys = { -0.127495f };

		Float[] LAnkleRollTimes = { 2f };
		Float[] LAnkleRollKeys = { -0.00151361f };

		Float[] LElbowRollTimes = { 1.33333f };
		Float[] LElbowRollKeys = { -1.36368f };

		Float[] LElbowYawTimes = { 1.33333f };
		Float[] LElbowYawKeys = { -1.18889f };

		Float[] LHandTimes = { 1.33333f };
		Float[] LHandKeys = { 0.501818f };

		Float[] LHipPitchTimes = { 2f };
		Float[] LHipPitchKeys = { -0.161291f };

		Float[] LHipRollTimes = { 2f };
		Float[] LHipRollKeys = { -0.122912f };

		Float[] LHipYawPitchTimes = { 2f };
		Float[] LHipYawPitchKeys = { -0.35133f };

		Float[] LKneePitchTimes = { 2f };
		Float[] LKneePitchKeys = { 0.400054f };

		Float[] LShoulderPitchTimes = { 1.33333f };
		Float[] LShoulderPitchKeys = { 0.697927f };

		Float[] LShoulderRollTimes = { 1.33333f };
		Float[] LShoulderRollKeys = { 0.13495f };

		Float[] LWristYawTimes = { 1.33333f };
		Float[] LWristYawKeys = { -0.538476f };

		Float[] RAnklePitchTimes = { 2f };
		Float[] RAnklePitchKeys = { -0.251603f };

		Float[] RAnkleRollTimes = { 2f };
		Float[] RAnkleRollKeys = { 0.0261178f };

		Float[] RElbowRollTimes = { 1.33333f };
		Float[] RElbowRollKeys = { 1.56319f };

		Float[] RElbowYawTimes = { 1.33333f };
		Float[] RElbowYawKeys = { 0.228525f };

		Float[] RHandTimes = { 1.33333f };
		Float[] RHandKeys = { 0.91966f };

		Float[] RHipPitchTimes = { 2f };
		Float[] RHipPitchKeys = { -0.417305f };

		Float[] RHipRollTimes = { 2f };
		Float[] RHipRollKeys = { -0.14571f };

		Float[] RKneePitchTimes = { 2f };
		Float[] RKneePitchKeys = { 0.708295f };

		Float[] RShoulderPitchTimes = { 1.33333f };
		Float[] RShoulderPitchKeys = { 1.39445f };

		Float[] RShoulderRollTimes = { 1.33333f };
		Float[] RShoulderRollKeys = { -0.730227f };

		Float[] RWristYawTimes = { 1.33333f };
		Float[] RWristYawKeys = { 0.837522f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			NaoRobotProxys.getInstance(url).getSpeech().async().say("Eres muy inteligente");
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación movementYouAreSoSmart", "movement_you_are_so_smart");
		}
	}

	/**
	 * NAO apunta con la mano derecha y mueve el brazo izquierdo hacia arriba y
	 * hacia abajo, con el codo doblado y el puño cerrado. Nao dice: "Eres muy
	 * inteligente". Esta es una operación sincrona.
	 * 
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),movement_you_are_so_smart_with_cheering]
	 */
	@OPERATION
	public void movementYouAreSoSmartWithCheering() {
		Float[] HeadPitchTimes = { 0.266667f, 0.533333f, 0.666667f, 0.8f, 0.933333f, 1.13333f, 1.6f, 1.93333f, 2.53333f,
				3f, 3.6f, 3.8f };
		Float[] HeadPitchKeys = { -0.368959f, -0.272371f, -0.185005f, -0.0417875f, 0.179769f, 0.26315f, -0.0994838f,
				0.0724156f, -0.121841f, -0.426485f, -0.191986f, -0.0261799f };

		Float[] LElbowRollTimes = { 0.933333f, 1.4f, 1.93333f, 2.46667f };
		Float[] LElbowRollKeys = { -0.98262f, -1.34414f, -1.52156f, -1.35727f };

		Float[] LHandTimes = { 1.4f, 2.4f };
		Float[] LHandKeys = { 0.06f, 0.17f };

		Float[] LShoulderPitchTimes = { 0.333333f, 0.666667f, 0.933333f, 1.2f, 1.4f, 1.6f, 1.93333f, 2.2f, 2.4f,
				2.66667f, 2.86667f, 3.2f, 3.4f, 3.6f, 3.8f, 3.93333f };
		Float[] LShoulderPitchKeys = { 1.73025f, 1.07861f, 0.631354f, 0.0715585f, -0.219262f, -0.532325f, 0.820305f,
				1.72922f, 0.617847f, -0.536912f, 0.207432f, 1.75545f, 0.389208f, -0.514347f, 0.762709f, 1.75453f };

		Float[] LShoulderRollTimes = { 2.2f, 2.4f };
		Float[] LShoulderRollKeys = { 0.232129f, 0.172788f };

		Float[] RElbowRollTimes = { 0.533333f, 0.8f, 1.6f, 1.93333f };
		Float[] RElbowRollKeys = { 0.699877f, 0.459353f, 0.333358f, 0.34732f };

		Float[] RElbowYawTimes = { 0.666667f };
		Float[] RElbowYawKeys = { 1.40324f };

		Float[] RHandTimes = { 0.666667f, 1.2f, 1.6f, 2.2f };
		Float[] RHandKeys = { 1f, 0.92f, 0.96f, 0.79f };

		Float[] RShoulderPitchTimes = { 0.333333f, 0.666667f, 1.6f, 1.93333f };
		Float[] RShoulderPitchKeys = { 0.790634f, 0.301942f, 0.18675f, 0.18675f };

		Float[] RShoulderRollTimes = { 0.533333f, 1f, 1.6f, 2f };
		Float[] RShoulderRollKeys = { -0.0541052f, -0.119408f, -0.143117f, -0.0249289f };

		String[] namesArray = { "HeadPitch", "LElbowRoll", "LHand", "LShoulderPitch", "LShoulderRoll", "RElbowRoll",
				"RElbowYaw", "RHand", "RShoulderPitch", "RShoulderRoll" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		try {
			NaoRobotProxys.getInstance(url).getSpeech().async().say("Eres muy inteligente");
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación movementYouAreSoSmartWithCheering",
					"movement_you_are_so_smart_with_cheering");
		}
	}

	/**
	 * NAO dobla las rodillas y tira los dos brazos hacia abajo mientras dice:
	 * "¡Sí, lo tienes!". Esta es una operación sincrona.
	 * 
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera:
	 *             [error_msg(Msg),movement_yes_you_got_it]
	 */
	@OPERATION
	public void movementyesYouGotIt() {
		Float[] HeadPitchTimes = { 3.66667f };
		Float[] HeadPitchKeys = { -0.016916f };

		Float[] HeadYawTimes = { 3.66667f };
		Float[] HeadYawKeys = { 0.00762803f };

		Float[] LAnklePitchTimes = { 1.53333f, 3.66667f };
		Float[] LAnklePitchKeys = { -0.515598f, -0.104354f };

		Float[] LAnkleRollTimes = { 1.53333f, 3.66667f };
		Float[] LAnkleRollKeys = { 0.0046224f, 0.00924597f };

		Float[] LElbowRollTimes = { 1f, 1.53333f, 2.06667f, 2.53333f, 3f, 3.66667f };
		Float[] LElbowRollKeys = { -0.558334f, -1.53242f, -1.48947f, -1.53242f, -1.48947f, -0.351244f };

		Float[] LElbowYawTimes = { 1f, 1.53333f, 2.06667f, 2.53333f, 3f, 3.66667f };
		Float[] LElbowYawKeys = { -0.274628f, -1.48649f, -1.46655f, -1.48649f, -1.46655f, -0.763974f };

		Float[] LHandTimes = { 1f, 1.53333f, 2.06667f, 2.53333f, 3f, 3.66667f };
		Float[] LHandKeys = { 0.916387f, 0.0709091f, 0.0734808f, 0.0709091f, 0.0734808f, 0.918933f };

		Float[] LHipPitchTimes = { 1.53333f, 3.66667f };
		Float[] LHipPitchKeys = { -0.339236f, 0.058334f };

		Float[] LHipRollTimes = { 1.53333f, 3.66667f };
		Float[] LHipRollKeys = { -0.0676881f, -0.032172f };

		Float[] LHipYawPitchTimes = { 1.53333f, 3.66667f };
		Float[] LHipYawPitchKeys = { 0.0183645f, 0.0184499f };

		Float[] LKneePitchTimes = { 1.53333f, 3.66667f };
		Float[] LKneePitchKeys = { 0.743671f, 0.068988f };

		Float[] LShoulderPitchTimes = { 1f, 1.53333f, 2.06667f, 2.53333f, 3f, 3.66667f };
		Float[] LShoulderPitchKeys = { 1.08143f, 0.12728f, 1.54316f, 0.12728f, 1.54316f, 1.55697f };

		Float[] LShoulderRollTimes = { 1f, 1.53333f, 2.06667f, 2.53333f, 3f, 3.66667f };
		Float[] LShoulderRollKeys = { 0.161028f, 0.024502f, 0f, 0.024502f, 0f, 0.299088f };

		Float[] LWristYawTimes = { 1f, 1.53333f, 2.06667f, 2.53333f, 3f, 3.66667f };
		Float[] LWristYawKeys = { -1.0447f, -1.14287f, -1.21497f, -1.14287f, -1.21497f, -1.03549f };

		Float[] RAnklePitchTimes = { 1.53333f, 3.66667f };
		Float[] RAnklePitchKeys = { -0.515452f, -0.095066f };

		Float[] RAnkleRollTimes = { 1.53333f, 3.66667f };
		Float[] RAnkleRollKeys = { -0.0337082f, -0.00302603f };

		Float[] RElbowRollTimes = { 1f, 1.53333f, 2.06667f, 2.53333f, 3f, 3.66667f };
		Float[] RElbowRollKeys = { 0.662729f, 1.53558f, 1.48649f, 1.53558f, 1.48649f, 0.309909f };

		Float[] RElbowYawTimes = { 1f, 1.53333f, 2.06667f, 2.53333f, 3f, 3.66667f };
		Float[] RElbowYawKeys = { 0.371186f, 1.79628f, 1.61679f, 1.79628f, 1.61679f, 0.788433f };

		Float[] RHandTimes = { 1f, 1.53333f, 2.06667f, 2.53333f, 3f, 3.66667f };
		Float[] RHandKeys = { 0.918933f, 0.02f, 0.021481f, 0.02f, 0.021481f, 0.917842f };

		Float[] RHipPitchTimes = { 1.53333f, 3.66667f };
		Float[] RHipPitchKeys = { -0.483268f, 0.0367741f };

		Float[] RHipRollTimes = { 1.53333f, 3.66667f };
		Float[] RHipRollKeys = { 0.0122924f, 0.01078f };

		Float[] RKneePitchTimes = { 1.53333f, 3.66667f };
		Float[] RKneePitchKeys = { 0.864762f, 0.084412f };

		Float[] RShoulderPitchTimes = { 1f, 1.53333f, 2.06667f, 2.53333f, 3f, 3.66667f };
		Float[] RShoulderPitchKeys = { 1.10606f, 0.116626f, 1.50183f, 0.116626f, 1.50183f, 1.56165f };

		Float[] RShoulderRollTimes = { 1f, 1.53333f, 2.06667f, 2.53333f, 3f, 3.66667f };
		Float[] RShoulderRollKeys = { -0.208666f, -0.030722f, -0.00924597f, -0.030722f, -0.00924597f, -0.286901f };

		Float[] RWristYawTimes = { 1f, 1.53333f, 2.06667f, 2.53333f, 3f, 3.66667f };
		Float[] RWristYawKeys = { 0.946436f, 1.30693f, 1.35601f, 1.30693f, 1.35601f, 0.989389f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			NaoRobotProxys.getInstance(url).getSpeech().async().say("¡Sí, lo tienes!");
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación movementyesYouGotIt", "movement_yes_you_got_it");
		}
	}

	/**
	 * Nao mueve la cabeza de arriba a abajo mientras dice: "Wow". Esta es una
	 * operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),movement_wow]
	 */
	@OPERATION
	public void movementWow() {
		Float[] HeadPitchTimes = { 0.64f, 0.88f, 1.08f, 1.32f, 1.56f, 1.84f, 2.12f, 2.72f };
		Float[] HeadPitchKeys = { -0.013848f, 0.415673f, -0.268493f, 0.415673f, -0.268493f, 0.415673f, -0.268493f,
				0.1733f };

		Float[] HeadYawTimes = { 0.64f, 0.88f, 1.08f, 1.32f, 1.56f, 1.84f, 2.12f, 2.72f };
		Float[] HeadYawKeys = { -4.19617e-05f, 0.010696f, 0.00456004f, 0.010696f, 0.00456004f, 0.010696f, 0.00456004f,
				-4.19617e-05f };

		Float[] LAnklePitchTimes = { 1.48f };
		Float[] LAnklePitchKeys = { -0.232481f };

		Float[] LAnkleRollTimes = { 1.48f };
		Float[] LAnkleRollKeys = { -0.187148f };

		Float[] LElbowRollTimes = { 0.48f, 0.88f, 1.52f, 2.04f };
		Float[] LElbowRollKeys = { -0.843657f, -1.10137f, -1.22102f, -0.74088f };

		Float[] LElbowYawTimes = { 0.48f, 0.88f, 1.52f, 2.04f };
		Float[] LElbowYawKeys = { -1.3561f, -1.10606f, -1.26559f, -1.25179f };

		Float[] LHandTimes = { 0.96f, 2.16f };
		Float[] LHandKeys = { 0.161481f, 0.161117f };

		Float[] LHipPitchTimes = { 1.48f };
		Float[] LHipPitchKeys = { 0.502304f };

		Float[] LHipRollTimes = { 1.48f };
		Float[] LHipRollKeys = { 0.282256f };

		Float[] LHipYawPitchTimes = { 1.48f };
		Float[] LHipYawPitchKeys = { -0.343615f };

		Float[] LKneePitchTimes = { 1.48f };
		Float[] LKneePitchKeys = { 0.109076f };

		Float[] LShoulderPitchTimes = { 0.48f, 0.88f, 1.52f, 2.04f };
		Float[] LShoulderPitchKeys = { 1.66742f, 1.41584f, 1.52169f, 1.60299f };

		Float[] LShoulderRollTimes = { 0.48f, 0.88f, 1.52f, 2.04f };
		Float[] LShoulderRollKeys = { 0.214717f, 0.15796f, 0.16563f, 0.16563f };

		Float[] LWristYawTimes = { 0.96f, 2.16f };
		Float[] LWristYawKeys = { -0.48632f, 0.185572f };

		Float[] RAnklePitchTimes = { 1.48f };
		Float[] RAnklePitchKeys = { -0.23555f };

		Float[] RAnkleRollTimes = { 1.48f };
		Float[] RAnkleRollKeys = { -0.0322141f };

		Float[] RElbowRollTimes = { 0.48f, 0.88f, 1.52f, 2.04f };
		Float[] RElbowRollKeys = { 1.1981f, 1.36837f, 1.38678f, 1.03089f };

		Float[] RElbowYawTimes = { 0.48f, 0.88f, 1.52f, 2.04f };
		Float[] RElbowYawKeys = { 0.816046f, 0.704064f, 1.05995f, 1.06149f };

		Float[] RHandTimes = { 0.96f, 2.16f };
		Float[] RHandKeys = { 0.280389f, 0.280389f };

		Float[] RHipPitchTimes = { 1.48f };
		Float[] RHipPitchKeys = { 0.399527f };

		Float[] RHipRollTimes = { 1.48f };
		Float[] RHipRollKeys = { 0.0475539f };

		Float[] RKneePitchTimes = { 1.48f };
		Float[] RKneePitchKeys = { 0.254806f };

		Float[] RShoulderPitchTimes = { 0.48f, 0.88f, 1.52f, 2.04f };
		Float[] RShoulderPitchKeys = { 1.37451f, 1.07691f, 1.23798f, 1.57393f };

		Float[] RShoulderRollTimes = { 0.48f, 0.88f, 1.52f, 2.04f };
		Float[] RShoulderRollKeys = { -0.084412f, -0.067538f, -0.081344f, -0.16418f };

		Float[] RWristYawTimes = { 0.96f, 2.16f };
		Float[] RWristYawKeys = { 0.722472f, -0.00157596f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			NaoRobotProxys.getInstance(url).getSpeech().async().say("Wow!");
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación movementWow", "movement_wow");
		}
	}

	/**
	 * NAO mueve los brazos arriba y abajo con los codos doblados y balancea el
	 * brazo izquierdo en otro movimiento de la danza del robot. Esta es una
	 * operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),robot_dance]
	 */
	@OPERATION
	public void robotDance() {
		Float[] HeadPitchTimes = { 1.26667f, 3.53333f, 4.33333f };
		Float[] HeadPitchKeys = { -0.012314f, 0.00609404f, 0.00609404f };

		Float[] HeadYawTimes = { 1.26667f, 3.53333f, 4.33333f };
		Float[] HeadYawKeys = { 0.00762803f, 0.00762803f, 0.00762803f };

		Float[] LAnklePitchTimes = { 1.26667f, 2f, 3.53333f, 4.33333f };
		Float[] LAnklePitchKeys = { 0.191576f, 0.205383f, 0.183907f, 0.183907f };

		Float[] LAnkleRollTimes = { 1.26667f, 2f, 3.53333f, 4.33333f };
		Float[] LAnkleRollKeys = { 0.0659823f, 0.0613804f, -0.0107176f, -0.0107176f };

		Float[] LElbowRollTimes = { 1.26667f, 2.73333f, 3.53333f, 4.33333f };
		Float[] LElbowRollKeys = { -1.4772f, -1.55697f, -0.010696f, -0.010696f };

		Float[] LElbowYawTimes = { 1.26667f, 2.73333f, 3.53333f, 4.33333f };
		Float[] LElbowYawKeys = { -1.71812f, -1.29627f, -0.216335f, -0.216335f };

		Float[] LHandTimes = { 1.26667f, 3.53333f, 4.33333f };
		Float[] LHandKeys = { 0.997114f, 0.995296f, 0.995296f };

		Float[] LHipPitchTimes = { 1.26667f, 2f, 3.53333f, 4.33333f };
		Float[] LHipPitchKeys = { 0.179256f, 0.162382f, -0.24106f, -0.24106f };

		Float[] LHipRollTimes = { 1.26667f, 2f, 3.53333f, 4.33333f };
		Float[] LHipRollKeys = { 0.0580999f, 0.07344f, -0.145922f, -0.145922f };

		Float[] LHipYawPitchTimes = { 1.26667f, 2f, 3.53333f, 4.33333f };
		Float[] LHipYawPitchKeys = { -0.730228f, -0.739431f, -0.487856f, -0.487856f };

		Float[] LKneePitchTimes = { 1.26667f, 2f, 3.53333f, 4.33333f };
		Float[] LKneePitchKeys = { 0.185295f, 0.169954f, 0.166886f, 0.166886f };

		Float[] LShoulderPitchTimes = { 1.26667f, 2.73333f, 3.53333f, 4.33333f };
		Float[] LShoulderPitchKeys = { 1.53089f, 0.179436f, 1.7073f, 1.7073f };

		Float[] LShoulderRollTimes = { 1.26667f, 2.73333f, 3.53333f, 4.33333f };
		Float[] LShoulderRollKeys = { 0.039842f, 0f, 1.35601f, 1.34374f };

		Float[] LWristYawTimes = { 1.26667f, 3.53333f, 4.33333f };
		Float[] LWristYawKeys = { -0.277696f, -0.289967f, -0.289967f };

		Float[] RAnklePitchTimes = { 1.26667f, 2f, 3.53333f, 4.33333f };
		Float[] RAnklePitchKeys = { -0.411138f, -0.401935f, 0.352792f, 0.352792f };

		Float[] RAnkleRollTimes = { 1.26667f, 2f, 3.53333f, 4.33333f };
		Float[] RAnkleRollKeys = { 0.0322537f, 0.0337877f, 0.248547f, 0.248547f };

		Float[] RElbowRollTimes = { 1.26667f, 2.73333f, 3.53333f, 4.33333f };
		Float[] RElbowRollKeys = { 1.46348f, 1.56319f, 1.54171f, 1.54171f };

		Float[] RElbowYawTimes = { 1.26667f, 2.73333f, 3.53333f, 4.33333f };
		Float[] RElbowYawKeys = { 1.52015f, 1.44499f, 1.65821f, 1.65668f };

		Float[] RHandTimes = { 1.26667f, 3.53333f, 4.33333f };
		Float[] RHandKeys = { 1f, 1f, 1f };

		Float[] RHipPitchTimes = { 1.26667f, 2f, 3.53333f, 4.33333f };
		Float[] RHipPitchKeys = { 0.489289f, 0.490823f, -0.279246f, -0.279246f };

		Float[] RHipRollTimes = { 1.26667f, 2f, 3.53333f, 4.33333f };
		Float[] RHipRollKeys = { -0.148778f, -0.15338f, -0.257691f, -0.257691f };

		Float[] RKneePitchTimes = { 1.26667f, 2f, 3.53333f, 4.33333f };
		Float[] RKneePitchKeys = { 0.47666f, 0.455184f, 0.0195278f, 0.0195278f };

		Float[] RShoulderPitchTimes = { 1.26667f, 2.73333f, 3.53333f, 4.33333f };
		Float[] RShoulderPitchKeys = { 0.00464395f, 2.07247f, 1.56779f, 1.56779f };

		Float[] RShoulderRollTimes = { 1.26667f, 2.73333f, 3.53333f, 4.33333f };
		Float[] RShoulderRollKeys = { -0.11816f, -0.154976f, -0.182588f, -0.182588f };

		Float[] RWristYawTimes = { 1.26667f, 3.53333f, 4.33333f };
		Float[] RWristYawKeys = { 0.408002f, 0.41107f, 0.41107f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación robotDance", "robot_dance");
		}
	}

	/**
	 * Este movimiento fue creado por Aldeberan y viene con Choregraphe. NAO
	 * realiza una serie de Movimientos de Tai Chi.Esta es una operación
	 * sincrona.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),tai_chi_chaun]
	 */
	@OPERATION
	public void taiChiChaun() {
		Float[] HeadPitchTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] HeadPitchKeys = { 0f, 8.95233e-08f, -4.76838e-07f, 8.89455e-08f, 1.04976e-07f, 0.331613f, 0.314159f,
				9.19019e-08f, -0.331613f, 0.139626f, -0.0872665f, 0.139626f, 0.383972f, 0.558505f, 0.383972f,
				-0.331613f, 0.139626f, -0.0872665f, 0.139626f, 0.383972f, 0f, -0.190258f };

		Float[] HeadYawTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f, 34.4f,
				37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] HeadYawKeys = { 0f, 8.42936e-08f, 8.42938e-08f, 8.42938e-08f, -4.76838e-07f, 0.314159f, -0.296706f,
				-1.18682f, -0.279253f, 0.20944f, 1.5708f, 0.20944f, 0.139626f, 0f, -0.139626f, 0.279253f, -0.20944f,
				-1.5708f, -0.20944f, -0.139626f, 0f, -0.00310993f };

		Float[] LAnklePitchTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 43.4f, 44.4f, 46.2f, 50f };
		Float[] LAnklePitchKeys = { 1.00403e-07f, 0f, -0.303687f, 0f, 0f, -0.647517f, -0.610865f, -1.0472f, -1.0472f,
				-1.0472f, -1.0472f, -1.0472f, -1.0472f, -0.872665f, -0.741765f, 0f, 1.00403e-07f, 0.523599f,
				1.00403e-07f, -0.555015f, -0.654498f, -1.0472f, 0.033706f };

		Float[] LAnkleRollTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				33.4f, 34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] LAnkleRollKeys = { 0.0523599f, 0.122173f, 0.174533f, -0.10472f, -0.10472f, 0.174533f, -0.261799f,
				0.0628318f, 0.1309f, 0f, 0f, 0f, 0.0872665f, 0f, -0.240855f, -0.55676f, -0.424115f, -0.349066f, 0f,
				-0.349066f, -0.312414f, 0f, -0.05058f };

		Float[] LElbowRollTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 45.4f, 46.2f, 50f };
		Float[] LElbowRollKeys = { 0f, -0.698132f, -1.0472f, 0f, 0f, -1.65806f, -0.959931f, -1.48353f, -1.01229f,
				-1.01229f, 0f, -1.01229f, -1.01229f, -0.890118f, -0.855211f, -1.11701f, -0.855211f, -1.25664f,
				-0.855211f, -0.855211f, -0.994838f, -1.4207f, -0.38806f };

		Float[] LElbowYawTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 45.4f, 46.2f, 50f };
		Float[] LElbowYawKeys = { -1.5708f, -1.5708f, -1.5708f, -1.5708f, -1.5708f, -0.383972f, 0f, 0f, 0f, 0f, 0f, 0f,
				0f, 0.20944f, 0.191986f, -0.418879f, -0.418879f, -0.0872665f, -0.418879f, 0.191986f, -0.378736f,
				-0.244346f, -1.18276f };

		Float[] LHandTimes = { 3f, 50f };
		Float[] LHandKeys = { 0f, 0.2984f };

		Float[] LHipPitchTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] LHipPitchKeys = { 0f, 0f, -0.349066f, 0f, 0f, -0.698132f, -0.610865f, -1.0472f, -1.0472f, -1.0472f,
				-1.0472f, -1.0472f, -1.0472f, -0.872665f, -0.741765f, -0.122173f, -0.872665f, 0f, -0.872665f,
				-0.654498f, -1.0472f, 0.216335f };

		Float[] LHipRollTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				33.4f, 34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] LHipRollKeys = { -0.0523599f, -0.122173f, -0.174533f, 0.10472f, 0.10472f, -0.174533f, 0.174533f,
				0.420624f, 0.528835f, 0.610865f, 0.610865f, 0.610865f, 0.349066f, 0f, -0.261799f, 0.251327f, 0.261799f,
				0.139626f, 0.698132f, 0.139626f, -0.261799f, 0f, 0.0414601f };

		Float[] LHipYawPitchTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] LHipYawPitchKeys = { -0.10821f, -0.120428f, -0.1309f, -0.120428f, -0.143117f, -0.167552f, -0.0994838f,
				0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, -0.0680678f, 0f, -0.194775f };

		Float[] LKneePitchTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] LKneePitchKeys = { 0f, 0f, 0.698132f, -9.9341e-08f, -9.9341e-08f, 1.39626f, 1.22173f, 2.0944f, 2.0944f,
				2.0944f, 2.0944f, 2.0944f, 2.1101f, 1.74533f, 1.48353f, 0.122173f, 1.74533f, 0f, 1.74533f, 1.309f,
				2.0944f, -0.0890141f };

		Float[] LShoulderPitchTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] LShoulderPitchKeys = { 1.5708f, 1.91986f, 2.0944f, 1.5708f, 0f, 0.366519f, 0.349066f, 0.191986f,
				-0.802851f, -0.174533f, -0.296706f, -0.174533f, 0.523599f, 0.471239f, 0.331613f, -0.471239f, 0.0698132f,
				-0.0698132f, 0.0698132f, 0.331613f, 1.69297f, 1.52936f };

		Float[] LShoulderRollTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] LShoulderRollKeys = { 0.174533f, 0.349066f, 0.174533f, 0.174533f, 0.174533f, 0.698132f, 0f, 0.0872665f,
				0.174533f, 0.401426f, 1.15192f, 0.401426f, 0.401426f, 0.174533f, 0f, 0.401426f, 0f, 0f, 0f, 0.20944f,
				0.942478f, 0.107338f };

		Float[] LWristYawTimes = { 3f, 50f };
		Float[] LWristYawKeys = { -1.53589f, 0.139552f };

		Float[] RAnklePitchTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] RAnklePitchKeys = { 1.00403e-07f, 0f, 0f, 0f, 0f, -0.698132f, -0.174533f, 0f, 0f, 1.00403e-07f,
				0.523599f, 1.00403e-07f, -0.741765f, -0.872665f, -1.0472f, -1.0472f, -1.0472f, -1.0472f, -1.0472f,
				-1.0472f, -1.0472f, 0.036858f };

		Float[] RAnkleRollTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] RAnkleRollKeys = { -0.0523599f, 0.1309f, 0.438078f, 0.10472f, 0.10472f, 0.294961f, 0.621337f, 0.785398f,
				0.74351f, 0.436332f, 0f, 0.349066f, 0.261799f, 0f, -0.174533f, -0.174533f, -0.0424667f, -0.0225556f,
				-0.0130542f, -0.00206581f, 0f, 0.0291878f };

		Float[] RElbowRollTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 45.4f, 46.2f, 50f };
		Float[] RElbowRollKeys = { 0f, 0.698132f, 1.0472f, 2.57424e-07f, 0f, 1.23918f, 1.64061f, 0.0698132f, 1.11701f,
				0.855211f, 1.25664f, 0.855211f, 0.855211f, 0.890118f, 1.01229f, 1.01229f, 1.01229f, 0.0349066f,
				1.01229f, 1.01229f, 1.13272f, 1.36659f, 0.395814f };

		Float[] RElbowYawTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 45.4f, 46.2f, 50f };
		Float[] RElbowYawKeys = { 1.5708f, 1.5708f, 1.5708f, 1.5708f, 1.5708f, 0.191986f, 0.349066f, 1.5708f, 0.418879f,
				0.418879f, 0.0872665f, 0.418879f, -0.191986f, -0.20944f, 0f, 0f, 0f, 0f, 0f, 0f, 0.342085f, 0.244346f,
				1.15966f };

		Float[] RHandTimes = { 3f, 50f };
		Float[] RHandKeys = { 0f, 0.302f };

		Float[] RHipPitchTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] RHipPitchKeys = { 0f, 0f, 0f, 0f, 0f, -0.698132f, -0.174533f, -0.10472f, -0.122173f, -0.872665f, 0f,
				-0.872665f, -0.741765f, -0.872665f, -1.0472f, -1.0472f, -1.0472f, -1.0472f, -1.0472f, -1.0472f,
				-1.0472f, 0.214717f };

		Float[] RHipRollTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] RHipRollKeys = { 0.0523599f, -0.122173f, -0.438078f, -0.10472f, -0.10472f, -0.349066f, -0.785398f,
				-0.541052f, -0.139626f, -0.139626f, -0.698132f, -0.139626f, 0.261799f, 0f, -0.349066f, -0.539307f,
				-0.610865f, -0.610865f, -0.610865f, -0.532325f, 0f, -0.021434f };

		Float[] RHipYawPitchTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 50f };
		Float[] RHipYawPitchKeys = { -0.10821f, -0.120428f, -0.1309f, -0.120428f, -0.143117f, -0.167552f, -0.194775f };

		Float[] RKneePitchTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] RKneePitchKeys = { 0f, 0f, 0f, 0f, 0f, 1.39626f, 0.349066f, 0.122173f, 0.122173f, 1.74533f, 0f,
				1.74533f, 1.48353f, 1.74533f, 2.0944f, 2.0944f, 2.0944f, 2.0944f, 2.0944f, 2.0944f, 2.0944f,
				-0.091998f };

		Float[] RShoulderPitchTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] RShoulderPitchKeys = { 1.5708f, 1.91986f, 2.0944f, 1.5708f, 0f, 0.174533f, 0.610865f, 1.0472f,
				-0.471239f, 0.0698132f, -0.0698132f, 0.0698132f, 0.331613f, 0.471239f, 0.523599f, -0.802851f,
				-0.174533f, -0.296706f, -0.174533f, 0.523599f, 1.69297f, 1.51563f };

		Float[] RShoulderRollTimes = { 3f, 5f, 7f, 9f, 11f, 13f, 15f, 17f, 19f, 21f, 23.6f, 26.2f, 28.4f, 30.4f, 32.4f,
				34.4f, 37f, 39.6f, 42.2f, 44.4f, 46.2f, 50f };
		Float[] RShoulderRollKeys = { -0.174533f, -0.174533f, -0.349066f, -0.174533f, -0.174515f, -0.0698132f,
				-0.837758f, -1.51844f, -0.401426f, 0f, 0f, 0f, 0f, -0.174533f, -0.401426f, -0.174533f, -0.401426f,
				-1.15192f, -0.401426f, -0.558505f, -0.942478f, -0.099752f };

		Float[] RWristYawTimes = { 3f, 50f };
		Float[] RWristYawKeys = { 1.53589f, 0.164096f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll",
				"RHipYawPitch", "RKneePitch", "RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación taiChiChaun", "tai_chi_chaun");
		}
	}

	/**
	 * NAO levanta el brazo derecho hacia arriba con ligera curva en el codo y
	 * puño cerrado. Esto también podría ser usado para refuerzo positivo. Esta
	 * es una operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),rock_on]
	 */
	@OPERATION
	public void rockOn() {
		Float[] HeadPitchTimes = { 1.66667f };
		Float[] HeadPitchKeys = { -0.00464395f };

		Float[] HeadYawTimes = { 1.66667f };
		Float[] HeadYawKeys = { 0.00456004f };

		Float[] LAnklePitchTimes = { 1.66667f };
		Float[] LAnklePitchKeys = { -0.107422f };

		Float[] LAnkleRollTimes = { 1.66667f };
		Float[] LAnkleRollKeys = { 0.00924597f };

		Float[] LElbowRollTimes = { 1.66667f };
		Float[] LElbowRollKeys = { -0.343573f };

		Float[] LElbowYawTimes = { 1.66667f };
		Float[] LElbowYawKeys = { -0.754769f };

		Float[] LHandTimes = { 1.66667f };
		Float[] LHandKeys = { 0.918205f };

		Float[] LHipPitchTimes = { 1.66667f };
		Float[] LHipPitchKeys = { 0.0568f };

		Float[] LHipRollTimes = { 1.66667f };
		Float[] LHipRollKeys = { -0.032172f };

		Float[] LHipYawPitchTimes = { 1.66667f };
		Float[] LHipYawPitchKeys = { 0.0184499f };

		Float[] LKneePitchTimes = { 1.66667f };
		Float[] LKneePitchKeys = { 0.0705221f };

		Float[] LShoulderPitchTimes = { 1.66667f };
		Float[] LShoulderPitchKeys = { 1.5631f };

		Float[] LShoulderRollTimes = { 1.66667f };
		Float[] LShoulderRollKeys = { 0.337438f };

		Float[] LWristYawTimes = { 1.66667f };
		Float[] LWristYawKeys = { -1.01555f };

		Float[] RAnklePitchTimes = { 1.66667f };
		Float[] RAnklePitchKeys = { -0.095066f };

		Float[] RAnkleRollTimes = { 1.66667f };
		Float[] RAnkleRollKeys = { -0.00302603f };

		Float[] RElbowRollTimes = { 1.66667f };
		Float[] RElbowRollKeys = { 0.842209f };

		Float[] RElbowYawTimes = { 1.66667f };
		Float[] RElbowYawKeys = { 0.068988f };

		Float[] RHandTimes = { 1.66667f };
		Float[] RHandKeys = { 0.00511738f };

		Float[] RHipPitchTimes = { 1.66667f };
		Float[] RHipPitchKeys = { 0.0352401f };

		Float[] RHipRollTimes = { 1.66667f };
		Float[] RHipRollKeys = { 0.01078f };

		Float[] RKneePitchTimes = { 1.66667f };
		Float[] RKneePitchKeys = { 0.084412f };

		Float[] RShoulderPitchTimes = { 1.66667f };
		Float[] RShoulderPitchKeys = { -1.01853f };

		Float[] RShoulderRollTimes = { 1.66667f };
		Float[] RShoulderRollKeys = { -0.93118f };

		Float[] RWristYawTimes = { 1.66667f };
		Float[] RWristYawKeys = { -0.00924597f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			await_time(2000);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación rockOn", "rock_on");
		}
	}

	/**
	 * Nao baila la canción Thriller igual que lo hacía Michael Jackson. Esta es
	 * una operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),thriller]
	 */
	@OPERATION
	public void thriller() {
		Float[] HeadPitchTimes = { 1.36f, 2.76f, 3.08f, 3.56f, 4.64f, 5.2f, 5.92f, 6.72f, 7.24f, 7.76f, 8.28f, 8.76f,
				9.24f, 9.76f, 10.28f, 10.76f, 11.28f, 11.76f, 12.28f, 12.76f, 13.28f, 14.28f, 14.76f, 15.28f, 15.76f,
				16.28f, 16.76f, 17.28f, 17.76f, 18.76f, 19.28f, 19.76f, 20.28f, 20.76f, 21.76f, 22.76f, 23.24f, 23.76f,
				24.24f, 24.76f, 25.28f, 25.76f, 26.28f, 26.76f, 27.28f, 27.76f, 28.76f, 29.76f, 30.12f, 30.28f, 30.76f,
				31.64f, 32.12f, 32.76f, 33.28f, 33.76f, 34.28f, 34.76f, 35.28f, 35.76f, 36.28f, 36.76f, 37.28f, 38.28f,
				38.76f, 39.28f, 39.76f, 40.28f, 40.76f, 41.28f, 41.76f, 45.56f, 46.72f, 48.48f, 50.32f, 51.24f, 52.2f };
		Float[] HeadPitchKeys = { 0.44175f, 0.44175f, -0.443368f, -0.443368f, 0.369652f, 0.496974f, 0.454021f,
				-0.084412f, -0.082878f, 0.325165f, -0.066004f, 0.217786f, -0.066004f, -0.375872f, 0.00456004f,
				-0.365133f, 0.00456004f, 0.205514f, -0.200996f, 0.00762803f, 0.131882f, 0.170232f, -0.0782759f,
				-0.50166f, -0.0782759f, -0.50166f, -0.0782759f, -0.50166f, -0.656595f, 0.343573f, 0.361981f, -0.04913f,
				-0.380475f, 0.240796f, 0.516916f, 0.108872f, 0.0981341f, 0.4034f, 0.532256f, 0.0735901f, 0.269941f,
				-0.182588f, 0.085862f, -0.182588f, 0.085862f, 0.325165f, 0.091998f, 0.467829f, 0.01223f, 0.380389f,
				0.380389f, 0.447886f, 0.340507f, 0.00609404f, -0.368202f, 0.434081f, -0.369736f, 0.38806f, -0.30224f,
				0.38806f, -0.369736f, 0.38806f, -0.30224f, -0.066004f, -0.635118f, 0.268407f, 0.397265f, -0.547679f,
				0.397265f, 0.523053f, -0.389678f, 0.085862f, -0.552281f, 0.481634f, 0.481634f, -0.475581f, 0.0705221f };

		Float[] HeadYawTimes = { 1.36f, 2.76f, 3.08f, 3.56f, 4.64f, 5.2f, 5.92f, 6.72f, 7.24f, 7.76f, 8.28f, 8.76f,
				9.24f, 9.76f, 10.28f, 10.76f, 11.28f, 11.76f, 12.28f, 12.76f, 13.28f, 14.28f, 14.76f, 15.28f, 15.76f,
				16.28f, 16.76f, 17.28f, 17.76f, 18.76f, 19.28f, 19.76f, 20.28f, 20.76f, 21.76f, 22.76f, 23.24f, 23.76f,
				24.24f, 24.76f, 25.28f, 25.76f, 26.28f, 26.76f, 27.28f, 27.76f, 28.76f, 29.76f, 30.12f, 30.28f, 30.76f,
				31.64f, 32.12f, 32.76f, 33.28f, 33.76f, 34.28f, 34.76f, 35.28f, 35.76f, 36.28f, 36.76f, 37.28f, 38.28f,
				38.76f, 39.28f, 39.76f, 40.28f, 40.76f, 41.28f, 41.76f, 45.56f, 46.72f, 48.48f, 50.32f, 51.24f, 52.2f };
		Float[] HeadYawKeys = { -0.467912f, -0.467912f, 0.260738f, 0.260738f, 0.230059f, 0.538392f, -0.512397f,
				-0.917375f, -0.021518f, -1.126f, -0.0629359f, -0.918907f, -0.0629359f, -1.02936f, -0.016916f, -1.02015f,
				-0.016916f, -1.09225f, 0.79457f, -0.214801f, 0.993989f, -1.18429f, -0.0859459f, -0.0706059f,
				-0.0859459f, -0.0706059f, -0.0859459f, -0.0706059f, -0.0782759f, 0.5568f, -0.411154f, -0.0537319f,
				0.0720561f, 0.493905f, -0.39428f, -0.015382f, -0.0184499f, -0.426494f, -0.20944f, 0.039842f, -0.14884f,
				-0.122762f, -0.128898f, -0.122762f, -0.128898f, 0.351244f, -0.763974f, 0.538392f, -0.627448f, -0.61671f,
				-0.61671f, 0.366584f, 0.642704f, 0.691793f, 0.87127f, -0.012314f, -0.65506f, 0.032172f, 0.621227f,
				0.032172f, -0.65506f, 0.032172f, 0.621227f, 0.11961f, -0.52467f, 0.658043f, 0.742414f, 0.115008f,
				-0.742414f, 0.4034f, -0.845275f, 0.113474f, 0.0459781f, -0.01078f, -0.01078f, -0.00464395f,
				-0.208666f };

		Float[] LAnklePitchTimes = { 1.32f, 3.04f, 3.6f, 4.04f, 5.12f, 5.44f, 6.68f, 7.84f, 8.84f, 9.84f, 10.36f,
				10.84f, 11.36f, 12.24f, 13.04f, 13.84f, 14.84f, 15.84f, 17.36f, 17.84f, 18.36f, 18.84f, 19.36f, 19.84f,
				20.36f, 20.84f, 21.36f, 21.84f, 22.84f, 24.32f, 26.36f, 27.36f, 27.84f, 28.84f, 29.52f, 30.36f, 30.72f,
				31.44f, 32.2f, 32.84f, 33.36f, 34.36f, 35.36f, 36.36f, 37.36f, 38.36f, 38.84f, 39.84f, 40.84f, 41.84f,
				45.48f, 46.76f, 48.4f, 50.28f, 52f };
		Float[] LAnklePitchKeys = { 0.0623418f, 0.0623418f, 0.0623418f, -0.0312323f, -0.0879903f, -0.0879903f,
				-0.0281642f, -0.0158923f, -0.0158923f, -0.493928f, -0.430072f, -0.493928f, -0.418879f, 0.020944f,
				-0.767552f, -0.568977f, -0.0910584f, -0.298148f, -0.298148f, -0.293215f, -0.682424f, -0.781907f,
				-0.357974f, -0.425471f, -0.256563f, -0.280998f, -0.518363f, -0.167552f, -0.292008f, -0.0112903f,
				-0.121738f, -0.121738f, -0.489898f, -0.879534f, -0.879534f, -0.176962f, -0.176962f, -0.00361468f,
				-0.308887f, 0.0101913f, 0.0608078f, 0.21881f, 0.162052f, 0.207694f, 0.162052f, -0.0802851f, -0.155334f,
				-0.1309f, -0.549725f, -0.595157f, -0.643299f, 0.0654097f, 0.224945f, 0.224945f, -0.0419703f };

		Float[] LAnkleRollTimes = { 1.32f, 3.04f, 3.6f, 4.04f, 5.12f, 5.44f, 6.68f, 7.84f, 8.84f, 9.84f, 10.36f, 10.84f,
				11.36f, 12.24f, 13.04f, 13.84f, 14.84f, 15.84f, 17.36f, 17.84f, 18.36f, 18.84f, 19.36f, 19.84f, 20.36f,
				20.84f, 21.36f, 21.84f, 22.84f, 24.32f, 26.36f, 27.36f, 27.84f, 28.84f, 29.52f, 30.36f, 30.72f, 31.44f,
				32.2f, 32.84f, 33.36f, 34.36f, 35.36f, 36.36f, 37.36f, 38.36f, 38.84f, 39.84f, 40.84f, 41.84f, 45.48f,
				46.76f, 48.4f, 50.28f, 52f };
		Float[] LAnkleRollKeys = { -0.151844f, -0.151844f, -0.0605488f, 0.131201f, 0.151143f, 0.151143f, 0.154211f,
				-0.0973648f, -0.0973648f, 0.0729092f, 0.143473f, 0.0729092f, 0.143473f, -0.0436748f, 0.138871f,
				0.100521f, -0.0329368f, 0.0192192f, 0.0192192f, 0.0667732f, -0.137881f, -0.0558505f, -0.165806f,
				-0.0268008f, 0.0279253f, 0.0558505f, 0.179769f, 0.219911f, 0.0227781f, -0.193732f, 0.0483652f,
				0.0483652f, -0.194007f, -0.0959931f, -0.0988987f, -0.39968f, -0.328998f, 0.225266f, -0.0139626f,
				0.139362f, 0.0621712f, 0.0330253f, -0.0927629f, 0.00437015f, -0.0927629f, -0.00532477f, -0.00532477f,
				0.140405f, -0.0927629f, -0.226221f, -0.246163f, -0.0912287f, 0.0253553f, 0.0253553f, -0.0636167f };

		Float[] LElbowRollTimes = { 1.44f, 3.12f, 3.64f, 4.48f, 5.24f, 6.04f, 6.8f, 7.04f, 7.32f, 7.8f, 8.32f, 8.8f,
				9.28f, 9.8f, 10.32f, 10.8f, 11.32f, 11.8f, 12.32f, 12.8f, 13.32f, 13.72f, 14.04f, 14.8f, 15.32f, 15.8f,
				16.32f, 16.8f, 17.32f, 17.8f, 18.32f, 18.8f, 19.32f, 19.8f, 20.32f, 20.8f, 21.32f, 21.8f, 22.8f, 23.28f,
				23.8f, 24.28f, 24.8f, 25.32f, 25.8f, 26.32f, 26.8f, 27.32f, 27.8f, 28.8f, 29.32f, 29.8f, 30.32f, 30.8f,
				31.68f, 32.16f, 32.8f, 33.32f, 33.8f, 34.32f, 35.32f, 36.32f, 37.32f, 37.8f, 38.32f, 38.8f, 39.32f,
				39.8f, 40.32f, 40.8f, 41.32f, 41.8f, 45.4f, 46.64f, 48.28f, 50.24f, 51.32f, 52.32f };
		Float[] LElbowRollKeys = { 0f, 0f, -0.016832f, -0.424876f, -1.27778f, -1.04615f, -1.05995f, -0.743948f,
				-0.539926f, -0.342041f, -1.44345f, -0.800706f, -0.969447f, -0.6335f, -0.131882f, -0.6335f, -0.131882f,
				-1.05228f, -0.254602f, -0.00609404f, -0.984786f, -0.966378f, -0.745483f, 0f, -0.612024f, 0f, -0.612024f,
				0f, -0.612024f, -0.523053f, -1.06149f, -0.455555f, -0.952573f, -0.492371f, -1.06149f, -0.83147f,
				-1.36837f, -1.14594f, -0.472429f, -0.624296f, -0.424876f, -0.61049f, -0.424876f, -0.596684f, -0.424876f,
				-0.601287f, -0.429478f, -0.605888f, -1.23636f, -0.920358f, -0.196309f, -0.093532f, -0.914223f,
				-0.914223f, -0.906552f, -1.23943f, -0.742414f, -0.759288f, -0.00609404f, -0.711735f, -0.0183661f,
				-0.711735f, -0.0183661f, -0.601287f, -0.676451f, -0.585945f, -0.223922f, -0.271475f, -0.736278f,
				-1.03081f, -0.929562f, -1.03541f, -1.01853f, -1.02314f, 0f, 0f, -1.47106f, -0.710201f };

		Float[] LElbowYawTimes = { 1.44f, 3.12f, 3.64f, 4.48f, 5.24f, 6.04f, 6.8f, 7.04f, 7.32f, 7.8f, 8.32f, 8.8f,
				9.28f, 9.8f, 10.32f, 10.8f, 11.32f, 11.8f, 12.32f, 12.8f, 13.32f, 13.72f, 14.04f, 14.8f, 15.32f, 15.8f,
				16.32f, 16.8f, 17.32f, 17.8f, 18.32f, 18.8f, 19.32f, 19.8f, 20.32f, 20.8f, 21.32f, 21.8f, 22.8f, 23.28f,
				23.8f, 24.28f, 24.8f, 25.32f, 25.8f, 26.32f, 26.8f, 27.32f, 27.8f, 28.8f, 29.32f, 29.8f, 30.32f, 30.8f,
				31.68f, 32.16f, 32.8f, 33.32f, 33.8f, 34.32f, 35.32f, 36.32f, 37.32f, 37.8f, 38.32f, 38.8f, 39.32f,
				39.8f, 40.32f, 40.8f, 41.32f, 41.8f, 45.4f, 46.64f, 48.28f, 50.24f, 51.32f, 52.32f };
		Float[] LElbowYawKeys = { -1.13213f, -1.13213f, -1.13827f, -1.18429f, -1.21344f, -0.964928f, -0.667332f,
				-1.2932f, -2.0944f, -2.08782f, -0.744032f, -2.05101f, -0.0245859f, -0.483252f, -0.481718f, -0.483252f,
				-0.481718f, -0.38661f, 0.06592f, -0.029188f, -0.196393f, -0.200996f, -0.544613f, -0.420357f, -0.374338f,
				-0.420357f, -0.374338f, -0.420357f, -0.374338f, -0.819198f, -1.6629f, -0.918907f, 0.075124f, -0.553816f,
				-1.6629f, -0.941834f, -0.523053f, -2.08007f, -2.00037f, -1.51563f, -1.68591f, -1.5233f, -1.68591f,
				-1.53097f, -1.68591f, -1.52484f, -1.6767f, -1.51717f, -1.20577f, 0.877407f, 0.507713f, 0.500042f,
				0.883542f, 0.883542f, -0.259288f, 0.207048f, 0.447886f, -0.506262f, -0.231677f, -0.204064f, -0.213269f,
				-0.204064f, -0.213269f, -0.538476f, -2.0944f, -0.415757f, -2.0944f, -2.0944f, -1.76414f, -0.366667f,
				-1.92674f, -0.337522f, -0.335988f, -1.3699f, -2.01418f, -2.01418f, -0.457173f, -1.11066f };

		Float[] LHandTimes = { 1.4f, 3.08f, 3.44f, 4.4f, 5.16f, 6.64f, 7.2f, 8.24f, 8.72f, 9.2f, 9.72f, 10.72f, 11.72f,
				12.72f, 13.72f, 15.24f, 17.24f, 18.24f, 19.24f, 20.72f, 22.24f, 22.72f, 23.72f, 27.88f, 28.72f, 30.24f,
				32.72f, 33.72f, 37.24f, 38.24f, 39.24f, 45.36f, 50.2f, 51.16f };
		Float[] LHandKeys = { 0.674206f, 0.674206f, 0.674206f, 0.258935f, 0.258935f, 0.258935f, 0.256753f, 0.613843f,
				0.613843f, 0.613843f, 0.614934f, 0.613479f, 0.613479f, 0.257844f, 0.257844f, 0.796751f, 0.796751f,
				0.295298f, 0.295298f, 0.296389f, 0.295298f, 0.295298f, 0.237117f, 0.24148f, 0.574934f, 0.577116f,
				0.577479f, 0.577843f, 0.577479f, 0.577843f, 0.577479f, 0.577843f, 0.577843f, 0.221818f };

		Float[] LHipPitchTimes = { 1.32f, 3.04f, 3.6f, 4.04f, 5.12f, 5.44f, 6.68f, 7.84f, 8.84f, 9.84f, 10.36f, 10.84f,
				11.36f, 12.24f, 13.04f, 13.84f, 14.84f, 15.84f, 17.36f, 17.84f, 18.36f, 18.84f, 19.36f, 19.84f, 20.36f,
				20.84f, 21.36f, 21.84f, 22.84f, 24.32f, 26.36f, 27.36f, 27.84f, 28.84f, 29.52f, 30.36f, 30.72f, 31.44f,
				32.2f, 32.84f, 33.36f, 34.36f, 35.36f, 36.36f, 37.36f, 38.36f, 38.84f, 39.84f, 40.84f, 41.84f, 45.48f,
				46.76f, 48.4f, 50.28f, 52f };
		Float[] LHipPitchKeys = { -0.127294f, -0.127294f, -0.127294f, -0.0920118f, 0.497044f, 0.497044f, 0.343645f,
				0.274614f, 0.274614f, 0.34978f, 0.406538f, 0.34978f, 0.406538f, 0.0123002f, 0.193313f, 0.00923218f,
				0.340577f, 0.25774f, 0.25774f, -0.13343f, -0.168712f, -0.329782f, 0.0567862f, 0.262342f, -0.185867f,
				0.107127f, -0.139847f, 0.186895f, 0.26973f, 0.33444f, 0.398869f, 0.398869f, -0.0199139f, -0.361995f,
				-0.361995f, 0.139622f, 0.139622f, 0.298877f, 0.162632f, 0.289672f, 0.397335f, -0.38654f, -0.400345f,
				-0.354607f, -0.400345f, 0.306829f, 0.306829f, 0.424946f, 0.497044f, 0.223992f, 0.230129f, 0.200982f,
				-0.383472f, -0.383472f, 0.266945f };

		Float[] LHipRollTimes = { 1.32f, 3.04f, 3.6f, 4.04f, 5.12f, 5.44f, 6.68f, 7.84f, 8.84f, 9.84f, 10.36f, 10.84f,
				11.36f, 12.24f, 13.04f, 13.84f, 14.84f, 15.84f, 17.36f, 17.84f, 18.36f, 18.84f, 19.36f, 19.84f, 20.36f,
				20.84f, 21.36f, 21.84f, 22.84f, 24.32f, 26.36f, 27.36f, 27.84f, 28.84f, 29.52f, 30.36f, 30.72f, 31.44f,
				32.2f, 32.84f, 33.36f, 34.36f, 35.36f, 36.36f, 37.36f, 38.36f, 38.84f, 39.84f, 40.84f, 41.84f, 45.48f,
				46.76f, 48.4f, 50.28f, 52f };
		Float[] LHipRollKeys = { 0.0610059f, 0.0610059f, 0.0610059f, -0.147618f, -0.189037f, -0.189037f, -0.127676f,
				0.134638f, 0.134638f, 0.014986f, 0.0870839f, 0.014986f, 0.0870839f, 0.073278f, 0.014986f, -0.0816561f,
				0.090152f, 0.0671419f, 0.0671419f, -0.0555781f, 0.39235f, 0.226678f, 0.16225f, 0.0870839f, -0.0496235f,
				-0.419317f, -0.345685f, -0.255179f, 0.0853684f, 0.295708f, 0.014986f, 0.014986f, 0.389282f, 0.754373f,
				0.754373f, 0.631654f, 0.631654f, -0.359491f, -0.397659f, -0.195353f, -0.077054f, -0.130744f, 0.0119179f,
				-0.18615f, 0.0119179f, 0.0349279f, 0.0349279f, -0.179832f, 0.212873f, 0.536546f, 0.550351f, 0.134638f,
				-0.0893261f, -0.0893261f, 0.133104f };

		Float[] LHipYawPitchTimes = { 1.32f, 3.04f, 3.6f, 4.04f, 5.12f, 5.44f, 6.68f, 7.84f, 8.84f, 9.84f, 10.36f,
				10.84f, 11.36f, 12.24f, 13.04f, 13.84f, 14.84f, 15.84f, 17.36f, 17.84f, 18.36f, 18.84f, 19.36f, 19.84f,
				20.36f, 20.84f, 21.36f, 21.84f, 22.84f, 24.32f, 26.36f, 27.36f, 27.84f, 28.84f, 29.52f, 30.36f, 30.72f,
				31.44f, 32.2f, 32.84f, 33.36f, 34.36f, 35.36f, 36.36f, 37.36f, 38.36f, 38.84f, 39.84f, 40.84f, 41.84f,
				45.48f, 46.76f, 48.4f, 50.28f, 52f };
		Float[] LHipYawPitchKeys = { -0.576742f, -0.576742f, -0.576742f, -0.576742f, -0.404934f, -0.404934f, -0.455555f,
				-0.289883f, -0.289883f, -0.54146f, -0.959931f, -0.54146f, -0.959931f, -0.496974f, -0.493905f,
				-0.404934f, -0.360449f, -0.384992f, -0.384992f, -0.483168f, -0.391128f, -0.340507f, -0.303691f,
				-0.354312f, -0.483168f, -0.391128f, -0.340507f, -0.303691f, -0.384992f, -0.401866f, -0.384992f,
				-0.384992f, -0.358915f, -0.628898f, -0.628898f, -0.582879f, -0.582879f, -0.358915f, -0.674919f,
				-0.582879f, -0.443284f, -0.500042f, -0.506179f, -0.506179f, -0.506179f, -0.329768f, -0.329768f,
				-0.489305f, -0.277612f, -0.354312f, -0.361981f, -0.253067f, -0.398797f, -0.398797f, -0.470897f };

		Float[] LKneePitchTimes = { 1.32f, 3.04f, 3.6f, 4.04f, 5.12f, 5.44f, 6.68f, 7.84f, 8.84f, 9.84f, 10.36f, 10.84f,
				11.36f, 12.24f, 13.04f, 13.84f, 14.84f, 15.84f, 17.36f, 17.84f, 18.36f, 18.84f, 19.36f, 19.84f, 20.36f,
				20.84f, 21.36f, 21.84f, 22.84f, 24.32f, 26.36f, 27.36f, 27.84f, 28.84f, 29.52f, 30.36f, 30.72f, 31.44f,
				32.2f, 32.84f, 33.36f, 34.36f, 35.36f, 36.36f, 37.36f, 38.36f, 38.84f, 39.84f, 40.84f, 41.84f, 45.48f,
				46.76f, 48.4f, 50.28f, 52f };
		Float[] LKneePitchKeys = { 0.212592f, 0.212592f, 0.212592f, 0.430419f, 0.0177736f, 0.0177736f, 0.0116376f,
				0.0346476f, 0.0346476f, 0.692733f, 0.67586f, 0.692733f, 0.67586f, 0.34605f, 1.02254f, 0.965785f,
				0.0714636f, 0.405876f, 0.405876f, 0.671257f, 1.14373f, 1.33343f, 0.497419f, 0.494848f, 0.699619f,
				0.540082f, 1.00949f, 0.268564f, 0.388217f, 0.0300457f, 0.109814f, 0.109814f, 0.729549f, 1.58552f,
				1.58552f, 0.408943f, 0.408943f, 0.0292604f, 0.849202f, 0.181127f, 0.0147056f, 0.0423176f, 0.0591916f,
				0.0538045f, 0.0591916f, 0.0315796f, 0.0315796f, 0.183446f, 0.493314f, 0.766365f, 0.738754f, 0.0131716f,
				0.0269776f, 0.0269776f, 0.114416f };

		Float[] LShoulderPitchTimes = { 1.44f, 3.12f, 3.64f, 4.48f, 5.24f, 6.04f, 6.8f, 7.04f, 7.32f, 7.8f, 8.32f, 8.8f,
				9.28f, 9.8f, 10.32f, 10.8f, 11.32f, 11.8f, 12.32f, 12.8f, 13.32f, 13.72f, 14.04f, 14.8f, 15.32f, 15.8f,
				16.32f, 16.8f, 17.32f, 17.8f, 18.32f, 18.8f, 19.32f, 19.8f, 20.32f, 20.8f, 21.32f, 21.8f, 22.8f, 23.28f,
				23.8f, 24.28f, 24.8f, 25.32f, 25.8f, 26.32f, 26.8f, 27.32f, 27.8f, 28.8f, 29.32f, 29.8f, 30.32f, 30.8f,
				31.68f, 32.16f, 32.8f, 33.32f, 33.8f, 34.32f, 35.32f, 36.32f, 37.32f, 37.8f, 38.32f, 38.8f, 39.32f,
				39.8f, 40.32f, 40.8f, 41.32f, 41.8f, 45.4f, 46.64f, 48.28f, 50.24f, 51.32f, 52.32f };
		Float[] LShoulderPitchKeys = { 1.08143f, 1.08143f, 1.12898f, 1.52169f, 1.95733f, 1.61986f, 1.35601f, 1.37135f,
				1.31613f, 1.55697f, 1.72417f, 1.54623f, 0.651908f, 1.64287f, 1.64287f, 1.64287f, 1.64287f, 1.15199f,
				1.29159f, 1.09523f, 1.04461f, 1.07529f, 1.34374f, -1.05237f, -0.823801f, -1.05237f, -0.823801f,
				-1.05237f, -0.823801f, -0.510863f, 1.3913f, 2.05704f, 2.0944f, 1.73491f, 1.3913f, 1.40058f, 0.704148f,
				0.808459f, 1.59072f, 1.57998f, 1.61066f, 1.60299f, 1.60912f, 1.60912f, 1.60912f, 1.60452f, 1.60452f,
				1.60606f, 1.39436f, -0.0706059f, 0.394197f, 0.394197f, -0.019984f, -0.019984f, 0.926494f, 0.555266f,
				0.139552f, -0.243948f, 0.917291f, 0.889678f, 0.88661f, 0.889678f, 0.88661f, 1.03081f, 0.29602f,
				-1.37297f, 0.256136f, 0.608956f, -0.10282f, -0.401949f, 0.312894f, 1.34067f, 1.35448f, 1.48487f,
				1.54163f, 1.54163f, 1.27778f, 1.59532f };

		Float[] LShoulderRollTimes = { 1.44f, 3.12f, 3.64f, 4.48f, 5.24f, 6.04f, 6.8f, 7.04f, 7.32f, 7.8f, 8.32f, 8.8f,
				9.28f, 9.8f, 10.32f, 10.8f, 11.32f, 11.8f, 12.32f, 12.8f, 13.32f, 13.72f, 14.04f, 14.8f, 15.32f, 15.8f,
				16.32f, 16.8f, 17.32f, 17.8f, 18.32f, 18.8f, 19.32f, 19.8f, 20.32f, 20.8f, 21.32f, 21.8f, 22.8f, 23.28f,
				23.8f, 24.28f, 24.8f, 25.32f, 25.8f, 26.32f, 26.8f, 27.32f, 27.8f, 28.8f, 29.32f, 29.8f, 30.32f, 30.8f,
				31.68f, 32.16f, 32.8f, 33.32f, 33.8f, 34.32f, 35.32f, 36.32f, 37.32f, 37.8f, 38.32f, 38.8f, 39.32f,
				39.8f, 40.32f, 40.8f, 41.32f, 41.8f, 45.4f, 46.64f, 48.28f, 50.24f, 51.32f, 52.32f };
		Float[] LShoulderRollKeys = { 0.085862f, 0.085862f, 0.115008f, 0.118076f, 0.223922f, 0.0291041f, 0.021434f,
				0.021434f, 0.075124f, 0.0260361f, 0.789967f, 0.484702f, 0.021434f, 0.911154f, 1.18881f, 0.911154f,
				1.18881f, 0.0551821f, 1.48027f, 0.144154f, 0.0275701f, 0.0352401f, 0.021434f, 0.432547f, 0.0352401f,
				0.432547f, 0.0352401f, 0.432547f, 0.0352401f, 0.507713f, 0.481634f, 0.51845f, 0.461692f, 0.719404f,
				0.481634f, 0.01078f, 0.016916f, 0.00771196f, 0.0643861f, 0.102736f, 0.0904641f, 0.0904641f, 0.0904641f,
				0.0904641f, 0.091998f, 0.0904641f, 0.0904641f, 0.093532f, 0.409536f, 0.052114f, 0.552198f, 0.731675f,
				0.076658f, 0.076658f, 0.977116f, 0.668782f, 1.43118f, 1.36982f, 0.207048f, 0.021434f, 0.863599f,
				0.021434f, 0.863599f, 0.358915f, 0.658043f, 0.187106f, 0.831386f, 0.751617f, 0.616627f, 0.033706f,
				0.613558f, 0.536858f, 0.549129f, 0.383458f, 0.438682f, 0.438682f, 0.391128f, 0.199378f };

		Float[] LWristYawTimes = { 1.4f, 3.08f, 3.44f, 4.4f, 5.16f, 6.64f, 7.2f, 7.72f, 8.24f, 8.72f, 9.2f, 9.72f,
				10.72f, 11.72f, 12.72f, 13.72f, 15.24f, 17.24f, 18.24f, 19.24f, 20.72f, 22.24f, 22.72f, 23.72f, 27.88f,
				28.72f, 30.24f, 32.72f, 33.72f, 37.24f, 38.24f, 39.24f, 45.36f, 50.2f };
		Float[] LWristYawKeys = { 0.415673f, 0.415673f, 0.243864f, -0.188724f, 0.351244f, 0.265341f, 1.10904f,
				-0.570723f, -0.79312f, -0.813062f, -0.79312f, -1.42666f, -1.15514f, -1.82551f, -0.93118f, -0.0767419f,
				-1.30394f, -1.30394f, 0.676451f, 0.839057f, 0.118076f, 0.136484f, 0.136484f, 0.567537f, 0.550664f,
				-0.231677f, -0.236277f, -0.22554f, -0.374338f, -0.372804f, -0.558418f, -0.673468f, -0.667332f,
				-0.667332f };

		Float[] RAnklePitchTimes = { 1.32f, 3.04f, 3.6f, 4.04f, 5.12f, 5.44f, 6.68f, 7.84f, 8.84f, 9.84f, 10.36f,
				10.84f, 11.36f, 12.24f, 13.04f, 13.84f, 14.84f, 15.84f, 17.36f, 17.84f, 18.36f, 18.84f, 19.36f, 19.84f,
				20.36f, 20.84f, 21.36f, 21.84f, 22.84f, 24.32f, 26.36f, 27.36f, 27.84f, 28.84f, 29.52f, 30.36f, 30.72f,
				31.44f, 32.2f, 32.84f, 33.36f, 34.36f, 35.36f, 36.36f, 37.36f, 38.36f, 38.84f, 39.84f, 40.84f, 41.84f,
				45.48f, 46.76f, 48.4f, 50.28f, 52f };
		Float[] RAnklePitchKeys = { -0.245987f, -0.245987f, -0.245987f, -0.426998f, -0.353366f, -0.353366f,
				-0.00668269f, 0.00098731f, 0.00098731f, -0.0802851f, 0.258309f, -0.0802851f, 0.246091f, -0.31765f,
				-0.594204f, 0.0575959f, -0.0787807f, -0.292008f, -0.292008f, -0.256563f, -0.293215f, -0.518363f,
				-0.207637f, -0.456145f, -0.293215f, -0.670206f, -0.681649f, -0.305433f, -0.298148f, -0.0358286f,
				-0.0296928f, -0.0296928f, -0.00361468f, -0.205949f, -0.179769f, 0.0101913f, 0.0101913f, -0.489898f,
				-0.86879f, -0.176962f, -0.0726446f, 0.181999f, 0.207694f, 0.162052f, 0.207694f, -0.116937f, -0.179769f,
				-0.1309f, -0.509835f, 0.144862f, 0.0746193f, -0.0404307f, 0.175863f, 0.175863f, 0.0332012f };

		Float[] RAnkleRollTimes = { 1.32f, 3.04f, 3.6f, 4.04f, 5.12f, 5.44f, 6.68f, 7.84f, 8.84f, 9.84f, 10.36f, 10.84f,
				11.36f, 12.24f, 13.04f, 13.84f, 14.84f, 15.84f, 17.36f, 17.84f, 18.36f, 18.84f, 19.36f, 19.84f, 20.36f,
				20.84f, 21.36f, 21.84f, 22.84f, 24.32f, 26.36f, 27.36f, 27.84f, 28.84f, 29.52f, 30.36f, 30.72f, 31.44f,
				32.2f, 32.84f, 33.36f, 34.36f, 35.36f, 36.36f, 37.36f, 38.36f, 38.84f, 39.84f, 40.84f, 41.84f, 45.48f,
				46.76f, 48.4f, 50.28f, 52f };
		Float[] RAnkleRollKeys = { -0.0657301f, -0.0657301f, -0.0657301f, 0.179769f, 0.179769f, 0.179769f, 0.233874f,
				0.179769f, 0.179769f, 0.261799f, 0.303687f, 0.303687f, 0.371755f, 0.233874f, 0.289725f, 0.579449f,
				0.165806f, 0.0680678f, 0.0558505f, -0.0933421f, -0.0558505f, -0.137881f, -0.151844f, 0.0278439f,
				0.0139626f, 0.137881f, 0.109637f, 0.179769f, -0.0192192f, -0.0780021f, 0.151844f, 0.0355138f,
				-0.225266f, 0.0680678f, 0.0558505f, -0.139362f, -0.139362f, 0.194007f, 0.106078f, 0.328998f, 0.34383f,
				0.165806f, -0.00437015f, 0.0927629f, -0.00437015f, 0.137881f, 0.205949f, 0.39968f, 0.0416498f,
				0.179769f, 0.136758f, 0.0661939f, -0.0289142f, -0.0289142f, -0.0749341f };

		Float[] RElbowRollTimes = { 1.44f, 3.12f, 3.64f, 4.48f, 5.24f, 6.04f, 6.8f, 7.04f, 7.32f, 7.8f, 8.32f, 8.8f,
				9.28f, 9.8f, 10.32f, 10.8f, 11.32f, 11.8f, 12.32f, 12.8f, 13.32f, 13.72f, 14.04f, 14.8f, 15.32f, 15.8f,
				16.32f, 16.8f, 17.32f, 17.8f, 18.32f, 18.8f, 19.32f, 19.8f, 20.32f, 20.8f, 21.32f, 21.8f, 22.8f, 23.28f,
				23.8f, 24.28f, 24.8f, 25.32f, 25.8f, 26.32f, 26.8f, 27.32f, 27.8f, 28.8f, 29.32f, 29.8f, 30.32f, 30.8f,
				31.68f, 32.16f, 32.8f, 33.32f, 33.8f, 34.32f, 35.32f, 36.32f, 37.32f, 37.8f, 38.32f, 38.8f, 39.32f,
				39.8f, 40.32f, 40.8f, 41.32f, 41.8f, 45.4f, 46.64f, 48.28f, 50.24f, 51.32f, 52.32f };
		Float[] RElbowRollKeys = { 1.55552f, 1.55552f, 1.54018f, 0.449504f, 1.02322f, 1.07384f, 1.55245f, 1.09685f,
				1.46501f, 0.446436f, 1.24718f, 0.673468f, 0.833004f, 0.398883f, 0.658129f, 0.398883f, 0.658129f,
				1.17355f, 0.521602f, 0.751701f, 0.023052f, 0.636652f, 1.11833f, 4.19617e-05f, 0.656595f, 4.19617e-05f,
				0.656595f, 4.19617e-05f, 0.656595f, 0.598302f, 0.980268f, 0.83147f, 1.36837f, 1.14594f, 0.980268f,
				0.455555f, 0.952573f, 0.492371f, 1.52944f, 0.89283f, 1.48649f, 0.994073f, 1.48956f, 0.994073f, 1.48956f,
				0.981802f, 1.46961f, 0.966462f, 1.55092f, 0.765508f, 1.55552f, 1.56319f, 0.691876f, 0.691876f,
				0.503194f, 0.582963f, 0.925044f, 1.26866f, 0.506262f, 0.019984f, 0.628982f, 0.019984f, 0.628982f,
				0.43263f, 0.717953f, 0.265424f, 1.17662f, 1.33155f, 1.44047f, 0.443368f, 1.35303f, 0.823801f, 0.840674f,
				1.18429f, 1.13674f, 1.13674f, 1.29627f, 0.687274f };

		Float[] RElbowYawTimes = { 1.44f, 3.12f, 3.64f, 4.48f, 5.24f, 6.04f, 6.8f, 7.04f, 7.32f, 7.8f, 8.32f, 8.8f,
				9.28f, 9.8f, 10.32f, 10.8f, 11.32f, 11.8f, 12.32f, 12.8f, 13.32f, 13.72f, 14.04f, 14.8f, 15.32f, 15.8f,
				16.32f, 16.8f, 17.32f, 17.8f, 18.32f, 18.8f, 19.32f, 19.8f, 20.32f, 20.8f, 21.32f, 21.8f, 22.8f, 23.28f,
				23.8f, 24.28f, 24.8f, 25.32f, 25.8f, 26.32f, 26.8f, 27.32f, 27.8f, 28.8f, 29.32f, 29.8f, 30.32f, 30.8f,
				31.68f, 32.16f, 32.8f, 33.32f, 33.8f, 34.32f, 35.32f, 36.32f, 37.32f, 37.8f, 38.32f, 38.8f, 39.32f,
				39.8f, 40.32f, 40.8f, 41.32f, 41.8f, 45.4f, 46.64f, 48.28f, 50.24f, 51.32f, 52.32f };
		Float[] RElbowYawKeys = { 0.662646f, 0.662646f, 1.44038f, 2.0908f, 1.99262f, 0.872804f, 0.569072f, 1.38209f,
				2.0908f, 2.08773f, 0.785367f, 2.08926f, -0.273093f, 0.516916f, 1.30539f, 0.516916f, 1.30539f, 0.555266f,
				1.18114f, 0.96331f, 0.955639f, 1.3238f, 0.593616f, 0.233125f, 0.170232f, 0.233125f, 0.170232f,
				0.233125f, 0.170232f, 0.984786f, 2.08007f, 0.941834f, 0.523053f, 2.08007f, 2.08007f, 0.918907f,
				-0.075124f, 0.553816f, 0.944902f, 2.0816f, 0.757754f, 2.0944f, 0.75622f, 2.07853f, 0.743948f, 2.07239f,
				0.74088f, 2.07699f, 0.450955f, -0.306841f, -0.405018f, -0.234743f, -0.306841f, -0.306841f, -0.217869f,
				0.282215f, -0.214801f, 0.628898f, 0.354312f, 0.615092f, 0.369652f, 0.615092f, 0.369652f, 0.575208f,
				2.09387f, 0.825249f, 0.33437f, -0.055266f, -1.0493f, 1.31153f, 0.793036f, -0.119694f, -0.11049f,
				1.15199f, 0.11961f, 0.11961f, 0.357381f, 1.18881f };

		Float[] RHandTimes = { 1.4f, 3.08f, 3.44f, 4.4f, 5.16f, 6.64f, 7.2f, 8.24f, 8.72f, 9.2f, 9.72f, 10.72f, 11.72f,
				12.72f, 13.72f, 15.24f, 17.24f, 18.24f, 19.24f, 20.72f, 22.24f, 22.72f, 23.72f, 24.24f, 24.76f, 25.52f,
				26.28f, 27f, 27.88f, 28.72f, 30.24f, 32.72f, 33.72f, 37.24f, 38.24f, 39.24f, 45.36f, 50.2f, 51.16f };
		Float[] RHandKeys = { 0.178571f, 0.178571f, 0.537479f, 0.381116f, 0.382207f, 0.382207f, 0.382207f, 0.585115f,
				0.585115f, 0.585115f, 0.587661f, 0.588025f, 0.587661f, 0.249844f, 0.249844f, 0.849115f, 0.849115f,
				0.340389f, 0.340389f, 0.340753f, 0.340389f, 0.340389f, 0.247273f, 0.630909f, 0.370909f, 0.723636f,
				0.469091f, 0.749091f, 0.469091f, 0.808388f, 0.810933f, 0.81166f, 0.81166f, 0.811297f, 0.811297f,
				0.814206f, 0.81166f, 0.81166f, 0.287273f };

		Float[] RHipPitchTimes = { 1.32f, 3.04f, 3.6f, 4.04f, 5.12f, 5.44f, 6.68f, 7.84f, 8.84f, 9.84f, 10.36f, 10.84f,
				11.36f, 12.24f, 13.04f, 13.84f, 14.84f, 15.84f, 17.36f, 17.84f, 18.36f, 18.84f, 19.36f, 19.84f, 20.36f,
				20.84f, 21.36f, 21.84f, 22.84f, 24.32f, 26.36f, 27.36f, 27.84f, 28.84f, 29.52f, 30.36f, 30.72f, 31.44f,
				32.2f, 32.84f, 33.36f, 34.36f, 35.36f, 36.36f, 37.36f, 38.36f, 38.84f, 39.84f, 40.84f, 41.84f, 45.48f,
				46.76f, 48.4f, 50.28f, 52f };
		Float[] RHipPitchKeys = { -0.0386032f, -0.0386032f, -0.0386032f, -0.0309332f, 0.0595728f, 0.0595728f, 0.202235f,
				0.277401f, 0.277401f, 0.0288929f, 0.355635f, 0.0288929f, 0.355635f, 0.153147f, 0.176156f, 0.337227f,
				0.355635f, 0.26973f, 0.26973f, -0.185867f, 0.107127f, -0.139847f, 0.186895f, 0.21144f, -0.13343f,
				-0.168712f, -0.329782f, 0.0567862f, 0.25774f, 0.397054f, 0.409325f, 0.409325f, 0.298877f, 0.165419f,
				0.165419f, 0.289672f, 0.289672f, -0.0199139f, -0.366879f, 0.139622f, 0.435402f, -0.331597f, -0.354607f,
				-0.400345f, -0.354607f, 0.317285f, 0.317285f, 0.435402f, 0.498297f, 0.337227f, 0.357169f, 0.183827f,
				-0.353073f, -0.353073f, 0.309614f };

		Float[] RHipRollTimes = { 1.32f, 3.04f, 3.6f, 4.04f, 5.12f, 5.44f, 6.68f, 7.84f, 8.84f, 9.84f, 10.36f, 10.84f,
				11.36f, 12.24f, 13.04f, 13.84f, 14.84f, 15.84f, 17.36f, 17.84f, 18.36f, 18.84f, 19.36f, 19.84f, 20.36f,
				20.84f, 21.36f, 21.84f, 22.84f, 24.32f, 26.36f, 27.36f, 27.84f, 28.84f, 29.52f, 30.36f, 30.72f, 31.44f,
				32.2f, 32.84f, 33.36f, 34.36f, 35.36f, 36.36f, 37.36f, 38.36f, 38.84f, 39.84f, 40.84f, 41.84f, 45.48f,
				46.76f, 48.4f, 50.28f, 52f };
		Float[] RHipRollKeys = { 0.0849055f, 0.0849055f, 0.0849055f, -0.206554f, -0.301663f, -0.301663f, -0.221894f,
				-0.159001f, -0.159001f, -0.439722f, -0.648346f, -0.439722f, -0.648346f, -0.341547f, -0.33541f,
				-0.622269f, -0.119116f, -0.0853684f, -0.0853684f, 0.0496235f, 0.419317f, 0.345685f, 0.255179f,
				-0.131389f, 0.0555781f, -0.39235f, -0.226678f, -0.16225f, -0.0671419f, 0.0511575f, -0.148262f,
				-0.148262f, 0.359491f, 0.382501f, 0.382501f, 0.195353f, 0.195353f, -0.389282f, -0.734251f, -0.631654f,
				-0.488811f, 0.0204776f, 0.18615f, -0.0119179f, 0.18615f, -0.178942f, -0.178942f, -0.531762f, -0.119116f,
				0.00973951f, -0.0163385f, -0.129854f, 0.103314f, 0.103314f, 0.00973951f };

		Float[] RKneePitchTimes = { 1.32f, 3.04f, 3.6f, 4.04f, 5.12f, 5.44f, 6.68f, 7.84f, 8.84f, 9.84f, 10.36f, 10.84f,
				11.36f, 12.24f, 13.04f, 13.84f, 14.84f, 15.84f, 17.36f, 17.84f, 18.36f, 18.84f, 19.36f, 19.84f, 20.36f,
				20.84f, 21.36f, 21.84f, 22.84f, 24.32f, 26.36f, 27.36f, 27.84f, 28.84f, 29.52f, 30.36f, 30.72f, 31.44f,
				32.2f, 32.84f, 33.36f, 34.36f, 35.36f, 36.36f, 37.36f, 38.36f, 38.84f, 39.84f, 40.84f, 41.84f, 45.48f,
				46.76f, 48.4f, 50.28f, 52f };
		Float[] RKneePitchKeys = { 0.41276f, 0.41276f, 0.41276f, 0.691948f, 0.6582f, 0.6582f, 0.0844844f, 0.0277265f,
				0.0277265f, 0.606045f, 0.142776f, 0.606045f, 0.142776f, 0.503266f, 0.905175f, 0.0277265f, 0.0476684f,
				0.388217f, 0.388217f, 0.699619f, 0.540082f, 1.00949f, 0.268564f, 0.561558f, 0.671257f, 1.14373f,
				1.33343f, 0.497419f, 0.405876f, 0.0307944f, 0.0292604f, 0.0292604f, 0.0292604f, 0.822338f, 0.822338f,
				0.181127f, 0.181127f, 0.729549f, 1.52951f, 0.408943f, 0.0461345f, 0.0246584f, 0.0538045f, 0.0591916f,
				0.0538045f, 0.0399984f, 0.0399984f, 0.173456f, 0.475655f, 0.0630085f, 0.0323284f, 0.115164f, 0.0323284f,
				0.0323284f, 0.0292604f };

		Float[] RShoulderPitchTimes = { 1.44f, 3.12f, 3.64f, 4.48f, 5.24f, 6.04f, 6.8f, 7.04f, 7.32f, 7.8f, 8.32f, 8.8f,
				9.28f, 9.8f, 10.32f, 10.8f, 11.32f, 11.8f, 12.32f, 12.8f, 13.32f, 13.72f, 14.04f, 14.8f, 15.32f, 15.8f,
				16.32f, 16.8f, 17.32f, 17.8f, 18.32f, 18.8f, 19.32f, 19.8f, 20.32f, 20.8f, 21.32f, 21.8f, 22.8f, 23.28f,
				23.8f, 24.28f, 24.8f, 25.32f, 25.8f, 26.32f, 26.8f, 27.32f, 27.8f, 28.8f, 29.32f, 29.8f, 30.32f, 30.8f,
				31.68f, 32.16f, 32.8f, 33.32f, 33.8f, 34.32f, 35.32f, 36.32f, 37.32f, 37.8f, 38.32f, 38.8f, 39.32f,
				39.8f, 40.32f, 40.8f, 41.32f, 41.8f, 45.4f, 46.64f, 48.28f, 50.24f, 51.32f, 52.32f };
		Float[] RShoulderPitchKeys = { 0.38661f, 0.38661f, 0.515466f, 1.60614f, 2.07555f, 1.44814f, 0.664264f, 1.2119f,
				0.952657f, 1.68591f, 1.78715f, 1.5095f, 0.96186f, 1.17815f, 1.2932f, 1.17815f, 1.2932f, 0.719487f,
				1.91908f, 2.08321f, 2.08782f, 1.48495f, 1.24258f, -1.11978f, -0.926494f, -1.11978f, -0.926494f,
				-1.11978f, -0.926494f, -0.404934f, 1.40979f, 1.40058f, 0.704148f, 0.808459f, 1.40979f, 2.05704f,
				2.0944f, 1.73491f, 1.34536f, 1.88839f, 1.3561f, 1.45427f, 1.38524f, 1.44967f, 1.40058f, 1.45734f,
				1.41899f, 1.46961f, 1.16895f, 0.247016f, 0.254685f, 0.273093f, 0.179519f, 0.179519f, 0.728692f,
				0.90817f, 0.211735f, 0.289967f, 0.948054f, 1.11373f, 0.823801f, 1.11373f, 0.823801f, 0.987938f,
				0.154976f, -1.19801f, -0.507713f, -0.506179f, -0.361981f, 0.335988f, 0.365133f, -1.44959f, -1.43885f,
				0.648924f, 0.935783f, 0.935783f, 1.18889f, 1.67977f };

		Float[] RShoulderRollTimes = { 1.44f, 3.12f, 3.64f, 4.48f, 5.24f, 6.04f, 6.8f, 7.04f, 7.32f, 7.8f, 8.32f, 8.8f,
				9.28f, 9.8f, 10.32f, 10.8f, 11.32f, 11.8f, 12.32f, 12.8f, 13.32f, 13.72f, 14.04f, 14.8f, 15.32f, 15.8f,
				16.32f, 16.8f, 17.32f, 17.8f, 18.32f, 18.8f, 19.32f, 19.8f, 20.32f, 20.8f, 21.32f, 21.8f, 22.8f, 23.28f,
				23.8f, 24.28f, 24.8f, 25.32f, 25.8f, 26.32f, 26.8f, 27.32f, 27.8f, 28.8f, 29.32f, 29.8f, 30.32f, 30.8f,
				31.68f, 32.16f, 32.8f, 33.32f, 33.8f, 34.32f, 35.32f, 36.32f, 37.32f, 37.8f, 38.32f, 38.8f, 39.32f,
				39.8f, 40.32f, 40.8f, 41.32f, 41.8f, 45.4f, 46.64f, 48.28f, 50.24f, 51.32f, 52.32f };
		Float[] RShoulderRollKeys = { -0.015382f, -0.015382f, -0.016916f, -0.039926f, -0.00464395f, -0.00924597f,
				-0.00771196f, -0.0798099f, -0.0890139f, -0.029188f, -0.710284f, -0.382007f, -0.01078f, -1.63068f,
				-1.57546f, -1.63068f, -1.57546f, -0.00310997f, -0.581429f, -0.848343f, -1.19503f, -0.434165f,
				-0.00464395f, -0.454107f, -0.0245859f, -0.454107f, -0.0245859f, -0.454107f, -0.0245859f, -0.604437f,
				-0.401949f, -0.01078f, -0.016916f, -0.00771196f, -0.401949f, -0.51845f, -0.461692f, -0.719404f,
				-0.112024f, -0.104354f, -0.570689f, -0.0706059f, -0.55535f, -0.0767419f, -0.543078f, -0.073674f,
				-0.526205f, -0.067538f, -0.642787f, -0.886694f, -0.612108f, -0.487854f, -1.00941f, -1.00941f,
				-0.00924597f, -0.303775f, -0.047596f, -0.04913f, -0.0537319f, -0.679603f, -0.019984f, -0.679603f,
				-0.019984f, -0.242414f, -0.400415f, -0.544613f, -0.285367f, -0.181053f, -0.972599f, -1.24872f,
				-0.846809f, -0.47865f, -0.480184f, -0.027654f, -0.00924597f, -0.00924597f, -0.33292f, -0.22554f };

		Float[] RWristYawTimes = { 1.4f, 3.08f, 3.44f, 4.4f, 5.16f, 6.64f, 7.2f, 8.24f, 8.72f, 9.2f, 9.72f, 10.72f,
				11.72f, 12.72f, 13.72f, 15.24f, 17.24f, 18.24f, 19.24f, 20.72f, 22.24f, 22.72f, 23.72f, 27.88f, 28.72f,
				30.24f, 32.72f, 33.72f, 37.24f, 38.24f, 39.24f, 45.36f, 50.2f };
		Float[] RWristYawKeys = { 0.37272f, 0.37272f, -0.0951499f, 0.18097f, 0.748551f, 0.85593f, 1.03387f, 0.826783f,
				0.849794f, 0.826783f, 1.0937f, 0.27301f, 1.43271f, 1.23636f, 1.61833f, 1.31153f, 1.31153f, -0.421891f,
				0.604353f, -0.366667f, -0.366667f, -0.366667f, 1.04921f, 1.04921f, 0.731675f, 0.731675f, 0.733209f,
				-0.0414599f, -0.0445279f, 0.429478f, 0.984786f, 0.989389f, 0.989389f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación thriller", "thriller");
		}
	}

	/**
	 * NAO mueve sus brazos hacia atrás como si quisiera pelea. Esta es una
	 * operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),go_irish]
	 */
	@OPERATION
	public void goIrish() {
		Float[] HeadPitchTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] HeadPitchKeys = { 0.145688f, 0.133416f, 0.133416f, 0.133416f, 0.133416f };

		Float[] HeadYawTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] HeadYawKeys = { 0.024502f, 0.0183661f, 0.016832f, 0.0183661f, 0.016832f };

		Float[] LAnklePitchTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] LAnklePitchKeys = { -0.173384f, -0.173384f, -0.173384f, -0.173384f, -0.173384f };

		Float[] LAnkleRollTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] LAnkleRollKeys = { -0.0183661f, -0.0183661f, -0.0183661f, -0.0183661f, -0.0183661f };

		Float[] LElbowRollTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] LElbowRollKeys = { -1.55083f, -1.56004f, -1.55237f, -1.56004f, -1.55237f };

		Float[] LElbowYawTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] LElbowYawKeys = { -1.18582f, -1.17202f, -1.25332f, -1.17202f, -1.25332f };

		Float[] LHandTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] LHandKeys = { 0.0251173f, 0.0251173f, 0.0251173f, 0.0251173f, 0.0251173f };

		Float[] LHipPitchTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] LHipPitchKeys = { -0.291418f, -0.289883f, -0.291418f, -0.289883f, -0.291418f };

		Float[] LHipRollTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] LHipRollKeys = { -0.049046f, -0.049046f, -0.049046f, -0.049046f, -0.049046f };

		Float[] LHipYawPitchTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] LHipYawPitchKeys = { -0.162562f, -0.162562f, -0.162562f, -0.162562f, -0.162562f };

		Float[] LKneePitchTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] LKneePitchKeys = { 0.532256f, 0.532256f, 0.532256f, 0.532256f, 0.532256f };

		Float[] LShoulderPitchTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] LShoulderPitchKeys = { 1.22869f, 0.674919f, 1.64594f, 0.674919f, 1.64594f };

		Float[] LShoulderRollTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] LShoulderRollKeys = { 0.00872665f, 0.00872665f, 0.00872665f, 0.00872665f, 0.00872665f };

		Float[] LWristYawTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] LWristYawKeys = { -1.11373f, -1.10606f, -1.10606f, -1.10606f, -1.10606f };

		Float[] RAnklePitchTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] RAnklePitchKeys = { -0.380389f, -0.380389f, -0.380389f, -0.380389f, -0.380389f };

		Float[] RAnkleRollTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] RAnkleRollKeys = { 0.039926f, 0.039926f, 0.039926f, 0.039926f, 0.039926f };

		Float[] RElbowRollTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] RElbowRollKeys = { 1.56165f, 1.56012f, 1.56012f, 1.56012f, 1.56012f };

		Float[] RElbowYawTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] RElbowYawKeys = { 1.1704f, 1.16733f, 1.24863f, 1.16733f, 1.24863f };

		Float[] RHandTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] RHandKeys = { 0.00802646f, 0.00802646f, 0.00802646f, 0.00802646f, 0.00802646f };

		Float[] RHipPitchTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] RHipPitchKeys = { -0.291501f, -0.291501f, -0.291501f, -0.291501f, -0.291501f };

		Float[] RHipRollTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] RHipRollKeys = { -0.101202f, -0.101202f, -0.101202f, -0.101202f, -0.101202f };

		Float[] RKneePitchTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] RKneePitchKeys = { 0.737896f, 0.736361f, 0.737896f, 0.736361f, 0.737896f };

		Float[] RShoulderPitchTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] RShoulderPitchKeys = { 0.840674f, 1.50796f, 0.561486f, 1.50796f, 0.561486f };

		Float[] RShoulderRollTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] RShoulderRollKeys = { -0.00872665f, -0.00872665f, -0.00872665f, -0.00872665f, -0.00872665f };

		Float[] RWristYawTimes = { 1.33333f, 2f, 2.66667f, 3.33333f, 4f };
		Float[] RWristYawKeys = { 1.13665f, 1.13665f, 1.13665f, 1.13665f, 1.13665f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación goIrish", "go_irish");
		}
	}

	/**
	 * NAO mueve el brazo derecho para limpiar el sudor de su frente. Esta es
	 * una operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),wearing_out]
	 */
	@OPERATION
	public void wearingOut() {
		Float[] HeadPitchTimes = { 0.96f, 1.68f, 3.28f, 3.96f, 4.52f, 5.08f };
		Float[] HeadPitchKeys = { -0.0261199f, 0.427944f, 0.308291f, 0.11194f, -0.013848f, 0.061318f };

		Float[] HeadYawTimes = { 0.96f, 1.68f, 3.28f, 3.96f, 4.52f, 5.08f };
		Float[] HeadYawKeys = { -0.234743f, -0.622845f, -0.113558f, -0.00617796f, -0.027654f, -0.036858f };

		Float[] LElbowRollTimes = { 0.8f, 1.52f, 3.12f, 3.8f, 4.36f, 4.92f };
		Float[] LElbowRollKeys = { -0.866668f, -0.868202f, -0.822183f, -0.992455f, -0.966378f, -0.990923f };

		Float[] LElbowYawTimes = { 0.8f, 1.52f, 3.12f, 3.8f, 4.36f, 4.92f };
		Float[] LElbowYawKeys = { -0.957257f, -0.823801f, -1.00788f, -0.925044f, -1.24412f, -0.960325f };

		Float[] LHandTimes = { 1.52f, 3.12f, 3.8f, 4.92f };
		Float[] LHandKeys = { 0.132026f, 0.132026f, 0.132026f, 0.132026f };

		Float[] LShoulderPitchTimes = { 0.8f, 1.52f, 3.12f, 3.8f, 4.36f, 4.92f };
		Float[] LShoulderPitchKeys = { 0.863599f, 0.858999f, 0.888144f, 0.929562f, 1.017f, 0.977116f };

		Float[] LShoulderRollTimes = { 0.8f, 1.52f, 3.12f, 3.8f, 4.36f, 4.92f };
		Float[] LShoulderRollKeys = { 0.286815f, 0.230059f, 0.202446f, 0.406468f, 0.360449f, 0.31903f };

		Float[] LWristYawTimes = { 1.52f, 3.12f, 3.8f, 4.92f };
		Float[] LWristYawKeys = { 0.386526f, 0.386526f, 0.386526f, 0.386526f };

		Float[] RElbowRollTimes = { 0.64f, 1.36f, 2.96f, 3.64f, 4.2f, 4.76f };
		Float[] RElbowRollKeys = { 1.28093f, 1.39752f, 1.57239f, 1.24105f, 1.22571f, 0.840674f };

		Float[] RElbowYawTimes = { 0.64f, 1.36f, 2.96f, 3.64f, 4.2f, 4.76f };
		Float[] RElbowYawKeys = { -0.128898f, -0.285367f, -0.15651f, 0.754686f, 1.17193f, 0.677985f };

		Float[] RHandTimes = { 1.36f, 2.96f, 3.64f, 4.76f };
		Float[] RHandKeys = { 0.166571f, 0.166208f, 0.166571f, 0.166208f };

		Float[] RShoulderPitchTimes = { 0.64f, 1.36f, 2.96f, 3.64f, 4.2f, 4.76f };
		Float[] RShoulderPitchKeys = { 0.0767419f, -0.59515f, -0.866668f, -0.613558f, 0.584497f, 0.882091f };

		Float[] RShoulderRollTimes = { 0.64f, 1.36f, 2.96f, 3.64f, 4.2f, 4.76f };
		Float[] RShoulderRollKeys = { -0.019984f, -0.019984f, -0.615176f, -0.833004f, -0.224006f, -0.214801f };

		Float[] RWristYawTimes = { 1.36f, 2.96f, 3.64f, 4.76f };
		Float[] RWristYawKeys = { -0.058334f, -0.0521979f, -0.067538f, -0.038392f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LElbowRoll", "LElbowYaw", "LHand", "LShoulderPitch",
				"LShoulderRoll", "LWristYaw", "RElbowRoll", "RElbowYaw", "RHand", "RShoulderPitch", "RShoulderRoll",
				"RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación wearingOut", "wearing_out");
		}
	}

	/**
	 * NAO balancea los brazos hacia adelante haciendo un baile. Esta es una
	 * operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese código se exportó a
	 * código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),dance_move]
	 */
	@OPERATION
	public void danceMove() {
		Float[] HeadPitchTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.4f, 3.66667f, 3.93333f, 4.2f,
				6f };
		Float[] HeadPitchKeys = { 0.00302603f, 0.00302603f, 0.340507f, -0.417291f, -0.664264f, 0.0459781f, 0.113474f,
				0.0996681f, 0.102736f, -0.00617796f };

		Float[] HeadYawTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.4f, 3.66667f, 3.93333f, 4.2f,
				6f };
		Float[] HeadYawKeys = { -1.10145f, 1.4097f, -0.00924597f, -0.254685f, 0.506179f, 0.627364f, -0.22554f,
				0.645772f, -0.217869f, 0.00456004f };

		Float[] LAnklePitchTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 6f };
		Float[] LAnklePitchKeys = { -0.359129f, -0.762571f, -0.777911f, -0.580025f, -0.58923f, -0.590764f, -0.116757f };

		Float[] LAnkleRollTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 6f };
		Float[] LAnkleRollKeys = { -0.282235f, -0.0598056f, -0.0552035f, -0.139574f, -0.133438f, -0.134972f,
				0.0138264f };

		Float[] LElbowRollTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.4f, 3.66667f, 3.93333f, 4.2f,
				4.93333f, 5.33333f, 6f };
		Float[] LElbowRollKeys = { -0.748551f, -0.901949f, -1.48947f, -1.05688f, -1.56157f, -1.08603f, -1.56464f,
				-0.866668f, -1.56464f, -1.02314f, -0.299088f, -0.315962f };

		Float[] LElbowYawTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.4f, 3.66667f, 3.93333f, 4.2f,
				4.93333f, 5.33333f, 6f };
		Float[] LElbowYawKeys = { 0.89428f, -0.681137f, -0.52467f, 0.383458f, -0.760906f, 0.0797261f, -0.83914f,
				0.329768f, -0.740964f, -0.339056f, -0.932714f, -0.7471f };

		Float[] LHandTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 6f };
		Float[] LHandKeys = { 0.920023f, 0.920023f, 0.921478f, 0.921478f, 0.921478f, 0.921478f, 0.920387f };

		Float[] LHipPitchTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 6f };
		Float[] LHipPitchKeys = { 0.232947f, -0.224186f, -0.21038f, -0.34077f, -0.325429f, -0.323895f, 0.0764786f };

		Float[] LHipRollTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 6f };
		Float[] LHipRollKeys = { 0.423192f, 0.31121f, 0.294336f, -0.050814f, -0.0661541f, -0.0661541f, -0.0492801f };

		Float[] LHipYawPitchTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 4.2f, 6f };
		Float[] LHipYawPitchKeys = { -0.113559f, -0.236279f, -0.230144f, -0.32065f, -0.311445f, -0.311445f, -0.302242f,
				0.0275685f };

		Float[] LKneePitchTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 6f };
		Float[] LKneePitchKeys = { 0.14541f, 1.11643f, 1.13331f, 1.01212f, 1.03513f, 1.02899f, 0.0871181f };

		Float[] LShoulderPitchTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.4f, 3.66667f, 3.93333f,
				4.2f, 4.93333f, 5.33333f, 6f };
		Float[] LShoulderPitchKeys = { 0.653443f, -0.934249f, 0.489305f, 0.589014f, 0.259204f, 0.714801f, 0.245399f,
				0.656511f, 0.274544f, 0.720938f, 0.437147f, 1.56004f };

		Float[] LShoulderRollTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.4f, 3.66667f, 3.93333f,
				4.2f, 4.93333f, 5.33333f, 6f };
		Float[] LShoulderRollKeys = { 1.32687f, 1.39743f, 0.010696f, 0.013764f, 0f, 0f, 0.013764f, 0f, 0f, 0.061318f,
				1.13052f, 0.302157f };

		Float[] LWristYawTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 6f };
		Float[] LWristYawKeys = { -1.02475f, -1.02475f, -1.02015f, -1.02629f, -1.02015f, -1.02629f, 1.04461f };

		Float[] RAnklePitchTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 4.2f, 6f };
		Float[] RAnklePitchKeys = { -0.102805f, -0.210185f, -0.220924f, -0.405004f, -0.412673f, -0.412673f, -0.421878f,
				-0.102805f };

		Float[] RAnkleRollTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 4.2f, 6f };
		Float[] RAnkleRollKeys = { -0.00456227f, -0.105806f, -0.10734f, -0.0229703f, -0.0245042f, -0.0260382f,
				-0.0275722f, -0.00456227f };

		Float[] RElbowRollTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.4f, 3.66667f, 3.93333f, 4.2f,
				4.93333f, 5.33333f, 6f };
		Float[] RElbowRollKeys = { 0.727158f, 1.10452f, 0.176453f, 0.170316f, 0.174919f, 0.174919f, 0.173384f,
				0.170316f, 0.173384f, 0.280764f, 1.34843f, 0.273093f };

		Float[] RElbowYawTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.4f, 3.66667f, 3.93333f, 4.2f,
				4.93333f, 5.33333f, 6f };
		Float[] RElbowYawKeys = { 0.946436f, -0.515466f, 0.728609f, 0.722472f, 0.730143f, 0.731675f, 0.730143f,
				0.71787f, 0.722472f, -1.22878f, -0.0537319f, 0.768491f };

		Float[] RHandTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 6f };
		Float[] RHandKeys = { 0.920387f, 0.920387f, 0.922933f, 0.922933f, 0.925478f, 0.922933f, 0.971296f };

		Float[] RHipPitchTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 4.2f, 6f };
		Float[] RHipPitchKeys = { 0.041361f, -0.24243f, -0.237827f, -0.856028f, -0.848359f, -0.852962f, -0.851427f,
				0.041361f };

		Float[] RHipRollTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 4.2f, 6f };
		Float[] RHipRollKeys = { 0.0153604f, 0.210178f, 0.21478f, -0.0889516f, -0.0843497f, -0.0843497f, -0.0797476f,
				0.0153604f };

		Float[] RKneePitchTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 4.2f, 6f };
		Float[] RKneePitchKeys = { 0.105432f, 0.619321f, 0.640798f, 1.26974f, 1.29121f, 1.28968f, 1.31576f, 0.105432f };

		Float[] RShoulderPitchTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.4f, 3.66667f, 3.93333f,
				4.2f, 4.93333f, 5.33333f, 6f };
		Float[] RShoulderPitchKeys = { -0.970981f, 0.782382f, -0.877407f, -0.863599f, -0.81758f, -0.814512f, -0.819114f,
				-0.811444f, -0.81758f, -0.085862f, 0.406552f, 1.55705f };

		Float[] RShoulderRollTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.4f, 3.66667f, 3.93333f,
				4.2f, 4.93333f, 5.33333f, 6f };
		Float[] RShoulderRollKeys = { -1.40519f, -1.30701f, -1.00174f, -1.01248f, -1.02629f, -1.02015f, -1.03856f,
				-1.05083f, -1.06004f, -1.25179f, -0.00617796f, -0.296104f };

		Float[] RWristYawTimes = { 0.866667f, 1.53333f, 2.13333f, 2.73333f, 3.13333f, 3.66667f, 6f };
		Float[] RWristYawKeys = { 0.977116f, 0.974047f, 0.964844f, 0.966378f, 0.98632f, 0.966378f, 1.49254f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación danceMove", "dance_move");
		}
	}

	/**
	 * NAO dobla las rodillas y se inclina hacia adelante en un movimiento de
	 * reverencia. Esta es una operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese timeline se exportó
	 * a código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),movement_bow]
	 */
	@OPERATION
	public void movementBow() {
		Float[] LAnklePitchTimes = { 0.933333f, 3f };
		Float[] LAnklePitchKeys = { -0.121359f, -0.409751f };

		Float[] LAnkleRollTimes = { 0.933333f, 3f };
		Float[] LAnkleRollKeys = { 0.0153604f, -0.116564f };

		Float[] LElbowRollTimes = { 0.933333f, 3f, 4.53333f };
		Float[] LElbowRollKeys = { -0.306757f, -1.30539f, -0.306757f };

		Float[] LElbowYawTimes = { 0.933333f, 3f, 4.53333f };
		Float[] LElbowYawKeys = { -0.12583f, 0.469363f, -0.12583f };

		Float[] LHandTimes = { 0.933333f, 3f, 4.53333f };
		Float[] LHandKeys = { 0.924024f, 0.924024f, 0.924024f };

		Float[] LHipPitchTimes = { 0.933333f, 3f };
		Float[] LHipPitchKeys = { 0.0749446f, -0.761086f };

		Float[] LHipRollTimes = { 0.933333f, 3f };
		Float[] LHipRollKeys = { -0.0477461f, 0.028954f };

		Float[] LHipYawPitchTimes = { 0.933333f, 3f };
		Float[] LHipYawPitchKeys = { 0.0291025f, -0.319116f };

		Float[] LKneePitchTimes = { 0.933333f, 3f };
		Float[] LKneePitchKeys = { 0.0855841f, 1.0474f };

		Float[] LShoulderPitchTimes = { 0.933333f, 3f, 4.53333f };
		Float[] LShoulderPitchKeys = { 2.0417f, 2.06319f, 2.0417f };

		Float[] LShoulderRollTimes = { 0.933333f, 3f, 4.53333f };
		Float[] LShoulderRollKeys = { 0.417205f, 0.246933f, 0.417205f };

		Float[] LWristYawTimes = { 0.933333f, 3f, 4.53333f };
		Float[] LWristYawKeys = { -0.998676f, -1.01708f, -0.998676f };

		Float[] RAnklePitchTimes = { 0.933333f, 3f };
		Float[] RAnklePitchKeys = { -0.102805f, -0.543063f };

		Float[] RAnkleRollTimes = { 0.933333f, 3f };
		Float[] RAnkleRollKeys = { -0.00456227f, 0.135032f };

		Float[] RElbowRollTimes = { 0.933333f, 3f, 4.53333f };
		Float[] RElbowRollKeys = { 0.435699f, 1.27786f, 0.435699f };

		Float[] RElbowYawTimes = { 0.933333f, 3f, 4.53333f };
		Float[] RElbowYawKeys = { 0.222388f, 0.374254f, 0.222388f };

		Float[] RHandTimes = { 0.933333f, 3f, 4.53333f };
		Float[] RHandKeys = { 0.917842f, 0.469091f, 0.917842f };

		Float[] RHipPitchTimes = { 0.933333f, 3f };
		Float[] RHipPitchKeys = { 0.041361f, -0.65354f };

		Float[] RHipRollTimes = { 0.933333f, 3f };
		Float[] RHipRollKeys = { 0.0168944f, -0.0444656f };

		Float[] RKneePitchTimes = { 0.933333f, 3f };
		Float[] RKneePitchKeys = { 0.103898f, 1.03657f };

		Float[] RShoulderPitchTimes = { 0.933333f, 3f, 4.53333f };
		Float[] RShoulderPitchKeys = { 1.06617f, 0.943452f, 1.06617f };

		Float[] RShoulderRollTimes = { 0.933333f, 3f, 4.53333f };
		Float[] RShoulderRollKeys = { -0.398883f, -0.0429939f, -0.398883f };

		Float[] RWristYawTimes = { 0.933333f, 3f, 4.53333f };
		Float[] RWristYawKeys = { 0.949504f, 0.964844f, 0.949504f };

		String[] namesArray = { "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand", "LHipPitch",
				"LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw", "RAnklePitch",
				"RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación movementBow", "movement_bow");
		}
	}

	/**
	 * NAO mueve su mano izquierda hasta su boca y la mueve hacia fuera y hacia
	 * adentro para simular que está lanzando besos. Esta es una operación
	 * sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese timeline se exportó
	 * a código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),blow_kisses]
	 */
	@OPERATION
	public void blowKisses() {
		Float[] HeadPitchTimes = { 1f, 2.33333f, 3.4f };
		Float[] HeadPitchKeys = { -0.01078f, -0.01078f, -0.112024f };

		Float[] HeadYawTimes = { 1f, 2.33333f, 3.4f };
		Float[] HeadYawKeys = { 0.010696f, 0.010696f, 0.338973f };

		Float[] LAnklePitchTimes = { 1.2f };
		Float[] LAnklePitchKeys = { -0.359129f };

		Float[] LAnkleRollTimes = { 1.2f };
		Float[] LAnkleRollKeys = { -0.0797476f };

		Float[] LElbowRollTimes = { 1f, 1.66667f, 2.33333f, 2.86667f, 3.4f };
		Float[] LElbowRollKeys = { -1.56617f, -0.658043f, -1.56617f, -0.658043f, -0.305225f };

		Float[] LElbowYawTimes = { 1f, 1.66667f, 2.33333f, 2.86667f, 3.4f };
		Float[] LElbowYawKeys = { -0.624379f, -1.26866f, -0.624379f, -1.26866f, -2.07862f };

		Float[] LHandTimes = { 1f, 1.66667f, 2.33333f, 2.86667f, 3.4f };
		Float[] LHandKeys = { 0.917114f, 1f, 0.917114f, 1f, 0.997478f };

		Float[] LHipPitchTimes = { 1.2f };
		Float[] LHipPitchKeys = { -0.27941f };

		Float[] LHipRollTimes = { 1.2f };
		Float[] LHipRollKeys = { 0.168548f };

		Float[] LHipYawPitchTimes = { 1.2f };
		Float[] LHipYawPitchKeys = { -0.170318f };

		Float[] LKneePitchTimes = { 1.2f };
		Float[] LKneePitchKeys = { 0.680776f };

		Float[] LShoulderPitchTimes = { 1f, 1.66667f, 2.33333f, 2.86667f, 3.4f };
		Float[] LShoulderPitchKeys = { 0.496974f, 0.225456f, 0.496974f, 0.225456f, 0.4034f };

		Float[] LShoulderRollTimes = { 1f, 1.66667f, 2.33333f, 2.86667f, 3.4f };
		Float[] LShoulderRollKeys = { 0f, 0.05058f, 0f, 0.05058f, 0.77923f };

		Float[] LWristYawTimes = { 1f, 1.66667f, 2.33333f, 2.86667f, 3.4f };
		Float[] LWristYawKeys = { -1.00941f, -1.00941f, -1.00941f, -1.00941f, -1.01095f };

		Float[] RAnklePitchTimes = { 1.2f };
		Float[] RAnklePitchKeys = { -0.184108f };

		Float[] RAnkleRollTimes = { 1.2f };
		Float[] RAnkleRollKeys = { 0.0675357f };

		Float[] RElbowRollTimes = { 1f, 1.66667f, 2.33333f, 2.86667f, 3.4f };
		Float[] RElbowRollKeys = { 0.504728f, 0.431096f, 0.504728f, 0.431096f, 0.596768f };

		Float[] RElbowYawTimes = { 1f, 1.66667f, 2.33333f, 2.86667f, 3.4f };
		Float[] RElbowYawKeys = { 0.42641f, 0.41107f, 0.42641f, 0.41107f, 0.196309f };

		Float[] RHandTimes = { 1f, 2.33333f, 3.4f };
		Float[] RHandKeys = { 0.630909f, 0.630909f, 0.572727f };

		Float[] RHipPitchTimes = { 1.2f };
		Float[] RHipPitchKeys = { -0.336004f };

		Float[] RHipRollTimes = { 1.2f };
		Float[] RHipRollKeys = { 0.0015544f };

		Float[] RKneePitchTimes = { 1.2f };
		Float[] RKneePitchKeys = { 0.556428f };

		Float[] RShoulderPitchTimes = { 1f, 1.66667f, 2.33333f, 2.86667f, 3.4f };
		Float[] RShoulderPitchKeys = { 1.14441f, 1.10912f, 1.14441f, 1.10912f, 1.27173f };

		Float[] RShoulderRollTimes = { 1f, 1.66667f, 2.33333f, 2.86667f, 3.4f };
		Float[] RShoulderRollKeys = { -0.271559f, -0.253151f, -0.271559f, -0.253151f, -0.579894f };

		Float[] RWristYawTimes = { 1f, 2.33333f, 3.4f };
		Float[] RWristYawKeys = { 0.958708f, 0.958708f, 0.944902f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación blowKisses", "blow_kisses");
		}
	}

	/**
	 * NAO hace el giro y balancea sus brazos arriba y abajo en un movimiento de
	 * danza. Esta es una operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese timeline se exportó
	 * a código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),birthday_dance]
	 */
	@OPERATION
	public void birthdayDance() {
		Float[] HeadPitchTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] HeadPitchKeys = { -0.0414599f, -0.154976f, -0.162646f, -0.154976f, -0.154976f, -0.154976f, -0.154976f,
				-0.154976f, -0.154976f, -0.1335f, -0.145772f, -0.1335f, -0.145772f, -0.1335f, -0.145772f, -0.1335f,
				-0.1335f };

		Float[] HeadYawTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f, 4.33333f,
				5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] HeadYawKeys = { -0.00157596f, -0.214801f, -0.213269f, -0.214801f, -0.2102f, -0.214801f, -0.2102f,
				-0.214801f, -0.2102f, -4.19617e-05f, -4.19617e-05f, -4.19617e-05f, -4.19617e-05f, -4.19617e-05f,
				-4.19617e-05f, -4.19617e-05f, -4.19617e-05f };

		Float[] LAnklePitchTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] LAnklePitchKeys = { -0.113689f, -0.340721f, -0.244079f, -0.340721f, -0.00170743f, -0.340721f,
				-0.00170743f, -0.340721f, -0.00170743f, -0.0139794f, -0.0262515f, -0.0139794f, -0.0262515f, -0.0139794f,
				-0.0262515f, -0.0139794f, 0.00749657f };

		Float[] LAnkleRollTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] LAnkleRollKeys = { 0.0168944f, -0.0398637f, -0.167186f, -0.0398637f, -0.265362f, -0.0398637f,
				-0.265362f, -0.0398637f, -0.265362f, -0.259225f, -0.254624f, -0.259225f, -0.254624f, -0.259225f,
				-0.254624f, -0.259225f, 0.0092244f };

		Float[] LElbowRollTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] LElbowRollKeys = { -0.340507f, -1.54776f, -1.54163f, -1.54776f, -1.54009f, -1.54776f, -1.54009f,
				-1.54776f, -1.54009f, -0.814512f, -0.653443f, -0.814512f, -0.653443f, -0.814512f, -0.653443f,
				-0.814512f, -0.558334f };

		Float[] LElbowYawTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] LElbowYawKeys = { -0.767043f, -1.14441f, -1.47882f, -1.14441f, -1.33155f, -1.14441f, -1.33155f,
				-1.14441f, -1.33155f, -1.66903f, -1.42666f, -1.66903f, -1.42666f, -1.66903f, -1.42666f, -1.66903f,
				-1.86232f };

		Float[] LHandTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f, 4.33333f,
				5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] LHandKeys = { 0.916387f, 0.41748f, 0.417116f, 0.41748f, 0.41748f, 0.41748f, 0.41748f, 0.41748f,
				0.41748f, 0.421116f, 0.421116f, 0.421116f, 0.421116f, 0.421116f, 0.421116f, 0.421116f, 1f };

		Float[] LHipPitchTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] LHipPitchKeys = { 0.0749446f, -0.242594f, 0.438502f, -0.242594f, 0.271296f, -0.242594f, 0.271296f,
				-0.242594f, 0.271296f, 0.286637f, 0.303511f, 0.286637f, 0.303511f, 0.286637f, 0.303511f, 0.286637f,
				0.378677f };

		Float[] LHipRollTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f, 4.33333f,
				5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] LHipRollKeys = { -0.0477461f, 0.17315f, 0.185422f, 0.17315f, 0.24218f, 0.17315f, 0.24218f, 0.17315f,
				0.24218f, 0.22684f, 0.211501f, 0.22684f, 0.211501f, 0.22684f, 0.211501f, 0.22684f, -0.0324061f };

		Float[] LHipYawPitchTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] LHipYawPitchKeys = { 0.0275685f, -0.523138f, -0.401951f, -0.523138f, -0.357466f, -0.523138f, -0.357466f,
				-0.523138f, -0.357466f, -0.346727f, -0.337524f, -0.346727f, -0.337524f, -0.346727f, -0.337524f,
				-0.346727f, -0.369738f };

		Float[] LKneePitchTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] LKneePitchKeys = { 0.0886521f, 0.775884f, 0.133138f, 0.775884f, 0.00581614f, 0.775884f, 0.00581614f,
				0.775884f, 0.00581614f, 0.0226902f, 0.0380302f, 0.0226902f, 0.0380302f, 0.0226902f, 0.0380302f,
				0.0226902f, 0.0119521f };

		Float[] LShoulderPitchTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] LShoulderPitchKeys = { 1.58765f, 1.45266f, 1.98343f, 1.45266f, 2.0187f, 1.45266f, 2.0187f, 1.45266f,
				2.0187f, -0.72409f, 1.62907f, -0.72409f, 1.62907f, -0.72409f, 1.62907f, -0.72409f, 0.708667f };

		Float[] LShoulderRollTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] LShoulderRollKeys = { 0.303691f, 0f, 0.115008f, 0f, 0f, 0f, 0f, 0f, 0f, 0.314428f, 0.059784f, 0.314428f,
				0.059784f, 0.314428f, 0.059784f, 0.314428f, 0f };

		Float[] LWristYawTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] LWristYawKeys = { -1.00328f, -0.694945f, -0.65506f, -0.694945f, -0.688808f, -0.694945f, -0.688808f,
				-0.694945f, -0.688808f, -0.690342f, -0.681137f, -0.690342f, -0.681137f, -0.690342f, -0.681137f,
				-0.690342f, -0.690342f };

		Float[] RAnklePitchTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] RAnklePitchKeys = { -0.104339f, 0.0398569f, -0.270011f, 0.0398569f, -0.451023f, 0.0398569f, -0.451023f,
				0.0398569f, -0.451023f, -0.458693f, -0.466362f, -0.458693f, -0.466362f, -0.458693f, -0.466362f,
				-0.458693f, -0.167233f };

		Float[] RAnkleRollTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] RAnkleRollKeys = { -0.00456227f, 0.108954f, 0.0859437f, 0.108954f, 0.00464174f, 0.108954f, 0.00464174f,
				0.108954f, 0.00464174f, 0.00310773f, 0.00157374f, 0.00310773f, 0.00157374f, 0.00310773f, 0.00157374f,
				0.00310773f, 0.147304f };

		Float[] RElbowRollTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] RElbowRollKeys = { 0.305309f, 1.12907f, 1.1214f, 1.12907f, 1.126f, 1.12907f, 1.126f, 1.12907f, 1.126f,
				1.33616f, 1.25485f, 1.33616f, 1.25485f, 1.33616f, 1.25485f, 1.33616f, 0.553816f };

		Float[] RElbowYawTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] RElbowYawKeys = { 0.786901f, 1.52475f, 1.18574f, 1.52475f, 1.51555f, 1.52475f, 1.51555f, 1.52475f,
				1.51555f, 1.26397f, 1.27931f, 1.26397f, 1.27931f, 1.26397f, 1.27931f, 1.26397f, 1.7886f };

		Float[] RHandTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f, 4.33333f,
				5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] RHandKeys = { 0.917842f, 0.411661f, 0.411661f, 0.411661f, 0.411661f, 0.411661f, 0.411661f, 0.411661f,
				0.411661f, 0.414571f, 0.414571f, 0.414571f, 0.414571f, 0.414571f, 0.414571f, 0.414571f, 1f };

		Float[] RHipPitchTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] RHipPitchKeys = { 0.039827f, 0.315946f, 0.010681f, 0.315946f, 0.042895f, 0.315946f, 0.042895f,
				0.315946f, 0.042895f, 0.047497f, 0.0520989f, 0.047497f, 0.0520989f, 0.047497f, 0.0520989f, 0.047497f,
				0.243849f };

		Float[] RHipRollTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f, 4.33333f,
				5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] RHipRollKeys = { 0.0153604f, 0.0260984f, -0.144176f, 0.0260984f, -0.0644075f, 0.0260984f, -0.0644075f,
				0.0260984f, -0.0644075f, -0.0598056f, -0.0552035f, -0.0598056f, -0.0552035f, -0.0598056f, -0.0552035f,
				-0.0598056f, -0.200933f };

		Float[] RKneePitchTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] RKneePitchKeys = { 0.103898f, 0f, 0.570234f, 0f, 0.665342f, 0f, 0.665342f, 0f, 0.665342f, 0.686818f,
				0.708295f, 0.686818f, 0.708295f, 0.686818f, 0.708295f, 0.686818f, 0.243493f };

		Float[] RShoulderPitchTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] RShoulderPitchKeys = { 1.55859f, 2.01572f, 1.48035f, 2.01572f, 1.43893f, 2.01572f, 1.43893f, 2.01572f,
				1.43893f, 0.615176f, -0.530721f, 0.615176f, -0.530721f, 0.615176f, -0.530721f, 0.615176f, 0.529271f };

		Float[] RShoulderRollTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] RShoulderRollKeys = { -0.296104f, -0.082878f, -0.01078f, -0.082878f, -0.081344f, -0.082878f, -0.081344f,
				-0.082878f, -0.081344f, -0.460242f, -0.679603f, -0.460242f, -0.679603f, -0.460242f, -0.679603f,
				-0.460242f, -0.00924597f };

		Float[] RWristYawTimes = { 0.733333f, 1.2f, 1.66667f, 2.06667f, 2.46667f, 2.93333f, 3.46667f, 3.93333f,
				4.33333f, 5f, 5.53333f, 6.2f, 6.73333f, 7.46667f, 8.06667f, 8.86667f, 9.73333f };
		Float[] RWristYawKeys = { 0.946436f, 0.76389f, 0.766959f, 0.76389f, 0.760822f, 0.76389f, 0.760822f, 0.76389f,
				0.760822f, 0.77923f, 0.777696f, 0.77923f, 0.777696f, 0.77923f, 0.777696f, 0.77923f, 0.77923f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación movementExcelent", "birthday_dance");
		}
	}

	/**
	 * Nao mueve los brazos en una posición de guitarra de aire y comienza a
	 * tocar.Esta es una operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese timeline se exportó
	 * a código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),air_guitar]
	 */
	@OPERATION
	public void airGuitar() {
		Float[] LAnklePitchTimes = { 1.8f };
		Float[] LAnklePitchKeys = { -0.359129f };

		Float[] LAnkleRollTimes = { 1.8f };
		Float[] LAnkleRollKeys = { -0.0766796f };

		Float[] LElbowRollTimes = { 1.26667f, 2.13333f, 2.6f, 3.06667f, 3.66667f, 4.33333f, 4.73333f, 4.86667f,
				5.53333f, 6.06667f, 6.53333f, 7.06667f, 7.73333f };
		Float[] LElbowRollKeys = { -1.06916f, -1.05688f, -1.05688f, -1.04461f, -1.04461f, -1.04461f, -1.04461f,
				-1.04461f, -1.04461f, -1.04461f, -1.04461f, -1.04461f, -1.04461f };

		Float[] LElbowYawTimes = { 1.26667f, 2.13333f, 2.6f, 3.06667f, 3.66667f, 4.33333f, 4.73333f, 4.86667f, 5.53333f,
				6.06667f, 6.53333f, 7.06667f, 7.73333f };
		Float[] LElbowYawKeys = { -1.76722f, -1.75341f, -1.75495f, -1.75034f, -1.75034f, -1.75034f, -1.75034f,
				-1.75034f, -1.75034f, -1.75034f, -1.75034f, -1.75034f, -1.75034f };

		Float[] LHandTimes = { 1.26667f };
		Float[] LHandKeys = { 0.922933f };

		Float[] LHipPitchTimes = { 1.8f };
		Float[] LHipPitchKeys = { -0.291681f };

		Float[] LHipRollTimes = { 1.8f };
		Float[] LHipRollKeys = { 0.144004f };

		Float[] LHipYawPitchTimes = { 1.8f };
		Float[] LHipYawPitchKeys = { -0.547681f };

		Float[] LKneePitchTimes = { 1.8f };
		Float[] LKneePitchKeys = { 0.999849f };

		Float[] LShoulderPitchTimes = { 1.26667f, 2.13333f, 2.6f, 3.06667f, 3.66667f, 4.33333f, 4.73333f, 4.86667f,
				5.53333f, 6.06667f, 6.53333f, 7.06667f, 7.73333f };
		Float[] LShoulderPitchKeys = { 0.720938f, 0.733209f, 0.743948f, 0.760822f, 0.760822f, 0.760822f, 0.760822f,
				0.760822f, 0.760822f, 0.760822f, 0.760822f, 0.760822f, 0.760822f };

		Float[] LShoulderRollTimes = { 1.26667f, 2.13333f, 2.6f, 3.06667f, 3.66667f, 4.33333f, 4.73333f, 4.86667f,
				5.53333f, 6.06667f, 6.53333f, 7.06667f, 7.73333f };
		Float[] LShoulderRollKeys = { 0.535324f, 0.516916f, 0.516916f, 0.501576f, 0.50311f, 0.50311f, 0.50311f,
				0.50311f, 0.50311f, 0.50311f, 0.50311f, 0.50311f, 0.50311f };

		Float[] LWristYawTimes = { 1.26667f };
		Float[] LWristYawKeys = { -1.01402f };

		Float[] RAnklePitchTimes = { 1.8f };
		Float[] RAnklePitchKeys = { 0.0383229f };

		Float[] RAnkleRollTimes = { 1.8f };
		Float[] RAnkleRollKeys = { 0.296102f };

		Float[] RElbowRollTimes = { 1.26667f, 2.13333f, 2.6f, 3.06667f, 3.66667f, 4.33333f, 4.73333f, 4.86667f,
				5.53333f, 6.06667f, 6.53333f, 7.06667f, 7.73333f };
		Float[] RElbowRollKeys = { 1.02629f, 0.523136f, 1.22264f, 0.681137f, 1.11066f, 0.656595f, 1.11066f, 0.656595f,
				1.11066f, 0.656595f, 1.11066f, 0.656595f, 1.11066f };

		Float[] RElbowYawTimes = { 1.26667f, 2.13333f, 2.6f, 3.06667f, 3.66667f, 4.33333f, 4.73333f, 4.86667f, 5.53333f,
				6.06667f, 6.53333f, 7.06667f, 7.73333f };
		Float[] RElbowYawKeys = { 0.276078f, -0.029188f, 0.408002f, -0.138102f, 0.391128f, -0.0982179f, 0.391128f,
				-0.0982179f, 0.391128f, -0.0982179f, 0.391128f, -0.0982179f, 0.391128f };

		Float[] RHandTimes = { 1.26667f, 2.13333f };
		Float[] RHandKeys = { 0.920751f, 0.452752f };

		Float[] RHipPitchTimes = { 1.8f };
		Float[] RHipPitchKeys = { -0.125845f };

		Float[] RHipRollTimes = { 1.8f };
		Float[] RHipRollKeys = { -0.309848f };

		Float[] RKneePitchTimes = { 1.8f };
		Float[] RKneePitchKeys = { 0.470523f };

		Float[] RShoulderPitchTimes = { 1.26667f, 2.13333f, 2.6f, 3.06667f, 3.66667f, 4.33333f, 4.73333f, 4.86667f,
				5.53333f, 6.06667f, 6.53333f, 7.06667f, 7.73333f };
		Float[] RShoulderPitchKeys = { 0.889762f, 0.868286f, 0.92351f, 0.935783f, 0.89283f, 0.886694f, 0.89283f,
				0.886694f, 0.89283f, 0.886694f, 0.89283f, 0.886694f, 0.89283f };

		Float[] RShoulderRollTimes = { 1.26667f, 2.13333f, 2.6f, 3.06667f, 3.66667f, 4.33333f, 4.73333f, 4.86667f,
				5.53333f, 6.06667f, 6.53333f, 7.06667f, 7.73333f };
		Float[] RShoulderRollKeys = { -0.181053f, -0.101286f, -0.0782759f, -0.16418f, -0.075208f, -0.104354f,
				-0.075208f, -0.104354f, -0.075208f, -0.104354f, -0.075208f, -0.104354f, -0.075208f };

		Float[] RWristYawTimes = { 1.26667f, 2.13333f };
		Float[] RWristYawKeys = { 0.96331f, 0.682588f };

		String[] namesArray = { "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand", "LHipPitch",
				"LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw", "RAnklePitch",
				"RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll", "RKneePitch",
				"RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación airGuitar", "air_guitar");
		}
	}

	/**
	 * NAO hace un baile de discoteca. Esta es una operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese timeline se exportó
	 * a código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),disco_dance]
	 */
	@OPERATION
	public void discoDance() {
		Float[] HeadPitchTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] HeadPitchKeys = { -0.136568f, -0.268493f, 0.118076f, -0.268493f, 0.118076f, -0.268493f, 0.118076f,
				-0.268493f, -0.144238f };

		Float[] HeadYawTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] HeadYawKeys = { -0.021518f, 0.384992f, -0.214803f, 0.384992f, -0.214803f, 0.384992f, -0.214803f,
				0.384992f, -0.032256f };

		Float[] LAnklePitchTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] LAnklePitchKeys = { 0.095066f, 0.174835f, 0.179436f, 0.174835f, 0.179436f, 0.174835f, 0.179436f,
				0.174835f, 0.093532f };

		Float[] LAnkleRollTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] LAnkleRollKeys = { -0.116542f, -0.0720561f, -0.141086f, -0.0720561f, -0.141086f, -0.0720561f,
				-0.141086f, -0.0720561f, -0.116542f };

		Float[] LElbowRollTimes = { 1f, 1.96f, 2.64f, 3.32f, 3.96f, 4.68f, 5.36f, 6.08f, 6.68f, 7.32f, 8f, 8.64f, 9.4f,
				10.04f, 11.56f };
		Float[] LElbowRollKeys = { -0.38806f, -0.199378f, -1.22562f, -0.391128f, -1.22562f, -0.199378f, -1.22562f,
				-0.391128f, -1.22562f, -0.199378f, -1.22562f, -0.391128f, -1.22562f, -0.199378f, -0.427944f };

		Float[] LElbowYawTimes = { 1f, 1.96f, 2.64f, 3.32f, 3.96f, 4.68f, 5.36f, 6.08f, 6.68f, 7.32f, 8f, 8.64f, 9.4f,
				10.04f, 11.56f };
		Float[] LElbowYawKeys = { -1.20883f, -1.56165f, -0.803859f, -0.337522f, -0.803859f, -1.56165f, -0.803859f,
				-0.337522f, -0.803859f, -1.56165f, -0.803859f, -0.337522f, -0.803859f, -1.56165f, -1.17509f };

		Float[] LHandTimes = { 1f, 1.96f, 2.64f, 3.32f, 3.96f, 4.68f, 5.36f, 6.08f, 6.68f, 7.32f, 8f, 8.64f, 9.4f,
				10.04f, 11.56f };
		Float[] LHandKeys = { 0.3056f, 0.8592f, 0.8592f, 0.8592f, 0.8592f, 0.8592f, 0.8592f, 0.8592f, 0.8592f, 0.8592f,
				0.8592f, 0.8592f, 0.8592f, 0.8592f, 0.3048f };

		Float[] LHipPitchTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] LHipPitchKeys = { 0.136568f, -0.243864f, 0.0552659f, -0.243864f, 0.0552659f, -0.243864f, 0.0552659f,
				-0.243864f, 0.138102f };

		Float[] LHipRollTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] LHipRollKeys = { 0.116626f, 0.066004f, 0.142704f, 0.066004f, 0.142704f, 0.066004f, 0.142704f, 0.066004f,
				0.11816f };

		Float[] LHipYawPitchTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] LHipYawPitchKeys = { -0.1733f, -0.371186f, -0.538392f, -0.371186f, -0.538392f, -0.371186f, -0.538392f,
				-0.371186f, -0.171766f };

		Float[] LKneePitchTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] LKneePitchKeys = { -0.090548f, 0.223922f, -0.00310993f, 0.223922f, -0.00310993f, 0.223922f,
				-0.00310993f, 0.223922f, -0.090548f };

		Float[] LShoulderPitchTimes = { 1f, 1.96f, 2.64f, 3.32f, 3.96f, 4.68f, 5.36f, 6.08f, 6.68f, 7.32f, 8f, 8.64f,
				9.4f, 10.04f, 11.56f };
		Float[] LShoulderPitchKeys = { 1.53089f, -1.08611f, 0.759288f, 0.960242f, 0.759288f, -1.08611f, 0.759288f,
				0.960242f, 0.759288f, -1.08611f, 0.759288f, 0.960242f, 0.759288f, -1.08611f, 1.48027f };

		Float[] LShoulderRollTimes = { 1f, 1.96f, 2.64f, 3.32f, 3.96f, 4.68f, 5.36f, 6.08f, 6.68f, 7.32f, 8f, 8.64f,
				9.4f, 10.04f, 11.56f };
		Float[] LShoulderRollKeys = { 0.130348f, 0.636569f, 0.228525f, -0.314159f, 0.228525f, 0.636569f, 0.228525f,
				-0.314159f, 0.228525f, 0.636569f, 0.228525f, -0.314159f, 0.228525f, 0.636569f, 0.0873961f };

		Float[] LWristYawTimes = { 1f, 1.96f, 2.64f, 3.32f, 3.96f, 4.68f, 5.36f, 6.08f, 6.68f, 7.32f, 8f, 8.64f, 9.4f,
				10.04f, 11.56f };
		Float[] LWristYawKeys = { 0.0843279f, 0.900415f, 1.23176f, 0.730143f, 1.23176f, 0.900415f, 1.23176f, 0.730143f,
				1.23176f, 0.900415f, 1.23176f, 0.730143f, 1.23176f, 0.900415f, 0.145688f };

		Float[] RAnklePitchTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] RAnklePitchKeys = { 0.101286f, 0.11049f, 0.108956f, 0.11049f, 0.108956f, 0.11049f, 0.108956f, 0.11049f,
				0.107422f };

		Float[] RAnkleRollTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] RAnkleRollKeys = { 0.075208f, 0.277696f, -0.0168321f, 0.277696f, -0.0168321f, 0.277696f, -0.0168321f,
				0.277696f, 0.07214f };

		Float[] RElbowRollTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] RElbowRollKeys = { 0.389678f, 0.392746f, 0.194861f, 0.392746f, 0.194861f, 0.392746f, 0.194861f,
				0.392746f, 0.389678f };

		Float[] RElbowYawTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] RElbowYawKeys = { 1.1796f, 1.18114f, 1.16887f, 1.18114f, 1.16887f, 1.18114f, 1.16887f, 1.18114f,
				1.17654f };

		Float[] RHandTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] RHandKeys = { 0.3068f, 0.3068f, 0.684f, 0.3068f, 0.684f, 0.3068f, 0.684f, 0.3068f, 0.3068f };

		Float[] RHipPitchTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] RHipPitchKeys = { 0.130348f, 0.131882f, -0.073674f, 0.131882f, -0.073674f, 0.131882f, -0.073674f,
				0.131882f, 0.131882f };

		Float[] RHipRollTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] RHipRollKeys = { -0.06592f, -0.292952f, 0.00771189f, -0.292952f, 0.00771189f, -0.292952f, 0.00771189f,
				-0.292952f, -0.06592f };

		Float[] RHipYawPitchTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] RHipYawPitchKeys = { -0.1733f, -0.371186f, -0.538392f, -0.371186f, -0.538392f, -0.371186f, -0.538392f,
				-0.371186f, -0.171766f };

		Float[] RKneePitchTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] RKneePitchKeys = { -0.0923279f, -0.0923279f, 0.196393f, -0.0923279f, 0.196393f, -0.0923279f, 0.196393f,
				-0.0923279f, -0.091998f };

		Float[] RShoulderPitchTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] RShoulderPitchKeys = { 1.52637f, 1.51563f, 1.49262f, 1.51563f, 1.49262f, 1.51563f, 1.49262f, 1.51563f,
				1.49569f };

		Float[] RShoulderRollTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] RShoulderRollKeys = { -0.10282f, -0.10282f, -0.158044f, -0.10282f, -0.158044f, -0.10282f, -0.158044f,
				-0.10282f, -0.104354f };

		Float[] RWristYawTimes = { 1f, 1.96f, 3.32f, 4.68f, 6.08f, 7.32f, 8.64f, 10.04f, 11.56f };
		Float[] RWristYawKeys = { 0.059784f, 0.06592f, -0.530805f, 0.06592f, -0.530805f, 0.06592f, -0.530805f, 0.06592f,
				0.0720561f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll",
				"RHipYawPitch", "RKneePitch", "RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación discoDance", "disco_dance");
		}
	}

	/**
	 * Nao baila la macarena. Esta es una operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese timeline se exportó
	 * a código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),macarena_dance]
	 */
	@OPERATION
	public void macarenaDance() {
		Float[] HeadPitchTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 6.52f, 7.12f, 7.64f, 8.36f, 9.08f,
				10.4f, 11.68f, 12.76f, 13.64f, 14.4f, 15.12f, 16f, 17.28f };
		Float[] HeadPitchKeys = { -0.237812f, -0.00617791f, 0.0643861f, 0.0643861f, 0.0643861f, 0.0643861f, 0.0643861f,
				0.153358f, 0.153358f, -0.190258f, -0.104354f, -0.285367f, 0.00609397f, 0.138018f, 0.222388f, 0.222388f,
				0.0735901f, 0.294486f, -0.227074f, -0.237812f };

		Float[] HeadYawTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 6.52f, 7.12f, 7.64f, 8.36f, 9.08f, 10.4f,
				11.68f, 12.76f, 13.64f, 14.4f, 15.12f, 16f, 17.28f };
		Float[] HeadYawKeys = { -4.19617e-05f, -0.312978f, 0.312894f, -0.233211f, 0.315962f, 0.315962f, -0.181053f,
				-0.352862f, -0.352862f, -0.234743f, 0.211651f, 0.535324f, 0.308291f, 0.024502f, -0.266959f, 0.550664f,
				-0.119694f, 0.501576f, -0.472515f, -4.19617e-05f };

		Float[] LAnklePitchTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 10.4f, 11.68f, 12.76f, 13.64f, 14.4f,
				15.12f, 16f, 17.28f };
		Float[] LAnklePitchKeys = { 0.0966001f, 0.0966001f, 0.095066f, 0.095066f, 0.0966001f, 0.0966001f, 0.095066f,
				0.0966001f, 0.0981341f, 0.0966001f, 0.0966001f, 0.0966001f, -0.190258f, -0.489389f, 0.0981341f };

		Float[] LAnkleRollTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 10.4f, 11.68f, 12.76f, 13.64f, 14.4f,
				15.12f, 16f, 17.28f };
		Float[] LAnkleRollKeys = { -0.116542f, -0.116542f, -0.116542f, -0.116542f, -0.116542f, -0.116542f, -0.116542f,
				-0.116542f, -0.115008f, -0.116542f, -0.116542f, -0.251533f, 0.0583338f, -0.211651f, -0.115008f };

		Float[] LElbowRollTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 4.64f, 5.08f, 5.52f, 5.92f, 6.52f, 7.12f, 7.64f,
				8.36f, 9.08f, 10.4f, 11.68f, 12.76f, 13.28f, 13.64f, 14.4f, 15.12f, 16f, 16.68f, 17.28f };
		Float[] LElbowRollKeys = { -0.391128f, -0.391128f, -0.406468f, -0.406468f, -0.406468f, -0.406468f, -0.406468f,
				-1.01393f, -0.928028f, -0.934165f, -0.934165f, -0.934165f, -0.782298f, -1.44345f, -1.44345f, -0.429478f,
				-0.435615f, -1.017f, -1.39897f, -1.42198f, -1.42198f, -1.52936f, -0.414139f, -0.417205f };

		Float[] LElbowYawTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 4.64f, 5.08f, 5.52f, 5.92f, 6.52f, 7.12f, 7.64f,
				8.36f, 9.08f, 10.4f, 11.68f, 12.76f, 13.28f, 13.64f, 14.4f, 15.12f, 16f, 16.68f, 17.28f };
		Float[] LElbowYawKeys = { -1.21957f, -1.21957f, -1.20577f, -1.20577f, -1.43587f, -1.43587f, -1.43587f,
				-1.23951f, -0.0123138f, -0.00617791f, -0.00617791f, -0.0107799f, -1.34689f, -0.983336f, -0.983336f,
				0.214717f, 0.205514f, 0.075124f, -4.19617e-05f, -0.016916f, -0.016916f, -0.237812f, -1.28247f,
				-1.18582f };

		Float[] LHandTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 4.64f, 5.08f, 5.52f, 5.92f, 6.52f, 7.12f, 7.64f, 8.36f,
				9.08f, 10.4f, 11.68f, 12.76f, 13.28f, 13.64f, 14.4f, 15.12f, 16f, 16.68f, 17.28f };
		Float[] LHandKeys = { 0.3056f, 0.3056f, 0.8488f, 0.8484f, 0.8484f, 0.8484f, 0.8488f, 0.8484f, 0.8488f, 0.8488f,
				0.8488f, 0.8488f, 0.8484f, 0.8484f, 0.8488f, 0.8484f, 0.8468f, 0.8468f, 0.8468f, 0.8484f, 0.8484f,
				0.8472f, 0.3056f, 0.3056f };

		Float[] LHipPitchTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 10.4f, 11.68f, 12.76f, 13.64f, 14.4f,
				15.12f, 16f, 17.28f };
		Float[] LHipPitchKeys = { 0.136568f, 0.135034f, 0.1335f, 0.136568f, 0.135034f, 0.135034f, 0.135034f, 0.135034f,
				0.135034f, 0.136568f, 0.136568f, 0.162646f, 0.0429941f, -0.535324f, 0.14117f };

		Float[] LHipRollTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 10.4f, 11.68f, 12.76f, 13.64f, 14.4f,
				15.12f, 16f, 17.28f };
		Float[] LHipRollKeys = { 0.115092f, 0.115092f, 0.115092f, 0.115092f, 0.115092f, 0.115092f, 0.115092f, 0.115092f,
				0.115092f, 0.115092f, 0.115092f, 0.312978f, -0.260738f, 0.431096f, 0.113558f };

		Float[] LHipYawPitchTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 10.4f, 11.68f, 12.76f, 13.64f,
				14.4f, 15.12f, 16f, 17.28f };
		Float[] LHipYawPitchKeys = { -0.1733f, -0.1733f, -0.1733f, -0.174835f, -0.174835f, -0.1733f, -0.1733f, -0.1733f,
				-0.1733f, -0.1733f, -0.1733f, -0.171766f, -0.240796f, -0.576742f, -0.1733f };

		Float[] LKneePitchTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 10.4f, 11.68f, 12.76f, 13.64f, 14.4f,
				15.12f, 16f, 17.28f };
		Float[] LKneePitchKeys = { -0.090548f, -0.090548f, -0.092082f, -0.0923279f, -0.092082f, -0.092082f, -0.092082f,
				-0.090548f, -0.090548f, -0.0890141f, -0.090548f, -0.092082f, 0.423342f, 1.33607f, -0.092082f };

		Float[] LShoulderPitchTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 4.64f, 5.08f, 5.52f, 5.92f, 6.52f, 7.12f, 7.64f,
				8.36f, 9.08f, 10.4f, 11.68f, 12.76f, 13.28f, 13.64f, 14.4f, 15.12f, 16f, 16.68f, 17.28f };
		Float[] LShoulderPitchKeys = { 1.49254f, 1.49254f, 0.397265f, 0.397265f, 0.432547f, 0.432547f, 0.434081f,
				0.239262f, 0.0720561f, 0.130348f, 0.15796f, 0.095066f, 0.223922f, -0.690342f, -0.690342f, 0.622761f,
				0.662646f, 0.920358f, 1.42965f, 1.41891f, 1.42044f, 1.14279f, 1.43271f, 1.50481f };

		Float[] LShoulderRollTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 4.64f, 5.08f, 5.52f, 5.92f, 6.52f, 7.12f, 7.64f,
				8.36f, 9.08f, 10.4f, 11.68f, 12.76f, 13.28f, 13.64f, 14.4f, 15.12f, 16f, 16.68f, 17.28f };
		Float[] LShoulderRollKeys = { 0.130348f, 0.130348f, 0.0137641f, 0.0137641f, 0.0106959f, 0.0106959f, 0.0106959f,
				-0.147306f, -0.27923f, -0.247016f, -0.248551f, -0.247016f, -0.0706061f, 0.303691f, 0.302157f,
				-0.314159f, -0.266959f, 0.375789f, 0.74088f, 0.780764f, 0.771559f, 0.67952f, 0.337438f, 0.130348f };

		Float[] LWristYawTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 4.64f, 5.08f, 5.52f, 5.92f, 6.52f, 7.12f, 7.64f,
				8.36f, 9.08f, 10.4f, 11.68f, 12.76f, 13.28f, 13.64f, 14.4f, 15.12f, 16f, 16.68f, 17.28f };
		Float[] LWristYawKeys = { 0.147222f, 0.144154f, 1.14586f, 1.14586f, -1.76875f, -1.76875f, -1.76722f, -0.77778f,
				-0.277696f, -0.309909f, -0.308375f, -0.308375f, -0.30224f, -0.846809f, -0.846809f, -0.461776f,
				-0.48632f, -0.731761f, -0.902033f, -0.920441f, -0.920441f, -0.38661f, 0.091998f, 0.0889301f };

		Float[] RAnklePitchTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 10.4f, 11.68f, 12.76f, 13.64f, 14.4f,
				15.12f, 16f, 17.28f };
		Float[] RAnklePitchKeys = { 0.10282f, 0.10282f, 0.101286f, 0.101286f, 0.101286f, 0.10282f, 0.10282f, 0.101286f,
				0.104354f, 0.10282f, 0.10282f, -0.095066f, -0.366584f, -0.653443f, 0.105888f };

		Float[] RAnkleRollTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 10.4f, 11.68f, 12.76f, 13.64f, 14.4f,
				15.12f, 16f, 17.28f };
		Float[] RAnkleRollKeys = { 0.073674f, 0.075208f, 0.073674f, 0.075208f, 0.075208f, 0.073674f, 0.075208f,
				0.073674f, 0.07214f, 0.073674f, 0.073674f, 0.00771189f, 0.391212f, 0.021518f, 0.07214f };

		Float[] RElbowRollTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 4.64f, 5.08f, 5.52f, 5.92f, 6.52f, 7.12f, 7.64f,
				8.36f, 9.08f, 10.4f, 11.68f, 12.76f, 13.28f, 13.64f, 14.4f, 15.12f, 16f, 16.68f, 17.28f };
		Float[] RElbowRollKeys = { 0.389678f, 0.401949f, 0.401949f, 0.418823f, 0.418823f, 1.22417f, 1.20423f, 1.13213f,
				1.1306f, 0.299172f, 0.684206f, 1.54325f, 1.53404f, 1.53251f, 0.681137f, 0.681137f, 1.34843f, 1.28247f,
				1.284f, 1.29627f, 1.29474f, 1.27019f, 0.417291f, 0.418823f };

		Float[] RElbowYawTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 4.64f, 5.08f, 5.52f, 5.92f, 6.52f, 7.12f, 7.64f,
				8.36f, 9.08f, 10.4f, 11.68f, 12.76f, 13.28f, 13.64f, 14.4f, 15.12f, 16f, 16.68f, 17.28f };
		Float[] RElbowYawKeys = { 1.17654f, 1.15506f, 1.15506f, 1.83308f, 1.83155f, 1.26397f, 0.141086f, -0.0153821f,
				-0.0153821f, -0.719487f, 1.33607f, 1.08603f, 1.09217f, 1.09217f, -0.092082f, -0.092082f, -0.286901f,
				-0.251617f, -0.239346f, -0.217869f, -0.222472f, -0.193327f, 1.17807f, 1.16273f };

		Float[] RHandTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 4.64f, 5.08f, 5.52f, 5.92f, 6.52f, 7.12f, 7.64f, 8.36f,
				9.08f, 10.4f, 11.68f, 12.76f, 13.28f, 13.64f, 14.4f, 15.12f, 16f, 16.68f, 17.28f };
		Float[] RHandKeys = { 0.31f, 0.9304f, 0.9304f, 0.9304f, 0.9304f, 0.9304f, 0.9304f, 0.9304f, 0.9304f, 0.9304f,
				0.9304f, 0.9304f, 0.9304f, 0.9304f, 0.9304f, 0.9304f, 0.9284f, 0.9284f, 0.9284f, 0.9304f, 0.9304f,
				0.9284f, 0.3096f, 0.3064f };

		Float[] RHipPitchTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 10.4f, 11.68f, 12.76f, 13.64f, 14.4f,
				15.12f, 16f, 17.28f };
		Float[] RHipPitchKeys = { 0.131882f, 0.131882f, 0.130348f, 0.131882f, 0.131882f, 0.131882f, 0.128814f,
				0.130348f, 0.131882f, 0.131882f, 0.131882f, 0.133416f, 0.131882f, -0.0767419f, 0.130348f };

		Float[] RHipRollTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 10.4f, 11.68f, 12.76f, 13.64f, 14.4f,
				15.12f, 16f, 17.28f };
		Float[] RHipRollKeys = { -0.0643861f, -0.0643861f, -0.0643861f, -0.0643861f, -0.0643861f, -0.0643861f,
				-0.0643861f, -0.0643861f, -0.0643861f, -0.0643861f, -0.0643861f, 0.046062f, -0.671851f, 0.0767419f,
				-0.06592f };

		Float[] RHipYawPitchTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 10.4f, 11.68f, 12.76f, 13.64f,
				14.4f, 15.12f, 16f, 17.28f };
		Float[] RHipYawPitchKeys = { -0.1733f, -0.1733f, -0.1733f, -0.174835f, -0.174835f, -0.1733f, -0.1733f, -0.1733f,
				-0.1733f, -0.1733f, -0.1733f, -0.171766f, -0.240796f, -0.576742f, -0.1733f };

		Float[] RKneePitchTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 5.08f, 5.92f, 10.4f, 11.68f, 12.76f, 13.64f, 14.4f,
				15.12f, 16f, 17.28f };
		Float[] RKneePitchKeys = { -0.091998f, -0.091998f, -0.0923279f, -0.0923279f, -0.0923279f, -0.0923279f,
				-0.091998f, -0.091998f, -0.091998f, -0.091998f, -0.091998f, 0.131966f, 0.455641f, 1.16588f,
				-0.0923279f };

		Float[] RShoulderPitchTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 4.64f, 5.08f, 5.52f, 5.92f, 6.52f, 7.12f, 7.64f,
				8.36f, 9.08f, 10.4f, 11.68f, 12.76f, 13.28f, 13.64f, 14.4f, 15.12f, 16f, 16.68f, 17.28f };
		Float[] RShoulderPitchKeys = { 1.49569f, 0.464844f, 0.466378f, 0.46331f, 0.46331f, 0.44797f, 0.346725f,
				0.518534f, 0.556884f, 0.656595f, 0.492455f, -0.621227f, -0.573674f, -0.573674f, 1.02015f, 1.02169f,
				1.08918f, 1.08611f, 1.17355f, 1.18122f, 1.18122f, 1.15975f, 1.37911f, 1.48802f };

		Float[] RShoulderRollTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 4.64f, 5.08f, 5.52f, 5.92f, 6.52f, 7.12f, 7.64f,
				8.36f, 9.08f, 10.4f, 11.68f, 12.76f, 13.28f, 13.64f, 14.4f, 15.12f, 16f, 16.68f, 17.28f };
		Float[] RShoulderRollKeys = { -0.0951499f, 0.076658f, 0.076658f, 0.16563f, 0.16563f, 0.314159f, 0.314159f,
				0.314159f, 0.314159f, -0.161112f, -0.237812f, -0.260822f, -0.254685f, -0.260822f, 0.306757f, 0.303691f,
				-0.60904f, -0.610574f, -0.610574f, -0.638187f, -0.639721f, -0.630516f, -0.319114f, -0.10282f };

		Float[] RWristYawTimes = { 1f, 1.96f, 2.76f, 3.48f, 4.16f, 4.64f, 5.08f, 5.52f, 5.92f, 6.52f, 7.12f, 7.64f,
				8.36f, 9.08f, 10.4f, 11.68f, 12.76f, 13.28f, 13.64f, 14.4f, 15.12f, 16f, 16.68f, 17.28f };
		Float[] RWristYawKeys = { 0.05825f, -1.15821f, -1.15821f, 1.19648f, 1.19648f, 0.561403f, 0.078192f, 0.0260359f,
				0.0275701f, 0.628898f, -0.351328f, 0.977116f, 0.960242f, 0.960242f, 0.694859f, 0.694859f, 0.944902f,
				0.84826f, 0.846726f, 0.825251f, 0.825251f, 0.812978f, 0.0398422f, 0.131882f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll",
				"RHipYawPitch", "RKneePitch", "RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación macarenaDance", "macarena_dance");
		}
	}

	private void sprinklerMovement1() {
		try {
			Float[] HeadPitchTimes = { 1f, 2.48f, 7f, 7.92f };
			Float[] HeadPitchKeys = { -0.12583f, -0.233874f, -0.173384f, -0.173384f };

			Float[] HeadYawTimes = { 1f, 2.48f, 7f, 7.92f };
			Float[] HeadYawKeys = { -0.0107799f, 0.912807f, 0.21932f, 0.21932f };

			Float[] LAnklePitchTimes = { 1f, 2.32f, 7f, 7.92f };
			Float[] LAnklePitchKeys = { 0.095066f, -0.214803f, -0.406552f, -0.406552f };

			Float[] LAnkleRollTimes = { 1f, 2.32f, 7f, 7.92f };
			Float[] LAnkleRollKeys = { -0.116542f, -0.14262f, -0.11961f, -0.11961f };

			Float[] LElbowRollTimes = { 1f, 2.4f, 3f, 7f, 7.92f };
			Float[] LElbowRollKeys = { -0.400331f, -0.0349066f, -0.0459781f, -0.358915f, -0.358915f };

			Float[] LElbowYawTimes = { 1f, 2.4f, 3f, 7f, 7.92f };
			Float[] LElbowYawKeys = { -1.21037f, -1.48956f, -1.48035f, -1.26099f, -1.26099f };

			Float[] LHandTimes = { 1f, 2.4f, 3f, 7f, 7.92f };
			Float[] LHandKeys = { 0.3056f, 0.9616f, 0.9592f, 0.9616f, 0.9616f };

			Float[] LHipPitchTimes = { 1f, 2.32f, 7f, 7.92f };
			Float[] LHipPitchKeys = { 0.136568f, -0.144154f, -0.268407f, -0.268407f };

			Float[] LHipRollTimes = { 1f, 2.32f, 7f, 7.92f };
			Float[] LHipRollKeys = { 0.115092f, 0.233211f, 0.173384f, 0.173384f };

			Float[] LHipYawPitchTimes = { 1f, 2.32f, 7f, 7.92f };
			Float[] LHipYawPitchKeys = { -0.1733f, -0.288349f, -0.28068f, -0.28068f };

			Float[] LKneePitchTimes = { 1f, 2.32f, 7f, 7.92f };
			Float[] LKneePitchKeys = { -0.090548f, 0.539926f, 0.820649f, 0.820649f };

			Float[] LShoulderPitchTimes = { 1f, 2.4f, 2.48f, 3f, 7f, 7.92f };
			Float[] LShoulderPitchKeys = { 1.53089f, -0.420357f, -0.560251f, -0.417134f, -0.0966839f, -0.0966839f };

			Float[] LShoulderRollTimes = { 1f, 2.4f, 3f, 3.44f, 3.8f, 4.2f, 4.6f, 4.96f, 5.4f, 5.8f, 6.16f, 6.6f, 7f,
					7.92f };
			Float[] LShoulderRollKeys = { 0.13495f, 0.819114f, 0.461692f, 0.610865f, 0.223402f, 0.460767f, 0.0436332f,
					0.312414f, -0.0610865f, 0.207694f, -0.195477f, 0.0593412f, -0.159578f, -0.159578f };

			Float[] LWristYawTimes = { 1f, 2.4f, 3f, 3.44f, 3.8f, 7f, 7.92f };
			Float[] LWristYawKeys = { 0.121144f, 0.312894f, 0.131882f, 0.0663225f, 0.0663225f, -0.29457f, -0.29457f };

			Float[] RAnklePitchTimes = { 1f, 2.32f, 7f, 7.92f };
			Float[] RAnklePitchKeys = { 0.10282f, -0.0199001f, -0.263807f, -0.263807f };

			Float[] RAnkleRollTimes = { 1f, 2.32f, 7f, 7.92f };
			Float[] RAnkleRollKeys = { 0.07214f, 0.116626f, 0.0828778f, 0.0828778f };

			Float[] RElbowRollTimes = { 1f, 2.44f, 2.96f, 3.4f, 3.8f, 4.24f, 4.6f, 5f, 5.48f, 5.8f, 6.2f, 6.56f, 7f,
					7.92f };
			Float[] RElbowRollKeys = { 0.385075f, 1.54462f, 1.53864f, 1.54462f, 1.53864f, 1.54462f, 1.53864f, 1.54462f,
					1.53864f, 1.54462f, 1.53864f, 1.54462f, 1.53558f, 1.53558f };

			Float[] RElbowYawTimes = { 1f, 2.44f, 2.96f, 3.4f, 3.8f, 4.24f, 4.6f, 5f, 5.48f, 5.8f, 6.2f, 6.56f, 7f,
					7.92f };
			Float[] RElbowYawKeys = { 1.18421f, 0.966378f, 1.38823f, 0.966378f, 1.38823f, 0.966378f, 1.38823f,
					0.966378f, 1.38823f, 0.966378f, 1.38823f, 0.966378f, 1.36982f, 1.36982f };

			Float[] RHandTimes = { 1f, 2.44f, 2.96f, 3.4f, 3.8f, 4.24f, 4.6f, 5f, 5.48f, 5.8f, 6.2f, 6.56f, 7f, 7.92f };
			Float[] RHandKeys = { 0.3068f, 0.7708f, 0.7696f, 0.7708f, 0.7696f, 0.7708f, 0.7696f, 0.7708f, 0.7696f,
					0.7708f, 0.7696f, 0.7708f, 0.7692f, 0.7692f };

			Float[] RHipPitchTimes = { 1f, 2.32f, 7f, 7.92f };
			Float[] RHipPitchKeys = { 0.130348f, 0.138018f, -0.251617f, -0.251617f };

			Float[] RHipRollTimes = { 1f, 2.32f, 7f, 7.92f };
			Float[] RHipRollKeys = { -0.06592f, -0.0383082f, -0.0291041f, -0.0291041f };

			Float[] RHipYawPitchTimes = { 1f, 2.32f, 7f, 7.92f };
			Float[] RHipYawPitchKeys = { -0.1733f, -0.288349f, -0.28068f, -0.28068f };

			Float[] RKneePitchTimes = { 1f, 2.32f, 7f, 7.92f };
			Float[] RKneePitchKeys = { -0.0904641f, 0.0798099f, 0.676537f, 0.676537f };

			Float[] RShoulderPitchTimes = { 1f, 2.44f, 2.96f, 3.4f, 3.8f, 4.24f, 4.6f, 5f, 5.48f, 5.8f, 6.2f, 6.56f, 7f,
					7.92f };
			Float[] RShoulderPitchKeys = { 1.51103f, -0.742414f, -0.566003f, -0.742414f, -0.566003f, -0.742414f,
					-0.566003f, -0.742414f, -0.566003f, -0.742414f, -0.566003f, -0.742414f, -0.544529f, -0.544529f };

			Float[] RShoulderRollTimes = { 1f, 2.44f, 2.96f, 3.4f, 3.8f, 4.24f, 4.6f, 5f, 5.48f, 5.8f, 6.2f, 6.56f, 7f,
					7.92f };
			Float[] RShoulderRollKeys = { -0.113558f, -0.43263f, -0.0614019f, -0.43263f, -0.0614019f, -0.43263f,
					-0.0614019f, -0.43263f, -0.0614019f, -0.43263f, -0.0614019f, -0.43263f, -0.075208f, -0.075208f };

			Float[] RWristYawTimes = { 1f, 2.44f, 2.96f, 3.4f, 3.8f, 4.24f, 4.6f, 5f, 5.48f, 5.8f, 6.2f, 6.56f, 7f,
					7.92f };
			Float[] RWristYawKeys = { 0.105804f, 1.30539f, 0.803775f, 1.30539f, 0.803775f, 1.30539f, 0.803775f,
					1.30539f, 0.803775f, 1.30539f, 0.803775f, 1.30539f, 0.820649f, 0.820649f };

			String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw",
					"LHand", "LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll",
					"LWristYaw", "RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch",
					"RHipRoll", "RHipYawPitch", "RKneePitch", "RShoulderPitch", "RShoulderRoll", "RWristYaw" };

			List<String> names = new ArrayList<>(Arrays.asList(namesArray));
			List<Object> times = new ArrayList<>();
			List<Object> keys = new ArrayList<>();

			times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

			times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
			keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

			motion.angleInterpolation(names, keys, times, true);
		} catch (CallError | InterruptedException e) {

		}
	}

	private void sprinklerMovement2() {
		Float[] HeadPitchTimes = { 2.48f, 7f, 7.44f, 8.76f };
		Float[] HeadPitchKeys = { -0.233874f, -0.173384f, -0.173384f, -0.173384f };

		Float[] HeadYawTimes = { 2.48f, 7f, 7.44f, 8.76f };
		Float[] HeadYawKeys = { -0.912807f, -0.21932f, -0.21932f, 0.0107799f };

		Float[] LAnklePitchTimes = { 2.32f, 7f, 7.44f, 8.76f };
		Float[] LAnklePitchKeys = { -0.0199001f, -0.263807f, -0.263807f, 0.107422f };

		Float[] LAnkleRollTimes = { 2.32f, 7f, 7.44f, 8.76f };
		Float[] LAnkleRollKeys = { -0.116626f, -0.0828778f, -0.0828778f, -0.073674f };

		Float[] LElbowRollTimes = { 2.44f, 2.96f, 3.4f, 3.8f, 4.24f, 4.6f, 5f, 5.48f, 5.8f, 6.2f, 6.56f, 7f, 7.44f,
				8.76f };
		Float[] LElbowRollKeys = { -1.54462f, -1.53864f, -1.54462f, -1.53864f, -1.54462f, -1.53864f, -1.54462f,
				-1.53864f, -1.54462f, -1.53864f, -1.54462f, -1.53558f, -1.53558f, -0.412688f };

		Float[] LElbowYawTimes = { 2.44f, 2.96f, 3.4f, 3.8f, 4.24f, 4.6f, 5f, 5.48f, 5.8f, 6.2f, 6.56f, 7f, 7.44f,
				8.76f };
		Float[] LElbowYawKeys = { -0.966378f, -1.38823f, -0.966378f, -1.38823f, -0.966378f, -1.38823f, -0.966378f,
				-1.38823f, -0.966378f, -1.38823f, -0.966378f, -1.36982f, -1.36982f, -1.19494f };

		Float[] LHandTimes = { 2.44f, 2.96f, 3.4f, 3.8f, 4.24f, 4.6f, 5f, 5.48f, 5.8f, 6.2f, 6.56f, 7f, 7.44f, 8.76f };
		Float[] LHandKeys = { 0.7708f, 0.7696f, 0.7708f, 0.7696f, 0.7708f, 0.7696f, 0.7708f, 0.7696f, 0.7708f, 0.7696f,
				0.7708f, 0.7692f, 0.7692f, 0.3068f };

		Float[] LHipPitchTimes = { 2.32f, 7f, 7.44f, 8.76f };
		Float[] LHipPitchKeys = { 0.138018f, -0.251617f, -0.251617f, 0.133416f };

		Float[] LHipRollTimes = { 2.32f, 7f, 7.44f, 8.76f };
		Float[] LHipRollKeys = { 0.0383082f, 0.0291041f, 0.0291041f, 0.0643861f };

		Float[] LHipYawPitchTimes = { 2.32f, 7f, 7.44f, 8.76f };
		Float[] LHipYawPitchKeys = { -0.288349f, -0.28068f, -0.28068f, -0.168698f };

		Float[] LKneePitchTimes = { 2.32f, 7f, 7.44f, 8.76f };
		Float[] LKneePitchKeys = { 0.0798099f, 0.676537f, 0.676537f, -0.091998f };

		Float[] LShoulderPitchTimes = { 2.44f, 2.96f, 3.4f, 3.8f, 4.24f, 4.6f, 5f, 5.48f, 5.8f, 6.2f, 6.56f, 7f, 7.44f,
				8.76f };
		Float[] LShoulderPitchKeys = { -0.742414f, -0.566003f, -0.742414f, -0.566003f, -0.742414f, -0.566003f,
				-0.742414f, -0.566003f, -0.742414f, -0.566003f, -0.742414f, -0.544529f, -0.544529f, 1.48189f };

		Float[] LShoulderRollTimes = { 2.44f, 2.96f, 3.4f, 3.8f, 4.24f, 4.6f, 5f, 5.48f, 5.8f, 6.2f, 6.56f, 7f, 7.44f,
				8.76f };
		Float[] LShoulderRollKeys = { 0.43263f, 0.0614019f, 0.43263f, 0.0614019f, 0.43263f, 0.0614019f, 0.43263f,
				0.0614019f, 0.43263f, 0.0614019f, 0.43263f, 0.075208f, 0.075208f, 0.0813439f };

		Float[] LWristYawTimes = { 2.44f, 2.96f, 3.4f, 3.8f, 4.24f, 4.6f, 5f, 5.48f, 5.8f, 6.2f, 6.56f, 7f, 7.44f,
				8.76f };
		Float[] LWristYawKeys = { -1.30539f, -0.803775f, -1.30539f, -0.803775f, -1.30539f, -0.803775f, -1.30539f,
				-0.803775f, -1.30539f, -0.803775f, -1.30539f, -0.820649f, -0.820649f, -0.167164f };

		Float[] RAnklePitchTimes = { 2.32f, 7f, 7.44f, 8.76f };
		Float[] RAnklePitchKeys = { -0.214803f, -0.406552f, -0.406552f, 0.101202f };

		Float[] RAnkleRollTimes = { 2.32f, 7f, 7.44f, 8.76f };
		Float[] RAnkleRollKeys = { 0.14262f, 0.11961f, 0.11961f, 0.118076f };

		Float[] RElbowRollTimes = { 2.4f, 3f, 7f, 7.44f, 8.76f };
		Float[] RElbowRollKeys = { 0.0349066f, 0.0459781f, 0.358915f, 0.358915f, 0.38806f };

		Float[] RElbowYawTimes = { 2.4f, 3f, 7f, 7.44f, 8.76f };
		Float[] RElbowYawKeys = { 1.48956f, 1.48035f, 1.26099f, 1.26099f, 1.21497f };

		Float[] RHandTimes = { 2.4f, 3f, 7f, 7.44f, 8.76f };
		Float[] RHandKeys = { 0.9616f, 0.9592f, 0.9616f, 0.9616f, 0.3052f };

		Float[] RHipPitchTimes = { 2.32f, 7f, 7.44f, 8.76f };
		Float[] RHipPitchKeys = { -0.144154f, -0.268407f, -0.268407f, 0.142704f };

		Float[] RHipRollTimes = { 2.32f, 7f, 7.44f, 8.76f };
		Float[] RHipRollKeys = { -0.233211f, -0.173384f, -0.173384f, -0.115092f };

		Float[] RHipYawPitchTimes = { 2.32f, 7f, 7.44f, 8.76f };
		Float[] RHipYawPitchKeys = { -0.288349f, -0.28068f, -0.28068f, -0.168698f };

		Float[] RKneePitchTimes = { 2.32f, 7f, 7.44f, 8.76f };
		Float[] RKneePitchKeys = { 0.539926f, 0.820649f, 0.820649f, -0.0923279f };

		Float[] RShoulderPitchTimes = { 2.4f, 2.48f, 3f, 7f, 7.44f, 8.76f };
		Float[] RShoulderPitchKeys = { -0.420357f, -0.560251f, -0.417134f, -0.0966839f, -0.0966839f, 1.48794f };

		Float[] RShoulderRollTimes = { 2.4f, 3f, 3.44f, 3.8f, 4.2f, 4.6f, 4.96f, 5.4f, 5.8f, 6.16f, 6.6f, 7f, 7.44f,
				8.76f };
		Float[] RShoulderRollKeys = { -0.819114f, -0.461692f, -0.610865f, -0.223402f, -0.460767f, -0.0436332f,
				-0.312414f, 0.0610865f, -0.207694f, 0.195477f, -0.0593412f, 0.159578f, 0.159578f, -0.0889301f };

		Float[] RWristYawTimes = { 2.4f, 3f, 3.44f, 3.8f, 7f, 7.44f, 8.76f };
		Float[] RWristYawKeys = { -0.312894f, -0.131882f, -0.0663225f, -0.0663225f, 0.29457f, 0.29457f, -0.161028f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll",
				"RHipYawPitch", "RKneePitch", "RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
		} catch (CallError | InterruptedException e) {

		}
	}

	/**
	 * Nao realiza el baile "sprinkler". Esta es una operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese timeline se exportó
	 * a código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),sprinkler_dance]
	 */
	@OPERATION
	public void sprinklerDance() {
		try {
			sprinklerMovement1();
			sprinklerMovement2();
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación sprinklerDance", "sprinkler_dance");
		}
	}

	/**
	 * NAO recoge pesos invisibles y ejercicios con ellos. Esta es una operación
	 * sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese timeline se exportó
	 * a código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),work_out]
	 */
	@OPERATION
	public void workOut() {
		Float[] HeadPitchTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] HeadPitchKeys = { -0.15651f, -0.136568f, -0.136568f, -0.136568f, -0.136568f, -0.136568f, -0.136568f,
				-0.136568f, -0.136568f, -0.136568f, -0.136568f, -0.136568f, -0.136568f, -0.136568f, -0.136568f,
				-0.136568f, -0.136568f };

		Float[] HeadYawTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] HeadYawKeys = { -0.019984f, -0.00617791f, -0.00464392f, -0.00617791f, -0.00464392f, -0.00464392f,
				-0.00617791f, -0.00617791f, -0.00464392f, -0.00617791f, -0.00617791f, -0.00617791f, -0.00617791f,
				-0.00617791f, -0.00617791f, -0.00617791f, -0.00617791f };

		Float[] LAnklePitchTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] LAnklePitchKeys = { -1.18944f, -1.18889f, 0.095066f, 0.095066f, 0.0966001f, 0.095066f, 0.0966001f,
				0.0966001f, 0.0966001f, 0.0966001f, 0.0966001f, 0.095066f, 0.0966001f, 0.0966001f, -1.18889f, -1.18944f,
				0.0966001f };

		Float[] LAnkleRollTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] LAnkleRollKeys = { -0.05058f, -0.049046f, -0.115008f, -0.115008f, -0.116542f, -0.116542f, -0.116542f,
				-0.116542f, -0.116542f, -0.115008f, -0.115008f, -0.116542f, -0.116542f, -0.115008f, -0.049046f,
				-0.05058f, -0.116542f };

		Float[] LElbowRollTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] LElbowRollKeys = { -0.397265f, -0.398797f, -0.384992f, -1.52936f, -0.397265f, -1.52936f, -0.237728f,
				-1.39283f, -0.37272f, -0.929562f, -0.174835f, -1.37442f, -1.36675f, -0.44175f, -0.444818f, -0.444818f,
				-0.418739f };

		Float[] LElbowYawTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] LElbowYawKeys = { -1.34996f, -1.37144f, -1.72272f, -1.52177f, -1.46348f, -1.45581f, -1.42666f,
				-1.47115f, -0.124296f, -0.128898f, -0.07214f, -1.61841f, -1.60614f, -1.67517f, -1.67517f, -1.67517f,
				-1.21037f };

		Float[] LHandTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f, 11.28f,
				12.32f, 13.52f, 14.56f, 16.04f };
		Float[] LHandKeys = { 0.9576f, 0.0352f, 0.016f, 0.0268f, 0.0268f, 0.0268f, 0.0268f, 0.0268f, 0.0268f, 0.0268f,
				0.0268f, 0.0268f, 0.0316f, 0.0316f, 0.0316f, 0.9616f, 0.6208f };

		Float[] LHipPitchTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] LHipPitchKeys = { -0.559868f, -0.559868f, 0.135034f, 0.135034f, 0.135034f, 0.135034f, 0.135034f,
				0.135034f, 0.135034f, 0.135034f, 0.135034f, 0.135034f, 0.135034f, 0.135034f, -0.619695f, -0.618161f,
				0.135034f };

		Float[] LHipRollTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] LHipRollKeys = { 0.16418f, 0.16418f, 0.115092f, 0.115092f, 0.115092f, 0.115092f, 0.115092f, 0.115092f,
				0.115092f, 0.115092f, 0.115092f, 0.115092f, 0.116626f, 0.116626f, 0.124296f, 0.124296f, 0.116626f };

		Float[] LHipYawPitchTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] LHipYawPitchKeys = { -0.504645f, -0.504645f, -0.1733f, -0.1733f, -0.1733f, -0.174835f, -0.1733f,
				-0.1733f, -0.1733f, -0.1733f, -0.1733f, -0.1733f, -0.1733f, -0.1733f, -0.408002f, -0.408002f,
				-0.1733f };

		Float[] LKneePitchTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] LKneePitchKeys = { 2.11255f, 2.11255f, -0.090548f, -0.090548f, -0.090548f, -0.092082f, -0.090548f,
				-0.090548f, -0.090548f, -0.090548f, -0.090548f, -0.092082f, -0.090548f, -0.090548f, 2.11255f, 2.11255f,
				-0.090548f };

		Float[] LShoulderPitchTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f,
				10.12f, 11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] LShoulderPitchKeys = { 1.70116f, 1.86684f, 1.71957f, 1.00933f, 1.49407f, 1.09063f, 1.36368f, 1.04308f,
				-1.55245f, -1.54631f, -1.53558f, -0.130432f, 1.52936f, 1.80547f, 1.82849f, 1.82696f, 1.53856f };

		Float[] LShoulderRollTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] LShoulderRollKeys = { 0.395731f, 0.292952f, 0.228525f, -0.00924586f, 0.00916195f, -0.00771189f,
				0.00762796f, -0.038392f, 0.34971f, 1.04154f, 0.078192f, 0.177901f, 0.136484f, 0.207048f, 0.196309f,
				0.196309f, 0.136484f };

		Float[] LWristYawTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] LWristYawKeys = { -0.352862f, -0.40962f, 0.0843279f, -1.62915f, -1.63835f, -1.61228f, -1.61688f,
				-1.70278f, -0.47865f, -0.47865f, -0.581429f, 0.021434f, 0.0199001f, 0.022968f, 0.021434f, 0.022968f,
				0.076658f };

		Float[] RAnklePitchTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] RAnklePitchKeys = { -1.18421f, -1.18574f, 0.101286f, 0.099752f, 0.101286f, 0.099752f, 0.101286f,
				0.101286f, 0.10282f, 0.099752f, 0.101286f, 0.0982179f, 0.101286f, 0.101286f, -1.1863f, -1.1863f,
				0.101286f };

		Float[] RAnkleRollTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] RAnkleRollKeys = { 0.0429941f, 0.0429941f, 0.073674f, 0.073674f, 0.073674f, 0.073674f, 0.073674f,
				0.073674f, 0.073674f, 0.073674f, 0.073674f, 0.073674f, 0.073674f, 0.073674f, 0.0521979f, 0.0506639f,
				0.073674f };

		Float[] RElbowRollTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] RElbowRollKeys = { 0.346725f, 0.441834f, 0.441834f, 1.5233f, 1.52484f, 0.159578f, 1.50029f, 1.50029f,
				0.309909f, 1.07691f, 0.038392f, 1.50029f, 1.36684f, 0.227074f, 0.230143f, 0.231675f, 0.389678f };

		Float[] RElbowYawTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] RElbowYawKeys = { 1.52169f, 1.5447f, 1.26704f, 1.30539f, 1.30693f, 1.61679f, 1.57691f, 1.57691f,
				0.754686f, 0.282215f, 0.314428f, 1.57077f, 1.4818f, 1.57998f, 1.58151f, 1.58611f, 1.1796f };

		Float[] RHandTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f, 11.28f,
				12.32f, 13.52f, 14.56f, 16.04f };
		Float[] RHandKeys = { 0.9432f, 0.0176001f, 0.0172f, 0.1056f, 0.1056f, 0.1056f, 0.1056f, 0.1056f, 0.1056f,
				0.1056f, 0.1056f, 0.1056f, 0.1056f, 0.1056f, 0f, 0.9484f, 0.4912f };

		Float[] RHipPitchTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] RHipPitchKeys = { -0.564555f, -0.575959f, 0.131882f, 0.131882f, 0.131882f, 0.131882f, 0.130348f,
				0.131882f, 0.131882f, 0.131882f, 0.131882f, 0.131882f, 0.131882f, 0.131882f, -0.622845f, -0.622845f,
				0.130348f };

		Float[] RHipRollTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] RHipRollKeys = { -0.115008f, -0.116542f, -0.0643861f, -0.0643861f, -0.0643861f, -0.0643861f,
				-0.0643861f, -0.0643861f, -0.0643861f, -0.0643861f, -0.0643861f, -0.0643861f, -0.0643861f, -0.0643861f,
				-0.11961f, -0.121144f, -0.0643861f };

		Float[] RHipYawPitchTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] RHipYawPitchKeys = { -0.504645f, -0.504645f, -0.1733f, -0.1733f, -0.1733f, -0.174835f, -0.1733f,
				-0.1733f, -0.1733f, -0.1733f, -0.1733f, -0.1733f, -0.1733f, -0.1733f, -0.408002f, -0.408002f,
				-0.1733f };

		Float[] RKneePitchTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] RKneePitchKeys = { 2.11255f, 2.11255f, -0.091998f, -0.091998f, -0.0923279f, -0.0923279f, -0.0923279f,
				-0.091998f, -0.091998f, -0.0904641f, -0.091998f, -0.0923279f, -0.091998f, -0.091998f, 2.11255f,
				2.11255f, -0.091998f };

		Float[] RShoulderPitchTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f,
				10.12f, 11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] RShoulderPitchKeys = { 1.67824f, 1.69818f, 1.55859f, 1.08611f, 1.08765f, 1.26252f, 1.11526f, 1.11679f,
				-1.37135f, -1.35908f, -1.4818f, 0.07214f, 1.51717f, 1.63989f, 1.63375f, 1.63222f, 1.53558f };

		Float[] RShoulderRollTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] RShoulderRollKeys = { -0.262356f, -0.506262f, -0.219404f, 0.174835f, 0.168698f, 0.06592f, 0.15029f,
				0.15029f, -0.236277f, -1.09839f, 0.0735901f, -0.260822f, -0.108956f, -0.16418f, -0.162646f, -0.16418f,
				-0.104354f };

		Float[] RWristYawTimes = { 1f, 1.76f, 2.92f, 3.84f, 4.56f, 5.12f, 5.72f, 6.32f, 7.48f, 8.52f, 9.44f, 10.12f,
				11.28f, 12.32f, 13.52f, 14.56f, 16.04f };
		Float[] RWristYawKeys = { 0.0168321f, 0.151824f, 0.239262f, 1.58458f, 1.58458f, 1.6306f, 1.53396f, 1.53396f,
				0.154892f, 0.154892f, 0.259204f, 0.45709f, 0.176367f, -0.0123138f, 0.05058f, 0.0152981f, 0.0735901f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll",
				"RHipYawPitch", "RKneePitch", "RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación workOut", "work_out");
		}
	}

	/**
	 * NAO gira sus brazos de delante a atras y a cada lado. Esta es una
	 * operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese timeline se exportó
	 * a código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),arm_dance]
	 */
	@OPERATION
	public void armDance() {
		Float[] HeadPitchTimes = { 1f, 2f, 2.4f, 3f, 3.6f, 4.2f, 4.8f, 5.4f, 6.32f, 7.2f, 7.6f, 8.2f, 8.8f, 9.4f, 10f,
				10.6f, 11.52f, 12.4f, 13.48f, 13.88f, 14.48f, 15.08f, 15.68f, 16.28f, 16.88f, 17.8f, 19.72f };
		Float[] HeadPitchKeys = { -0.18719f, -0.185656f, 0.0291041f, -0.185656f, 0.00149202f, -0.185656f, 0.0812599f,
				-0.185656f, -0.354396f, -0.185656f, 0.0291041f, -0.185656f, 0.00149202f, -0.185656f, 0.0812599f,
				-0.185656f, -0.354396f, -0.354396f, -0.185656f, 0.0291041f, -0.185656f, 0.00149202f, -0.185656f,
				0.0812599f, -0.185656f, -0.354396f, -0.233211f };

		Float[] HeadYawTimes = { 1f, 2f, 2.4f, 3f, 3.6f, 4.2f, 4.8f, 5.4f, 6.32f, 7.2f, 7.6f, 8.2f, 8.8f, 9.4f, 10f,
				10.6f, 11.52f, 12.4f, 13.48f, 13.88f, 14.48f, 15.08f, 15.68f, 16.28f, 16.88f, 17.8f, 19.72f };
		Float[] HeadYawKeys = { -0.00157595f, -0.00157595f, -0.00157595f, 0.00609397f, -4.19617e-05f, 0.00609397f,
				-4.19617e-05f, 0.00609397f, 0.431013f, 0.00157595f, 0.00157595f, -0.00609397f, 4.19617e-05f,
				-0.00609397f, 4.19617e-05f, -0.00609397f, -0.431013f, -0.431013f, -0.00157595f, -0.00157595f,
				0.00609397f, -4.19617e-05f, 0.00609397f, -4.19617e-05f, 0.00609397f, 0.431013f, -0.00924586f };

		Float[] LAnklePitchTimes = { 1f, 2f, 3.32f, 5.4f, 6.32f, 7.2f, 8.52f, 10.6f, 11.52f, 12.4f, 13.48f, 14.8f,
				16.88f, 17.8f, 19.72f };
		Float[] LAnklePitchKeys = { 0.105804f, -0.417291f, -0.607505f, -0.421891f, -0.671934f, -0.31136f, -0.355846f,
				-0.31136f, -0.443284f, -0.443284f, -0.417291f, -0.607505f, -0.421891f, -0.671934f, 0.108872f };

		Float[] LAnkleRollTimes = { 1f, 2f, 3.32f, 5.4f, 6.32f, 7.2f, 8.52f, 10.6f, 11.52f, 12.4f, 13.48f, 14.8f,
				16.88f, 17.8f, 19.72f };
		Float[] LAnkleRollKeys = { -0.0735901f, -0.0858622f, -0.125746f, -0.0858622f, -0.105804f, -0.124296f,
				-0.144238f, -0.124296f, -0.314512f, -0.314512f, -0.0858622f, -0.125746f, -0.0858622f, -0.105804f,
				-0.113474f };

		Float[] LElbowRollTimes = { 1f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f, 3.2f, 3.4f, 3.6f, 3.8f, 4f, 4.2f, 4.4f, 4.6f,
				4.8f, 5f, 5.2f, 5.4f, 6.32f, 7.2f, 7.4f, 7.6f, 7.8f, 8f, 8.2f, 8.4f, 8.6f, 8.8f, 9f, 9.2f, 9.4f, 9.6f,
				9.8f, 10f, 10.2f, 10.4f, 10.6f, 11.52f, 12.4f, 13.48f, 13.68f, 13.88f, 14.08f, 14.28f, 14.48f, 14.68f,
				14.88f, 15.08f, 15.28f, 15.48f, 15.68f, 15.88f, 16.08f, 16.28f, 16.48f, 16.68f, 16.88f, 17.8f, 19.72f };
		Float[] LElbowRollKeys = { -0.435615f, -1.24863f, -1.07529f, -0.990921f, -1.4005f, -1.54462f, -1.39897f,
				-1.24863f, -1.07529f, -0.990921f, -1.4005f, -1.54462f, -1.39897f, -1.24863f, -1.07529f, -0.990921f,
				-1.4005f, -1.54462f, -1.39897f, -1.53856f, -1.33309f, -1.34689f, -1.43126f, -1.31468f, -0.83147f,
				-1.16281f, -1.33309f, -1.34689f, -1.43126f, -1.31468f, -0.83147f, -1.16281f, -1.33309f, -1.34689f,
				-1.43126f, -1.31468f, -0.83147f, -1.16281f, -0.932714f, -0.932714f, -1.24863f, -1.07529f, -0.990921f,
				-1.4005f, -1.54462f, -1.39897f, -1.24863f, -1.07529f, -0.990921f, -1.4005f, -1.54462f, -1.39897f,
				-1.24863f, -1.07529f, -0.990921f, -1.4005f, -1.54462f, -1.39897f, -1.53856f, -0.446352f };

		Float[] LElbowYawTimes = { 1f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f, 3.2f, 3.4f, 3.6f, 3.8f, 4f, 4.2f, 4.4f, 4.6f,
				4.8f, 5f, 5.2f, 5.4f, 6.32f, 7.2f, 7.4f, 7.6f, 7.8f, 8f, 8.2f, 8.4f, 8.6f, 8.8f, 9f, 9.2f, 9.4f, 9.6f,
				9.8f, 10f, 10.2f, 10.4f, 10.6f, 11.52f, 12.4f, 13.48f, 13.68f, 13.88f, 14.08f, 14.28f, 14.48f, 14.68f,
				14.88f, 15.08f, 15.28f, 15.48f, 15.68f, 15.88f, 16.08f, 16.28f, 16.48f, 16.68f, 16.88f, 17.8f, 19.72f };
		Float[] LElbowYawKeys = { -1.21344f, -0.434165f, -0.31758f, -0.16418f, 0.061318f, 0.190241f, -0.176453f,
				-0.434165f, -0.31758f, -0.165714f, 0.061318f, 0.190241f, -0.176453f, -0.434165f, -0.31758f, -0.16418f,
				0.061318f, 0.190241f, -0.176453f, -1.57086f, -0.185572f, -0.144154f, -0.174835f, -0.351244f, -0.446352f,
				0.036858f, -0.185572f, -0.144154f, -0.176367f, -0.351244f, -0.446352f, 0.036858f, -0.185572f,
				-0.144154f, -0.174835f, -0.351244f, -0.446352f, 0.036858f, 0.389678f, 0.389678f, -0.434165f, -0.31758f,
				-0.16418f, 0.061318f, 0.190241f, -0.176453f, -0.434165f, -0.31758f, -0.165714f, 0.061318f, 0.190241f,
				-0.176453f, -0.434165f, -0.31758f, -0.16418f, 0.061318f, 0.190241f, -0.176453f, -1.57086f, -1.19503f };

		Float[] LHandTimes = { 1f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f, 3.2f, 3.4f, 3.6f, 3.8f, 4f, 4.2f, 4.4f, 4.6f, 4.8f,
				5f, 5.2f, 5.4f, 6.32f, 7.2f, 7.4f, 7.6f, 7.8f, 8f, 8.2f, 8.4f, 8.6f, 8.8f, 9f, 9.2f, 9.4f, 9.6f, 9.8f,
				10f, 10.2f, 10.4f, 10.6f, 11.52f, 12.4f, 13.48f, 13.68f, 13.88f, 14.08f, 14.28f, 14.48f, 14.68f, 14.88f,
				15.08f, 15.28f, 15.48f, 15.68f, 15.88f, 16.08f, 16.28f, 16.48f, 16.68f, 16.88f, 17.8f, 19.72f };
		Float[] LHandKeys = { 0.3136f, 0f, 0.1872f, 0.188f, 0.184f, 0f, 0.1908f, 0f, 0.1872f, 0.188f, 0.184f, 0f,
				0.1908f, 0f, 0.1872f, 0.188f, 0.184f, 0f, 0.1908f, 1f, 0f, 0.1568f, 0.178f, 0.1616f, 0.1672f, 0.1668f,
				0f, 0.1568f, 0.178f, 0.1616f, 0.1672f, 0.1668f, 0f, 0.1568f, 0.178f, 0.1616f, 0.1672f, 0.1668f, 1f, 1f,
				0f, 0.1872f, 0.188f, 0.184f, 0f, 0.1908f, 0f, 0.1872f, 0.188f, 0.184f, 0f, 0.1908f, 0f, 0.1872f, 0.188f,
				0.184f, 0f, 0.1908f, 1f, 0.2976f };

		Float[] LHipPitchTimes = { 1f, 2f, 3.32f, 5.4f, 6.32f, 7.2f, 8.52f, 10.6f, 11.52f, 12.4f, 13.48f, 14.8f, 16.88f,
				17.8f, 19.72f };
		Float[] LHipPitchKeys = { 0.131966f, -0.052114f, -0.33437f, -0.052114f, -0.406468f, -0.0767419f, -0.44797f,
				-0.0798099f, -0.104354f, -0.104354f, -0.052114f, -0.33437f, -0.052114f, -0.406468f, 0.144238f };

		Float[] LHipRollTimes = { 1f, 2f, 3.32f, 5.4f, 6.32f, 7.2f, 8.52f, 10.6f, 11.52f, 12.4f, 13.48f, 14.8f, 16.88f,
				17.8f, 19.72f };
		Float[] LHipRollKeys = { 0.06447f, 0.1335f, 0.174919f, 0.131966f, 0.276162f, 0.128814f, 0.108872f, 0.128814f,
				0.208583f, 0.208583f, 0.1335f, 0.174919f, 0.131966f, 0.276162f, 0.115092f };

		Float[] LHipYawPitchTimes = { 1f, 2f, 3.32f, 5.4f, 6.32f, 7.2f, 8.52f, 10.6f, 11.52f, 12.4f, 13.48f, 14.8f,
				16.88f, 17.8f, 19.72f };
		Float[] LHipYawPitchKeys = { -0.170232f, -0.36505f, -0.378855f, -0.36505f, -0.371186f, -0.36505f, -0.378855f,
				-0.36505f, -0.371186f, -0.371186f, -0.36505f, -0.378855f, -0.36505f, -0.371186f, -0.171766f };

		Float[] LKneePitchTimes = { 1f, 2f, 3.32f, 5.4f, 6.32f, 7.2f, 8.52f, 10.6f, 11.52f, 12.4f, 13.48f, 14.8f,
				16.88f, 17.8f, 19.72f };
		Float[] LKneePitchKeys = { -0.0874801f, 0.731677f, 1.13358f, 0.730143f, 1.30079f, 0.653526f, 1.00481f,
				0.650458f, 0.856014f, 0.856014f, 0.731677f, 1.13358f, 0.730143f, 1.30079f, -0.092082f };

		Float[] LShoulderPitchTimes = { 1f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f, 3.2f, 3.4f, 3.6f, 3.8f, 4f, 4.2f, 4.4f,
				4.6f, 4.8f, 5f, 5.2f, 5.4f, 6.32f, 7.2f, 7.4f, 7.6f, 7.8f, 8f, 8.2f, 8.4f, 8.6f, 8.8f, 9f, 9.2f, 9.4f,
				9.6f, 9.8f, 10f, 10.2f, 10.4f, 10.6f, 11.52f, 12.4f, 13.48f, 13.68f, 13.88f, 14.08f, 14.28f, 14.48f,
				14.68f, 14.88f, 15.08f, 15.28f, 15.48f, 15.68f, 15.88f, 16.08f, 16.28f, 16.48f, 16.68f, 16.88f, 17.8f,
				19.72f };
		Float[] LShoulderPitchKeys = { 1.4818f, 0.357381f, 0.510779f, 0.584411f, 0.605888f, 0.101229f, 0.282215f,
				0.357381f, 0.510779f, 0.584411f, 0.605888f, 0.101229f, 0.282215f, 0.357381f, 0.510779f, 0.584411f,
				0.605888f, 0.101229f, 0.282215f, 0.095066f, 0.624379f, 0.744032f, 0.510865f, 0.309909f, 0.61671f,
				0.725624f, 0.624379f, 0.744032f, 0.510865f, 0.309909f, 0.61671f, 0.725624f, 0.624379f, 0.744032f,
				0.510865f, 0.309909f, 0.61671f, 0.725624f, 0.72409f, 0.72409f, 0.357381f, 0.510779f, 0.584411f,
				0.605888f, 0.101229f, 0.282215f, 0.357381f, 0.510779f, 0.584411f, 0.605888f, 0.101229f, 0.282215f,
				0.357381f, 0.510779f, 0.584411f, 0.605888f, 0.101229f, 0.282215f, 0.095066f, 1.4726f };

		Float[] LShoulderRollTimes = { 1f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f, 3.2f, 3.4f, 3.6f, 3.8f, 4f, 4.2f, 4.4f, 4.6f,
				4.8f, 5f, 5.2f, 5.4f, 6.32f, 7.2f, 7.4f, 7.6f, 7.8f, 8f, 8.2f, 8.4f, 8.6f, 8.8f, 9f, 9.2f, 9.4f, 9.6f,
				9.8f, 10f, 10.2f, 10.4f, 10.6f, 11.52f, 12.4f, 13.48f, 13.68f, 13.88f, 14.08f, 14.28f, 14.48f, 14.68f,
				14.88f, 15.08f, 15.28f, 15.48f, 15.68f, 15.88f, 16.08f, 16.28f, 16.48f, 16.68f, 16.88f, 17.8f, 19.72f };
		Float[] LShoulderRollKeys = { 0.0797259f, 0.159494f, 0.141086f, 0.151824f, 0.0889301f, -0.0907571f, -0.066004f,
				0.159494f, 0.141086f, 0.151824f, 0.0889301f, -0.0907571f, -0.066004f, 0.159494f, 0.141086f, 0.151824f,
				0.0889301f, -0.0907571f, -0.066004f, 0.466294f, 0.0337899f, -0.164096f, -0.124212f, -0.182504f,
				-0.0843279f, -0.052114f, 0.0337899f, -0.164096f, -0.124212f, -0.182504f, -0.0843279f, -0.052114f,
				0.0337899f, -0.164096f, -0.124212f, -0.182504f, -0.0843279f, -0.052114f, 0.193327f, 0.193327f,
				0.159494f, 0.141086f, 0.151824f, 0.0889301f, -0.0907571f, -0.066004f, 0.159494f, 0.141086f, 0.151824f,
				0.0889301f, -0.0907571f, -0.066004f, 0.159494f, 0.141086f, 0.151824f, 0.0889301f, -0.0907571f,
				-0.066004f, 0.466294f, 0.12728f };

		Float[] LWristYawTimes = { 1f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f, 3.2f, 3.4f, 3.6f, 3.8f, 4f, 4.2f, 4.4f, 4.6f,
				4.8f, 5f, 5.2f, 5.4f, 6.32f, 7.2f, 7.4f, 7.6f, 7.8f, 8f, 8.2f, 8.4f, 8.6f, 8.8f, 9f, 9.2f, 9.4f, 9.6f,
				9.8f, 10f, 10.2f, 10.4f, 10.6f, 11.52f, 12.4f, 13.48f, 13.68f, 13.88f, 14.08f, 14.28f, 14.48f, 14.68f,
				14.88f, 15.08f, 15.28f, 15.48f, 15.68f, 15.88f, 16.08f, 16.28f, 16.48f, 16.68f, 16.88f, 17.8f, 19.72f };
		Float[] LWristYawKeys = { -0.213269f, -0.573758f, -0.615176f, -0.412688f, -0.194861f, -0.961676f, -0.763974f,
				-0.573758f, -0.615176f, -0.412688f, -0.194861f, -0.961676f, -0.763974f, -0.573758f, -0.615176f,
				-0.411154f, -0.194861f, -0.961676f, -0.763974f, -1.79483f, -0.421808f, -0.409536f, -0.46476f,
				-0.486237f, -0.472429f, -0.131882f, -0.421808f, -0.409536f, -0.46476f, -0.486237f, -0.472429f,
				-0.131882f, -0.421808f, -0.409536f, -0.46476f, -0.486237f, -0.472429f, -0.131882f, -0.581345f,
				-0.581345f, -0.573758f, -0.615176f, -0.412688f, -0.194861f, -0.961676f, -0.763974f, -0.573758f,
				-0.615176f, -0.412688f, -0.194861f, -0.961676f, -0.763974f, -0.573758f, -0.615176f, -0.411154f,
				-0.194861f, -0.961676f, -0.763974f, -1.79483f, 0.0843279f };

		Float[] RAnklePitchTimes = { 1f, 2f, 3.32f, 5.4f, 6.32f, 7.2f, 8.52f, 10.6f, 11.52f, 12.4f, 13.48f, 14.8f,
				16.88f, 17.8f, 19.72f };
		Float[] RAnklePitchKeys = { 0.0951499f, -0.31136f, -0.355846f, -0.31136f, -0.443284f, -0.417291f, -0.607505f,
				-0.421891f, -0.671934f, -0.671934f, -0.31136f, -0.355846f, -0.31136f, -0.443284f, 0.105888f };

		Float[] RAnkleRollTimes = { 1f, 2f, 3.32f, 5.4f, 6.32f, 7.2f, 8.52f, 10.6f, 11.52f, 12.4f, 13.48f, 14.8f,
				16.88f, 17.8f, 19.72f };
		Float[] RAnkleRollKeys = { 0.122762f, 0.124296f, 0.144238f, 0.124296f, 0.314512f, 0.0858622f, 0.125746f,
				0.0858622f, 0.105804f, 0.105804f, 0.124296f, 0.144238f, 0.124296f, 0.314512f, 0.073674f };

		Float[] RElbowRollTimes = { 1f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f, 3.2f, 3.4f, 3.6f, 3.8f, 4f, 4.2f, 4.4f, 4.6f,
				4.8f, 5f, 5.2f, 5.4f, 6.32f, 7.2f, 7.4f, 7.6f, 7.8f, 8f, 8.2f, 8.4f, 8.6f, 8.8f, 9f, 9.2f, 9.4f, 9.6f,
				9.8f, 10f, 10.2f, 10.4f, 10.6f, 11.52f, 12.4f, 13.48f, 13.68f, 13.88f, 14.08f, 14.28f, 14.48f, 14.68f,
				14.88f, 15.08f, 15.28f, 15.48f, 15.68f, 15.88f, 16.08f, 16.28f, 16.48f, 16.68f, 16.88f, 17.8f, 19.72f };
		Float[] RElbowRollKeys = { 0.385075f, 1.33309f, 1.34689f, 1.43126f, 1.31468f, 0.83147f, 1.16281f, 1.33309f,
				1.34689f, 1.43126f, 1.31468f, 0.83147f, 1.16281f, 1.33309f, 1.34689f, 1.43126f, 1.31468f, 0.83147f,
				1.16281f, 0.932714f, 1.24863f, 1.07529f, 0.990921f, 1.4005f, 1.54462f, 1.39897f, 1.24863f, 1.07529f,
				0.990921f, 1.4005f, 1.54462f, 1.39897f, 1.24863f, 1.07529f, 0.990921f, 1.4005f, 1.54462f, 1.39897f,
				1.53856f, 1.53856f, 1.33309f, 1.34689f, 1.43126f, 1.31468f, 0.83147f, 1.16281f, 1.33309f, 1.34689f,
				1.43126f, 1.31468f, 0.83147f, 1.16281f, 1.33309f, 1.34689f, 1.43126f, 1.31468f, 0.83147f, 1.16281f,
				0.932714f, 0.428028f };

		Float[] RElbowYawTimes = { 1f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f, 3.2f, 3.4f, 3.6f, 3.8f, 4f, 4.2f, 4.4f, 4.6f,
				4.8f, 5f, 5.2f, 5.4f, 6.32f, 7.2f, 7.4f, 7.6f, 7.8f, 8f, 8.2f, 8.4f, 8.6f, 8.8f, 9f, 9.2f, 9.4f, 9.6f,
				9.8f, 10f, 10.2f, 10.4f, 10.6f, 11.52f, 12.4f, 13.48f, 13.68f, 13.88f, 14.08f, 14.28f, 14.48f, 14.68f,
				14.88f, 15.08f, 15.28f, 15.48f, 15.68f, 15.88f, 16.08f, 16.28f, 16.48f, 16.68f, 16.88f, 17.8f, 19.72f };
		Float[] RElbowYawKeys = { 1.23176f, 0.185572f, 0.144154f, 0.174835f, 0.351244f, 0.446352f, -0.036858f,
				0.185572f, 0.144154f, 0.176367f, 0.351244f, 0.446352f, -0.036858f, 0.185572f, 0.144154f, 0.174835f,
				0.351244f, 0.446352f, -0.036858f, -0.389678f, 0.434165f, 0.31758f, 0.16418f, -0.061318f, -0.190241f,
				0.176453f, 0.434165f, 0.31758f, 0.165714f, -0.061318f, -0.190241f, 0.176453f, 0.434165f, 0.31758f,
				0.16418f, -0.061318f, -0.190241f, 0.176453f, 1.57086f, 1.57086f, 0.185572f, 0.144154f, 0.174835f,
				0.351244f, 0.446352f, -0.036858f, 0.185572f, 0.144154f, 0.176367f, 0.351244f, 0.446352f, -0.036858f,
				0.185572f, 0.144154f, 0.174835f, 0.351244f, 0.446352f, -0.036858f, -0.389678f, 1.17347f };

		Float[] RHandTimes = { 1f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f, 3.2f, 3.4f, 3.6f, 3.8f, 4f, 4.2f, 4.4f, 4.6f, 4.8f,
				5f, 5.2f, 5.4f, 6.32f, 7.2f, 7.4f, 7.6f, 7.8f, 8f, 8.2f, 8.4f, 8.6f, 8.8f, 9f, 9.2f, 9.4f, 9.6f, 9.8f,
				10f, 10.2f, 10.4f, 10.6f, 11.52f, 12.4f, 13.48f, 13.68f, 13.88f, 14.08f, 14.28f, 14.48f, 14.68f, 14.88f,
				15.08f, 15.28f, 15.48f, 15.68f, 15.88f, 16.08f, 16.28f, 16.48f, 16.68f, 16.88f, 17.8f, 19.72f };
		Float[] RHandKeys = { 0.3112f, 0f, 0.1568f, 0.178f, 0.1616f, 0.1672f, 0.1668f, 0f, 0.1568f, 0.178f, 0.1616f,
				0.1672f, 0.1668f, 0f, 0.1568f, 0.178f, 0.1616f, 0.1672f, 0.1668f, 1f, 0f, 0.1872f, 0.188f, 0.184f, 0f,
				0.1908f, 0f, 0.1872f, 0.188f, 0.184f, 0f, 0.1908f, 0f, 0.1872f, 0.188f, 0.184f, 0f, 0.1908f, 1f, 1f, 0f,
				0.1568f, 0.178f, 0.1616f, 0.1672f, 0.1668f, 0f, 0.1568f, 0.178f, 0.1616f, 0.1672f, 0.1668f, 0f, 0.1568f,
				0.178f, 0.1616f, 0.1672f, 0.1668f, 1f, 0.3044f };

		Float[] RHipPitchTimes = { 1f, 2f, 3.32f, 5.4f, 6.32f, 7.2f, 8.52f, 10.6f, 11.52f, 12.4f, 13.48f, 14.8f, 16.88f,
				17.8f, 19.72f };
		Float[] RHipPitchKeys = { 0.139552f, -0.0767419f, -0.44797f, -0.0798099f, -0.104354f, -0.052114f, -0.33437f,
				-0.052114f, -0.406468f, -0.406468f, -0.0767419f, -0.44797f, -0.0798099f, -0.104354f, 0.136484f };

		Float[] RHipRollTimes = { 1f, 2f, 3.32f, 5.4f, 6.32f, 7.2f, 8.52f, 10.6f, 11.52f, 12.4f, 13.48f, 14.8f, 16.88f,
				17.8f, 19.72f };
		Float[] RHipRollKeys = { -0.116542f, -0.128814f, -0.108872f, -0.128814f, -0.208583f, -0.1335f, -0.174919f,
				-0.131966f, -0.276162f, -0.276162f, -0.128814f, -0.108872f, -0.128814f, -0.208583f, -0.0628521f };

		Float[] RHipYawPitchTimes = { 1f, 2f, 3.32f, 5.4f, 6.32f, 7.2f, 8.52f, 10.6f, 11.52f, 12.4f, 13.48f, 14.8f,
				16.88f, 17.8f, 19.72f };
		Float[] RHipYawPitchKeys = { -0.170232f, -0.36505f, -0.378855f, -0.36505f, -0.371186f, -0.36505f, -0.378855f,
				-0.36505f, -0.371186f, -0.371186f, -0.36505f, -0.378855f, -0.36505f, -0.371186f, -0.171766f };

		Float[] RKneePitchTimes = { 1f, 2f, 3.32f, 5.4f, 6.32f, 7.2f, 8.52f, 10.6f, 11.52f, 12.4f, 13.48f, 14.8f,
				16.88f, 17.8f, 19.72f };
		Float[] RKneePitchKeys = { -0.0858622f, 0.653526f, 1.00481f, 0.650458f, 0.856014f, 0.731677f, 1.13358f,
				0.730143f, 1.30079f, 1.30079f, 0.653526f, 1.00481f, 0.650458f, 0.856014f, -0.091998f };

		Float[] RShoulderPitchTimes = { 1f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f, 3.2f, 3.4f, 3.6f, 3.8f, 4f, 4.2f, 4.4f,
				4.6f, 4.8f, 5f, 5.2f, 5.4f, 6.32f, 7.2f, 7.4f, 7.6f, 7.8f, 8f, 8.2f, 8.4f, 8.6f, 8.8f, 9f, 9.2f, 9.4f,
				9.6f, 9.8f, 10f, 10.2f, 10.4f, 10.6f, 11.52f, 12.4f, 13.48f, 13.68f, 13.88f, 14.08f, 14.28f, 14.48f,
				14.68f, 14.88f, 15.08f, 15.28f, 15.48f, 15.68f, 15.88f, 16.08f, 16.28f, 16.48f, 16.68f, 16.88f, 17.8f,
				19.72f };
		Float[] RShoulderPitchKeys = { 1.46808f, 0.624379f, 0.744032f, 0.510865f, 0.309909f, 0.61671f, 0.725624f,
				0.624379f, 0.744032f, 0.510865f, 0.309909f, 0.61671f, 0.725624f, 0.624379f, 0.744032f, 0.510865f,
				0.309909f, 0.61671f, 0.725624f, 0.72409f, 0.357381f, 0.510779f, 0.584411f, 0.605888f, 0.101229f,
				0.282215f, 0.357381f, 0.510779f, 0.584411f, 0.605888f, 0.101229f, 0.282215f, 0.357381f, 0.510779f,
				0.584411f, 0.605888f, 0.101229f, 0.282215f, 0.095066f, 0.095066f, 0.624379f, 0.744032f, 0.510865f,
				0.309909f, 0.61671f, 0.725624f, 0.624379f, 0.744032f, 0.510865f, 0.309909f, 0.61671f, 0.725624f,
				0.624379f, 0.744032f, 0.510865f, 0.309909f, 0.61671f, 0.725624f, 0.72409f, 1.46501f };

		Float[] RShoulderRollTimes = { 1f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f, 3.2f, 3.4f, 3.6f, 3.8f, 4f, 4.2f, 4.4f, 4.6f,
				4.8f, 5f, 5.2f, 5.4f, 6.32f, 7.2f, 7.4f, 7.6f, 7.8f, 8f, 8.2f, 8.4f, 8.6f, 8.8f, 9f, 9.2f, 9.4f, 9.6f,
				9.8f, 10f, 10.2f, 10.4f, 10.6f, 11.52f, 12.4f, 13.48f, 13.68f, 13.88f, 14.08f, 14.28f, 14.48f, 14.68f,
				14.88f, 15.08f, 15.28f, 15.48f, 15.68f, 15.88f, 16.08f, 16.28f, 16.48f, 16.68f, 16.88f, 17.8f, 19.72f };
		Float[] RShoulderRollKeys = { -0.067538f, -0.0337899f, 0.164096f, 0.124212f, 0.182504f, 0.0843279f, 0.052114f,
				-0.0337899f, 0.164096f, 0.124212f, 0.182504f, 0.0843279f, 0.052114f, -0.0337899f, 0.164096f, 0.124212f,
				0.182504f, 0.0843279f, 0.052114f, -0.193327f, -0.159494f, -0.141086f, -0.151824f, -0.0889301f,
				0.0907571f, 0.066004f, -0.159494f, -0.141086f, -0.151824f, -0.0889301f, 0.0907571f, 0.066004f,
				-0.159494f, -0.141086f, -0.151824f, -0.0889301f, 0.0907571f, 0.066004f, -0.466294f, -0.466294f,
				-0.0337899f, 0.164096f, 0.124212f, 0.182504f, 0.0843279f, 0.052114f, -0.0337899f, 0.164096f, 0.124212f,
				0.182504f, 0.0843279f, 0.052114f, -0.0337899f, 0.164096f, 0.124212f, 0.182504f, 0.0843279f, 0.052114f,
				-0.193327f, -0.0767419f };

		Float[] RWristYawTimes = { 1f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f, 3.2f, 3.4f, 3.6f, 3.8f, 4f, 4.2f, 4.4f, 4.6f,
				4.8f, 5f, 5.2f, 5.4f, 6.32f, 7.2f, 7.4f, 7.6f, 7.8f, 8f, 8.2f, 8.4f, 8.6f, 8.8f, 9f, 9.2f, 9.4f, 9.6f,
				9.8f, 10f, 10.2f, 10.4f, 10.6f, 11.52f, 12.4f, 13.48f, 13.68f, 13.88f, 14.08f, 14.28f, 14.48f, 14.68f,
				14.88f, 15.08f, 15.28f, 15.48f, 15.68f, 15.88f, 16.08f, 16.28f, 16.48f, 16.68f, 16.88f, 17.8f, 19.72f };
		Float[] RWristYawKeys = { -0.10282f, 0.421808f, 0.409536f, 0.46476f, 0.486237f, 0.472429f, 0.131882f, 0.421808f,
				0.409536f, 0.46476f, 0.486237f, 0.472429f, 0.131882f, 0.421808f, 0.409536f, 0.46476f, 0.486237f,
				0.472429f, 0.131882f, 0.581345f, 0.573758f, 0.615176f, 0.412688f, 0.194861f, 0.961676f, 0.763974f,
				0.573758f, 0.615176f, 0.412688f, 0.194861f, 0.961676f, 0.763974f, 0.573758f, 0.615176f, 0.411154f,
				0.194861f, 0.961676f, 0.763974f, 1.79483f, 1.79483f, 0.421808f, 0.409536f, 0.46476f, 0.486237f,
				0.472429f, 0.131882f, 0.421808f, 0.409536f, 0.46476f, 0.486237f, 0.472429f, 0.131882f, 0.421808f,
				0.409536f, 0.46476f, 0.486237f, 0.472429f, 0.131882f, 0.581345f, 0.118076f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll",
				"RHipYawPitch", "RKneePitch", "RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación armDance", "arm_dance");
		}
	}

	/**
	 * NAO se pone en una posición de flexion y luego hace varias flexiones
	 * antes de levantarse de nuevo. Esta es una operación sincrona.
	 * 
	 * El movimiento original fue diseñado por el FUN lab de la universidad de
	 * Notre Dame, empleando el timeline de Choregraphe. Ese timeline se exportó
	 * a código Java utilizando el ChoregrapheTranspiler.
	 * 
	 * @throws Failed
	 *             Puede generar un error, el cual se puede interceptar en Jason
	 *             de la siguiente manera: [error_msg(Msg),movement_pushups]
	 */
	@OPERATION
	public void movementPushups() {
		Float[] HeadPitchTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] HeadPitchKeys = { -0.116626f, -0.116626f, -0.115092f, -0.115092f, -0.11816f, -0.115092f, -0.11816f,
				-0.115092f, -0.11816f, -0.115092f, -0.11816f, -0.115092f, -0.601371f, -0.535408f, -0.536942f,
				-0.544613f, -0.437231f, -0.270025f, -0.222472f, -0.230143f };

		Float[] HeadYawTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] HeadYawKeys = { -0.0107799f, -0.0107799f, -0.0138481f, -0.00464392f, 0.0413762f, -0.00464392f,
				0.0413762f, -0.00464392f, 0.0413762f, -0.00464392f, 0.0413762f, -0.00464392f, -0.0261199f, -0.0276539f,
				-0.0276539f, -0.0353239f, -0.019984f, -0.00617791f, -0.019984f, -0.0123138f };

		Float[] LAnklePitchTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] LAnklePitchKeys = { -1.18429f, -1.18944f, -1.18944f, -1.18944f, -1.18944f, -1.18944f, -1.18944f,
				-1.18944f, -1.18944f, -1.18944f, -1.18944f, -1.18944f, -1.18944f, -1.18944f, -1.18944f, -1.01095f,
				-1.17355f, -1.18944f, -0.895898f, 0.0981341f };

		Float[] LAnkleRollTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] LAnkleRollKeys = { 0.075208f, 0.066004f, -0.05058f, -0.049046f, -0.049046f, -0.049046f, -0.049046f,
				-0.049046f, -0.049046f, -0.049046f, -0.049046f, -0.049046f, -0.00455999f, 0.066004f, 0.0429941f,
				-0.05825f, -0.061318f, 0.066004f, 0.067538f, -0.115008f };

		Float[] LElbowRollTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] LElbowRollKeys = { -0.361981f, -0.0398422f, -0.0398422f, -0.0536479f, -1.44345f, -0.0536479f, -1.44345f,
				-0.0536479f, -1.44345f, -0.0536479f, -1.44345f, -0.0536479f, -0.0352399f, -0.0643861f, -0.049046f,
				-0.052114f, -0.0429101f, -0.0352399f, -0.0444441f, -0.389594f };

		Float[] LElbowYawTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] LElbowYawKeys = { -1.25179f, -0.834538f, -0.668866f, -0.668866f, -0.983336f, -0.668866f, -0.983336f,
				-0.668866f, -0.983336f, -0.668866f, -0.983336f, -0.668866f, -1.22571f, -1.23951f, -1.23798f, -1.23951f,
				-1.23951f, -1.23491f, -1.21037f, -1.21344f };

		Float[] LHandTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f, 13.68f,
				14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] LHandKeys = { 0.6256f, 0.0140001f, 0.0244f, 0.0244f, 0.064f, 0.0244f, 0.064f, 0.0244f, 0.064f, 0.0244f,
				0.064f, 0.0244f, 0.0648f, 0.0684f, 0.0684f, 0.0684f, 0.0668f, 0.0648f, 0.0707999f, 0.2976f };

		Float[] LHipPitchTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] LHipPitchKeys = { -0.608956f, -1.39283f, -1.57384f, -0.400331f, -0.400331f, -0.400331f, -0.400331f,
				-0.400331f, -0.400331f, -0.400331f, -0.400331f, -0.400331f, -1.47106f, -1.38516f, -1.26704f, -0.990921f,
				-0.766959f, -0.631966f, -0.581345f, 0.1335f };

		Float[] LHipRollTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] LHipRollKeys = { -0.0536479f, 0.460242f, -0.0843279f, -0.021434f, -0.021434f, -0.021434f, -0.021434f,
				-0.021434f, -0.021434f, -0.021434f, -0.021434f, -0.021434f, -0.303691f, -0.185572f, -0.345107f,
				-0.151824f, 0.167248f, -0.0413762f, -0.0152981f, 0.112024f };

		Float[] LHipYawPitchTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] LHipYawPitchKeys = { -0.366584f, -0.253067f, -0.340507f, -0.0551819f, -0.0551819f, -0.0551819f,
				-0.0551819f, -0.0551819f, -0.0551819f, -0.0551819f, -0.0551819f, -0.0551819f, -0.493905f, -0.673385f,
				-0.739346f, -0.601285f, -0.530721f, -0.473963f, -0.481634f, -0.1733f };

		Float[] LKneePitchTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] LKneePitchKeys = { 2.11255f, 2.11228f, 2.06319f, 0.501576f, 0.506179f, 0.501576f, 0.506179f, 0.501576f,
				0.506179f, 0.501576f, 0.506179f, 0.501576f, 2.11228f, 2.11255f, 2.11255f, 1.82849f, 2.11255f, 2.11075f,
				1.66895f, -0.0874801f };

		Float[] LShoulderPitchTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] LShoulderPitchKeys = { 1.65975f, 0.562937f, 0.167164f, -0.0153821f, 0.745483f, -0.0153821f, 0.745483f,
				-0.0153821f, 0.745483f, -0.0153821f, 0.745483f, -0.0153821f, 0.378855f, 0.259204f, 0.386526f, 1.25017f,
				1.28238f, 1.15353f, 1.17193f, 1.48947f };

		Float[] LShoulderRollTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] LShoulderRollKeys = { 0.223922f, 0.345107f, 0.00762796f, 0.0106959f, 0.734743f, 0.0106959f, 0.734743f,
				0.0106959f, 0.734743f, 0.0106959f, 0.734743f, 0.0106959f, 0.0152981f, -0.0414601f, -4.19617e-05f,
				0.289883f, 0.268407f, 0.260738f, 0.225456f, 0.128814f };

		Float[] LWristYawTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] LWristYawKeys = { 0.0367741f, 0.812978f, 0.584411f, 0.584411f, 1.51862f, 0.584411f, 1.51862f, 0.584411f,
				1.51862f, 0.584411f, 1.51862f, 0.584411f, 1.10904f, 0.983252f, 1.017f, 0.960242f, 0.94797f, 0.710201f,
				0.693327f, 0.147222f };

		Float[] RAnklePitchTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] RAnklePitchKeys = { -1.1863f, -1.07836f, -1.1863f, -1.1863f, -1.18574f, -1.1863f, -1.18574f, -1.1863f,
				-1.18574f, -1.1863f, -1.18574f, -1.1863f, -1.1863f, -0.228525f, 0.713353f, 0.823801f, -0.506179f,
				-1.18421f, -0.776162f, 0.10282f };

		Float[] RAnkleRollTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] RAnkleRollKeys = { -0.0367741f, 0.142704f, -0.0444441f, 0.0291878f, 0.0291878f, 0.0291878f, 0.0291878f,
				0.0291878f, 0.0291878f, 0.0291878f, 0.0291878f, 0.0291878f, 0.00464392f, 0.207132f, 0.093616f,
				-0.115008f, 0.231675f, -0.0137641f, 0.0614019f, 0.07214f };

		Float[] RElbowRollTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] RElbowRollKeys = { 0.27923f, 0.297638f, 0.204064f, 0.231675f, 1.35763f, 0.231675f, 1.35763f, 0.231675f,
				1.35763f, 0.231675f, 1.35763f, 0.231675f, 0.038392f, 0.0583338f, 0.0567998f, 0.06447f, 0.0598679f,
				0.0349066f, 0.0429941f, 0.411154f };

		Float[] RElbowYawTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] RElbowYawKeys = { 1.18267f, 1.18114f, 1.15659f, 1.21182f, 0.990921f, 1.21182f, 0.990921f, 1.21182f,
				0.990921f, 1.21182f, 0.990921f, 1.21182f, 1.15813f, 1.18574f, 1.18574f, 1.18114f, 1.17807f, 1.18881f,
				1.17654f, 1.16887f };

		Float[] RHandTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f, 13.68f,
				14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] RHandKeys = { 0.7316f, 0.7316f, 0.2236f, 0.224f, 0.1612f, 0.224f, 0.1612f, 0.224f, 0.1612f, 0.224f,
				0.1612f, 0.224f, 0.1528f, 0.1572f, 0.1572f, 0.1572f, 0.1556f, 0.1528f, 0.1572f, 0.306f };

		Float[] RHipPitchTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] RHipPitchKeys = { -0.602905f, -0.472515f, -1.57699f, -0.403483f, -0.40962f, -0.403483f, -0.40962f,
				-0.403483f, -0.40962f, -0.403483f, -0.40962f, -0.403483f, -1.49723f, -1.65369f, -1.63989f, -1.26559f,
				-1.24718f, -0.658129f, -0.619779f, 0.133416f };

		Float[] RHipRollTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] RHipRollKeys = { -0.0106959f, 0.366667f, 0.312978f, -0.136484f, -0.136484f, -0.136484f, -0.136484f,
				-0.136484f, -0.136484f, -0.136484f, -0.136484f, -0.136484f, 0.265424f, 0.280764f, 0.29457f, 0.288435f,
				-0.216252f, -0.0413762f, -0.021434f, -0.061318f };

		Float[] RHipYawPitchTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] RHipYawPitchKeys = { -0.366584f, -0.253067f, -0.340507f, -0.0551819f, -0.0551819f, -0.0551819f,
				-0.0551819f, -0.0551819f, -0.0551819f, -0.0551819f, -0.0551819f, -0.0551819f, -0.493905f, -0.673385f,
				-0.739346f, -0.601285f, -0.530721f, -0.473963f, -0.481634f, -0.1733f };

		Float[] RKneePitchTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] RKneePitchKeys = { 2.11255f, 1.2073f, 2.06481f, 0.513931f, 0.520068f, 0.513931f, 0.520068f, 0.513931f,
				0.520068f, 0.513931f, 0.520068f, 0.513931f, 2.11255f, 1.6f, 0.598302f, 0.2102f, 1.97891f, 2.11255f,
				1.61995f, -0.0889301f };

		Float[] RShoulderPitchTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] RShoulderPitchKeys = { 1.61995f, 1.61688f, 0.319114f, 0.0414601f, 0.605971f, 0.0414601f, 0.605971f,
				0.0414601f, 0.605971f, 0.0414601f, 0.605971f, 0.0414601f, 0.365133f, 1.29627f, 1.29934f, 1.30087f,
				1.30701f, 1.1398f, 1.17202f, 1.47728f };

		Float[] RShoulderRollTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] RShoulderRollKeys = { -0.188724f, -0.182588f, 0.022968f, 0.022968f, -0.70108f, 0.022968f, -0.70108f,
				0.022968f, -0.70108f, 0.022968f, -0.70108f, 0.022968f, 0.0152981f, -0.377407f, -0.397349f, -0.343659f,
				-0.296104f, -0.204064f, -0.181053f, -0.101286f };

		Float[] RWristYawTimes = { 1.68f, 3.16f, 4.32f, 5.96f, 6.8f, 7.56f, 8.24f, 9f, 9.8f, 10.56f, 11.4f, 12.16f,
				13.68f, 14.68f, 15.68f, 16.68f, 17.68f, 18.68f, 19.76f, 21.4f };
		Float[] RWristYawKeys = { 0.10427f, 0.10427f, -1.1352f, -1.13367f, -1.06617f, -1.13367f, -1.06617f, -1.13367f,
				-1.06617f, -1.13367f, -1.06617f, -1.13367f, -1.13827f, -1.14441f, -1.14441f, -1.13827f, -1.12293f,
				-0.857548f, -0.840674f, 0.0720561f };

		String[] namesArray = { "HeadPitch", "HeadYaw", "LAnklePitch", "LAnkleRoll", "LElbowRoll", "LElbowYaw", "LHand",
				"LHipPitch", "LHipRoll", "LHipYawPitch", "LKneePitch", "LShoulderPitch", "LShoulderRoll", "LWristYaw",
				"RAnklePitch", "RAnkleRoll", "RElbowRoll", "RElbowYaw", "RHand", "RHipPitch", "RHipRoll",
				"RHipYawPitch", "RKneePitch", "RShoulderPitch", "RShoulderRoll", "RWristYaw" };

		List<String> names = new ArrayList<>(Arrays.asList(namesArray));
		List<Object> times = new ArrayList<>();
		List<Object> keys = new ArrayList<>();

		times.add(new ArrayList<Float>(Arrays.asList(HeadPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(HeadYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(HeadYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(LWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(LWristYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnklePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnklePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RAnkleRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RAnkleRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RElbowYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RElbowYawKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHandTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHandKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RHipYawPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RKneePitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RKneePitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderPitchKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RShoulderRollTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RShoulderRollKeys)));

		times.add(new ArrayList<Float>(Arrays.asList(RWristYawTimes)));
		keys.add(new ArrayList<Float>(Arrays.asList(RWristYawKeys)));

		try {
			motion.angleInterpolation(names, keys, times, true);
			this.posture.applyPosture("Stand", (float) 1.0);
		} catch (CallError | InterruptedException e) {
			failed("No se pudo completar la operación movementPushups", "movement_pushups");
		}
	}

	private static float measureDistance(ALMotion motion, List<String> joint, List<Float> desired) {
		float distanceSum = 0.0f;
		try {
			ArrayList<Float> angles = (ArrayList<Float>) motion.getAngles(joint, true);
			if ((desired.size() <= 0) || (desired.size() != angles.size())) {
				return 0f; // Could print a warning too, or even assert
			}

			for (int i = 0; i < angles.size(); i++) {
				distanceSum += Math.abs(desired.get(i) - angles.get(i));
			}

			return distanceSum / desired.size();
		} catch (CallError callError) {
			callError.printStackTrace();
			return 0f;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return 0f;
		}
	}

	@SuppressWarnings("unused")
	private static float measureDistance(ALMotion motion, String joint, List<Float> desired) {
		float distanceSum = 0.0f;
		try {
			ArrayList<Float> angles = (ArrayList<Float>) motion.getAngles(joint, true);
			if ((desired.size() <= 0) || (desired.size() != angles.size())) {
				return 0f; // Could print a warning too, or even assert
			}

			for (int i = 0; i < angles.size(); i++) {
				distanceSum += Math.abs(desired.get(i) - angles.get(i));
			}

			return distanceSum / desired.size();
		} catch (CallError callError) {
			callError.printStackTrace();
			return 0f;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return 0f;
		}
	}
}
