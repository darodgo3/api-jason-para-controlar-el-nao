/**
 * En este ejemplo se abordan las diferentes operaciones de manejo de
 * los leds.
 */

/* Initial beliefs and rules */

naoConfig("tcp://192.168.1.13:9559", true).

/* Initial goals */

!start.

/* Plans */

{ include("nao_helper.asl") }

+!start <- 
	!createNAO;
	
	wakeUp;
	
	rastaLeds(5);
	
	fadeLeds("AllLeds", 0.1, 2);
	
	randomEyeLeds(2);
	
	ledsOff("FaceLeds");
	
	.wait(2000);
	
	ledsOn("FaceLeds");
	
	goRest;
	
	!stopNAO;
.
