/* Initial beliefs and rules */

naoConfig("tcp://192.168.1.4:9559", false).

/* Initial goals */

!initNAO. 

{ include("nao_helper.asl") }

/* Plans */

+!initNAO  : true <-
	!createNAO;
	makeArtifact("gui","es.upv.inf.darodgo3.naoAPI.Artifacts.GamePad",[no],Id);
    focus(Id);
//    say("hola amigo");
//    wakeUp;
	rebootRobot;
	.

// X
+thumb2(1.0) : true <-
	sayHelloAnimated.
	
//+thumb2(0.0) : true <-
//	println("adios top");.

// Cuadrado
+top(1.0) : true <-
	attackNAO;
	posture("Stand", normal).
	
//+top(0.0) : true <-
//	println("adios top");.
	
// Circulo
+thumb(1.0) : true <-
	!attackTwo;
	posture("Stand", normal).
	
//+thumb(0.0) : true <-
//	println("adios top");.
	
// Triangulo
+trigger(1.0) : true <-
	rastaLedsAsync(3).
	
//+trigger(0.0) : true <-
//	println("adios top");.

// Flecha sin pulsar
+pov(0.0) : true <-
	stopMoveNAO;
	posture("Stand", lento).

// Flecha izquierda-arriba
+pov(0.125) : true <-
	moveNAO(1.0,1.0,0).
	
// Flecha arriba
+pov(0.25) : true <-
	moveNAO(1.0,0,0).

// Flecha arriba-derecha
+pov(0.375) : true <-
	moveNAO(1.0,-1.0,0).
	
// Flecha derecha
+pov(0.5) : true <-
	moveNAO(0,0,-1).
	
// Flecha derecha-abajo
+pov(0.625) : true <-
	moveNAO(-1.0,-1.0,0).
		
// Flecha abajo
+pov(0.75) : true <-
	moveNAO(-1,0,0).
	
// Flecha abajo-izquierda
+pov(0.875) : true <-
	moveNAO(-1.0,1.0,0).
	
// Flecha izquierda
+pov(1.0) : true <-
	moveNAO(0,0,1).
	
// R1
+base2(1.0) : true <-
	posture("SitRelax",lento);.
	
//+base2(0.0) : true <-
//	println("adios top");.
	
// R2
+pinkie(1.0) : true <-
	posture("Stand",lento);.

//+pinkie(0.0) : true <-
//	println("adios top");.
	
// R3
+base6(1.0) : true <-
	!stopNAO;
.
	
+base6(0.0) : true <-
	println("adios top");.

// L1
+base(1.0) : true <-
	thriller.
	
+base(0.0) : true <-
	println("adios top");.
	
// L2
+top2(1.0) : true <-
	println("adios top");.

+top2(0.0) : true <-
	println("adios top");.

// L3
+base5(1.0) : true <-
	println("adios top");.
	
+base5(0.0) : true <-
	println("adios top");.
	
// start
+base4(1.0) : true <-
	robotIsWakeUp(X);
	if (X == true){
		goRest;
	} else {
		wakeUp;
	}.
	
//+base4(0.0) : true <-
//	println("adios top");.
	
// select
+base3(1.0) : true <-
	println("adios top");.
	
+base3(0.0) : true <-
	println("adios top");.
	
+footContactChanged(false) <-
	say("Me cai");
.

+!attackTwo: true <-
RElbowRollTimes = [0.68, 1.36, 2.00000001, 2.68, 3.32, 4.00000001];
RElbowRollKeys = [0.357464, 1.51563, 0.0349066, 1.40979, 0.0349066, 0.0349066];

RElbowYawTimes = [0.68, 1.36, 2.00000001, 2.68, 3.32, 4.00000001];
RElbowYawKeys = [1.57077, 1.56157, 1.56464, 1.5631, 1.56464, 1.56464];

RHandTimes = [0.68, 1.36, 2.00000001, 2.68, 3.32, 4.00000001];
RHandKeys = [0.308, 0.308, 0.308, 0.308, 0.3084, 0.308];

RShoulderPitchTimes = [0.68, 1.36, 2.00000001, 2.68, 3.32, 4.00000001];
RShoulderPitchKeys = [0.093616, 1.51103, -0.171766, 1.47882, -0.210116, 1.26406];

RShoulderRollTimes = [0.68, 1.36, 2.00000001, 2.68, 3.32, 4.00000001];
RShoulderRollKeys = [0.0966001, -0.00310993, 0.0137641, 0.0183661, -0.04913, 0.0183661];

RWristYawTimes = [0.68, 1.36, 2.00000001, 2.68, 3.32, 4.00000001];
RWristYawKeys = [0.22699, -0.0521979, 0.15029, -0.0399261, 0.133416, 0.156426];

Names = ["RElbowRoll", "RElbowYaw", "RHand", "RShoulderPitch", "RShoulderRoll", "RWristYaw"];
AllTimes = [ RElbowRollTimes, RElbowYawTimes, RHandTimes, RShoulderPitchTimes, RShoulderRollTimes, RWristYawTimes];
AllKeys = [ RElbowRollKeys, RElbowYawKeys, RHandKeys, RShoulderPitchKeys, RShoulderRollKeys, RWristYawKeys];

angleInterpolationNAO(Names, AllKeys, AllTimes, true);
.
