/**
 * En este ejemplo se consultar� la informaci�n de los sensores. 
 * El valor se mostrar� por consola.
 */
/* Initial beliefs and rules */

/* Initial goals */
naoConfig("tcp://192.168.1.13:9559", false).

!start.

/* Plans */

{ include("nao_helper.asl") }

+!start <-
	!createNAO;
	
	wakeUp;
	
	showCamera(true);
	
	.wait(20000);
	
	closeCamera;
	
	!stopNAO;
.
