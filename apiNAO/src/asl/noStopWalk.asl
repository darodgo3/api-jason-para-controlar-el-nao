/* Initial beliefs and rules */

walking(false).
girando(false).

/* Initial goals */

!initNAO. 

/* Plans */

+!initNAO  : true <-
	IpNao =  "tcp://192.168.1.14:9559";
//	IpNao =  "tcp://127.0.0.1:40032";
	makeArtifact("audio", "es.upv.inf.darodgo3.naoAPI.Artifacts.Audio", [IpNao, false],AId1);
	makeArtifact("motion", "es.upv.inf.darodgo3.naoAPI.Artifacts.Motion", [IpNao, false],AId2);
	makeArtifact("sensors", "es.upv.inf.darodgo3.naoAPI.Artifacts.Sensors", [IpNao, false],AId3);
	focus(AId3);
	focus(AId1);
	wakeUp;
	-+walking(true);
	moveNAO(1,0,0);
.


@changeDirection[atomic]
+!wallDetected(X, V): true <-
	-+walking(false);
	
	stopMoveNAO;
	?sonarLeft(SL);
	?sonarRight(SR);
	
	
	if(X == "bumper" | V < 0.3) {
		moveToNAO(-(0.5 - V),0,0);
		waitUntilMoveIsFinished;
	}
	
	if (X == "middle") {
		if (SL > 0 & SL > SR){
			moveToNAO(0,0,-90);
		} else {
			if ( SR > 0 & SR > SL){
				moveToNAO(0,0, 90);
			} else {
				moveToNAO(0,0,-90);
			}
		}
	} else {
		if (X == "left"){
			moveToNAO(0,0,-90);
		} else {
			moveToNAO(0,0, 90);
		}
	}
	
	waitUntilMoveIsFinished;
	
	.drop_all_events;
	.drop_all_intentions;
	
	-+walking(true);
	moveNAO(1,0,0);
.

+leftBumperPressed: true <-
	!!wallDetected("bumper", 0).

+rightBumperPressed: true <-
	!!wallDetected("bumper", 0).
	
	
+sonarMiddle(X): X > 0 <-
	!!wallDetected("middle", X);
.

+sonarLeft(X) : X > 0 <-
	!!wallDetected("left", X);
.
	
+sonarRight(X) : X > 0 <-
	!!wallDetected("right", X);
.

+sonarLateralRight(X): walking(true) <-
	-+girando("derecha");
	moveNAO(1,0,0.3);
.

+sonarLateralLeft(X): walking(true) <-
	-+girando("izquierda");
	moveNAO(1,0,-0.3);
.
	
+sonarNothingLeft(X) : X > 0 & walking(true) & girando("izquierda") <-
	-+girando(false);
	moveNAO(1,0,0).
	
+sonarNothingRight(X) : X > 0 & walking(true) &  girando("derecha")<-
	-+girando(false);
	moveNAO(1,0,0).


