// Agent sample_agent in project tfg

/* Initial beliefs and rules */

naoConfig("tcp://127.0.0.1:44742", true).
//naoConfig("tcp://192.168.1.4:9559", true).

/* Initial goals */	

!initNAO. 

/* Plans */

{ include("nao_helper.asl") }

+!initNAO  : true <-
	!createNAO;
//	focus(IdAudio);
//	makeArtifact("generalOP", "es.upv.inf.darodgo3.Artifacts.GeneralRobotOperations", [IpNao],AId1);
//	makeArtifact("motion", "es.upv.inf.darodgo3.Artifacts.Motion", [IpNao],AId2);
//	makeArtifact("walking", "es.upv.inf.darodgo3.Artifacts.Walking", [IpNao],AId4);
//	makeArtifact("sensors", "es.upv.inf.darodgo3.Artifacts.Sensors", [IpNao],AId3);
//	makeArtifact("vision", "es.upv.inf.darodgo3.naoAPI.Artifacts.Vision", [IpNao],AId5);
//	makeArtifact("audio", "es.upv.inf.darodgo3.Artifacts.Audio", [IpNao],AId6);
//	makeArtifact("leds", "es.upv.inf.darodgo3.Artifacts.Leds", [IpNao],AId6);
//	focus(AId3);
//	focus(AId1);
	// +generalOP(AId1);
	
//	movementPushups;
	wakeUp;
	macarenaDance;
	
//	speechGetAvailableLanguages(Asdf);
//	speechGetAvailableVoices(Asdf2);
//	println(Asdf2);
//	sayAsyncWithDelay("Hola amigo mio", 2000);
//	
//	.wait(3000);
//	
//	showCamera(true);
//	
//	.wait(5000);
//	
//	closeCamera;
//	
//	.wait(3000);
//	
	!closeNAO;
	
//	wakeUp;
//	setOrthogonalSecurityDistance(0.3);
//	moveToNAO(0,0,90);
//	setAnglesNAOBlocking(["LHand", "RHand"], [1, 1], 0.1);
//	setAnglesNAO(["LHand", "RHand"], [0, 0], 0.1);
//
//	wakeUp;
//	!hello;
//	startAwareness("MoveContextually");
//stopAwareness;
//	fadeLeds("AllLeds", 0, 1);
//	closeHandNAO("RHand");
//	closeHandNAO("LHand");
//	createLedsGroup("Pichin", ["RightFaceLed1", "RightFaceLed2", "RightFaceLed3"]);
//	ledsOff("Pichin");
//	+hola(0x123444);
//	setIntensityLeds("AllLeds", mucho);
//	goRest;
//	wakeUp;
//	setVocabulary(["arriba", "hola", "adios", "sientate"], false);
//	startWordRecognition;
//	navigateTo(1,0,0);
//	!attack;
//	attackOneNAO;
//	wakeUp;
//	moveBothHandNAO("todo", "muylento");
//	moveBothHandNAO(0, 0.5);
//	
//	getAnglesNAO(["RHand", "LHand"], false, X2);
//	
//	.println(X2);
	
//	getMemoryData("SonarMiddleDetected", X);
//	
//	.println(X);
	
	
//	sayHelloAnimated;
//	
//	attackOneNAO;
//	!attackTwo;
	
//	navigateTo(1,0,0);
	
//	goRest;
	
//	!sayHello;
	
//	wakeUp; 
//	posture("LyingBack",lento);
//	.wait(5000);
//	posture("StandZero","rapido");
	//.wait(5000);
//	posture("Stand","rapido");
	//.wait(5000);
	//posture("StandInit",rapido);
	//.wait(5000);
//	openBothHandNAO(nada, lento);
//	say("Manos abiertas");
	//.wait(3000);
//	openBothHandNAO(nada, lento);
//	moveNAO(1,0,0);
//	navigateTo(-1,0);
//	.wait(2000);
//	stopMoveNAO;
	.

+wordRecognized("arriba") <-
	posture("StandInit",lento);
.

+wordRecognized("hola") <-
	sayHelloAnimated;
.

+wordRecognized("sientate") <-
	posture("LyingBelly",lento);
.

+wordRecognized("adios") <-
	sayAsync("adios");
	goRest;
	stopWordRecognition;	
.

+rearTactilTouched <-
	say("terminando");
	goRest;
	endCore;
	endAudio;
	endPeoplePerception;
	endMotion;
	endVision;
	endSensors;
//	exit;
.


//+gyroscope(X) <-
//	pritnln("gyroscope");
//	println(X);
//.
//
//+accelerometer(X,Y,Z) <-
//	println("accelerometer");
//	println(X, Y, Z);
//.
//
//+angles(X,Y,Z) <-
//	println("angles");
//	println(X, Y, Z);
//.
	
+!attackTwo: true <-
RElbowRollTimes = [0.68, 1.36, 2.00000001, 2.68, 3.32, 4.00000001];
RElbowRollKeys = [0.357464, 1.51563, 0.0349066, 1.40979, 0.0349066, 0.0349066];

RElbowYawTimes = [0.68, 1.36, 2.00000001, 2.68, 3.32, 4.00000001];
RElbowYawKeys = [1.57077, 1.56157, 1.56464, 1.5631, 1.56464, 1.56464];

RHandTimes = [0.68, 1.36, 2.00000001, 2.68, 3.32, 4.00000001];
RHandKeys = [0.308, 0.308, 0.308, 0.308, 0.3084, 0.308];

RShoulderPitchTimes = [0.68, 1.36, 2.00000001, 2.68, 3.32, 4.00000001];
RShoulderPitchKeys = [0.093616, 1.51103, -0.171766, 1.47882, -0.210116, 1.26406];

RShoulderRollTimes = [0.68, 1.36, 2.00000001, 2.68, 3.32, 4.00000001];
RShoulderRollKeys = [0.0966001, -0.00310993, 0.0137641, 0.0183661, -0.04913, 0.0183661];

RWristYawTimes = [0.68, 1.36, 2.00000001, 2.68, 3.32, 4.00000001];
RWristYawKeys = [0.22699, -0.0521979, 0.15029, -0.0399261, 0.133416, 0.156426];

Names = ["RElbowRoll", "RElbowYaw", "RHand", "RShoulderPitch", "RShoulderRoll", "RWristYaw"];
AllTimes = [ RElbowRollTimes, RElbowYawTimes, RHandTimes, RShoulderPitchTimes, RShoulderRollTimes, RWristYawTimes];
AllKeys = [ RElbowRollKeys, RElbowYawKeys, RHandKeys, RShoulderPitchKeys, RShoulderRollKeys, RWristYawKeys];

angleInterpolationNAO(Names, AllKeys, AllTimes, true);
.

+!attack: true <-
RElbowRollTimes = [0.52, 1.04, 1.52, 2.04, 2.52, 3.04, 3.52, 4.04, 4.52, 5.04, 5.52, 6.04, 6.52, 7.04, 7.52, 8.04, 8.52];
RElbowRollKeys = [0.07214, 0.07214, 0.07214, 0.0813439, 0.0828779, 0.119694, 0.119694, 0.128898, 0.22554, 0.247016, 0.224006, 0.216336, 0.220938, 0.219404, 0.214802, 0.213268, 0.214802];

RElbowYawTimes = [0.52, 1.04, 1.52, 2.04, 2.52, 3.04, 3.52, 4.04, 4.52, 5.04, 5.52, 6.04, 6.52, 7.04, 7.52, 8.04, 8.52];
RElbowYawKeys = [1.00319, 1.00319, 1.00319, 1.00319, 1.00473, 1.00626, 1.00626, 1.00626, 1.00626, 1.00626, 1.00626, 1.00626, 1.0078, 1.00626, 1.00626, 1.0078, 1.0078];

RHandTimes = [0.52, 1.04, 1.52, 2.04, 2.52, 3.04, 3.52, 4.04, 4.52, 5.04, 5.52, 6.04, 6.52, 7.04, 7.52, 8.04, 8.52];
RHandKeys = [0.314, 0.314, 0.314, 0.314, 0.314, 0.314, 0.314, 0.314, 0.314, 0.314, 0.314, 0.314, 0.314, 0.314, 0.314, 0.314, 0.314];

RShoulderPitchTimes = [0.52, 1.04, 1.52, 2.04, 2.52, 3.04, 3.52, 4.04, 4.52, 5.04, 5.52, 6.04, 6.52, 7.04, 7.52, 8.04, 8.52];
RShoulderPitchKeys = [1.24258, 1.24258, 1.24258, 1.1306, 0.627448, 0.434164, 0.154976, -0.179436, -0.4034, -0.45709, -0.285282, 0.0353239, 0.480184, 0.70875, 0.9772, 1.16895, 1.1981];

RShoulderRollTimes = [0.52, 1.04, 1.52, 2.04, 2.52, 3.04, 3.52, 4.04, 4.52, 5.04, 5.52, 6.04, 6.52, 7.04, 7.52, 8.04, 8.52];
RShoulderRollKeys = [0.231592, 0.231592, 0.233126, 0.18864, 0.0689881, 0.191708, -0.00771189, -0.20253, -0.190258, -0.196394, -0.19486, -0.18719, -0.14117, -0.0614018, 0.0122299, 0.0214341, 0.0214341];

RWristYawTimes = [0.52, 1.04, 1.52, 2.04, 2.52, 3.04, 3.52, 4.04, 4.52, 5.04, 5.52, 6.04, 6.52, 7.04, 7.52, 8.04, 8.52];
RWristYawKeys = [0.447886, 0.447886, 0.447886, 0.447886, 0.52612, 0.52612, 0.506178, 0.328234, 0.329768, 0.331302, 0.33437, 0.37272, 0.401866, 0.404934, 0.421808, 0.440216, 0.440216];

Names = ["RElbowRoll", "RElbowYaw", "RHand", "RShoulderPitch", "RShoulderRoll", "RWristYaw"];
AllTimes = [ RElbowRollTimes, RElbowYawTimes, RHandTimes, RShoulderPitchTimes, RShoulderRollTimes, RWristYawTimes];
AllKeys = [ RElbowRollKeys, RElbowYawKeys, RHandKeys, RShoulderPitchKeys, RShoulderRollKeys, RWristYawKeys];

angleInterpolationNAO(Names, AllKeys, AllTimes, true);.

+!hello: true <-
HeadPitchTimes = [0.8, 1.56, 2.24, 2.8, 3.48, 4.6];
HeadPitchKeys = [0.29602, -0.170316, -0.340591, -0.0598679, -0.193327, -0.01078];

HeadYawTimes = [0.8, 1.56, 2.24, 2.8, 3.48, 4.6];
HeadYawKeys = [-0.135034, -0.351328, -0.415757, -0.418823, -0.520068, -0.375872];

LElbowRollTimes = [0.72, 1.48, 2.16, 2.72, 3.4, 4.52];
LElbowRollKeys = [-1.37902, -1.29005, -1.18267, -1.24863, -1.3192, -1.18421];

LElbowYawTimes = [0.72, 1.48, 2.16, 2.72, 3.4, 4.52];
LElbowYawKeys = [-0.803859, -0.691876, -0.679603, -0.610574, -0.753235, -0.6704];

LHandTimes = [1.48, 4.52];
LHandKeys = [0.238207, 0.240025];

LShoulderPitchTimes = [0.72, 1.48, 2.16, 2.72, 3.4, 4.52];
LShoulderPitchKeys = [1.11824, 0.928028, 0.9403, 0.862065, 0.897349, 0.842125];

LShoulderRollTimes = [0.72, 1.48, 2.16, 2.72, 3.4, 4.52];
LShoulderRollKeys = [0.363515, 0.226991, 0.20398, 0.217786, 0.248467, 0.226991];

LWristYawTimes = [1.48, 4.52];
LWristYawKeys = [0.147222, 0.11961];

RElbowRollTimes = [0.64, 1.4, 1.68, 2.08, 2.4, 2.64, 3.04, 3.32, 3.72, 4.44];
RElbowRollKeys = [1.38524, 0.242414, 0.349066, 0.934249, 0.680678, 0.191986, 0.261799, 0.707216, 1.01927, 1.26559];

RElbowYawTimes = [0.64, 1.4, 2.08, 2.64, 3.32, 3.72, 4.44];
RElbowYawKeys = [-0.312978, 0.564471, 0.391128, 0.348176, 0.381923, 0.977384, 0.826783];

RHandTimes = [1.4, 3.32, 4.44];
RHandKeys = [0.853478, 0.854933, 0.425116];

RShoulderPitchTimes = [0.64, 1.4, 2.08, 2.64, 3.32, 4.44];
RShoulderPitchKeys = [0.247016, -1.17193, -1.0891, -1.26091, -1.14892, 1.02015];

RShoulderRollTimes = [0.64, 1.4, 2.08, 2.64, 3.32, 4.44];
RShoulderRollKeys = [-0.242414, -0.954191, -0.460242, -0.960325, -0.328317, -0.250085];

RWristYawTimes = [1.4, 3.32, 4.44];
RWristYawKeys = [-0.312978, -0.303775, 0.182504];

Names = ["HeadPitch", "HeadYaw", "LElbowRoll", "LElbowYaw", "LHand", "LShoulderPitch", "LShoulderRoll", "LWristYaw", "RElbowRoll", "RElbowYaw", "RHand", "RShoulderPitch", "RShoulderRoll", "RWristYaw"];
AllTimes = [ HeadPitchTimes, HeadYawTimes, LElbowRollTimes, LElbowYawTimes, LHandTimes, LShoulderPitchTimes, LShoulderRollTimes, LWristYawTimes, RElbowRollTimes, RElbowYawTimes, RHandTimes, RShoulderPitchTimes, RShoulderRollTimes, RWristYawTimes];
AllKeys = [ HeadPitchKeys, HeadYawKeys, LElbowRollKeys, LElbowYawKeys, LHandKeys, LShoulderPitchKeys, LShoulderRollKeys, LWristYawKeys, RElbowRollKeys, RElbowYawKeys, RHandKeys, RShoulderPitchKeys, RShoulderRollKeys, RWristYawKeys];

angleInterpolationNAO(Names, AllKeys, AllTimes, true);.
