/**
 * En este ejemplo se consultar� la informaci�n de los sensores. 
 * El valor se mostrar� por consola.
 */
/* Initial beliefs and rules */

/* Initial goals */
naoConfig("tcp://192.168.1.13:9559", false).

!start.

/* Plans */

{ include("nao_helper.asl") }

+!start <-
	!createNAO;
	
	wakeUp;
.

+frontTactilTouched <-
	say("Botón cabeza delante");
.

+middleTactilTouched <-
	say("Botón cabeza medio");
.

+rearTactilTouched <-
	say("Adios");
	goRest;
	!stopNAO;
.

+leftBumperPressed <-
	say("Bumper pie izquierdo");
.

+rightBumperPressed <-
	say("Bumper pie derecho");
.

+handLeftTouched <-
	say("Me tocaste la mano izquierda");
.

+handRightTouched <-
	say("Me tocaste la mano derecha");
.

+footContactChanged <-
	say("Me caí");
.