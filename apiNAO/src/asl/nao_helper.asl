+!createNAO : true <-
	?naoConfig(Ip, IsVirtual);
	makeArtifact("nao_robot", "es.upv.inf.darodgo3.naoAPI.Artifacts.NaoRobot", [],_);
	createRobotArtifacts(Ip,IsVirtual,IdSensors,IdAudio,IdCore,IdMotion,IdVision,IdPeoplePerception);
	focus(IdSensors);
	focus(IdAudio);
	focus(IdCore);
	focus(IdMotion);
	focus(IdVision);
	focus(IdPeoplePerception);
.

+!stopNAO <-
	goRest;
	endCore;
	endAudio;
	endPeoplePerception;
	endMotion;
	endVision;
	endSensors;
	exit;
.