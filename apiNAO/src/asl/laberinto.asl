/* Initial beliefs and rules */

walking(false).
girando(false).

naoConfig("tcp://192.168.1.13:9559", false).

/* Initial goals */

!initNAO. 

/* Plans */

{ include("nao_helper.asl") }

+!initNAO  : true <-
	!createNAO;
	wakeUp;
	-+walking(true);
	moveNAO(0.3,0,0);
.

+rearTactilTouched <-
	say("terminando");
	!stopNAO;
.

@changeDirection[atomic]
+!wallDetected(X, V): true <-
	sayAsync(X);
	-+walking(false);
	stopMoveNAO;
	posture("Stand", normal);
	?sonarLateralLeft(SL);
	?sonarNothingLeft(SNL);
	
	if(X == "bumper" | V < 0.2) {
		moveToNAO(-0.2 - V,0,0);
		waitUntilMoveIsFinished;
	}
	
	if (SNL < SL) {
		moveToNAO(0,0,90);
	} else {
		moveToNAO(0,0,-90);
	}
	
	waitUntilMoveIsFinished;
	
	.drop_all_events;
	.drop_all_intentions;
	
	sayAsync("adios");
	-+walking(true);
	moveNAO(0.3,0,0);
.


+leftBumperPressed: walking(true) <-
	!!wallDetected("bumper", 0).

+rightBumperPressed: walking(true) <-
	!!wallDetected("bumper", 0).
	
	
+sonarMiddle(X): walking(true) & X > 0 & X < 0.4 <-
	!!wallDetected("middle", X);
.

+sonarLeft(X) : walking(true) & X > 0 & X < 0.4 <-
	!!wallDetected("left", X);
.
	
+sonarRight(X) : walking(true) & X > 0 & X < 0.4 <-
	!!wallDetected("right", X);
.

+sonarLateralLeft(X): walking(true) & X < 0.5 & X > 0 <-
	sayAsync("entrando 1");
	-+walking(false);
	.drop_all_events;
	.drop_all_intentions;
	stopMoveNAO;
	posture("Stand", normal);
	moveToNAO(0,0,-10);
	waitUntilMoveIsFinished;
	moveNAO(0.3,0,0);
	-+walking(true);
	
.

+sonarNothingRight(X): walking(true) & X > 2 <-
	sayAsync("entrando 2");
	-+walking(false);
	.drop_all_events;
	.drop_all_intentions;
	stopMoveNAO;
	posture("Stand", normal);
	moveToNAO(0,0,10);
	waitUntilMoveIsFinished;
	moveNAO(0.5,0,0);
	-+walking(true);
.

//+sonarNothingLeft(X) <-
// .print("no lateral" , X);
//.
//
//+sonarNothingRight(X) <-
// .print("no lateral r " , X);
//.
//
//+sonarLateralLeft(X) <-
// .print("lateral" , X);
//.
	
//+sonarNothingRight(X) : X > 0 & walking(true) &  girando("derecha")<-
//	-+girando(false);
//	moveNAO(1,0,0).

