/**
 * En este ejemplo se consultar� la informaci�n de los sensores. 
 * El valor se mostrar� por consola.
 */
/* Initial beliefs and rules */

naoConfig("tcp://192.168.1.13:9559", false).

/* Initial goals */

!start.

/* Plans */

{ include("nao_helper.asl") }

+!start <-
	!createNAO;
	
	wakeUp;
	say("Empezando el programa");
	setVocabulary(["hola", "nombre", "adios"], false);
	startWordRecognition;
.

+wordRecognized("hola") <-
	stopWordRecognition;
	sayHelloAnimated;
	startWordRecognition;
.

+wordRecognized("nombre") <-
	stopWordRecognition;
	sayAnimated("BowShort1", "Hola");
	say("Mi nombre es Nao");
	startWordRecognition;
.

+wordRecognized("adios") <-
	stopWordRecognition;
	say("Adios");
	goRest;
	!stopNAO;
.