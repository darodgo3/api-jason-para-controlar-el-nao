package es.upv.darodgo3.choregrapheTranspiler.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Transpiler {
	public static String transpileToJava(String original) {
		String[] sp = original.split("\n");
		List<String> o = new ArrayList<>(Arrays.asList(sp));
		List<String> t = new ArrayList<>();
		List<String> joints = new ArrayList<>();

		Pattern p1 = Pattern.compile("(names.push_back).\"(\\w+)\".");

		// Pattern p2 =
		// Pattern.compile("(times|keys).*=\\s*(-*\\d+.\\d+|-*\\d+).*$");
		Pattern p2 = Pattern.compile("(times|keys).*=\\s*(.*);.*$");
		Matcher m1;
		Matcher m2;

		boolean fstK = true;
		boolean fstT = true;
		String nextTimes = "";
		String nextKeys = "";

		for (String s : o) {
			m1 = p1.matcher(s);
			m2 = p2.matcher(s);
			if (m1.find()) {
				if (!joints.isEmpty()) {
					nextTimes += "};";
					nextKeys += "};";
					t.add(nextTimes);
					t.add(nextKeys);
					t.add("");
				}

				fstK = true;
				fstT = true;
				joints.add(m1.group(2));
				nextTimes = "Float[] " + m1.group(2) + "Times = { ";
				nextKeys = "Float[] " + m1.group(2) + "Keys = { ";
			} else if (m2.find()) {
				if (m2.group(1).equals("times")) {
					if (!fstT) {
						nextTimes += ", ";
					} else {
						fstT = false;
					}

					nextTimes += m2.group(2) + "f";
				} else {
					if (!fstK) {
						nextKeys += ", ";
					} else {
						fstK = false;
					}

					nextKeys += m2.group(2) + "f";
				}
			}
		}

		// ultima iteración
		nextTimes += "};";
		nextKeys += "};";
		t.add(nextTimes);
		t.add(nextKeys);
		t.add("");

		String strJoints = "String[] namesArray = {";
		boolean f = true;

		for (String i : joints) {
			if (!f) {
				strJoints += ", ";
			} else {
				f = false;
			}

			strJoints += "\"" + i + "\"";
		}

		strJoints += "};";

		t.add(strJoints);
		t.add("");
		t.add("List<String> names = new ArrayList<>(Arrays.asList(namesArray));");
		t.add("List<Object> times = new ArrayList<>();");
		t.add("List<Object> keys = new ArrayList<>();");
		t.add("");

		for (String i : joints) {
			t.add("times.add(new ArrayList<Float>(Arrays.asList(" + i + "Times)));");
			t.add("keys.add(new ArrayList<Float>(Arrays.asList(" + i + "Keys)));");
			t.add("");
		}

		t.add("motion.angleInterpolation(names, keys, times, true);");

		return String.join("\n", t);
	}

	public static String transpileToJason(String original) {
		String[] sp = original.split("\n");
		List<String> o = new ArrayList<>(Arrays.asList(sp));
		List<String> t = new ArrayList<>();
		List<String> joints = new ArrayList<>();

		Pattern p1 = Pattern.compile("(names.push_back).\"(\\w+)\".");

		// Pattern p2 =
		// Pattern.compile("(times|keys).*=\\s*(-*\\d+.\\d+|-*\\d+).*$");
		Pattern p2 = Pattern.compile("(times|keys).*=\\s*(.*);.*$");
		Matcher m1;
		Matcher m2;

		boolean fstK = true;
		boolean fstT = true;
		String nextTimes = "";
		String nextKeys = "";

		for (String s : o) {
			m1 = p1.matcher(s);
			m2 = p2.matcher(s);
			if (m1.find()) {
				if (!joints.isEmpty()) {
					nextTimes += "];";
					nextKeys += "];";
					t.add(nextTimes);
					t.add(nextKeys);
					t.add("");
				}

				fstK = true;
				fstT = true;
				joints.add(m1.group(2));
				nextTimes = m1.group(2) + "Times = [";
				nextKeys = m1.group(2) + "Keys = [";
			} else if (m2.find()) {
				if (m2.group(1).equals("times")) {
					if (!fstT) {
						nextTimes += ", ";
					} else {
						fstT = false;
					}

					if (m2.group(2).contains(".")) {
						nextTimes += m2.group(2);
					} else {
						nextTimes += m2.group(2);
					}
				} else {
					if (!fstK) {
						nextKeys += ", ";
					} else {
						fstK = false;
					}

					if (m2.group(2).contains(".")) {
						nextKeys += m2.group(2);
					} else {
						nextKeys += m2.group(2);
					}
				}
			}
		}

		// ultima iteración
		nextTimes += "];";
		nextKeys += "];";
		t.add(nextTimes);
		t.add(nextKeys);
		t.add("");

		String strJoints = "Names = [";
		boolean f = true;

		for (String i : joints) {
			if (!f) {
				strJoints += ", ";
			} else {
				f = false;
			}

			strJoints += "\"" + i + "\"";
		}

		strJoints += "];";

		t.add(strJoints);

		String allKeys = "AllKeys = [ ";
		String allTimes = "AllTimes = [ ";
		for (String i : joints) {
			allTimes += i + "Times, ";
			allKeys += i + "Keys, ";
		}

		allKeys = allKeys.substring(0, allKeys.length() - 2) + "];";
		allTimes = allTimes.substring(0, allTimes.length() - 2) + "];";

		t.add(allTimes);
		t.add(allKeys);
		t.add("");

		t.add("angleInterpolationNAO(Names, AllKeys, AllTimes, true);");

		return String.join("\n", t);
	}
}
