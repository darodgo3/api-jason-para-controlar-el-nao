package es.upv.darodgo3.choregrapheTranspiler.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import es.upv.darodgo3.choregrapheTranspiler.Main;
import es.upv.darodgo3.choregrapheTranspiler.model.Transpiler;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Controller {

	@FXML // ResourceBundle that was given to the FXMLLoader
	private ResourceBundle resources;

	@FXML // URL location of the FXML file that was given to the FXMLLoader
	private URL location;

	@FXML // fx:id="originalCode"
	private TextArea originalCode; // Value injected by FXMLLoader

	@FXML // fx:id="execButton"
	private Button execButton; // Value injected by FXMLLoader

	@FXML // fx:id="transpiledCode"
	private TextArea transpiledCode; // Value injected by FXMLLoader

	@FXML
	private RadioMenuItem menuJava;
	
	@FXML
	private RadioMenuItem menuJason;
	
	@FXML
    private Label tagOutput;
	
	private final ChangeListener<Boolean> ch = (observable, oldValue, newValue) -> {
		if (newValue == null) return;
		if (newValue == true){
			tagOutput.setText("Java");
		}
	};
	
	private final ChangeListener<Boolean> ch2 = (observable, oldValue, newValue) -> {
		if (newValue == null) return;
		if (newValue == true){
			tagOutput.setText("Jason");
		}
	};

	@FXML
	private void initialize() {
		menuJava.selectedProperty().addListener(ch);
		menuJason.selectedProperty().addListener(ch2);
	}

	@FXML
	void transpile(ActionEvent event) {
		if (menuJava.isSelected()) {
			transpiledCode.setText(Transpiler.transpileToJava(originalCode.getText()));
		} else if (menuJason.isSelected()) {
			transpiledCode.setText(Transpiler.transpileToJason(originalCode.getText()));
		}
	}

	@FXML
	void close(ActionEvent event) {
		Stage stage = (Stage) originalCode.getScene().getWindow();
		stage.close();
	}

	@FXML
	private void ejecutaAbout(ActionEvent event) {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/AboutLayout.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Sobre Choregraphe Transpiler");
			dialogStage.initModality(Modality.APPLICATION_MODAL);
			dialogStage.initOwner(transpiledCode.getScene().getWindow());

			dialogStage.resizableProperty().set(false);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			dialogStage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
